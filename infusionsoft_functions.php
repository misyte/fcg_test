<?php

    function infusionsoft_runactionset($action, $email, $name = '',$webinar_date = '',$webinar_time = '',$webinar_topic = '', $webinar_presenter = '',$shortlive, $shortreplay, $username,$useremail)
    {
        $action =   strtolower( trim($action) );
        $email =    strtolower( trim($email) );
        $name =     ucwords( strtolower( trim($name) ) );

        /** values to be used in short codes **/
        $webinar_date = trim($webinar_date);
        $webinar_time = trim($webinar_time);
        $webinar_topic = ucwords( strtolower( trim($webinar_topic) ) );
        $webinar_presenter = ucwords( strtolower( trim($webinar_presenter) ) );
        $shortlive = trim($shortlive);
        $shortreplay = trim($shortreplay);
        $username = ucwords( strtolower( trim($username) ) );
        $useremail = strtolower( trim($useremail) );


        global
            $infsoft_actionset,
            $infsoft_leadsourceid,
            $myInfusionsoft,
            $sc_webinar_date,
            $sc_webinar_time,
            $sc_webinar_topic,
            $sc_webinar_presenter,
            $sc_shortlive,
            $sc_shortreplay,
            $sc_username,
            $sc_useremail;



        if ($infsoft_actionset[$action] > 0 && $myInfusionsoft)
        {
            $fields = array('Id');
            $contact = $myInfusionsoft->findByEmail($email, $fields);
            $contact_id = $contact[0]['Id'];
            if ($contact_id == 0)
            {
                $fields = array(
                    'Email'=>$email
                );
                if ($name > '')
                {
                    $fields['FirstName'] = $name;
                }
                if ($infsoft_leadsourceid > 0)
                {
                    $fields['LeadSourceId'] =   $infsoft_leadsourceid;
                }

                /** additional custom fields  **/
                    $fields['_'.$sc_webinar_date] = $webinar_date;
                    $fields['_'.$sc_webinar_time] = $webinar_time;
                    $fields['_'.$sc_webinar_topic] = $webinar_topic;
                    $fields['_'.$sc_webinar_presenter] = $webinar_presenter;
                    $fields['_'.$sc_shortlive] = $shortlive;
                    $fields['_'.$sc_shortreplay] = $shortreplay;
                    $fields['_'.$sc_username] = $username;
                    $fields['_'.$sc_useremail] = $useremail;
                /**  end custom fields **/

                $contact_id = $myInfusionsoft->addCon($fields);
            }


            /**  updating contact data **/
            $conDat = array('_'.$sc_webinar_date => $webinar_date,
                '_'.$sc_webinar_time  => $webinar_time,
                '_'.$sc_webinar_topic  => $webinar_topic,
                '_'.$sc_webinar_presenter  => $webinar_presenter,
                '_'.$sc_shortlive  => $shortlive,
                '_'.$sc_shortreplay  => $shortreplay,
                '_'.$sc_username  => $username,
                '_'.$sc_useremail  => $useremail,
                    'LeadSourceId'  => $infsoft_leadsourceid,
                );

            if($action == 'registered') {
                $conID = $myInfusionsoft->updateCon($contact_id, $conDat);
            }

            #echo 'infusionsoft_runactionset->contact_id::' . intval($contact_id) . '<br />';
            #echo 'infusionsoft_runactionset->action_id::' . intval($infsoft_actionset[$action]) . '<br />';
            $result = $myInfusionsoft->runAS( intval($contact_id), intval($infsoft_actionset[$action]) );

            return $result;


        }
        return false;

    }

?>