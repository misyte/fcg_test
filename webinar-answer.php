<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
include('loadinfo.php');
include('mailer.php');
include 'languages/'.$langtype.'/lang.'.$langtype.'.php';



  $name = $_POST['name'];
  $name = decode_unicode_url($name);
  $from = $_POST['email'];
  $question = decode_unicode_url($_POST['question']);
  $page = $_POST['reg_page'];
  $webi_topic=unserialize(stripslashes($topic));
  $to = $_POST['admin_email'];
  $subject =  "Webinar Question Posted";
  $message = "- Webinar: ".decode_unicode_url($webi_topic[$page])."<br><br>- Senders Name: $name<br><br>- Senders Email: $from<br><br>- Senders Question: $question<br><br>";
  $subject = decode_unicode_url($subject);

  if(!empty($name) && !empty($from)) {
    if(trim($question)==""){}
    else{
        if($_SESSION['e_question']==trim($question)){ echo '<p style="margin-top:20px;font-size:14px; font-family:Verdana, Geneva, sans-serif; font-weight:bold;text-align:center;"><b><font color="gray">'.$lang['LIVE_CBOX2_TXT_8'].'.</font></b></p>'; }
        else{
            $_SESSION['e_question']=trim($question);
                if(sendmailchat($to, $from, $name, $subject, $message)){
          echo '<p style="margin-top:20px;font-size:14px; font-family:Verdana, Geneva, sans-serif; font-weight:bold;text-align:center;"><b><font color="green">'.$lang['LIVE_CBOX2_TXT_7'].'</font></b></p>';

             }
         }
    }
  }
  else{

    echo '<p style=" margin-top:20px;font-size:14px; font-family:Verdana, Geneva, sans-serif; font-weight:bold;text-align:center;"><font color="red">'.$lang['LIVE_CBOX2_TXT_6'].'</font></p>';

  }



function unicode_urldecode($url)
{
    preg_match_all('/%u([[:alnum:]]{4})/', $url, $a);

    foreach ($a[1] as $uniord)
    {
        $dec = hexdec($uniord);
        $utf = '';

        if ($dec < 128)
        {
            $utf = chr($dec);
        }
        else if ($dec < 2048)
        {
            $utf = chr(192 + (($dec - ($dec % 64)) / 64));
            $utf .= chr(128 + ($dec % 64));
        }
        else
        {
            $utf = chr(224 + (($dec - ($dec % 4096)) / 4096));
            $utf .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
            $utf .= chr(128 + ($dec % 64));
        }

        $url = str_replace('%u'.$uniord, $utf, $url);
    }

    return urldecode($url);
}

  function decode_unicode_url($str)
{
  $res = '';

  $i = 0;
  $max = strlen($str) - 6;
  while ($i <= $max)
  {
    $character = $str[$i];
    if ($character == '%' && $str[$i + 1] == 'u')
    {
      $value = hexdec(substr($str, $i + 2, 4));
      $i += 6;

      if ($value < 0x0080) // 1 byte: 0xxxxxxx
        $character = chr($value);
      else if ($value < 0x0800) // 2 bytes: 110xxxxx 10xxxxxx
        $character =
            chr((($value & 0x07c0) >> 6) | 0xc0)
          . chr(($value & 0x3f) | 0x80);
      else // 3 bytes: 1110xxxx 10xxxxxx 10xxxxxx
        $character =
            chr((($value & 0xf000) >> 12) | 0xe0)
          . chr((($value & 0x0fc0) >> 6) | 0x80)
          . chr(($value & 0x3f) | 0x80);
    }
    else
      $i++;

    $res .= $character;
  }

  return $res . substr($str, $i);
}

?>

