<?php
# GET THE VALUE OF APPID AND KEY FROM OA #
$appid = $_GET['a'];
$key = $_GET['k'];

# GET VARIABLES FROM THE URL #
$cmd = $_GET['c'];
$webiname = urldecode($_GET['webi']);
$userid = @$_GET['userid'];


switch($cmd){
    # CREATE OA CUSTOM FIELDS #
    case "addFields":   $data = <<<STRING
    <data>
        <Group_Tag name="Webinar : $webiname">
            <field name="User ID ($webiname)" type="text" editable="1"/>
            <field name="WebinarDate ($webiname)" type="fulldate" editable="1"/>
            <field name="WebinarTime ($webiname)" type="text" editable="1"/>
            <field name="Webinar Topic ($webiname)" type="text" editable="1"/>
            <field name="Webinar Presenter ($webiname)" type="text" editable="1"/>
            <field name="Short Live Link ($webiname)" type="text" editable="1"/>
            <field name="Short Replay Link ($webiname)" type="text" editable="1"/>
            <field name="Status Webinar ($webiname)" type="tdrop" editable="1" />
        </Group_Tag>
    </data>
STRING;

                        $data = urlencode(urlencode($data));
                        $response = runAPICommand("add_section",$data);
$data = <<<STRING
<data>
<field name="Status Webinar ($webiname)">
<option>Registered</option>
<option>RegisteredNotAttended</option>
<option>AttendedNotSawOffer</option>
<option>StartedOffer</option>
<option>SawOffer</option>
<option>SawReplay</option>
</field>
</data>
STRING;
                        $data = urlencode(urlencode($data));
                        $response = runAPICommand("add_dropdown",$data);
                        break;

    case "addContact":  $userid = @$_GET['userid'];
                        $name = $_GET['name'];
                        $email = $_GET['email'];
                        $webidate = @$_GET['webidate'];
                        $webitime = @$_GET['webitime'];
                        $webitopic = @$_GET['webitopic'];
                        $webipresenter =@ $_GET['webipresenter'];
                        $livelink = @$_GET['livelink'];
                        $replaylink = @$_GET['replaylink'];
                        $data = <<<STRING
<contact>
    <Group_Tag name="Contact Information">
        <field name="First Name">$name</field>
        <field name="E-Mail">$email</field>
    </Group_Tag>
    <Group_Tag name="Webinar : $webiname">
        <field name="User ID ($webiname)" >$userid</field>
        <field name="WebinarDate ($webiname)" >$webidate</field>
        <field name="WebinarTime ($webiname)" >$webitime</field>
        <field name="Webinar Topic ($webiname)" >$webitopic</field>
        <field name="Webinar Presenter ($webiname)" >$webipresenter</field>
        <field name="Short Live Link ($webiname)" >$livelink</field>
        <field name="Short Replay Link ($webiname)" >$replaylink</field>
        <field name="Status Webinar ($webiname)">Registered</field>
    </Group_Tag>
</contact>
STRING;
                        $data = urlencode(urlencode($data));
                        $response = runAPICommand("add",$data);
                        break;

    case "attended":    $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"Attended");
                        break;

    case "startedoffer":    $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"StartedOffer");
                        break;

    case "sawoffer":    $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"SawOffer");
                        break;

    case "sawreplay":   $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"SawReplay");
                        break;

    case "registerednotattended":   $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"RegisteredNotAttended");
                        break;

    case "attendednotsawoffer": $userid = $_GET['userid'];
                        $contact_id = getContactId($userid,$webiname);
                        $response = addTag($contact_id,$webiname,"AttendedNotSawOffer");
                        break;


    default:break;
}


#header("Content-Type: text/xml");
echo $contact_id ."SawReplay".$response;



#FUNCTIONS#

function runAPICommand($reqType,$data){
    global $appid,$key;
    $postargs = "appid=".$appid."&key=".$key."&return_id=1&reqType=".$reqType."&data=".$data;
    $arr=explode("_",$key);
    if($arr[1]>19999)
        $request = "https://api.ontraport.com/cdata.php";
    else
        $request = "http://api.moon-ray.com/cdata.php";
    $session = curl_init($request);
    curl_setopt ($session, CURLOPT_POST, true);
    curl_setopt ($session, CURLOPT_POSTFIELDS, $postargs);
    curl_setopt ($session, CURLOPT_HEADER, false);
    curl_setopt ($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    return $response;
}

function getContactId($userid,$webiname){
    $data = <<<STRING
<search>
    <equation>
        <field>User ID ($webiname)</field>
        <op>e</op>
        <value>$userid</value>
    </equation>
</search>
STRING;
    $response = runAPICommand("search",$data);
    $myXML = new SimpleXMLElement($response);
    return $myXML->contact['id'];
}

function addTag($contact_id,$webiname,$tag){
    global $appid,$key;
    $data = <<<STRING
    <contact id="$contact_id">
    <Group_Tag name="Webinar : $webiname">
        <field name="Status Webinar ($webiname)">$tag</field>
    </Group_Tag>
    </contact>
STRING;


    $data = urlencode(urlencode($data));
    $response = runAPICommand("update",$data);
    return $response;
}
?>

