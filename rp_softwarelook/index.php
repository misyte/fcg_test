<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo htmlentities($page_title, ENT_QUOTES, "UTF-8"); ?></title>

<meta property="og:title" content="<?php echo $rp_topic;?>" />
<meta property="og:type" content="activity" />
<meta property="og:url" content="<?php echo $domain_name.'webinar-register.php';?>" />
<meta property="og:image" content="<?php echo $domain_name.'webinar-files/but-ical.png';?>" />
<meta property="og:site_name" content="<?php echo $rp_topic;?>" />
<meta property="fb:admins" content="100003007913048" />
<meta property="og:description" content="<?php echo $rp_presenter;?> is conducting a great webinar on <?php echo $rp_topic;?>.  I have already signed up! You can register here: <?php echo $domain_name.'webinar-register.php';?>"/>

<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">

<link rel="stylesheet" type="text/css" href="softwarelook/style.css" />
<style type="text/css">
.regtop {
background:url(../webinar/languages/<?php echo $langtype.'/images/softwarelook/regtop2.jpg';?>) no-repeat left top;
}
.formline input[type="submit"] {
background: transparent url(../webinar/languages/<?php echo $langtype.'/images/softwarelook/registernow.jpg';?>) no-repeat;
width:298px;
height:57px;
margin:0 0 0 43px;
display:block;
cursor:pointer;
border: 0 none;
}

</style>

<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script language="javascript" src="js/cookie.js" type="text/javascript"></script>
<script language="javascript" src="js/dynajax.js" type="text/javascript"></script>
<!--<script language="javascript" src="js/gen_validatorv31.js"></script>-->
<script language="javascript" src="js/detect_timezone.js"></script>
<script language="javascript" type="text/javascript"  charset="utf-8">

$(document).ready(function(){
    $('#sms_slide').slideUp();
    $('#sms_on').click(function(){
        if($("#sms_on").is(':checked'))
        {
            $('#sms_slide').slideDown();
        }
        else
        {
            $('#sms_slide').slideUp();
        }
    });

     var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == 'undefined') {
        response_text = 'UTC';
    }
    else {
        response_text = tz_info.timezone.display();
    }

document.myform.timezone.value = response_text;



    $('#frmsubmit').click(function() {

        var sdates = $("#dates").val();
        var stimes = $("#times").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var sname =  $("#name").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var semail = $("#email").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');

        if(sdates == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELDATE']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_DATE']);?>');
            return false;
        }

        if(stimes == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELTIME']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_TIME']);?>');
            return false;
        }

        if(stimes == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELTIME_2']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_TIME']);?>');
            return false;
        }

        if(sname == '' || sname == '<?php echo trim(addslashes($lang['REG_FORM_TXTBOX_NAME']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_NAME']);?>');
            return false;
        }
        if(semail == '' || semail == '<?php echo trim(addslashes($lang['REG_FORM_TXTBOX_EMAIL']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_EMAIL']);?>');
            return false;
        }




});

});



function validate()
{
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(document.myform.time.options[document.myform.time.selectedIndex].value=='')
    {
        alert('Please Choose A Time For The Broad Cast'); return false;
    }

    if(document.myform.date.options[document.myform.date.selectedIndex].value==0)
    {
        alert('Please Choose A Date For The Broad Cast'); return false;
    }
    if((document.myform.name.value=='Enter Your Name Here...') || (document.myform.name.value==''))
    {
        alert('Please Enter Your Name'); return false;
    }

    if((document.myform.email.value=='Enter Valid Email Here...') || (document.myform.email.value==''))
    {
        alert('Please Enter A Valid Email Address'); return false;
    }

  if (!filter.test(document.myform.email.value)) {alert('<?php echo addslashes($lang['REG_FORM_ALERT_EMAIL_2']);?>'); return false;}

    copy_fields();

    return true;
}
var rname="";
var remail="";
var is_infusion=0;
function copy_fields(){

    rdateset=document.getElementById('dates').value;
    rwebitime=document.getElementById('times').value;
    rname=document.getElementById('name').value;
    remail=document.getElementById('email').value;
    rtimezone=document.getElementById('timezone').value;
    $.cookie("dateset", rdateset, { path: '/' });
    $.cookie("webitime", rwebitime, { path: '/' });
    $.cookie("name", rname, { path: '/' });
    $.cookie("email", remail, { path: '/' });
    $.cookie("timezone", rtimezone, { path: '/' });
    $('#ajaxloader').css({'display': 'block'});

    <?php
    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        document.getElementById('name1').value = document.getElementById('name').value;
        document.getElementById('email1').value = document.getElementById('email').value;
        <?php
    }


?>
}
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}

<?php




    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        function submitForm2(){
            if(is_infusion)
                ajax_posttt();
            else
                document.getElementById("form2").submit();
            <?php $rregpage=$ht.$domainname."/".$memberid."/".$webid."/webinar-register-thankyou.php"; ?>
            setTimeout('top.location="<?php echo $rregpage; ?>";',5000);

        }
        <?php
    }


?>

function isNumberKey(evt) {
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}

function setCountryCode() {
    document.myform.phonenumber.value = document.myform.cc.options[document.myform.cc.selectedIndex].value;
    return true;
}


</script>

<?php
if($rp_splash==1){
 ?>
<script type="text/javascript">
var the_height=0;
var is_exit=0;

function closeIt()
{
    if(is_exit==0){
        is_exit=1;
        document.getElementById("iframeholder").style.visibility='visible';
        document.getElementById("iframeholder").style.overflow='';
        document.getElementById("iframeholder").style.height='auto';
        document.getElementById("exitimg").style.display='block';
        document.getElementById("webinar-register-exit").style.display='block';
        document.getElementById("currentPage").style.display='none';
        document.getElementById("resize").style.visibility='visible';
        $('#player_wrapper').remove();
        var myplayer = '<object style="height: 0px; width: 0px;" type="application/x-shockwave-flash" data="player_mp3.swf" width="1" height="1"><param name="movie" value="player_mp3.swf" /><param name="FlashVars" value="mp3=webinar-files/<?php if($rp_audio_file==1) echo renamefilereg($rp_own_audio, $memberid); else echo $rp_builtin_audio; ?>&autoplay=1" /></object>';
        $('#webinar-register-exit').append(myplayer);

        document.body.style.background = 'white';
        setTimeout('document.getElementById("exitimg").style.display="none"',5000);

        return "<?php echo $rp_popup_message; ?>";
    }
    is_exit=0;
}
window.onbeforeunload = closeIt;



</script>
<?php } ?>

<?php echo stripslashes($google_analytics_header); ?>
<script language="javascript" src="liveclock.js"></script>
</head>
<body onLoad="show_clock()">
<div id="ajaxloader" style="display:none;background-color: rgba(0, 0, 0, 0.8);     color: #FFFFFF;          font-family: Tahoma,Arial,Helvetica,sans-serif;     height: 50px;     padding-top: 10px;     position: fixed;     text-align: center;     top: 0;     width: 100%;     z-index: 100;"><div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo $lang['REG_LOADING'];?></span></div></div>
<div id="currentPage">
<div class="wrapper">
    <div class="header">
        <?php if($rp_regheader == 1){ ?>
         <center><img src="<?php echo 'webinar-files/'.renamefilereg($rp_headerimg, $memberid);?>" alt="Live Webinar Broadcast"/></center>
        <?php }else{ ?>
        <h1 class="<?php
        if($rp_regtplcolor=="blue") echo 'blue-head';
        if($rp_regtplcolor=="darkgrey") echo 'darkgray-head';
        if($rp_regtplcolor=="lightgrey") echo 'lightgray-head';
        if($rp_regtplcolor=="yellow") echo 'yelow-head';
        if($rp_regtplcolor=="green") echo 'green-head';
        if($rp_regtplcolor=="red") echo 'red-head';
    ?>"><?php echo $rp_regtextheader1 ?></h1>
        <h2><?php echo $rp_regtextheader2; ?></h2>
        <?php } ?>

    </div>
    <?php
        if($rp_regtplcolor=="blue") echo '<img src="softwarelook/images/divider-blue.jpg" class="divider" />';
        if($rp_regtplcolor=="darkgrey") echo '<img src="softwarelook/images/divider-dg.jpg" class="divider" />';
        if($rp_regtplcolor=="lightgrey") echo '<img src="softwarelook/images/divider.jpg" class="divider" />';
        if($rp_regtplcolor=="yellow") echo '<img src="softwarelook/images/divider-yellow.jpg" class="divider" />';
        if($rp_regtplcolor=="green") echo '<img src="softwarelook/images/divider-green.jpg" class="divider" />';
        if($rp_regtplcolor=="red") echo '<img src="softwarelook/images/divider-red.jpg" class="divider" />';
    ?>

    <div class="content">
        <h1><?php echo $rp_headerline; ?></h1>

        <?php if($rp_vibrantvid==1){ ?>
        <div class="videobox" id="<?php
        if($rp_regtplcolor=="blue") echo 'blue';
        if($rp_regtplcolor=="darkgrey") echo 'darkgray';
        if($rp_regtplcolor=="lightgrey") echo '';
        if($rp_regtplcolor=="yellow") echo 'yelow';
        if($rp_regtplcolor=="green") echo 'green';
        if($rp_regtplcolor=="red") echo 'red';
    ?>">
            <!--video code here 640 x 360-->
            <?php if(0){ ?>

            <?php }else{ ?>

                <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
                <object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="player" width="640" height="385">
                <param name="movie" value="player/player.swf" />
                <param name="allowfullscreen" value="true" />
                <param name="allowscriptaccess" value="always" />
                <param name="flashvars" value="file=<?php echo $rp_regvid;?>&feature=related&autostart=<?php echo $rp_regvidauto;?>" />
                <embed
                type="application/x-shockwave-flash"
                id="player"
                name="player"
                src="player/player.swf"
                width="640"
                height="385"
                allowscriptaccess="always"
                allowfullscreen="true"
                allowNetworking="internal"
                flashvars="file=<?php echo $rp_regvid;?>&autostart=<?php if($rp_regvidauto=="checkbox") echo "true";?>&controlbar=over"
                />
                </object>
                <!-- END OF THE PLAYER EMBEDDING -->
                <div id="player"></div>

            <?php } ?>
            <!--end of video code-->
        </div>
        <?php } ?>

        <div class="textbox">
            <?php ## presentors box ## ?>
        <?php
        if($rp_intro_box==1){
            $is_affiliate=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=q');
            $a_affiliate_name=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=n');
            $a_affiliate_picture=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=i');
            include 'presentation-box/presentersboxrp.php';
        ?>

        <?php } ?>
        <?php ## end presentors box ## ?>
            <?php if($rp_vibrantdesc==1){ ?>
            <?php  echo $rp_webidesc; ?>
            <?php } ?>
        </div>
    </div>
    <?php
        if($rp_regtplcolor=="blue") echo '<img src="softwarelook/images/divider2-blue.jpg" class="divider" />';
        if($rp_regtplcolor=="darkgrey") echo '<img src="softwarelook/images/divider2-dg.jpg" class="divider" />';
        if($rp_regtplcolor=="lightgrey") echo '<img src="softwarelook/images/divider2.jpg" class="divider" />';
        if($rp_regtplcolor=="yellow") echo '<img src="softwarelook/images/divider2-yellow.jpg" class="divider" />';
        if($rp_regtplcolor=="green") echo '<img src="softwarelook/images/divider2-green.jpg" class="divider" />';
        if($rp_regtplcolor=="red") echo '<img src="softwarelook/images/divider2-red.jpg" class="divider" />';
    ?>

    <?php include 'modules/regdatetime/index2.php'; ?>
    <div class="regregion">
    <?php

    ################EXPIRED STICKER####################
    include 'get_form_expire.php';
    $show=1;
    if($dif <= 0 && $type==2){
    echo "<a href='webinar-replay.php?access=ok&webid=$webid&memberid=$memberid' target='_top' onClick='is_exit=1;'><img src='webinar-files/expiredsticker2.png'  /></a>";
    $show=0;
    }
    if($show==1 && $dif2 <= 0)
    {
        echo "<a href='webinar-replay.php?access=ok&webid=$webid&memberid=$memberid' target='_top' onClick='is_exit=1;'><img src='webinar-files/expiredsticker2.png'  /></a>";
        $show=0;
    }
    ?>
    <div style="display: <?php if($show) echo 'block;'; else echo 'none;'; ?>">
    <img src="languages/<?php echo $langtype.'/images/softwarelook/arrow.jpg';?>"  class="imgLeft" />
        <div class="regbox">
            <div class="regtop">
                <div class="regbottom">
                    <div class="regcontent">
                        <form  action="loadregister.php" method="post" name="myform" target="iframe2" onsubmit="return validate()">
                            <div class="formline">
                                <p><?php echo $lang['REG_FORM_TXT_SELECTDATE'];?></p>
                                <select name="date" id="dates">
                                    <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELDATE'];?></option>
                                    <?php echo $udateval; ?>

                                </select>
                            </div>
                            <div class="formline">
                                <p><?php echo $lang['REG_FORM_TXT_SELECTTIME'];?> <br />
<span id="localtime">Your Local Time: <span class="red"><span id="LiveClockIE"></span></span></span></p>
                                <span id="time">
                                <select name="time" id="times">
                                    <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELTIME'];?></option>

                                </select>
                                </span>
                            </div>
                            <div class="formline">
                                <p><?php echo $lang['REG_FORM_TXT_SENDINVITATION'];?></p>
                                <input name="name" id="name" style="margin-bottom:10px;" type="text" onblur="if (this.value == '') {this.value = '<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>';}" onfocus="if(this.value == '<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>') {this.value = '';}" value="<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>"  /><br class="clear" />

                                <input name="email" id="email" type="text" onblur="if (this.value == '') {this.value = '<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>';}" onfocus="if(this.value == '<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>') {this.value = '';}" value="<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>"  />
                            </div>

                            <?php if($rp_sms == 1 || $rp_voice == 1){ ?>
                            <div class="formline">
                                <p style="width: 85px;"><?php echo $lang['REG_FORM_TXT_NOTIFICATION'];?></p><div class="sms"><input type="checkbox" id="sms_on" value="1" /><?php echo $lang['REG_FORM_SMS_TXT1'];?></div>
                            </div>
                            <?php include_once('modules/sms/index2.php');  } ?>

                            <div class="formline">
                                <input onClick='is_exit=1;' class="submit" id="frmsubmit"  type="submit" value=" " />
                            </div>

                            <input type="hidden" name="timezone" id="timezone">
                            <input type="hidden" name="memberid" id="memberid" value="<?php echo $memberid;?>">
                            <input type="hidden" name="webid" id="webid" value="<?php echo $webid;?>">
                            <input type="hidden" name="submit" value="submit">
                            <input type="hidden" name="page" value="<?php echo $page;?>">


                        </form>
                        <br class="clear" />
                        <?php include 'autoresponder.php'; ?>
                        <iframe sandbox="allow-same-origin allow-forms allow-scripts" name="iframe3" style="display:none"></iframe>
                        <iframe name="iframe2" style="display:none"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <br class="clear" />
    </div>
    </div>
    </div>

<div class="graybox">
    <?php include 'footer.php'; ?>
</div>
<?php echo stripslashes($google_analytics_body); ?>

</div>
<?php
if($rp_splash==1){
 ?>
<div id="webinar-register-exit" style="display:none;height:0;"></div>
 <div id="iframeholder" style="visibility:hidden; overflow:hidden; height:0px;">
<iframe src="webinar-register-exit2.php?webid=<?php echo $webid; ?>&memberid=<?php echo $memberid;?>&landingpage=<?php echo $landingpage;?>" scrolling="no"  align="middle" frameborder="0" width="100%" id = "resize"  onload = "setIframeHeight( this.id )" style="visibility:hidden;overflow:hidden;"  height="0" name="frame1"></iframe>
</div>

<script type='text/javascript'>

function setIframeHeight( iframeId ) /** IMPORTANT: All framed documents *must* have a DOCTYPE applied **/
{
 var ifDoc, ifRef = document.getElementById( iframeId );

 try
 {
  ifDoc = ifRef.contentWindow.document.documentElement;
 }
 catch( e )
 {
  try
  {
   ifDoc = ifRef.contentDocument.documentElement;
  }
  catch(ee)
  {
  }
 }

 if( ifDoc )
 {
  ifRef.height = 1;
  ifRef.height = ifDoc.scrollHeight;

  /* For width resize, enable below.  */

  // ifRef.width = 1;
  // ifRef.width = ifDoc.scrollWidth;
 }
}

</script>

<div id="exitimg" style="display:none; position:fixed; right:10px; top:10px; z-index:10000"><img id="image" name="image" border="0" src="webinar-files/exitimg.gif" /></div>
<?php } ?>

<script type="text/javascript">
    window.fbAsyncInit = function() {
    FB.Canvas.setAutoResize( 100 );
    }

    function sizeChangeCallback() {
    FB.Canvas.setSize({ width: 810, height:1300 });
    }
    </script>
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
FB.init({
appId : 'YOUR APP ID',
status : true,
cookie : true,
xfbml : true
});
</script>
</body>
<?php echo stripslashes($tracker_reg); ?>
<?php echo stripslashes($google_analytics_footer); ?>
</html>