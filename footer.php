<?php
/**
 * footer links
 * pagetype 1 : webinar registration page
 * pagetype 2 : webinar registration thank you page
 * pagetype 3 : webinar live page
 * pagetype 4 : webinar replay page
 */

@include 'footer-css.php';


/** unserialising footer variables **/
$disclaimer_text = stripslashes($disclaimer_text);
$get_footerlinkname  = unserialize(base64_decode($footerlinkname));
$get_footerlinkcontent  = unserialize(base64_decode($footerlinkcontent));
$get_footerlink_url  = unserialize(base64_decode($footerlink_url));
$get_footerlink_internal  = unserialize(base64_decode($footerlink_internal));
$get_embedfooter  = unserialize($embedfooter);
$get_footercolor  = unserialize($footercolor);


/** get the list of pages to output the footer links **/
$allowfooter = 0;
if(is_array($get_embedfooter) && $pagetype>0)
{
    if (in_array($pagetype, $get_embedfooter)) {
        $allowfooter = 1;
    }
}


if($allowfooter == 1)
{
    /**  disclaimer message text  **/
    if($disclaimer_opt == 1)
    {
        echo '<div id="divdisclaimer" style="width:700px; margin: auto; font-size: 12px;">'.$disclaimer_text.'</div>';
    }
    /** end disclaimer message text **/



    /**  footer links  **/
    if($footerlinks_opt == 1)
    {
        echo '<div id="navfooter" align="center" style="margin-top:15px;">';
        echo '<ul id="navfooterlist">';

        for($a=0;$a<$addfooterlink_opt;$a++)
        {
            if($get_footerlinkcontent[$a] == 1)
            {
                $hlink = $get_footerlink_url[$a];
            }
            if($get_footerlinkcontent[$a] == 2)
            {
                $hlink = str_replace(' ','',$get_footerlinkname[$a]);
                $hlink = 'statements.php?link='.$hlink;

            }
            echo '<li><a href="'.$hlink.'" target="_blank" style="color:#'.$get_footercolor['footertextcolor'].'">'.$get_footerlinkname[$a].'</a></li>';
        }

        echo '</ul>';
        echo '</div>';
    }
    /**  end footer links  **/


    /** footer affiliate link **/
    if($footerlink_opt == 1 && ($pagetype == 1 || $pagetype == 2))
    {
        if($disclaimer_opt == 0 && $footerlinks_opt == 0)
        {
            echo '<div id="Poweredby" style="margin-top:15px;">';
        }
        else
        {
            echo '<div id="Poweredby" style="margin-top:30px;">';
        }
        echo '<p><a href="'.makeurl(stripslashes($footerlink)).'" style="color:#'.$get_footercolor['afftextcolor'].'">Powered by Automated Webinar Generator</a></p>';
        echo '</div>';
    }
    /** end footer affiliate link **/

}



?>