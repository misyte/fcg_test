<meta property="og:title" content="<?php echo $rp_topic;?>" />
<meta property="og:type" content="activity" />
<meta property="og:url" content="<?php echo $domain_name.'webinar-register.php';?>" />
<meta property="og:image" content="<?php echo $domain_name.'webinar-files/but-ical.png';?>" />
<meta property="og:site_name" content="<?php echo $rp_topic;?>" />
<meta property="fb:admins" content="100003007913048" />
<meta property="og:description" content="<?php echo $rp_presenter;?> is conducting a great webinar on <?php echo $rp_topic;?>.  I have already signed up! You can register here: <?php echo $domain_name.'webinar-register.php';?>"/>

<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">

<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script language="javascript" src="js/cookie.js" type="text/javascript"></script>
<script language="javascript" src="js/dynajax.js" type="text/javascript"></script>
<!--<script language="javascript" src="js/gen_validatorv31.js"></script>-->
<script language="javascript" src="js/detect_timezone.js"></script>
<script language="javascript" src="liveclock.js"></script>
<script language="javascript" type="text/javascript"  charset="utf-8">

$(document).ready(function(){
show_clock();
    $('#sms_slide').slideUp();
    $('#sms_on').click(function(){
        if ($("#sms_on").is(':checked')) {
            $('#sms_slide').slideDown();
        } else {
            $('#sms_slide').slideUp();
        }
    });

    var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == 'undefined') {
        response_text = 'UTC';
    } else {
        response_text = tz_info.timezone.display();
    }

    document.myform.timezone.value = response_text;
        /*      $.ajax({
                        type: "POST",
                        url: "modules/regdatetime/index.php",
                        data: "tzone=" + response_text + "&id=<?php echo $member_id;?>" + "&show=dates",
                        success: function(data){
                            $("#date").html(data);

                    }
                });

                $.ajax({
                        type: "POST",
                        url: "modules/regdatetime/index.php",
                        data: "tzone=" + response_text + "&id=<?php echo $member_id;?>" + "&show=times",
                        success: function(data){
                            $("#time").html(data);

                    }
                });
        */
    $('#frmsubmit').click(function() {
    var sdates = $("#dates").val();
        var stimes = $("#times").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var sname =  $("#name").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var semail = $("#email").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');

        if(sdates == 'Select desired date'){
            alert('Please fill out the Date field');
            return false;
        }

        if(stimes == 'Select your date first'){
            alert('Please fill out the Time field');
            return false;
        }

        if(stimes == 'Select desired time'){
            alert('Please fill out the Time field');
            return false;
        }

        if(sname == '' || sname == 'Enter Your Name Here...'){
            alert('Please fill out the Name field');
            return false;
        }
        if(semail == '' || semail == 'Enter Your Email Here...'){
            alert('Please fill out the Email field');
            return false;
        }
    });
});


function validate()
{
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(document.myform.time.options[document.myform.time.selectedIndex].value=='')
    {
        alert('Please Choose A Time For The Broad Cast'); return false;
    }

    if(document.myform.date.options[document.myform.date.selectedIndex].value==0)
    {
        alert('Please Choose A Date For The Broad Cast'); return false;
    }
    if((document.myform.name.value=='Enter Your Name Here...') || (document.myform.name.value==''))
    {
        alert('Please Enter Your Name'); return false;
    }

    if((document.myform.email.value=='Enter Valid Email Here...') || (document.myform.email.value==''))
    {
        alert('Please Enter A Valid Email Address'); return false;
    }

    if (!filter.test(document.myform.email.value)) {alert('Please Enter A Valid Email Address'); return false;}

    copy_fields();

    return true;
}
var rname="";
var remail="";
var is_infusion=0;
function copy_fields(){

    rdateset=document.getElementById('dates').value;
    rwebitime=document.getElementById('times').value;
    rname=document.getElementById('name').value;
    remail=document.getElementById('email').value;
    rtimezone=document.getElementById('timezone').value;
    $.cookie("dateset", rdateset, { path: '/' });
    $.cookie("webitime", rwebitime, { path: '/' });
    $.cookie("name", rname, { path: '/' });
    $.cookie("email", remail, { path: '/' });
    $.cookie("timezone", rtimezone, { path: '/' });
    $('#ajaxloader').css({'display': 'block'});
    <?php
    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        document.getElementById('name1').value = document.getElementById('name').value;
        document.getElementById('email1').value = document.getElementById('email').value;
        <?php
    }


?>

}
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}

<?php
    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        function submitForm2(){
            if(is_infusion)
                ajax_posttt();
            else
                document.getElementById("form2").submit();
            <?php $rregpage=$domainname.'/'.$webifolder."/webinar-register-thankyou.php"; ?>
            setTimeout('window.location="<?php echo $rregpage; ?>";',5000);

        }
        <?php
    }


?>




function isNumberKey(evt) {
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}

function setCountryCode() {
    document.myform.phonenumber.value = document.myform.cc.options[document.myform.cc.selectedIndex].value;
    return true;
}
</script>

<?php
if($rp_splash==1){
 ?>
<script type="text/javascript">
var the_height=0;
var is_exit=0;

function closeIt()
{
    if(is_exit==0){
        is_exit=1;
        document.getElementById("iframeholder").style.visibility='visible';
        document.getElementById("iframeholder").style.overflow='';
        document.getElementById("iframeholder").style.height='auto';
        document.getElementById("exitimg").style.display='block';
        document.getElementById("webinar-register-exit").style.display='block';
        document.getElementById("currentPage").style.display='none';
        document.getElementById("resize").style.visibility='visible';

        var myplayer = '<object style="height: 0px; width: 0px;" type="application/x-shockwave-flash" data="player_mp3.swf" width="1" height="1"><param name="movie" value="player_mp3.swf" /><param name="FlashVars" value="mp3=<?php if($rp_audio_file==1) echo 'http://eWebinars.com/'.$memberid.'/'.$rp_own_audio; else echo 'webinar-files/'.$rp_builtin_audio; ?>&autoplay=1" /></object>';
        $('#webinar-register-exit').append(myplayer);

        document.body.style.background = 'white';
        setTimeout('document.getElementById("exitimg").style.display="none"',5000);

        return "<?php echo $rp_popup_message; ?>";
    }
    is_exit=0;
}
window.onbeforeunload = closeIt;



</script>
<?php } ?>

<?php

include 'csscode.php';

?>