<style type="text/css">

@charset "utf-8";
*{
margin:0;
padding:0;
}


img, a img{
border:0 none;
}

img{
vertical-align:baseline;
}

a{
outline:0;
}

input:focus, textarea:focus, select:focus{
outline: none;
}

textarea {
resize: none;
}

/*img, div, input { behavior: url("iepngfix.htc") }*/


.imgCenter{
display:block;
margin:0 auto;
clear:both;
}

.imgRight{
float:right;
}

.imgLeft{
float:left;
}

.none{
display:none;
}

.underlined{
text-decoration:underline;
}

.textCenter{
text-align:center;
}

.textLeft{
text-align:left;
}

.textRight{
text-align:right;
}

.clear{
clear:both;
height:0;
line-height:0;
font-size:0;
}

body{
font-size:12px;
color:#555555;
background:transparent url(panorama/images/bg.jpg) scroll repeat-x left top;
font-family:Arial, Helvetica, sans-serif;
}

/*-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: alpha(opacity=0);*/


.wrapper{
    width:882px;
    margin:0 auto;
    display:block;
    }

.box{
    width:850px;
    padding:16px;
    background:#FFF;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    margin-bottom:25px;
    font-size:14px;
    line-height:normal;
    }


.boxtext{
    width:850px;
    padding:30px;
    background:#FFF;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    margin-bottom:25px;
    font-size:14px;
    line-height:20px;
    }

.headingbox{
    padding:8px 16px;
    }

h1, h2, h3{
    text-align:center;
    color:#fff;
    }

h1{
    font-size: 42px;
    font-weight: normal;
    letter-spacing: -0.05em;
    line-height: 45px;
    margin: 20px 0 0;
    }

h2{
    font-size: 18px;
    margin: 0 0 16px;
    }

.box h3{
    color: #CE0000;
    font-size: 25px;
    letter-spacing: -0.02em;
    }
.page-panorama{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:#00396c url(panorama/images/bg-blue.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:#202020 url(panorama/images/bg-darkGray.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:#b8b8b8 url(panorama/images/bg-gray.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:#a67c00 url(panorama/images/bg-yellow.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="green") echo 'background:#054950 url(panorama/images/bg-green.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="red") echo 'background:#9a0000 url(panorama/images/bg-red.jpg) no-repeat center top;';
    ?>
}

.video-panorama{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:#e5f2fa;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:#e3e3e3;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:#d7d7d7;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:#ede0bb;';
        if($get_regtplcolor[$page]=="green") echo 'background:#dfeff1;';
        if($get_regtplcolor[$page]=="red") echo 'background:#fa3737;';
    ?>
}

.bgArrow{
    width:758px;
    margin:0 auto 25px;
    display:block;
    background:url(panorama/images/bg-arrow.png) no-repeat center 75px;
    }

.optinbox{
    width:600px;
    padding:30px;
    position:relative;
    margin:0 auto;
    color:#fff;
    display:block;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    }

.left-arrow{
    position:absolute;
    left:-49px;
    top:0;
    }

.right-arrow{
    position:absolute;
    right:-49px;
    top:0;
    }

.optinbox h2{
    font-size: 20px;
    font-weight: normal;
    margin-bottom: 40px;
    }

.inputRow{
    display:inline-block;
    font-size: 14px;
    font-weight: bold;
    margin-bottom: 10px;
    }

.inputRow span{
    display:block;
    float:left;
    width:267px;
    }

.marginBtm {
    margin-bottom:20px;
    }

.marginBtm10{
    margin-bottom:10px;
    }

.optinbox p small{
    font-size: 12px;
    font-weight: normal;
    padding-left: 11px;
    }

.optinbox input, .optinbox select{
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    padding:2px 3px;
    color:#555555;
    width:323px;
    float:left;
    }

.optinbox select{
    width:330px;
    color:#555555;
    }

.optinbox input[type="checkbox"]{
    width: 20px;
    border:0 none;
    }

.bgArrow input[type="image"]{
    border:0 none;
    margin:0 auto;
    float:none;
    display:block;
    clear:both;
    }

.optin-panorama{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:#2873b6;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:#656565;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:#d2d2d2;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:#d7a921;';
        if($get_regtplcolor[$page]=="green") echo 'background:#13838f;';
        if($get_regtplcolor[$page]=="red") echo 'background:#d80d0d;';
    ?>
    }


.optin-panorama input, .optin-panorama select{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'border:1px solid #044e8f;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'border:1px solid #202020;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'border:1px solid #999999;';
        if($get_regtplcolor[$page]=="yellow") echo 'border:1px solid #ce9f15;';
        if($get_regtplcolor[$page]=="green") echo 'border:1px solid #0b636c;';
        if($get_regtplcolor[$page]=="red") echo 'border:1px solid #9a0000;';
    ?>
    }

    #sms_slide{ float: right; width: 335px; margin-bottom:10px; }
.sms{ text-align:left !important    ; }
#phonenumber{  margin-top: 10px; margin-bottom: 10px; }
.footerFull{
    width:100%;
    }

.footer-panorama{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'border-top-color:#010e1a;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'border-top-color:#0e0e0e;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'border-top-color:#888888;';
        if($get_regtplcolor[$page]=="yellow") echo 'border-top-color:#674d01;';
        if($get_regtplcolor[$page]=="green") echo 'border-top-color:#023237;';
        if($get_regtplcolor[$page]=="red") echo 'border-top-color:#5a0000;';
    ?>
    }

    .footer-panorama p, .footer-panorama p a{
        <?php
        if($get_regtplcolor[$page]=="blue") echo 'color: #010e1a;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color: #0e0e0e;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #888888;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #674d01;';
        if($get_regtplcolor[$page]=="green") echo 'color: #023237;';
        if($get_regtplcolor[$page]=="red") echo 'color: #5a0000;';
        ?>
        }




#footer{
    width:940px;
    padding:13px 0 0;
    margin:0 auto 10px;
    border-top-style:solid;
    border-top-width:1px;
    }

#footer p{
    text-align:center;
    }

#footer p a{
    text-decoration:none;
    }

#footer p a:hover{
    text-decoration:underline;
    }




/** footer css adds **/
#footer {
    height: auto;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 0;
    padding-right: 0;
    background: url(images/footer2_bg.png);
}
#footer p{
    line-height: normal;
    }

#navfooterlist li
{
display: inline;
list-style-type: none;
padding-right: 10px;

}
#navfooterlist li a{
    text-decoration:none;
    font-size: 12px;

}
/** end footer css adds **/


</style>