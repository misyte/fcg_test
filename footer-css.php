<?php

/** REGISTER PAGE TEMPLATE OPTION **/

if($is_rp==1)
{
        /**  vertical design **/
        if($rp_regtpl == 0)
        {
            if($rp_regtplcolor == 'lightgrey' || $rp_regtplcolor == 'yellow')
            {
                $navlinkcolor = '#272727';
            }
            else
            {
                $navlinkcolor = '#ededed';
            }
        }

        /**  horizontal design **/
        elseif($rp_regtpl == 1)
        {
            if($rp_regtplcolor == 'lightgrey' || $rp_regtplcolor == 'yellow')
            {
                $navlinkcolor = '#272727';
            }
            else
            {
                $navlinkcolor = '#ededed';
            }
        }


        /**  horizontal design **/
        elseif($rp_regtpl == 3)
        {
            $navlinkcolor = '#ededed';
        }

        /**  stylish design **/
        elseif($rp_regtpl == 4)
        {
            if($rp_regtplcolor == 'blue')
            {
                $navlinkcolor = '#5888b3';
            }
            elseif($rp_regtplcolor == 'darkgrey')
            {
                $navlinkcolor = '#999999';
            }
            elseif($rp_regtplcolor == 'green')
            {
                $navlinkcolor = '#789a9f';
            }
            elseif($rp_regtplcolor == 'lightgrey')
            {
                $navlinkcolor = '#999999';
            }
            elseif($rp_regtplcolor == 'red')
            {
                $navlinkcolor = '#d58f8f';
            }
            elseif($rp_regtplcolor == 'yellow')
            {
                $navlinkcolor = '#bea14d';
            } else {
                $navlinkcolor = '#5888b3';
            }
        }

        /**  panorama design **/
        elseif($rp_regtpl == 5)
        {
            if($rp_regtplcolor == 'blue')
            {
                $navlinkcolor = '#010e1a';
            }
            elseif($rp_regtplcolor == 'darkgrey')
            {
                $navlinkcolor = '#0e0e0e';
            }
            elseif($rp_regtplcolor == 'green')
            {
                $navlinkcolor = '#023237';
            }
            elseif($rp_regtplcolor == 'lightgrey')
            {
                $navlinkcolor = '#888888';
            }
            elseif($rp_regtplcolor == 'red')
            {
                $navlinkcolor = '#5a0000';
            }
            elseif($rp_regtplcolor == 'yellow')
            {
                $navlinkcolor = '#674d01';
            } else {
                $navlinkcolor = '#0e0e0e';
            }

        }


        /**  facebook design **/
        elseif($rp_regtpl == 6)
        {
            if($rp_regtplcolor == 'blue')
            {
                $navlinkcolor = '#e1e1e1';
            }
            elseif($rp_regtplcolor == 'darkgrey')
            {
                $navlinkcolor = '#e1e1e1';
            }
            elseif($rp_regtplcolor == 'green')
            {
                $navlinkcolor = '#fff';
            }
            elseif($rp_regtplcolor == 'lightgrey')
            {
                $navlinkcolor = '#000000';
            }
            elseif($rp_regtplcolor == 'red')
            {
                $navlinkcolor = '#fff';
            }
            elseif($rp_regtplcolor == 'yellow')
            {
                $navlinkcolor = '#000000';
            } else {
                $navlinkcolor = '#0e0e0e';
            }
        }

        /**  softwarelook design **/
        elseif($rp_regtpl == 7)
        {
            $navlinkcolor = '#ecac00';
        }



        /**  ethereal design **/
        elseif($rp_regtpl == 8)
        {
            $navlinkcolor = '#828282';
        }

        else {
            $navlinkcolor = '#828282';
        }
}
else
{


        /**  vertical design **/
        if($get_regtpl[$page] == 0)
        {
            if($get_regtplcolor[$page] == 'lightgrey' || $get_regtplcolor[$page] == 'yellow')
            {
                $navlinkcolor = '#272727';
            }
            else
            {
                $navlinkcolor = '#ededed';
            }
        }

        /**  horizontal design **/
        elseif($get_regtpl[$page] == 1)
        {
            if($get_regtplcolor[$page] == 'lightgrey' || $get_regtplcolor[$page] == 'yellow')
            {
                $navlinkcolor = '#272727';
            }
            else
            {
                $navlinkcolor = '#ededed';
            }
        }


        /**  horizontal design **/
        elseif($get_regtpl[$page] == 3)
        {
            $navlinkcolor = '#ededed';
        }

        /**  stylish design **/
        elseif($get_regtpl[$page] == 4)
        {
            if($get_regtplcolor[$page] == 'blue')
            {
                $navlinkcolor = '#5888b3';
            }
            elseif($get_regtplcolor[$page] == 'darkgrey')
            {
                $navlinkcolor = '#999999';
            }
            elseif($get_regtplcolor[$page] == 'green')
            {
                $navlinkcolor = '#789a9f';
            }
            elseif($get_regtplcolor[$page] == 'lightgrey')
            {
                $navlinkcolor = '#999999';
            }
            elseif($get_regtplcolor[$page] == 'red')
            {
                $navlinkcolor = '#d58f8f';
            }
            elseif($get_regtplcolor[$page] == 'yellow')
            {
                $navlinkcolor = '#bea14d';
            } else {
                $navlinkcolor = '#5888b3';
            }
        }

        /**  panorama design **/
        elseif($get_regtpl[$page] == 5)
        {
            if($get_regtplcolor[$page] == 'blue')
            {
                $navlinkcolor = '#010e1a';
            }
            elseif($get_regtplcolor[$page] == 'darkgrey')
            {
                $navlinkcolor = '#0e0e0e';
            }
            elseif($get_regtplcolor[$page] == 'green')
            {
                $navlinkcolor = '#023237';
            }
            elseif($get_regtplcolor[$page] == 'lightgrey')
            {
                $navlinkcolor = '#888888';
            }
            elseif($get_regtplcolor[$page] == 'red')
            {
                $navlinkcolor = '#5a0000';
            }
            elseif($get_regtplcolor[$page] == 'yellow')
            {
                $navlinkcolor = '#674d01';
            } else {
                $navlinkcolor = '#0e0e0e';
            }

        }


        /**  facebook design **/
        elseif($get_regtpl[$page] == 6)
        {
            if($get_regtplcolor[$page] == 'blue')
            {
                $navlinkcolor = '#e1e1e1';
            }
            elseif($get_regtplcolor[$page] == 'darkgrey')
            {
                $navlinkcolor = '#e1e1e1';
            }
            elseif($get_regtplcolor[$page] == 'green')
            {
                $navlinkcolor = '#fff';
            }
            elseif($get_regtplcolor[$page] == 'lightgrey')
            {
                $navlinkcolor = '#000000';
            }
            elseif($get_regtplcolor[$page] == 'red')
            {
                $navlinkcolor = '#fff';
            }
            elseif($get_regtplcolor[$page] == 'yellow')
            {
                $navlinkcolor = '#000000';
            } else {
                $navlinkcolor = '#0e0e0e';
            }
        }


        /**  softwarelook design **/
        elseif($get_regtpl[$page] == 7)
        {
            $navlinkcolor = '#ecac00';
        }



        /**  ethereal design **/
        elseif($get_regtpl[$page] == 8)
        {
            $navlinkcolor = '#828282';
        }

        else {
            $navlinkcolor = '#828282';
        }
}


/**  live and replay page navlink color **/
if($pagetype == 3 || $pagetype == 4)
{
    $navlinkcolor = '#ededed';
    if($broadcast_design == 3)
    {
        $navlinkcolor = '#0e0e0e';
    }
}


?>