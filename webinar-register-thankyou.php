<?php  ob_start();
error_reporting(0);
session_start();

include_once 'functions.php';
include 'loadinfo.php';
/** force url to load in http://domain.com  without www  **/
if (substr($_SERVER['HTTP_HOST'],0,3) == 'www' && substr($domainname,0,10) != 'http://www') {
    $shost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
    $actual_link = full_url();
    $actual_url = parse_url($actual_link);
    $ishttp = strtolower($actual_url['scheme']);

    header('HTTP/1.1 301 Moved Permanently');
    if( $ishttp == 'https') {
        header('Location: https://'.$shost.$_SERVER['REQUEST_URI']);
    } else {
        header('Location: http://'.$shost.$_SERVER['REQUEST_URI']);
    }
}/** end **/

include 'config.php';
include('split-register.php');
$tpltype = 'software';
$pagetype = 2;
if(!$_SESSION['isactionsetreg'])
{
    $_SESSION['isactionsetreg'] = 1;
}


$domain_name = $domainname."/".$webifolder.'/';
$webi_presenter = unserialize(stripslashes($presenter));
$webi_topic = unserialize(stripslashes($topic));
$get_regheader = unserialize(stripslashes($regheader));
$get_regtxtheader1 = unserialize(stripslashes($reg_textheader1));
$get_regtxtheader2 = unserialize(stripslashes($reg_textheader2));
$get_headerimg = unserialize(stripslashes($reg_headerimg));
$get_regtpl = unserialize(stripslashes($regtpl));

include 'languages/'.$langtype.'/lang.'.$langtype.'.php';

$_SESSION['wemail'] = $_COOKIE['email'];
$_SESSION['wdate'] = $_COOKIE['dateset'];
$_SESSION['wtime'] = $_COOKIE['webitime'];

/** also used in the ie cookie inside iframe issue **/
$_SESSION['wemail'] = $_COOKIE['user_email'];
$_SESSION['wdate'] = $_COOKIE['user_date'];
$_SESSION['wtime'] = $_COOKIE['user_time'];
$_SESSION['wname'] = $_COOKIE['user_name'];
/** end */


/**  infusionsoft actionset registered  **/
  if ( function_exists('infusionsoft_runactionset') )
  {
        if($_SESSION['actionsetreg'])
        {

        $pieces = explode("||",$_SESSION['actionsetreg']);
        infusionsoft_runactionset($pieces[0], $pieces[1], $pieces[2],$pieces[3],$pieces[4],$pieces[5],$pieces[6],$pieces[7],$pieces[8],$pieces[9],$pieces[10]);

        unset($_SESSION['actionsetreg']);
        }

  }

/**  end **/


/** infusionsoft welcome notification happens here  **/
if($is_welcome == 1) {
    if($autoresponderopt==2 && $autorespondertype==1 && $via_infusion==1 || ($autoresponderopt==1 && $autorespondertype==1)) {
        $uid = $_SESSION['regid'];
        $myFile = 'emailtpl_'.$uid.'.txt';
        $fh = fopen($myFile, 'r');
        $theData = fread($fh, filesize($myFile));
        fclose($fh);
        if(unlink($myFile)) {
            if($infsoft_apikey!="")
                if(infusionsoft_sendmail($_SESSION['wemail'],$from_email,$from_name,$_SESSION['emailtplsubj'],$theData)) {
                    $mailsuccess = true;
                }
        }
    }
} /** end **/

/** infusionsoft referral **/
#if($autoresponderopt==2 && $autorespondertype==1 && $via_infusion==1 || ($autoresponderopt==1 && $autorespondertype==1)) {
if($autoresponderopt==2 && $autorespondertype==1  || ($autoresponderopt==1 && $autorespondertype==1)) {
    if($infsoft_apikey!="" && isset($_COOKIE['inf_affiliateid']))  infusionsoft_referral($_SESSION['wemail'],$_COOKIE['inf_affiliateid']);
}
/** end  **/



$_SESSION['wlive'] = $domainname."/".$webifolder."/webinar-live.php?user=".base64_encode($_COOKIE['name'])."&email=".base64_encode($_COOKIE['email'])."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($_COOKIE['webitime'])."&webidate=".base64_encode($_COOKIE['dateset'])."&tz=".base64_encode($_COOKIE['timezone'])."";;
$_SESSION['wreplay'] = $domainname."/".$webifolder."/webinar-replay.php?user=".base64_encode($_COOKIE['name'])."&email=".base64_encode($_COOKIE['email'])."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($_COOKIE['webitime'])."&webidate=".base64_encode($_COOKIE['dateset'])."&tz=".base64_encode($_COOKIE['timezone'])."";;

$_SESSION['wtitle']=$webi_topic[$page];
$_SESSION['whost']=$webi_presenter[$page];


$you=0;
$me=0;
if($webischedopt == 1) {
    $tz = $webi_tz;
    //substitute the tz with users tz
date_default_timezone_set($_COOKIE['timezone']);
$me=date('Z').'<br>';
//substitute the tz with admin tz
date_default_timezone_set($tz);
$you=date('Z').'<br>';

$dif=$me-$you;


$diff=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00" ) + $dif;


$userown=$diff-$me;
$userown2=$diff;

$_SESSION['wdate3']=date('Y-M-d',$userown2);
$_SESSION['wtime3']=date('G',$userown2);


$_SESSION['wdate2']=date('Y-M-d',$userown);
$_SESSION['wtime2']=date('G',$userown);
    $_SESSION['yourtimelabel']="(".date('g:i a',strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00")-$you)." GMT)";
} else {
    $tz =$_COOKIE['timezone'];

    //substitute the tz with users tz
    date_default_timezone_set($tz);
    $me=date('Z').'<br>';
    $userown=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00")-$me;
    $_SESSION['wdate3']=date('Y-M-d',strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00"));
$_SESSION['wtime3']=date('G',strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00"));
$_SESSION['wdate2']=date('Y-M-d',$userown);

$_SESSION['wtime2']=date('G',$userown);
$_SESSION['yourtimelabel']="(".date('g:i a',strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00")-$me)." GMT)";
}




?>
<?php

if($ty_tpl == 0 && $_COOKIE['rn_date'] != 'rn' && $_COOKIE['rn_time'] !== 'rn') {
    include('thankyoutpl1.php');
}
elseif($ty_tpl == 1 && $_COOKIE['rn_date'] != 'rn' && $_COOKIE['rn_time'] !== 'rn') {
    include('thankyoutpl2.php');
}
elseif($_COOKIE['rn_date'] == 'rn' && $_COOKIE['rn_time'] == 'rn') {
    // include nothing
} else {
    include('thankyoutpl1.php');
}


?>



<?php
error_reporting(0);
if (!isset($_COOKIE['split_ip'])) {
    setcookie('split_ip',$_SERVER['REMOTE_ADDR'],time()+31622400);
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
else{
    $ip_address = $_COOKIE['split_ip'];
}

if (!isset($_COOKIE['ip_address'])) {
    setcookie('ip_address',$_SERVER['REMOTE_ADDR'],time()+31622400);
    $ip_address1 = $_SERVER['REMOTE_ADDR'];
}
else{
    $ip_address1 = $_COOKIE['ip_address'];
}


setcookie('user_date',$_SESSION['wdate'],time()+31622400);
setcookie('user_time',$_SESSION['wtime'],time()+31622400);
setcookie('user_email',$_SESSION['wemail'],time()+31622400);

/*** REGISTERED USER TRACKERS PART HERE ***/

include 'ebs-tracker/ty-tracker.php';

/*** END TRACKERS PART ***/



?>


<?php stripslashes($google_analytics); ?>

<img src="userupdate.php?action=registered&memberid=<?php echo $member_id;?>&webid=<?php echo $webid;?>&webtime=<?php echo $_SESSION['wtime'];?>&dateset=<?php echo $_SESSION['wdate'];?>&email=<?php echo $_SESSION['wemail'];?>" style="display:none; border: none;" />

<?php

/** if the date and time s equal to 'rn' , redirect to live page **/
if($_COOKIE['rn_date'] == 'rn' && $_COOKIE['rn_time'] == 'rn') {

    sleep(3);
    header("location:".$_SESSION['slink_replay']);

    exit;
}

?>
<?php echo stripslashes($tracker_ty); ?>
</body>
</html>