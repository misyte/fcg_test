<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
include 'loadinfo.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script src="js/gen_validatorv31.js" language="javascript" type="text/javascript"></script>
<script language="javascript" src="js/detect_timezone.js"></script>

<script type="text/javascript" language="javascript">

$(document).ready(function(){

    var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == 'undefined') {
        response_text = 'UTC';
    }
    else {
        response_text = tz_info.timezone.display();
    }

    document.myform.timezone.value = response_text;
});




</script>

<style type="text/css">
#logintbl{
 font-family: Arial, Helvetica, sans-serif;
 font-size: 12px;
 margin-top: 20px;
 border: 2px #CCCCCC solid;
}

.htxt{
}


#time{
    width: 200px;
    margin-top: 10px;
}
#times{
    width: 150px;
}

#dates{
    width: 200px;
    margin-top: 10px;
}

#dates{
    width: 150px;
}

</style>

</head>
<body>




<form action="webinar-live.php" method="post" name="myform">
<table width="27%" border="0" align="center" cellpadding="5" cellspacing="2" id="logintbl">
  <tr bgcolor="#868686">
    <td colspan="2"><span class="htxt"><b>Webinar Login Area</b></span></td>
    </tr>
  <tr>
    <td width="38%">select date </td>
    <td width="62%" rowspan="2">
        <?php include 'modules/regdatetime/index3.php';?>
  <tr>
    <td>select time </td>
    </tr>
  <tr>
    <td>name</td>
    <td><input name="name" type="text" id="name" style="width: 200px;" /></td>
  </tr>
  <tr>
    <td>email address </td>
    <td><input name="email" type="text" id="email" style="width: 200px;" /></td>
  </tr>
  <tr>
    <td><input type="hidden" name="timezone" id="timezone"> </td>
    <td><input type="submit" name="submit" value="Submit" /></td>
  </tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("myform");

 frmvalidator.addValidation("date","dontselect=0");
 frmvalidator.addValidation("time","dontselect=0");
 frmvalidator.addValidation("name","req");
 frmvalidator.addValidation("email","maxlen=50");
 frmvalidator.addValidation("email","req");
 frmvalidator.addValidation("email","email");

 </script>


</body>
</html>
