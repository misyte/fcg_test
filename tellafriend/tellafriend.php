<?php
 error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

include 'loadinfo.php';
    $radio="";
    $first_referral="";
    $is_first=1;
    if($is_email!=''){
        if($is_first==1){ $radio.="<div class='radio' id='emailRadio'><input type='radio' name='method' value='email' checked/> Email</div>"; $is_first=0; $first_referral="email"; }
        else  $radio.="<div class='radio' id='emailRadio'><input type='radio' name='method' value='email' /> Email</div>";


    }
    if($is_facebook!=''){
        if($is_first==1){ $radio.="<div class='radio' id='facebookRadio'><input type='radio' name='method' value='facebook' checked/> Facebook</div>"; $is_first=0; $first_referral="facebook"; }
        else $radio.="<div class='radio' id='facebookRadio'><input type='radio' name='method' value='facebook'  /> Facebook</div>";
    }
    if($is_twitter!=''){
        if($is_first==1){ $radio.="<div class='radio' id='twitterRadio'><input type='radio' name='method' value='twitter' checked/> Twitter</div>"; $is_first=0; $first_referral="twitter"; }
        else $radio.="<div class='radio' id='twitterRadio'><input type='radio' name='method' value='twitter'  /> Twitter</div>";
    }
    if($is_blog!=''){
        if($is_first==1){ $radio.="<div class='radio' id='blogRadio'><input type='radio' name='method' value='blog' checked/> Blog Comments</div>"; $is_first=0; $first_referral="blog"; }
        else $radio.="<div class='radio' id='blogRadio'><input type='radio' name='method' value='blog' /> Blog Comments</div>";
    }
    if($is_forum!=''){
       if($is_first==1){ $radio.="<div class='radio' id='forumRadio'><input type='radio' name='method' value='forum' checked/> Forum Posting</div>"; $is_first=0; $first_referral="forum"; }
       else $radio.="<div class='radio' id='forumRadio'><input type='radio' name='method' value='forum' /> Forum Posting</div>";
    }
    if($is_ownwebsite!=''){
        if($is_first==1){ $radio.="<div class='radio' id='ownsiteRadio'><input type='radio' name='method' value='ownwebsite' checked/> Own Website or Blog</div>"; $is_first=0; $first_referral="ownwebsite"; }
        else $radio.="<div class='radio' id='ownsiteRadio'><input type='radio' name='method' value='ownwebsite' /> Own Website or Blog</div>";
    }





?>
<html>
<head>
    <title>TELL A FRIEND BOX</title>
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="tellafriend/style.css" />
    <script src="tellafriend/js/jquery.js"></script>
    <script type="text/javascript" src="tellafriend/js/dynajax.js" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {

            if("<?php echo $first_referral; ?>"=="email")
                $('#email_r').show();
            if("<?php echo $first_referral; ?>"=="facebook")
                $('#facebook_r').show();
            if("<?php echo $first_referral; ?>"=="twitter")
                $('#twitter_r').show();
            if("<?php echo $first_referral; ?>"=="blog")
                $('#blog_r').show();
            if("<?php echo $first_referral; ?>"=="forum")
                $('#forum_r').show();
            if("<?php echo $first_referral; ?>"=="ownwebsite")
                $('#ownwebsite_r').show();

            $("input[name='method']").click(function(){
                if($('input[name=method]:checked').val()=='email' )
                {
                    $('#email_r').show();
                    $('#facebook_r').hide();
                    $('#twitter_r').hide();
                    $('#blog_r').hide();
                    $('#forum_r').hide();
                    $('#ownwebsite_r').hide();

                }
                if($('input[name=method]:checked').val()=='facebook' )
                {
                    $('#email_r').hide();
                    $('#facebook_r').show();
                    $('#twitter_r').hide();
                    $('#blog_r').hide();
                    $('#forum_r').hide();
                    $('#ownwebsite_r').hide();

                }
                if($('input[name=method]:checked').val()=='twitter' )
                {
                    $('#email_r').hide();
                    $('#facebook_r').hide();
                    $('#twitter_r').show();
                    $('#blog_r').hide();
                    $('#forum_r').hide();
                    $('#ownwebsite_r').hide();

                }
                if($('input[name=method]:checked').val()=='blog' )
                {
                    $('#email_r').hide();
                    $('#facebook_r').hide();
                    $('#twitter_r').hide();
                    $('#blog_r').show();
                    $('#forum_r').hide();
                    $('#ownwebsite_r').hide();

                }
                if($('input[name=method]:checked').val()=='forum' )
                {
                    $('#email_r').hide();
                    $('#facebook_r').hide();
                    $('#twitter_r').hide();
                    $('#blog_r').hide();
                    $('#forum_r').show();
                    $('#ownwebsite_r').hide();

                }
                if($('input[name=method]:checked').val()=='ownwebsite' )
                {
                    $('#email_r').hide();
                    $('#facebook_r').hide();
                    $('#twitter_r').hide();
                    $('#blog_r').hide();
                    $('#forum_r').hide();
                    $('#ownwebsite_r').show();

                }

             });
        });
        function facebook_ok(social)
        {
            if(social=="facebook"){
                verifyURL = $('#verifyURLf').val();
                verifyURL = verifyURL.toLowerCase();
                if(verifyURL.indexOf("facebook.com") != -1){
                     $('#twrapper').hide();
                     $('#cwrapper').show();
                }
                else{
                    alert("<?php echo addslashes($lang['TY_TXT_FREEBONUS_ALERT_3']);?>");
                }
            }
            if(social=="twitter"){
                verifyURL = $('#verifyURLt').val();
                verifyURL = verifyURL.toLowerCase();
                if(verifyURL.indexOf("twitter.com") != -1){
                     $('#twrapper').hide();
                     $('#cwrapper').show();
                }
                else{
                    alert("<?php echo addslashes($lang['TY_TXT_FREEBONUS_ALERT_3']);?>");
                }
            }

        }
        function validate_email()
        {
            if(document.email_referral.name.value==""){ alert("<?php echo addslashes($lang['TY_TXT_FREEBONUS_ALERT_1']);?>"); return 0; }

            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var address = document.email_referral.email.value;
            if(reg.test(address) == false) {
               alert('<?php echo $lang['TY_TXT_FREEBONUS_ALERT_2'];?>');
               return false;
            }
            <?php
            for($i=0;$i<$email_unlock;$i++)
            {
            ?>
                if(document.email_referral.<?php echo "name".($i+1); ?>.value==""){ alert("<?php echo ($i+1).') '.addslashes($lang['TY_TXT_FREEBONUS_ALERT_1']);?>"); return 0; }

                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                var address = document.email_referral.<?php echo "email".($i+1); ?>.value;
                if(reg.test(address) == false) {
                   alert('<?php echo ($i+1).') '.addslashes($lang['TY_TXT_FREEBONUS_ALERT_2']);?>');
                   return false;
                }

            <?php
            }
            ?>

            dynajax.request('respfile: tellafriend/sendmail.php; resultloc: t_header2; paramloc:t_head2_content; ');

        $('#twrapper').hide();
             $('#cwrapper').show();
        }
    </script>



</head>
<body>

<div id="t_header2">
    <div id="twrapper">

        <header>
        <h1><?php echo $lang['TY_TXT_FREEBONUS_H1'];?></h1>
        <h2><?php echo $lang['TY_TXT_FREEBONUS_H2'];?></h2>
    <!--end of header--></header>
        <section>
          <div class="step" id="step1">
                          <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP1'];?></h3>
          </div>
          <form id="radio"><?php echo $radio; ?></form>
        </section>
        <?php
         if($is_email!=''){
         ?>
            <div id="email_r" style="display:none;">
             <section>
                <div class="step" id="step2">
                <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP2'];?> (Minimum of <?php echo $email_unlock; ?> Friends)</h3>
            </div>

                        <form name="email_referral"><span id="t_head2_content">
                           <div class="owndetails">
                    <label><?php echo $lang['TY_TXT_FREEBONUS_STEP2_NAME'];?>:</label>
                        <input type="text" name="name" class="firstname" />
                    <label><?php echo $lang['TY_TXT_FREEBONUS_STEP2_EMAIL'];?>:</label>
                        <input type="text" name="email" class="lastname" />
                  </div>
                           <div class="clear"></div>
                           <div class="friendsdetails">
                    <label><?php echo $lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'];?>:</label>
                    <label><?php echo $lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'];?>:</label>
                                        <div class="clear"></div>
                                        <?php
                                            for($i=0;$i<$email_num;$i++)
                                            {

                                                if($email_unlock>$i) echo '<label class="numbers">'.($i+1).'</label>';
                                                else echo '<label class="numbers"><span>'.($i+1).'</span></label>';
                                                echo '<input name="names[]" id="name'.($i+1).'" type="text"  />
                                                      <input name="emails[]" id="email'.($i+1).'" type="text"  /><div class="clear"></div>';
                                            }
                                        ?>

                           </div>
                           <div class="clear"></div>
                           <div class="emailMessage">
                    <p><?php echo $lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'];?>:</p>
                    <label style="margin-top:4px;"><?php echo $lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'];?>:</label>
                        <input readonly type="text" name="mail_subject" value="<?php echo htmlentities(stripslashes_deep($email_subject), ENT_QUOTES, "UTF-8"); ?>" />
                    <label><?php echo $lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'];?>:</label>
                        <textarea readonly name="mail_content"><?php echo str_replace("#link#",$referral_link, stripslashes_deep($email_content)); ?></textarea>
                                                <input name="referral_link" type="hidden" value="<?php echo $referral_link; ?>" />
                    <input type="button" onClick="return validate_email()" value="<?php echo $lang['TY_TXT_FREEBONUS_STEP2_SEND'];?>" class="button" id="sendMail">
                </div>
                <div class="clear"></div>
                                </span>
            </form>
        <!--end of section--></section>
    <!--end of email--></div>


            <?php
            }
            ?>

            <?php if($is_facebook!='')
            { ?>
                <div id="facebook_r" style="display:none;">
                  <section>
                    <div class="step" id="step2">
                <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP2_FB'];?></h3>
            </div>
                        <form>
                <div class="message">
                    <textarea  readonly="readonly"><?php echo stripslashes_deep($facebook_title)."\n\n".str_replace("#link#",$referral_link, stripslashes_deep($facebook_description)); ?>
                    </textarea>
                    <input type="button" onClick="window.open('http://www.facebook.com/dialog/feed?app_id=169935636401090&link=<?php echo urlencode($referral_link); ?>&picture=http://fbrell.com/f8.jpg&name=<?php echo urlencode($facebook_title); ?>&caption=<?php echo urlencode($facebook_caption); ?>&description=<?php echo urlencode(str_replace("#link#",$referral_link, $facebook_description)); ?>&redirect_uri=http://facebook.com','FacebookPosting','width=600,height=400,scrollbars=yes,toolbar=yes,menubar=yes');" value="<?php echo $lang['TY_TXT_FREEBONUS_POST_FB'];?>" class="button" id="">
                </div>
                <div class="clear"></div>
            </form>
                  </section>
                  <section>
                   <div class="step" id="step3">
                <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP3_FB'];?></h3>
            </div>
                    <form>
                <div class="verify">
                    <p><?php echo $lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'];?></p>
                    <input type="text" name="verify_url" value="http://" id="verifyURLf" />
                    <input type="button" onClick="facebook_ok('facebook');" value="<?php echo $lang['TY_TXT_FREEBONUS_VERIFY_FB'];?>" class="button" >
                </div>
                <div class="clear"></div>
            </form>
                  </section>
                </div>
            <?php
            } ?>

            <?php if($is_twitter!='')
            { ?>
                <div id="twitter_r" style="display:none;">
                    <section>
                             <div class="step" id="step2">
                                     <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP2_TWITTER'];?></h3>
                             </div>
                             <form>
                                     <div class="message">
                                             <textarea  readonly="readonly"><?php echo str_replace("#link#",$referral_link, stripslashes_deep($twitter_message)); ?></textarea>
                                             <input type="button" onClick=" window.open('http://twitter.com/share?url=&amp;text=<?php echo urlencode(str_replace("#link#",$referral_link, stripslashes_deep($twitter_message))); ?>','TwitterShare','width=600,height=400');" value="<?php echo $lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ;?>" class="button" id="">
                                     </div>
                                     <div class="clear"></div>
                             </form>
                     <!--end of section--></section>

                     <section>

                    <div class="step" id="step3">
                <h3><?php echo $lang['TY_TXT_FREEBONUS_STEP3_TWITTER'];?></h3>
            </div>
            <form>
                <div class="verify">
                    <p><?php echo $lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'];?></p>
                    <input type="text" name="verify_url" value="http://" id="verifyURLt" />
                    <input type="button" onClick="facebook_ok('twitter');" value="<?php echo $lang['TY_TXT_FREEBONUS_VERIFY_FB'];?>" class="button" >
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>
    <!--end of twitter--></div>

            <?php
            } ?>

            <?php if($is_blog!='')
            { ?>
                <div id="blog_r" style="display:none;">
                    <section>
            <div class="step" id="step2">
                <h3>Blog Comments</h3>
            </div>
            <form>
                <div class="message">
                    <textarea  readonly="readonly"><?php echo str_replace("#link#",$referral_link, stripslashes_deep($blog_message)); ?></textarea>
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>

        <section>
                 <div class="step" id="step3">
                <h3>Verify Blog Comment</h3>
            </div>
            <form>
                <div class="verify">
                    <p>Enter your Blog page URL below to verify the comment. Upon verification, your hidden bonus will be unlocked right away!</p>
                    <input type="text" name="verify_url" value="http://" />
                    <input type="button" onClick="facebook_ok();" value="Verify URL!" class="button" id="">
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>
                </div>
            <?php
            } ?>

            <?php if($is_forum!='')
            { ?>
                <div id="forum_r" style="display:none;">
                    <section>
            <div class="step" id="step2">
                <h3>Forum Posting</h3>
            </div>
            <form>
                <div class="message">
                    <textarea  readonly="readonly"><?php echo str_replace("#link#",$referral_link, stripslashes_deep($forum_message)); ?></textarea>
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>
                    <section>
            <div class="step" id="step3">
                <h3>Verify Forum Post</h3>
            </div>
            <form>
                <div class="verify">
                    <p>Enter your Forum page URL below to verify the post. Upon verification, your hidden bonus will be unlocked right away!</p>
                    <input type="text" name="verify_url" value="http://" />
                    <input type="button" onClick="facebook_ok();" value="Verify URL!" class="button" id="">
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>

                </div>
            <?php
            } ?>

            <?php if($is_ownwebsite!='')
            { ?>
                <div id="ownwebsite_r" style="display:none;">
                    <section>
            <div class="step" id="step2">
                <h3>Send Email To Your Friends (Minimum of 4 Friends)</h3>
            </div>
            <form>
                <div class="emailMessage">
                    <label style="margin-top:4px;">Post title:</label>
                        <input  readonly="readonly" type="text" name="mail_subject" value="<?php echo htmlentities(stripslashes_deep($ownwebsite_title), ENT_QUOTES, "UTF-8"); ?>" />
                    <label>Post message:</label>
                        <textarea  readonly="readonly" name="mail_content"><?php echo str_replace("#link#",$referral_link, stripslashes_deep($ownwebsite_message)); ?></textarea>
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>
                    <section>
            <div class="step" id="step3">
                <h3>Verify Posting</h3>
            </div>
            <form>
                <div class="verify">
                    <p>Enter your Posting page URL below to verify the post. Upon verification, your hidden bonus will be unlocked right away!</p>
                    <input type="text" name="verify_url" value="http://" />
                    <input type="button" onClick="facebook_ok();" value="<?php echo $lang['TY_TXT_FREEBONUS_VERIFY_FB'];?>" class="button" id="">
                </div>
                <div class="clear"></div>
            </form>
        <!--end of section--></section>
                </div>
            <?php
            } ?>


         <footer><?php if($is_affiliate!=''){ ?>
        <div id="t_footer" style="text-align:right;"><a target="_blank"  style="margin-right:10px;text-decoration:none;font-family:'Arial';font-size:10px;color: #999;" href="http://google.com/affiliate.php?affiliate=<?php echo

$affiliate_id ?>">Powered by Tell A Friend</a></div>
    <?php } ?>   </footer>
    </div>


    </div>
</div>

<?php
?>
<div id="cwrapper" style="display:none;">
   <header>
        <h1><?php echo $lang['TY_TXT_FREEBONUS_UNLOCK_H1'];?></h1>
        <h2><?php echo $lang['TY_TXT_FREEBONUS_UNLOCK_H2'];?></h2>
    <!--end of header--></header>
   <section>
        <!--replace img with video code--><center> <?php
            if($is_video!='')
            { ?>
            <div id="player1" style="background-color:#000000;"></div>
                <script type="text/javascript" src="tellafriend/js/jwplayer.js"></script>
               <script type="text/javascript">

                        jwplayer("player1").setup({
                       // debug: 'console',
                        'flashplayer': "tellafriend/js/player.swf",
                        'file': '<?php echo $video_url; ?>',
                        'autostart': 'false',
                        'controlbar': 'bottom',
                        'width': '440',
                        'height': '320'


                        });


                </script>
        <?php
            } ?></center><br>
        <div class="step" id="freebar"><h3 style="padding-left:-60px;"><span style="color:#FF0;"><?php echo $lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'];?>:</span> <?php echo $lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'];?></h3></div>
        <div class="download">
            <?php
                         if($is_download!='')
            { ?>


                        <a target="_blank" href="<?php if(strlen(strstr($download_link,"http://"))>0) echo $download_link; else echo $download_link; ?>"><?php if($download_text!="") echo stripslashes_deep($download_text);  else echo "Click

here"; ?></a>

                </div>
        <?php
            }
        ?>

    <!--end of secion--></section>

    <footer></footer>


</div>
</body>

</html>