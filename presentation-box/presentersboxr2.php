<?php
/**
 * new presenter's info
 */

$get_npresenter =  unserialize(stripslashes($npresenter));
$get_npresenter_pic = unserialize(stripslashes($npresenter_pic));

if($is_rp == 1) {

    $webi_presenter[$page] = $rp_presenter;
    $get_presenter[$page] = $rp_presenter;
    $get_npresenter[$page] = $rp_npresenter;

    $epresenter_pic =  'http://eWebinars.com/'.$memberid.'/'.$rp_presenter_pic;
    $get_presenter_pic[$page] = $rp_presenter_pic;
    $get_npresenter_pic[$page] = $rp_npresenter_pic;


} else {
        $epresenter_pic = 'webinar-files/'.renamefilereg($get_presenter_pic[$page],$memberid);
}




?>


<div style="width:95%; background:#cecece; padding:1px; margin-bottom:20px;margin-left:15px;">
    <div style="width:99%; background:#f1f1f1; border:4px solid #fff;">
            <h1 style="border-bottom: 1px dotted #CCCCCC;
    color: #DA0000;
    font-family:Arial, Helvetica, sans-serif;
    font-size: 21px;
    letter-spacing: -0.01em;
    line-height: 38px;
    margin: 0 15px 10px;
    padding: 0 0 2px;
    text-align: left;
    text-shadow:0px 0px 0px; "><?php echo $lang['PBOX_H1'];?></h1>
    <?php if($get_presenter_pic[$page]!=""){ ?>
        <div style="background:url('webinar-files/box-pic.gif') no-repeat left top; float:left; width:100px; height:123px; padding:7px; margin:0px 10px 10px 10px;"> <img width="100" height="100" src="<?php echo $epresenter_pic; ?>" />
            <p style="font-size: 12px;
    line-height: 11px;
    color:#5c5c5c;
    margin: 6px 0 0;
    padding:3px 0px 0px;
    font-family:Arial, Helvetica, sans-serif;
    text-align: center;font-size:10px;"><?php echo $webi_presenter[$page]; ?></p>
        </div>
    <?php  } ?>

        <?php

        if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1 || ($get_npresenter[$page] != '')){
            if(trim($get_npresenter[$page]) != '' && $is_rp==0) {
                $aff_new_presenter_pic = 'webinar-files/'.renamefilereg($get_npresenter_pic[$page],$memberid);
                $aff_new_presenter_name = $get_npresenter[$page];

            }
            elseif($is_rp == 1 ) {

                $aff_new_presenter_pic = 'http://eWebinars.com/'.$memberid.'/'.$rp_npresenter_pic;
                $aff_new_presenter_name = $rp_npresenter;


            } else {
                $aff_new_presenter_pic  = 'http://eWebinars.com/'.$memberid.'/'.$a_affiliate_picture;
                $aff_new_presenter_name =  $a_affiliate_name;
            }

        ?>
        <div style="background:url('webinar-files/box-pic.gif') no-repeat left top; float:left; width:100px; height:123px; padding:7px; margin: 0px 10px 10px 0px;"> <img width="100" height="100" src="<?php echo $aff_new_presenter_pic; ?>" />
            <p style="font-size: 12px;
    line-height: 11px;
    color:#5c5c5c;
    margin: 6px 0 0;
    font-family:Arial, Helvetica, sans-serif;
    text-align: center;font-size:10px;"><?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1  || ($get_npresenter[$page] != '')) echo  $aff_new_presenter_name; ?></p>
        </div>
        <?php } ?>
        <p style="color: #666666;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 15px;
    line-height: 24px;
    margin:0 0 0 10px;
    padding:0px 10px 0px 0px;
    text-align: left;">
<strong style="color:#111111;"><?php echo $lang['PBOX_TXT_TOPIC'];?>:</strong> <?php echo $webi_topic[$page]; ?><br />
<strong style="color:#111111;"><?php echo $lang['PBOX_TXT_PRESENTER'];?>:</strong> <?php echo $webi_presenter[$page]; ?><br />

<?php if($get_npresenter[$page] != '') {?>
<strong style="color:#111111;"><?php echo $lang['PBOX_TXT_PRESENTER'];?>:</strong> <?php echo $get_npresenter[$page]; ?><br />
<?php } ?>


<?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1){ ?>
    <strong style="color:#111111;"><?php echo $lang['PBOX_TXT_HOST'];?>:</strong> <?php echo  $a_affiliate_name; ?><br />
<?php } ?>
<strong style="color:#111111;"><?php echo $lang['PBOX_TXT_DATEANDTIME'];?>:</strong> <?php echo $lang['REG_PBOX_DATETIME_VAL'];?>
    </p>
        <br style="clear:both;" />
    </div>
</div>

