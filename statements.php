<?php

$pagename = $_GET['link'];
$id = $_GET['id']; // for preview only

#include 'loadinfo.php';
include 'loadinfo.php'; // for preview only

$get_footerlinkname  = unserialize(base64_decode($footerlinkname));
$get_footerlink_internal  = unserialize(base64_decode($footerlink_internal));
$arr = array();
if(is_array($get_footerlinkname))
{
    foreach($get_footerlinkname as $var)
    {
        $arr[] = str_replace(' ','',$var);
    }
}

if (in_array($pagename, $arr)) {
    $key = array_search($pagename, $arr);
    $content = $get_footerlink_internal[$key];
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo htmlentities($page_title, ENT_QUOTES, "UTF-8"); ?></title>
</head>
<body>

<?php echo $content; ?>

</body>
</html>