<style type="text/css">

html, body {
    padding: 0;
    margin: 0;

}

.clear {
    clear: both;
}

#page {
    color: #555;
    font-size: 12px;
    font-family: 'Arial', sans-serif;
    line-height: normal;
    width: 520px;
    background: url(facebook/images/content-bg.gif) left top repeat-y;
    margin: 0 auto;
}

#page-top {
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/content-top-blue.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/content-top-darkgrey.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background: url(facebook/images/content-top-lightgrey.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background: url(facebook/images/content-top-yellow.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background: url(facebook/images/content-top-green.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'background: url(facebook/images/content-top-red.gif) left top no-repeat;';
    ?>

}

#page-bottom {
    background: url(facebook/images/content-bottom.gif) left bottom no-repeat;
    padding: 15px 15px 20px;
}

#header {
    text-align: center;
    height:95px;
    overflow: hidden;
    vertical-align:middle
}

#header h1 {
    font-size: 32px;

    <?php
        if($get_regtplcolor[$page]=="blue") echo 'color: #e1e1e1; background: url(facebook/images/header-line-blue.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color: #e1e1e1; background: url(facebook/images/header-line-darkgrey.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #000000; background: url(facebook/images/header-line-lightgrey.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #000000; background: url(facebook/images/header-line-yellow.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'color: #fff; background: url(facebook/images/header-line-green.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'color: #fff; background: url(facebook/images/header-line-red.gif) center bottom no-repeat;';
    ?>

    padding-bottom: 5px;
    margin: 0;
}

#header h2 {
    font-size: 18px;
     <?php
        if($get_regtplcolor[$page]=="blue") echo 'color:#6FF;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color: #ccc;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #707070;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #898989;';
        if($get_regtplcolor[$page]=="green") echo 'color:#6F9;';
        if($get_regtplcolor[$page]=="red") echo 'color:#F96;';
    ?>
    margin: 5px 0 0;
    font-weight: normal;
}

#content {
    padding: 0 7px;
}

#content h1 {
    text-align: center;
    font-size: 25px;
    color: #ce0000;
    line-height: 28px;
    letter-spacing: -0.5px;
    padding-bottom: 15px;
    padding-top:0px;
    background: url(facebook/images/title-bottom.png) center bottom no-repeat;
    margin-bottom: 15px;
}

#video {
    background: url(facebook/images/video-bg.png) left top no-repeat;
    width: 477px;
    height: 295px;
    margin: 10px 0px 0px 0px;
    padding: 9px 0 0 9px;
}

#optin {
     <?php
        if($get_regtplcolor[$page]=="blue") echo 'color: #fff; background: url(facebook/images/optin-bg-blue.gif) left top repeat-y;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color: #fff; background: url(facebook/images/optin-bg-darkgrey.gif) left top repeat-y;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #707070; background: url(facebook/images/optin-bg-lightgrey.gif) left top repeat-y;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #363636; background: url(facebook/images/optin-bg-yellow.gif) left top repeat-y;';
        if($get_regtplcolor[$page]=="green") echo 'color: #fff; background: url(facebook/images/optin-bg-green.gif) left top repeat-y;';
        if($get_regtplcolor[$page]=="red") echo 'color: #D7D7D7; background: url(facebook/images/optin-bg-red.gif) left top repeat-y;';
    ?>

    font-size: 15px;
    font-weight: bold;
    margin-top:20px;
    width: 501px;
    margin-left: -10px;
    border-radius: 10px 10px 0 0;
}

#optin-top {

    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/optin-top-blue.gif) 12px top no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/optin-top-darkgrey.gif) center top no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background: url(facebook/images/optin-top-lightgrey.gif) center top no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background: url(facebook/images/optin-top-yellow.gif) left top no-repeat;';
        if($get_regtplcolor[$page]=="green") echo ' background: url(facebook/images/optin-top-green.gif) center top no-repeat;';
        if($get_regtplcolor[$page]=="red") echo ' background: url(facebook/images/optin-top-red.gif) left top no-repeat;';
    ?>
}

#optin-bottom {
    padding: 0 25px 20px;
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/optin-bottom-blue.gif) left bottom no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/optin-bottom-darkgrey.gif) left bottom no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background: url(facebook/images/optin-bottom-lightgrey.gif) left bottom no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background: url(facebook/images/optin-bottom-yellow.gif) left bottom no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background: url(facebook/images/optin-bottom-green.gif) left bottom no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'background: url(facebook/images/optin-bottom-red.gif) left bottom no-repeat;';
    ?>

}

#optin .header {
    text-align: center;
}

#optin .header h1 {
    font-family: 'Helvetica Neue', 'Myriad Pro', sans-serif;
    font-size: 30px;
     <?php
        if($get_regtplcolor[$page]=="blue") echo 'color: #ffbd00;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color: #ffbd00;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #cc0000;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #363636;';
        if($get_regtplcolor[$page]=="green") echo 'color: #ffbd00;';
        if($get_regtplcolor[$page]=="red") echo 'color: #FC0;';
    ?>
    font-weight: bold;
    margin: 0;
    padding: 20px 0 4px;
    background: none;
}

#optin .header h2 {
    padding: 0 0 10px;
    margin: 0 0 20px;
    font-weight: normal;

    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/optin-line-blue.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/optin-line-darkgrey.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color: #707070; background: url(facebook/images/optin-line-lightgrey.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'color: #666666; background: url(facebook/images/optin-line-yellow.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background: url(facebook/images/optin-line-green.gif) center bottom no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'color: #d7d7d7; background: url(facebook/images/optin-line-red.gif) center bottom no-repeat;';
    ?>
}

#optin .field {
    margin-bottom: 10px;
    <?php
        if($get_regtplcolor[$page]=="blue") echo '';
        if($get_regtplcolor[$page]=="darkgrey") echo '';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color:#333;';
        if($get_regtplcolor[$page]=="yellow") echo '';
        if($get_regtplcolor[$page]=="green") echo '';
        if($get_regtplcolor[$page]=="red") echo 'color:#ffffff;';
    ?>
}

#optin .field label {
    float: left;
    width: 270px;
}

#optin .field div.input {
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/textfield-bg-blue.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/textfield-bg-darkgrey.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background: url(facebook/images/textfield-bg-lightgrey.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background: url(facebook/images/textfield-bg-yellow.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background: url(facebook/images/textfield-bg-green.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'background: url(facebook/images/textfield-bg-red.png) left top no-repeat;';
    ?>

    width: 160px;
    height: 20px;
    padding: 6px 10px 8px;
    float: right;
}

#optin .field select,
#optin .field input,
 .inputbg select,
 .inputbg input {
    width: 160px;
    height: 20px;
    border: none;
    background: #f4f4f4;
}

#optin .field div.email-input {
    margin-top: 10px;
}

#optin .field label .current-time {
    padding-left: 15px;
    font-weight: normal;

    <?php
        if($get_regtplcolor[$page]=="blue") echo 'color:#CCC;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color:#CCC;';
        if($get_regtplcolor[$page]=="lightgrey") echo '';
        if($get_regtplcolor[$page]=="yellow") echo 'color:#666;';
        if($get_regtplcolor[$page]=="green") echo 'color:#CCC;';
        if($get_regtplcolor[$page]=="red") echo 'color:#cccccc;';
    ?>
}

#optin form {
    padding-bottom: 75px;
    position: relative;
}

#optin .button {
    height: 55px;
    position: absolute;
    bottom: 5px;
    left: 40px;
}

#optin .field-checkbox {
}

#optin .field-checkbox span.label {
    float: left;
}

#optin .field-checkbox label {
    position: relative;
    padding-left: 25px;
    float: right;
    width: 320px;
    font-size: 13px;
    font-weight: normal;
    <?php
        if($get_regtplcolor[$page]=="lightgrey") echo 'color:#666;';
        else echo 'color: #e5f2fa;';

    ?>

}

#optin .field-checkbox label input {
    position: absolute;
    left: 0;
    top: 2px;
}

#optin .button input.submit {
    text-shadow: 1px 1px 1px #fff;
    cursor: pointer;
    font-family: 'Helvetica Neue', Arial, sans-serif;
    font-size: 33px;
    font-weight: bold;
    color: #222;
    height: 55px;
    line-height: 55px;
    text-align: center;
    display: block;
    width: 370px;
    margin: 0 auto;
    background: none;
    border: none;
}
.sms{padding-top:45px;font-size:12px;font-weight:normal;text-align:left !important;}
#sms_slide{padding-left:50px;}
.inputbg {
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background: url(facebook/images/textfield-bg-blue.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background: url(facebook/images/textfield-bg-darkgrey.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background: url(facebook/images/textfield-bg-lightgrey.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background: url(facebook/images/textfield-bg-yellow.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background: url(facebook/images/textfield-bg-green.png) left top no-repeat;';
        if($get_regtplcolor[$page]=="red") echo 'background: url(facebook/images/textfield-bg-red.png) left top no-repeat;';
    ?>

    width: 160px;
    height: 20px;
    padding: 6px 10px 8px;
    float: left;

}
.inputbg select,.inputbg input{font-family:'Arial',sans-serif;  font-size:12px !important;}
#expiredsticker{margin-left:-15px;margin-top:20px;}
</style>