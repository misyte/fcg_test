<?php session_start();

    $Filename = "webinar-alert.vcs";
    header("Content-Type: text/x-vCalendar");
    header("Content-Disposition: inline; filename=$Filename");
    include 'loadinfo.php';
?>
<?php
                $datetimereg=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00");
                if($webischedopt == 1) {
                    $yourtimelabel=makeoffsetcity2($webischedopt,$webi_tz);

                } else {
                    $yourtimelabel='Your Local Time';

                }
                $yourtimelabel=$yourtimelabel." ".$_SESSION['yourtimelabel'];


            ?>
BEGIN:VCALENDAR
VERSION:1.0
BEGIN:VEVENT
CATEGORIES:MEETING
DTSTART:<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($_SESSION['wtime2'])==1) echo "0".$_SESSION['wtime2']; else echo $_SESSION['wtime2']; ?>0000
<?php $_SESSION['wtime2']=$_SESSION['wtime2']+1; ?>
DTEND:<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($_SESSION['wtime2'])==1) echo "0".$_SESSION['wtime2']; else echo $_SESSION['wtime2']; ?>0000
SUMMARY:WEBINAR ALERT
DESCRIPTION:WEBINAR ALERT\n\n- Webinar: <?php echo $_SESSION['wtitle']; ?>\n- Host: <?php echo $_SESSION['whost']; ?>\n- Starting time: <?php echo $_SESSION['wdate']; ?> at <?php echo date("g:00 a",$datetimereg)." ".$yourtimelabel; ?>\n- Access the webinar room here: <?php echo $_SESSION['wlive']; ?>
CLASS:PRIVATE
END:VEVENT
END:VCALENDAR