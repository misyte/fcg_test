<?php

$webinar_name = base64_encode(stripslashes($webiname));
$member_id = $memberid;
$rpage = $splitregopt;
$bpage = $splitliveopt;

if(strtolower($_GET['access']) != 'ok') {

    if (!isset($_COOKIE['split_ip'])) {
        setcookie('split_ip',$_SERVER['REMOTE_ADDR'],time()+31622400);
        $ip_address = $_SERVER['REMOTE_ADDR'];
    } else {
        $ip_address = $_COOKIE['split_ip'];
    }

    if($thispage == 'replay' && !($_COOKIE['rn_date'] == 'rn' && $_COOKIE['rn_time'] == 'rn')) {
    $url = $split_url.'tracker.php?webinar_name='.urlencode($webiname).'&webid='.$webid.'&member_id='.$member_id.'&pagetype=sawreplay&webinar_date='.base64_decode($_GET['webidate']).'&webinar_time='.base64_decode($_GET['webitime']).'&registrant='.urlencode(base64_decode($_GET['user'])).'&email='.base64_decode($_GET['email']).'&ip='.$ip_address;
    } else {
    $url = $split_url.'tracker.php?webinar_name='.urlencode($webiname).'&webid='.$webid.'&member_id='.$member_id.'&pagetype=broadcast&webinar_date='.base64_decode($_GET['webidate']).'&webinar_time='.base64_decode($_GET['webitime']).'&registrant='.urlencode(base64_decode($_GET['user'])).'&email='.base64_decode($_GET['email']).'&ip='.$ip_address;
    }
/*
* comment out $page var this if you don't want to split test register page
*/
    $page = curlURL($url);
    $page = trim($page);

    if($page != '' && $page >0) {
        if($page > $bpage) {
            $page=0;
        } else {
            $page = $page -1;
        }
    } else {
        $page = 0;
    }


} else { // if direct access is enabled
    $page = $pageaccess;
}



$get_regtpl = unserialize(stripslashes($regtpl));
$get_regtplcolor = unserialize(stripslashes($regtplcolor));
if($get_regtpl[$page] == 0 ) {
    $tpltype = 'software';
}
if($get_regtpl[$page] == 1 ) {
    $tpltype = 'squeeze';
}


?>