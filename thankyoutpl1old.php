<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="<?php echo $tpltype.'/'.$get_regtplcolor[$page].'/';?>style.css" media="screen" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
  $.ajax({
                        type: "POST",
                        url: "<?php echo $_SESSION['action_url']; ?>?",
                        data: "<?php echo $_SESSION['post_string']; ?>",
                        success: function(data){
                            $("#tot_loggedin").html(data);

                    }
                });





});
</script>
<?php echo stripslashes_deep($google_analytics_header); ?>
</head>

<style type="text/css">
@charset "utf-8";
*{
margin:0;
padding:0;
}


img, a img{
border:0 none;
}

img{
vertical-align:baseline;
}

a{
outline:0;
}

input:focus, textarea:focus, select:focus{
outline: none;
}

textarea {
resize: none;
}

/*img, div, input { behavior: url("iepngfix.htc") }*/
#header{
background:none;
}

.imgCenter{
display:block;
margin:0 auto;
clear:both;
}

.imgRight{
float:right;
}

.imgLeft{
float:left;
}

.none{
display:none;
}

.underlined{
text-decoration:underline;
}

.textCenter{
text-align:center;
}

.textLeft{
text-align:left;
}

.textRight{
text-align:right;
}

.clear{
clear:both;
height:0;
line-height:0;
font-size:0;
}

body{
font-size:14px;
color:#333333;
font-family:Arial, Helvetica, sans-serif;
}

/*-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: alpha(opacity=0);*/

.wrapperout{
    background:#cecece;
    width:900px;
    border:1px solid #fcfcfc;
    margin: auto;

    }
.wrapper{
    width:892px;
    margin:4px;
    min-height:300px;
    background:#FFF;
    }

.left{
    width:477px;
    float:left;
    margin:30px;
    min-height:200px;
    }

.left h1{
    color: #DA0000;
    font-size: 30px;
    letter-spacing: -0.04em;
    margin-bottom: 12px;
    font-family:Arial,Helvetica,sans-serif;
    text-shadow: 0 0 0;
    }

.video{
    width:467px;
    padding:5px;
    background:#e7e7e7;
    }

.customLink{
    width:477px;
    height:154px;
    margin:10px 0 0;
    background:url(webinar-files/customlink.jpg) no-repeat left top;
}

.codeLink{
    width:350px;
    height:65px;
    margin:66px 0 0 105px;
    float:left;
    overflow:scroll;
    }

.right{
    float:left;
    width:331px;
    margin:30px 0 0 5px;
    }

.infoWebinar{
    background: none repeat scroll 0 0 #E5E5E5;
    border-bottom: 1px solid #F8F8F8;
    border-left: 1px solid #F8F8F8;
    border-right: 1px solid #F8F8F8;
    padding: 0 0 20px;
    }

.bordertop{
    border-top: 1px solid #F8F8F8;
    padding: 9px 10px 6px;
    background:#ebebeb;
    }

.paddingtop{
    padding-top:1px !important;
    }

.infoWebinar p{
    padding:0 28px;
    margin:0 0 5px;
    }

.infoWebinar p strong{
    display:inline-block;
    width: 70px;
    }

.infoWebinar .grayText{
    color:#9c9c9c;
    text-indent:70px;
    font-size:12px;
    line-height:14px;
    }

.infoWebinarOut{
    background:#d0d0d0;
    padding:0 1px 1px;
    }

.calendars{
    list-style-type:none;
    padding-left:0px;
    }

.calendars li{
    display:inline-block;
    }

.socmed{
    float: right !important;
    margin-top: 13px !important;
    }
</style>

<script type="text/javascript">


function selectText() {
        if (document.selection) {
        var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById('codeLink'));
        range.select();
        }
        else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById('codeLink'));
        window.getSelection().addRange(range);
        }
    }


</script>

<body>


    <div id="header">
    <?php if($get_regtpl[$page] != 2) { ?>
        <?php if(trim($get_regheader[$page]) == 1){ ?>
         <center><?php if($get_headerimg[$page]!=""){ ?><img src="<?php echo 'webinar-files/'.renamefilereg($get_headerimg[$page], $memberid);?>" alt="Live Webinar Broadcast"/><?php } ?></center>
        <?php }else{ ?>
        <h1><?php echo $get_regtxtheader1[$page]; ?></h1>
        <h2><?php echo $get_regtxtheader2[$page]; ?></h2>
        <?php } ?>
     <?php } else { ?>
        <h1>Webinar Event</h1>
        <h2><?php echo $webi_topic[$page]; ?></h2>
     <?php } ?>


    </div> <!-- end header -->



    <div class="wrapperout">
    <div class="wrapper">
        <div class="left">
            <h1>Congratulations! You're registered!</h1>
            <div class="video">
                <!--video code here width = 467px-->
             <div style="background-color:#000; vertical-align:middle; text-align:center; width:467px; height:343px;">

                                    <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
                                    <script type="text/javascript" src="player/jwplayer.js"></script>
                                    <!-- END OF THE PLAYER EMBEDDING -->
                                <div id="player"></div>
                                    <script type="text/javascript">
                                    jwplayer('player').setup({
                                      'flashplayer': 'player/player.swf',
                                      'file': '<?php if($ty_vid == 1 && $ty_freebonus==1){ ?>https://s3.amazonaws.com/internet-marketing-tools/webinarregisterthankyou1.mov<?php }elseif($ty_vid == 1 && $ty_freebonus==0){ ?>https://s3.amazonaws.com/internet-marketing-tools/webinarregisterthankyou2.mov<?php }else{ echo $ty_vidurl; } ?>',
                                      'feature': 'related',
                                      'controlbar': 'over',
                                      'autostart': 'true',
                                      'width': '467',
                                      'height': '343',
                                      'plugins': ' '
                                    });
                                    </script>



    </div>
                <!--end of video code-->
            </div>
            <div class="customLink">
                <div class="codeLink" id="codeLink" onClick="selectText()"  style="font-size:12px;"><?php echo ($_SESSION['slink_live']); ?></div>
                <p>&nbsp;</p>
            </div>
        </div>
        <div class="right"> <a href="javascript:window.print()"><img src="webinar-files/print.jpg" /></a>
            <div class="ticket"> <img src="webinar-files/ticket.jpg" class="imgCenter" />
                <div class="infoWebinarOut">
                    <div class="infoWebinar">
                        <p><strong>Webinar:</strong> <?php echo stripslashes_deep($webi_topic[$page]); ?></p>

                        <p><strong>Host:</strong> <?php echo stripslashes_deep($webi_presenter[$page]); ?></p>
                        <p><strong>Name:</strong>  <?php echo stripslashes_deep($_SESSION['wname']); ?></p>
                        <p><strong>Email:</strong> <?php echo $_SESSION['wemail']; ?></p>
                        <p><strong>Date:</strong> <?php echo $datenowval =  date('jS \o\f F',strtotime( $_SESSION['wdate'])); ?></p>
                        <p><strong>Time:</strong> <?php echo get12hr2($_SESSION['wtime']); echo makeoffsetcity($webischedopt,$webi_tz);?></p>
                    </div>
                </div>
            </div>
            <div style="margin-top:10px;"><img src="webinar-files/phonealert.gif" /></div>
            <?php
                $datetimereg=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00");
                if($webischedopt == 1) {
                    $yourtimelabel=makeoffsetcity2($webischedopt,$webi_tz);
                } else {
                    $yourtimelabel='Your Local Time';
                }
                $yourtimelabel=urlencode($yourtimelabel)." ".$_SESSION['yourtimelabel'];
            ?>
            <ul class="calendars">
                <li><a href="vcs2.php"><img src="webinar-files/ical.jpg" /></a></li>
                <?php $time2=$_SESSION['wtime2']+1;  ?>
                <li><a target="_blank" href="http://www.google.com/calendar/event?action=TEMPLATE&text=WEBINAR ALERT&dates=<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($_SESSION['wtime2'])==1) echo "0".$_SESSION['wtime2']; else echo $_SESSION['wtime2']; ?>0000Z/<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($time2)==1) echo "0".$time2; else echo $time2; ?>0000Z&details=WEBINAR ALERT:%0A%0A- Webinar: <?php echo urlencode(addslashes($webi_topic[$page])); ?>%0A- Host: <?php echo addslashes($webi_presenter[$page]); ?>%0A- Starting time: <?php echo $_SESSION['wdate']; ?> at <?php echo date("g:00 a",$datetimereg)." ".$yourtimelabel; ?>%0A- Access the webinar room here: <?php echo urlencode($_SESSION['wlive']); ?>&location=&trp=true&sprop=<?php echo $domainname; ?>&sprop=name:<?php echo addslashes($from_name); ?>"><img src="webinar-files/googleCal.jpg" /></a></li>
                <li><img src="webinar-files/outlookCal.jpg" />
                <div style="text-align:center;font-size:11px;margin-top:-15px;color:#3F7FFD;line-height:12px">Click on Version<br><a style="text-decoration:none;color:#3F7FFD;" href="vcs2.php">2003</a>&nbsp; <a style="color:#3F7FFD;text-decoration:none" href="vcs.php">2007</a>&nbsp;<a style="text-decoration:none;color:#3F7FFD;" href="vcs2.php">2010</a></div></li>

            </ul>
            <div class="infoWebinarOut paddingtop">
                <div class="infoWebinar bordertop">

                    <div id="fb-root"></div>
                    <script src="http://connect.facebook.net/en_US/all.js#appId=203398326385631&amp;xfbml=1"></script>
                    <fb:like href="<?php echo $domain_name;?>webinar-register.php" send="true" layout="box_count" width="50" show_faces="false" action="like" font=""></fb:like>
                    <a href="#" target="_parent"><img class="socmed" src="webinar-files/twitter.jpg" onClick="window.open('http://twitter.com/share?url=<?php echo $domain_name;?>webinar-register.php&text=<?php echo addslashes($webi_presenter[$page]); ?> is conducting a great webinar on <?php echo addslashes($webi_topic[$page]); ?>. I have already signed up! You can register here: ','TwitterShare','width=600,height=400');" /></a>
                    <img src="webinar-files/facebook.jpg" class="socmed"  /></div>
            </div>
        </div>
        <br class="clear" />

        <?php
if($ty_freebonus==1)
{ ?>
 <table width="100%" border="0" cellpadding="0" style="margin-top:10px;">
    <tr>
        <td colspan="2"> <center><img src="webinar-files/tellafriendtitle.gif"  /></center></td>
    </tr>
    <tr>
        <td style="width:584px;vertical-align:text-top;padding-right:20px;padding-left:30px;padding-bottom:30px;line-height:24px;"><br>
                <?php


                        echo stripslashes_deep($freebonus_desc);

                ?>
        </td>
        <td style="width:340px;vertical-align:text-top;text-align:center;">
            <br>
                <?php
                if($iv_opt==1)
                {
                    $iv_img_rename=explode(".", $iv_img);
                    $imgsrc="webinar-files/".$iv_img_rename[0]."_".$memberid.".".$iv_img_rename[1];
                    list($widths1, $heights1, $types1, $attrs1) = getimagesize($imgsrc);


                    if($widths1>320) echo '<img src="'.$imgsrc.'" style="width:320px;height:auto;"  />';
                    else echo '<img src="'.$imgsrc.'" style="width:auto;height:auto;"  />';
                }
                else{ ?>


          <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
                                    <script type="text/javascript" src="player/jwplayer.js"></script>
                                    <!-- END OF THE PLAYER EMBEDDING -->
                                  <div style="text-align:center;"><div id="player2" style="background-color:#000000;"></div></div>
                                    <script type="text/javascript">
                                    jwplayer('player2').setup({
                                      'flashplayer': 'player/player.swf',
                                      'file': '<?php echo $iv_vid;?>',
                                      'feature': 'related',
                                      'controlbar': 'over',
                                      'autostart': 'false',
                                      'width': '320',
                                      'height': '265',
                                      'plugins': ' '
                                    });
                                    </script>







            <?php } ?>
        </td>
    </tr>
 </table>
<?php
 include 'tellafriend/tellafriend.php';
 ?>
<?php
 }
 ?>
    </div>
</div>




    <div id="footer">
        <?php  include 'footer.php'; ?>
    </div> <!-- end footer -->

    <?php echo stripslashes_deep($google_analytics_body); ?>
    </body>
    <?php echo stripslashes_deep($google_analytics_footer); ?>
    </html>