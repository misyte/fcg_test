<?php

/*
 * getting the right version of register split version data
 * for this new design, the old design gets the reg split version
 * at the stylecode.php
 * variable: $page1
 */


$webinar_name = urlencode(stripslashes($webiname));
$member_id = $memberid;
$rpage = $splitregopt;
$bpage = $splitliveopt;

if (!isset($_COOKIE['split_ip'])) {
    setcookie('split_ip',$_SERVER['REMOTE_ADDR'],time()+31622400);
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
else{
    $ip_address = $_COOKIE['split_ip'];
}






/*
* comment out $page1 var this if you don't want to split test register page
*/
$page1 = curlURL($split_url.'tracker.php?webinar_name='.$webinar_name.'&webid='.$webid.'&member_id='.$member_id.'&pagetype=register&rpage='.$rpage.'&bpage='.$bpage.'&ip='.$ip_address);

$page1 = trim($page1);

if($page1 != '' && $page1 >0) {
    if($page1 > $rpage)
    {
        $page1=0;
    }
    else{
        $page1 = $page1 -1;
    }
} else {
    $page1 = 0;
}


if(strtolower($_GET['access'])!= '' && strtolower($_GET['access']) == 'ok') {
    $page1 = $page1access;
}



?>