<?php

$webinar_name = urlencode(stripslashes($webiname));
$member_id = $memberid;
$rpage = $splitregopt;
$bpage = $splitliveopt;

if (!isset($_COOKIE['split_ip'])) {
    setcookie('split_ip',$_SERVER['REMOTE_ADDR'],time()+31622400);
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
else{
    $ip_address = $_COOKIE['split_ip'];
}



/*
* comment out $page1 var this if you don't want to split test register page
*/
$page1 = curlURL($split_url.'tracker.php?webinar_name='.$webinar_name.'&webid='.$webid.'&member_id='.$member_id.'&pagetype=register&rpage='.$rpage.'&bpage='.$bpage.'&ip='.$ip_address.'&stylecode=1');

$page1 = trim($page1);

if($page1 != '' && $page1 >0) {
    if($page1 > $rpage)
    {
        $page1=0;
    }
    else{
        $page1 = $page1 -1;
    }
} else {
    $page1 = 0;
}

/** for direct access live **/
if(strtolower($_GET['access'])!= '' && strtolower($_GET['access']) == 'ok' && $isreppage != 1) {
    $page1 = $page1access;
}
/* end **/

/** for direct access replay **/
if(strtolower($_GET['access'])!= '' && strtolower($_GET['access']) == 'ok' && $isreppage == 1) {
    $page1 = 0;
}
/* end **/



?>

<style type="text/css">
<?php
    $get_regtplcolor = unserialize(stripslashes($regtplcolor));
    $tpl=$get_regtplcolor[$page1];
?>
body, h1, h2, h3, h4, h5, p, form, ul, ol, li, fieldset {
margin: 0;
padding: 0;
}

object, embed {
outline: none;
}

ul, ol, li {
list-style: none;
}

img, fieldset {
border: 0;
}

body {
background: #ededed url(broadcast/images/<?php echo $get_regtplcolor[$page1]."_"; ?>bg.png) repeat-x;
font-family: arial, verdana, helvetica, san-serif;
font-size: 15px;
color: #595959;
}

#header {
height: 185px;
background: url(broadcast/images/<?php echo $get_regtplcolor[$page1]."_"; ?>header_bg.png) no-repeat;
width: 1000px;
margin: 0 auto;

}

#header h1 {
padding-top: 4px;
}

h1 {
font-size: 48px;
font-family: impact, verdana, arial, tahoma, san-serif;
letter-spacing: 2px;
font-weight: normal;
color: <?php if($tpl=="blue") echo '#fff'; elseif($tpl=="darkgrey") echo '#fff'; elseif($tpl=="green") echo '#fff'; elseif($tpl=="lightgrey") echo '#5f605f'; elseif($tpl=="red") echo '#fff'; elseif($tpl=="yellow") echo '#414448';  ?>;
margin: 0;
padding: 0;
text-align: center;
text-shadow: 0 2px 2px #000;
}

h2 {
font-size: 36px;
font-family: impact, verdana, arial, san-serif;
letter-spacing: 2px;
font-weight: normal;
color: <?php if($tpl=="blue") echo '#3abeff'; elseif($tpl=="darkgrey") echo '#ff8400'; elseif($tpl=="green") echo '#274800'; elseif($tpl=="lightgrey") echo '#fff'; elseif($tpl=="red") echo '#fe8686'; elseif($tpl=="yellow") echo '#fff';  ?>;
margin: 0;
padding: 0;
text-align: center;
}

#wrapper {
width: 1000px;
margin: 0 auto;
background: url(broadcast/images/body_bg.png) repeat-y;
}

#room {
width: 1000px;
margin: 0 30px;
}

#video {
width: 660px;
float: left;
margin-bottom: 25px;
}

#video_top {
width: 660px;
background: #f6f6f6 url(broadcast/images/v_top.png);
float: left;
margin: 0px;
padding:0px;
}

#video_mid {
width: 660px;
background: #f6f6f6 url(broadcast/images/v_mid.png);
float: left;
margin: 0px;
padding:0px;
}

#video_bottom {
width: 660px;
background: #f6f6f6 url(broadcast/images/v_bottom.png);
float: left;
margin: 0px;
padding:0px;
}

#video_player {
width: 640px;
margin: 0 auto;

}

#download {
width: 640px;
text-align: center;
height: 137px;
margin: 0 auto;
margin-top: 30px;
background-color: #f6f6f6;
}

#question_form {
margin: 25px 25px 25px 35px;
color: #000;
width: 600px;
}

#question_form form {
margin: 20px 0 20px 10px;
}

#question_form label {
width: 165px;
float: left;
}

.entry {
font-size: 13px;
color: #3d3d3d;
width: 350px;
border: 1px solid #ccc;
float: left;
margin: 0 0 8px 25px;
padding: 7px 0 7px 5px;
}

.submit {
margin: 12px 0 0 190px;
}

#instructions {
line-height: 24px;
font-size: 15px;
}

.clear {
clear: both;
}

#chat_list {
width: 283px;
float: left;
}

#chat_list_header {
background: url(broadcast/images/chat_list_header.png) no-repeat;
height: 55px;
width: 283px;
color: #fff;
font-size: 15px;
text-align: center;
line-height: 60px;
margin-top: -10px;
}

#chat_listing {
background: url(broadcast/images/chat_list_bg.png) repeat-y;
width: 283px;
}

#chat_list_list {
height: 440px;
width: 273px;
overflow: auto;
}

#chat_listing ul {
margin: 0;
padding-left:10px;
}

#chat_listing li {
line-height: 27px;
height: 27px;
border-bottom: 1px solid #ccc;
padding-left: 30px;
padding-right: 0;
}

#chat_listing li.last {
border-bottom: none;
}

#chat_listing li a {
color: #000;
text-decoration: none;
font-size: 13px;
font-weight: bold;
}

#chat_listing li a span {
color: #fff;
text-decoration: none;
font-size: 13px;
font-weight: bold;
}

#host {
background: #fd9500 url(broadcast/images/mic.gif) no-repeat center right;
}

#chat_list_bottom {
background: url(broadcast/images/chat_list_bottom.png) no-repeat;
height: 17px;
width: 283px;
}

#footer {
width: 1000px;
margin: 0 auto;
background: url(broadcast/images/footer_bg.png) no-repeat;
height: 88px;
text-align: center;
}

#footer p {
line-height: 88px;
color: #c5c5c5;
font-size: 12px;
}

#footer a {
text-decoration: none;
color: #c5c5c5;
padding: 0 10px;
}

#ajaxloader{
  position: fixed;
  top: 0;
  z-index:100;
  width:100%;
  height:50px;
  position:fixed;
  padding-top: 10px;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.8);
  text-align: center;
  font-family:Tahoma, Arial, Helvetica, sans-serif;
  color: #fff;
  display: none;
}

#buffertime, #preview1{
  position: fixed;
  top: 0;
  z-index:100;
  width:100%;
  height:50px;
  position:fixed;
  padding-top: 10px;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.8);
  text-align: center;
  font-family:Tahoma, Arial, Helvetica, sans-serif;
  color: #fff;
  display: block;
}







</style>

<?php

// register topic and tpl
$get_regtpl = unserialize(stripslashes($regtpl));
$get_topic = unserialize(stripslashes($topic));



?>