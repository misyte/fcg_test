<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
</head>
<body>


<?php

include('./split-register.php');
/** show if the user view the page in a mobile device */
if(detect_mobile()) {
    echo '<div style="width:100%; margin:0;height:50px;padding-top:10px;background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.8);text-align: center; font-family:Tahoma, Arial, Helvetica, sans-serif;color: #fff;">This webinar is not compatible with mobile devices, so please log in from your desktop or laptop computer</div>';
    exit;
}





$resdate = getdate(strtotime($webidate));

$dd = $resdate[mday];
$mm = $resdate[mon];
$yy = $resdate[year];

$mytz = base64_encode($tz);


?>
<link href="broadcast/dark.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="js/countdownjs.php?querystring=<?php echo urlencode($_SERVER["QUERY_STRING"]);?>&mytz=<?php echo $mytz;?>"></script>



<?php

$get_intro_box = unserialize(stripslashes($intro_box));
$webi_topic=unserialize(stripslashes($topic));
$webi_presenter=unserialize(stripslashes($presenter));
$get_presenter_pic = unserialize(stripslashes($presenter_pic));
$get_regtpl = unserialize(stripslashes($regtpl));
if($get_intro_box[$page]==1 && $get_regtpl[$page] != 2){
                        include 'loadinfo.php';
                        $is_affiliate=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=q');
                        $a_affiliate_name=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=n');
                        $a_affiliate_picture=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=i');

                    ?>

<style type="text/css">
body {
    background-color: #333;
}
</style>
<div style="background-color:#333333;">

<table align="center" width="900" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#D8D8D8">
  <tr>
    <td colspan="5" align="center" valign="middle" style=" padding:10px;">
    <span style="color:#FC6; font-family:Impact, Tahoma, Arial; font-size:36px; font-style:italic;">Webinar: </span>
    <span style="color:#FFF; font-family:Impact, Tahoma, Arial; font-size:28px; font-style:italic;"><?php echo $webi_topic[$page]; ?></span>
    </td>
  </tr>
  <tr>

    <td width="129" align="center" valign="top" style="line-height:18px; padding:10px; width:125px;">&nbsp;</td>


    <td  <?php if($is_affiliate!=1 || $affiliate_info!=1 || $affiliate_opt!=1){ echo 'colspan="2"'; }?> height="125" width="125" align="center" valign="center" style="line-height:18px; "><img width="100" height="100" style="margin-top:0px; border:3px solid #ccc;" src="webinar-files/<?php echo renamefilereg($get_presenter_pic[$page],$memberid); ?>"  alt="presenter"></td>

    <?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1){ ?>
    <td height="125" width="125" align="center" valign="center" style="line-height:18px; "><img width="100" height="100" style="margin-top:0px; border:3px solid #ccc;" src="<?php echo 'http://eWebinars.com/'.$memberid.'/'.$a_affiliate_picture.''; ?>" alt="host"></td>
    <?php } ?>
    <td width="445" colspan="2" rowspan="2" align="left" valign="top" style="line-height:18px; padding:10px;">

<p style="margin-top:10px;"><span style="font-size:16px; font-weight:bold; color:#FF9;">- Presenter: </span><?php echo $webi_presenter[$page]; ?></p>
 <?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1){ ?>
<p style="margin-top:10px;"><span style="font-size:16px; font-weight:bold; color:#FF9;">- Host: </span><?php echo  $a_affiliate_name; ?></p>
<?php } ?>
<p style="margin-top:10px;"><span style="font-size:16px; font-weight:bold; color:#FF9;">- Date & Time: </span><?php echo date("l, jS F Y",strtotime($webidate)); ?> at <?php echo get12hr2($webitime).' '.makeoffsetcity($webischedopt,$webi_tz);?></p>

    </td>

  </tr>
  <tr>
    <td align="center" valign="top" style="line-height:18px; padding:0px; "></td>
    <td  <?php if($is_affiliate!=1 || $affiliate_info!=1 || $affiliate_opt!=1){ echo 'colspan="2"'; }?> align="center" valign="top" style="line-height:18px; padding:0px;">
    <span style=" font-size:10px; color:#CCC; text-align:center"><?php echo $webi_presenter[$page]; ?></span>
    </td>
    <?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1){ ?>
    <td  align="center" valign="top" style="line-height:18px; padding:0px;">
    <span style=" font-size:10px; color:#CCC; text-align:center"><?php echo  $a_affiliate_name; ?></span>
    </td>
    <?php } ?>
  </tr>
</table>

<?php } ?>

<div style="width:100%; padding-top: 5px; border-top: 5px solid #cccccc; padding-bottom: 5px; background-color: #000000; margin-top:10px;">


<div id="container">

            <h2 class="subtitle"><span id="headertitle">This Webinar Will Start Soon...</span></h2>

        <!-- Countdown dashboard start -->
        <div id="countdown_dashboard">
            <div class="dash weeks_dash">
                <span class="dash_title">weeks</span>
                <div class="digit">0</div>
                <div class="digit">0</div>
            </div>

            <div class="dash days_dash">
                <span class="dash_title">days</span>
                <div class="digit">0</div>
                <div class="digit">0</div>
            </div>

            <div class="dash hours_dash">
                <span class="dash_title">hours</span>
                <div class="digit">0</div>
                <div class="digit">0</div>
            </div>

            <div class="dash minutes_dash">
                <span class="dash_title">minutes</span>
                <div class="digit">0</div>
                <div class="digit">0</div>
            </div>

            <div class="dash seconds_dash">
                <span class="dash_title">seconds</span>
                <div class="digit">0</div>
                <div class="digit">0</div>
            </div>

        </div>
        <!-- Countdown dashboard end -->
      <div class="subscribe"></div>

        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function() {
                $('#countdown_dashboard').countDown({

                targetDate: {
                        'day':      <?php echo $dd; ?>,
                        'month':    <?php echo $mm; ?>,
                        'year':     <?php echo $yy; ?>,
                        'hour':     <?php echo $webitime; ?>,
                        'min':      <?php echo $min; ?>,
                        'sec':      <?php echo $sec; ?>
                    }


                });


        setInterval("location.reload(true)", 300000);


            });
        </script>


</div>
</div>

<div style="background-color:#333333; text-align:center; width:100%; height:295px; margin: auto; padding-top:20px; padding-bottom:20px; border-top: 5px solid #ccc; ">


<?php if($cd_vid != 0){ ?>
<script type="text/javascript" src="js/jwplayer.js"></script>
<center><table width="480" align="center" border="0"><tr><td><div id="thePlayer" style="width: 480px;margin:0 auto;"></div></td></tr></table></center>
<script type="text/javascript">
    jwplayer("thePlayer").setup({
        flashplayer: "js/player.swf",
        file: "<?php echo $cd_vidurl;?>",
        <?php if(!isset($_COOKIE['lcdvidplay'])){ setcookie("lcdvidplay", "1", time()+3600,'/'); ?>
        autostart: "true",
        <?php }else{ ?>
        autostart: "false",
        <?php } ?>
        width: '480',
        height: '295',


    controlbar: "bottom"
    });


</script>
<?php }?>

</div>
</div>


</body>
</html>