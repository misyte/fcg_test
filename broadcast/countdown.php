<?php
include('./split-register.php');


/** show if the user view the page in a mobile device */
if(0) {
    echo '<div style="width:100%; margin:0;height:50px;padding-top:10px;background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.8);text-align: center; font-family:Tahoma, Arial, Helvetica, sans-serif;color: #fff;">This webinar is not compatible with mobile devices, so please log in from your desktop or laptop computer</div>';
    exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_600.font.js"></script>
<script type="text/javascript">
<?php if($langtype == 'en') { ?>

    Cufon.replace('h1', { fontFamily: 'Myriad Pro', textShadow: '1px 1px rgba(0, 0, 0, 0.2)' });
<?php } ?>
</script>
<style type="text/css">
body {  margin:0;   background: #FFF url(webinar-files/bg2.jpg) repeat-x !important;    font: normal 16px Arial !important; color: #ffffff;}
/*--container--*/
#wrapper{   margin:0 auto;  padding:0;  width:900px;}
#header{    margin:0;   padding:0;  width:900px;    height:75px;    text-align:center;}
#header h1{ padding-top:15px;   text-align:center;  color:#C00; font-size:50px; font-style:italic;}
#webinarinfo{   margin:0 auto;  padding:0px 0px 0px 0px;    width:900px;    height:130px;   text-align:center;  margin-top:135px;}
#bottom{    margin:0;   padding:0;  width:900px;    height:24px;}
/*--text--*/
p{  margin:0;   padding:8px 0;}
h1{ margin:0;   padding:8px 0; border:none !important; line-height: 38px;}
h2{ margin:0;   padding:8px 0;}
h3{ margin:0;   padding:8px 0;}
h4{ margin:0;   padding:8px 0;}
h5{ margin:0;   padding:8px 0;}
h6{ margin:0;   padding:8px 0;}
/*--video--*/
.video{ width:640px;    margin:0 auto;  background:url(webinar-files/bottom.jpg) no-repeat;}
/*--foot information--*/
.footinfo{  margin:10px auto;   padding:10px 0; width:640px;    border-top:1px solid #cdcdcd;   color:#666; font:normal 10px verdana. helvetica, arial, sans-serif; text-align:center;}
</style>
</head>

<body>
<?php

$resdate = getdate(strtotime($webidate));

$dd = $resdate[mday];
$mm = $resdate[mon];
$yy = $resdate[year];

$mytz = base64_encode($tz);


?>
<link href="broadcast/dark.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="js/countdownjs.php?querystring=<?php echo urlencode($_SERVER["QUERY_STRING"]);?>&mytz=<?php echo $mytz;?>"></script>


<div id="wrapper">
    <div id="header">
    <h1><?php echo $lang['COUNTDOWN_TXT_H1'];?></h1>
    <!-- Countdown dashboard start -->
    <div id="countdown_dashboard">
        <div class="dash weeks_dash">
            <span class="dash_title">weeks</span>
            <div class="digit">0</div>
            <div class="digit">0</div>
        </div>

        <div class="dash days_dash">
            <span class="dash_title">days</span>
            <div class="digit">0</div>
            <div class="digit">0</div>
        </div>

        <div class="dash hours_dash">
            <span class="dash_title">hours</span>
            <div class="digit">0</div>
            <div class="digit">0</div>
        </div>

        <div class="dash minutes_dash">
            <span class="dash_title">minutes</span>
            <div class="digit">0</div>
            <div class="digit">0</div>
        </div>

        <div class="dash seconds_dash">
            <span class="dash_title">seconds</span>
            <div class="digit">0</div>
            <div class="digit">0</div>
        </div>

    </div>
    <!-- Countdown dashboard end -->
    </div>

</div>

<!-- Buy Now/Add To Cart Button Start Here -->
<div id="webinarinfo">
<?php
/**
 * new presenter's info
 */

$get_npresenter =  unserialize(stripslashes($npresenter));
$get_npresenter_pic = unserialize(stripslashes($npresenter_pic));
$get_intro_box = unserialize(stripslashes($intro_box));
$webi_topic=unserialize(stripslashes($topic));
$webi_presenter=unserialize(stripslashes($presenter));
$get_presenter_pic = unserialize(stripslashes($presenter_pic));
$get_regtpl = unserialize(stripslashes($regtpl));



                        include 'loadinfo.php';
                        $is_affiliate=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=q');
                        $a_affiliate_name=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=n');
                        $a_affiliate_picture=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=i');

                    ?>
<table width="800" border="0" cellspacing="0" cellpadding="5">
  <tr>

    <td  align="center" valign="middle" width="124">
        <?php

        if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1 || ($get_npresenter[$page] != '')){
            if(trim($get_npresenter[$page]) != '') {
                $aff_new_presenter_pic = 'webinar-files/'.renamefilereg($get_npresenter_pic[$page],$memberid);
                $aff_new_presenter_name = $get_npresenter[$page];
            } else {
                $aff_new_presenter_pic  = 'http://eWebinars.com/'.$memberid.'/'.$a_affiliate_picture;
                $aff_new_presenter_name =  $a_affiliate_name;
            }

        ?>



    <div style="background:url('webinar-files/box-pic.gif') no-repeat left top; float:left; width:100px; height:123px; padding:7px; margin:0px 10px 10px 10px;">
    <img width="100" height="100" src="<?php echo $aff_new_presenter_pic; ?>">
    <p style="font-size: 12px; line-height: 10px; color:#5c5c5c; margin: 0px 0 0; font-family:Arial, Helvetica, sans-serif; text-align: center;font-size:10px;"><?php echo  $aff_new_presenter_name; ?></p>
    </div>
<?php } ?>
     </td>

    <?php if($get_presenter_pic[$page]!="" && $get_intro_box[$page]==1){ ?>
    <td  align="center" valign="middle" width="124">

       <div style="background:url('webinar-files/box-pic.gif') no-repeat left top; float:left; width:100px; height:123px; padding:7px; margin:0px 10px 10px 10px;">
    <img width="100" height="100" src="webinar-files/<?php echo renamefilereg($get_presenter_pic[$page],$memberid); ?>">
    <p style="font-size: 12px; line-height: 10px; color:#5c5c5c; margin: 0px 0 0; font-family:Arial, Helvetica, sans-serif; text-align: center;font-size:10px;"><?php echo $webi_presenter[$page]; ?></p>
    </div>
    </td>
    <?php } ?>

   <td align="left" valign="top">
    <span style="color:#FF9; font-style:italic; font-weight:bold;"><?php echo $webi_topic[$page]; ?></span>
    <br />
    <ul>
    <li style="padding-bottom:5px;"><b><?php echo $lang['COUNTDOWN_TXT_PRESENTER'];?>:</b> <span style="color:#E3E3E3;"><?php echo $webi_presenter[$page]; ?> <?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1 || ($get_npresenter[$page] != '')){ ?>   and <?php echo  $aff_new_presenter_name; ?><?php } ?></span></li>
    <li style="padding-bottom:5px;"><b><?php echo $lang['COUNTDOWN_TXT_DATE'];?>:</b> <span style="color:#E3E3E3;">
    <?php
    /** convert date language **/
    if(!$langmonths) {
        echo $datenowval =  date("l, jS F Y",strtotime($webidate));
    }
    elseif(is_array($langmonths) && is_array($langdays)) {
        echo $datenowval = convertdatelang($webidate,'cd');

    } else {
        echo $datenowval =  date("l, jS F Y",strtotime($webidate));

    }


    ?>

    </span></li>
    <li style="padding-bottom:5px;"><b><?php echo $lang['COUNTDOWN_TXT_TIME'];?>:</b> <span style="color:#E3E3E3;"><?php echo get12hr2($webitime).' '.makeoffsetcity($webischedopt,$webi_tz);?></span></li>
    </ul>
    </td>
  </tr>
</table>

<center>
<?php if($cd_vid != 0){ ?>
<div style="margin-top:40px; border:10px solid #EBEBEB; width:480px; height:270px;">
<!-- VIDEO EMBED CODE START HERE (JUST PASTE IT BELOW THIS LINE) -->

<script type="text/javascript" src="js/jwplayer.js"></script>
<div id="thePlayer" style="margin:0 auto;"></div></td></tr></table>
<script type="text/javascript">
    jwplayer("thePlayer").setup({
        flashplayer: "js/player2.swf",
        file: "<?php echo $cd_vidurl;?>",
        <?php if(!isset($_COOKIE['lcdvidplay'])){ setcookie("lcdvidplay", "1", time()+3600,'/'); ?>
        autostart: "true",
        <?php }else{ ?>
        autostart: "false",
        <?php } ?>
        width: '480',
        height: '270',


    controlbar: "over"
    });


</script>

<!-- VIDEO EMBED CODE END HERE -->
</div>
<?php }?>
</center>

</div>



<!-- Buy Now/Add To Cart Button End Here -->


<script language="javascript" type="text/javascript">
            jQuery(document).ready(function() {
                $('#countdown_dashboard').countDown({

                targetDate: {
                        'day':      <?php echo $dd; ?>,
                        'month':    <?php echo $mm; ?>,
                        'year':     <?php echo $yy; ?>,
                        'hour':     <?php echo $webitime; ?>,
                        'min':      <?php echo $min; ?>,
                        'sec':      <?php echo $sec; ?>
                    }


                });


        setInterval("location.reload(true)", 300000);


            });
        </script>
</body>
</html>