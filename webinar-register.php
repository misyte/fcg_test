<?php session_start();  ob_start();
error_reporting(0);
header('P3P: CP="NOI ADM DEV COM NAV OUR STP"');

include_once 'functions.php';
include('loadinfo.php');

/** force url to load in http://domain.com  without www  **/
if (substr($_SERVER['HTTP_HOST'],0,3) == 'www' && substr($domainname,0,10) != 'http://www') {
    $shost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
    $actual_link = full_url();
    $actual_url = parse_url($actual_link);
    $ishttp = strtolower($actual_url['scheme']);

    header('HTTP/1.1 301 Moved Permanently');
    if( $ishttp == 'https') {
        header('Location: https://'.$shost.$_SERVER['REQUEST_URI']);
    } else {
        header('Location: http://'.$shost.$_SERVER['REQUEST_URI']);
    }
}/** end **/



include('split-register.php');
include('emailersql.php');

error_reporting(0);
header('p3p: CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"');
$domain_name = $domainname.'/'.$webifolder.'/';
error_reporting(0);
$pagetype = 1;

/**  TEMPLATE VARS **/
$webioffset = '0';
$get_regtpl = unserialize(stripslashes($regtpl));
$get_presenter =  unserialize(stripslashes($presenter));
$get_topic =  unserialize(stripslashes($topic));
$get_regtplcolor = unserialize(stripslashes($regtplcolor));
$get_reginsertopt = unserialize(stripslashes($reginsert_opt));
$get_regheader = unserialize(stripslashes($regheader));
$get_regtxtheader1 = unserialize(stripslashes($reg_textheader1));
$get_regtxtheader2 = unserialize(stripslashes($reg_textheader2));
$get_headerimg = unserialize(stripslashes($reg_headerimg));
$get_regimg = unserialize(stripslashes($regimg));
$get_regvid= unserialize(stripslashes($regvid));
$get_regvidauto = unserialize(stripslashes($regvidauto));
$get_headerline = unserialize(stripslashes(base64_decode($headerline)));
$get_webidesc = unserialize(stripslashes(base64_decode($webidesc)));
$get_sms = unserialize(stripslashes($sms));
$get_voice = unserialize(stripslashes($voice));
$get_intro_box = unserialize(stripslashes($intro_box));
$get_presenter_pic = unserialize(stripslashes($presenter_pic));
$webi_topic=unserialize(stripslashes($topic));
$webi_presenter=unserialize(stripslashes($presenter));

$get_splash=unserialize(stripslashes($splash));
$get_popup_message=unserialize(stripslashes($popup_message));
$get_audio_file=unserialize(stripslashes($audio_file));
$get_builtin_audio=unserialize(stripslashes($builtin_audio));
$get_own_audio=unserialize(stripslashes($own_audio));


$get_vibrantvid=unserialize(stripslashes($vibrantvid));
$get_vibrantarrow=unserialize(stripslashes($vibrantarrow));
$get_vibrantdesc=unserialize(stripslashes($vibrantdesc));


if(isset($_GET['landingpage'])){
    $is_rp=curlURL("http://eWebinars.com/rp/getdetails.php?member_id=".$memberid."&rp_name=".$_GET['landingpage']."&q=0");
}
else $is_rp=0;

include 'languages/'.$langtype.'/lang.'.$langtype.'.php';




//creating cookie of IS affiliatid
$inf_affiliateid = $_GET['affiliate'];
setcookie('inf_affiliateid',$inf_affiliateid,time()+31622400);


if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 2) {
    $tpltype = 'software';
}
if($get_regtpl[$page] == 1 ) {
    $tpltype = 'squeeze';
}

if($get_regvidauto[$page] == 'checkbox') {
    $get_regvidauto[$page] = 'true';
}


if($get_regvidauto[$page] == 'checkbox') {
    $get_regvidauto[$page] = 'true';
}


if($affiliate_opt==1)
{
    if(trim($_GET['affiliate'])=="" && !isset($_COOKIE['affiliate_id'])) $affiliate_id='No-affiliate';
    elseif(trim($_GET['affiliate'])!="" && !isset($_COOKIE['affiliate_id'])) $affiliate_id=trim($_GET['affiliate']);


    if(trim($_GET['affiliate'])=="" && isset($_COOKIE['affiliate_id']))
    {
        $affiliate_id=$_COOKIE['affiliate_id'];
    }
    elseif(trim($_GET['affiliate'])!="" && isset($_COOKIE['affiliate_id']))
        $affiliate_id=trim($_GET['affiliate']);


    setcookie('affiliate_id',$affiliate_id,time()+31622400);


}
/** REGISTER PAGE TEMPLATE OPTION **/
if(isset($_GET['landingpage'])){
    $is_rp=curlURL("http://eWebinars.com/rp/getdetails.php?member_id=".$memberid."&rp_name=".$_GET['landingpage']."&q=0");
}
else $is_rp=0;

if($is_rp==1)
{
    $rp_data=curlURL("http://eWebinars.com/rp/getdetails.php?member_id=".$memberid."&rp_name=".$_GET['landingpage']."&q=1");
    $datas=array();
    $data1=explode("^&*()",$rp_data);
    foreach($data1 as $val1)
    {
        $data2=explode("!@#$%",$val1);
        if(sizeof($data2)==2)
            $datas[$data2[0]]=$data2[1];
    }


    $landingpage=$_GET['landingpage'];
    $rp_id = $datas['rp_id'];
    $rp_name=$datas['rp_name'];
    $rp_regtpl=$datas['rp_regtpl'];
    $rp_regtplcolor=$datas['rp_regtplcolor'];
    $rp_topic=$datas['rp_topic'];
    $rp_presenter=$datas['rp_presenter'];
    $rp_npresenter=$datas['rp_npresenter'];
    $rp_intro_box=$datas['rp_intro_box'];
    $rp_presenter_pic=$datas['rp_presenter_pic'];
    $rp_npresenter_pic=$datas['rp_npresenter_pic'];
    $rp_regheader=$datas['rp_regheader'];
    $rp_headerimg=$datas['rp_headerimg'];
    $rp_regtextheader1=$datas['rp_regtextheader1'];
    $rp_regtextheader2=$datas['rp_regtextheader2'];
    $rp_headerline=$datas['rp_headerline'];
    $rp_reginsert_opt=$datas['rp_reginsert_opt'];
    $rp_regimg=$datas['rp_regimg'];
    $rp_regvid=$datas['rp_regvid'];
    $rp_regvidauto=$datas['rp_regvidauto'];
    $rp_webidesc=$datas['rp_webidesc'];
    $rp_htmldesign=$datas['rp_htmldesign'];
    $rp_sms=$datas['rp_sms'];
    $rp_token=$datas['rp_token'];
    $rp_sms_msg=$datas['rp_sms_msg'];
    $rp_voice=$datas['rp_voice'];
    $rp_voice_token=$datas['rp_voice_token'];
    $rp_voice_audio=$datas['rp_voice_audio'];

    $rp_splash=$datas['rp_splash'];
    $rp_popup_message=$datas['rp_popup_message'];
    $rp_audio_file=$datas['rp_audio_file'];
    $rp_builtin_audio=$datas['rp_builtin_audio'];
    $rp_own_audio=$datas['rp_own_audio'];
    $rp_splash_html=$datas['rp_splash_html'];

    $rp_vibrantvid=$datas['rp_vibrantvid'];
        $rp_vibrantarrow=$datas['rp_vibrantarrow'];
        $rp_vibrantdesc=$datas['rp_vibrantdesc'];

    if($rp_regtpl==0 && $_GET['auto'] != 1)
    {
        include 'rp_software/index.php';
    }
    elseif($rp_regtpl==1 && $_GET['auto'] != 1)
    {
        include 'rp_squeeze/index.php';
    }
    elseif($rp_regtpl==2 && $_GET['auto'] != 1)
    {
        include 'rp_userdesign/index.php';
    }
    elseif($rp_regtpl==3 && $_GET['auto'] != 1)
    {

        include 'rp_vibrant/index.php';

    }
    elseif($rp_regtpl==4 && $_GET['auto'] != 1)
    {

        include 'rp_stylish/index.php';

    }
    elseif($rp_regtpl==5 && $_GET['auto'] != 1)
    {

        include 'rp_panorama/index.php';

    }
    elseif($rp_regtpl==6 && $_GET['auto'] != 1)
    {

        include 'rp_facebook/index.php';

    }
    elseif($rp_regtpl==7 && $_GET['auto'] != 1)
    {

        include 'rp_softwarelook/index.php';

    }
    elseif($rp_regtpl==8 && $_GET['auto'] != 1)
    {

        include 'rp_ethereal/index.php';

    }
}
else{
    if( $_GET['embed']==1 && isset($_GET['embed']) && $is_iframe==1)
    {
        //vertical
        if($iframe_type)
            include('embed_registration/vertical/index2.php');
        else
            include('embed_registration/square/index2.php');
    }
    else{
        if($get_regtpl[$page] == 0 && $_GET['auto'] != 1){
            include('software/index.php');
        }
        if($get_regtpl[$page] == 1 && $_GET['auto'] != 1){
            include('squeeze/index.php');
        }
        if($get_regtpl[$page] == 2 && $_GET['auto'] != 1){
            include 'userdesign/index.php';

        }
        if($get_regtpl[$page] == 3 && $_GET['auto'] != 1){
        include 'vibrant/index.php';

        }
        if($get_regtpl[$page] == 4 && $_GET['auto'] != 1){
        include 'stylish/index.php';

        }
        if($get_regtpl[$page] == 5 && $_GET['auto'] != 1){
        include 'panorama/index.php';

        }
        if($get_regtpl[$page] == 6 && $_GET['auto'] != 1){
        include 'facebook/index.php';

        }
        if($get_regtpl[$page] == 7 && $_GET['auto'] != 1){
        include 'softwarelook/index.php';

        }
        if($get_regtpl[$page] == 8 && $_GET['auto'] != 1){
        include 'ethereal/index.php';

        }
    }
}

if($_GET['trackingID1']!=""){
    $trackingID1=$_GET['trackingID1'];
    setcookie('trackingID1',$_GET['trackingID1'],time()+31622400);
}
else{
    $trackingID1="blank";
    setcookie('trackingID1',"blank",time()+31622400);
}

if($_GET['trackingID2']!=""){
    $trackingID2=$_GET['trackingID2'];
    setcookie('trackingID2',$_GET['trackingID2'],time()+31622400);
}
else{
    $trackingID2="blank";
    setcookie('trackingID2',"blank",time()+31622400);
}


/*** UNIQUE VISITOR TRACKERS PART HERE [SPLIT TRACKER FOR UNIQUE VISITOR NOT INCLUDED HERE ] ***/
include 'ebs-tracker/register-tracker.php';
/*** END TRACKERS PART ***/
/** Office autopilot**/
if($oap_appid!="" && $oap_apikey!=""){
$oapresult=curlURL($domainname."/$webifolder/oa_functions.php?a=$oap_appid&k=$oap_apikey&c=addFields&webi=".urlencode($webiname));

}
?>