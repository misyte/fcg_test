<?php

session_start();
ob_start();

include 'config.php';
include 'split-register.php';
include 'mailer.php';
include 'languages/'.$langtype.'/lang.'.$langtype.'.php';

$webid      = trim(urldecode($_GET['webid']));
$memberid   = trim(urldecode($_GET['memberid']));
$name       = trim(urldecode($_GET['name']));
$email      = trim(urldecode($_GET['email']));
$reg_date   = trim(urldecode($_GET['date']));
$webtime    = trim(urldecode($_GET['time']));
$phonealert     = trim(urldecode($_GET['phonealert']));
$cc         = trim($_GET['cc']);
$phone      = trim(urldecode($_GET['phone']));
$af         = trim(urldecode($_GET['aff']));
$tz         = trim(urldecode($_GET['timezone']));


if($phonealert == 'false') {
    $cc = '';
    $phone = '';
}

$reg_date = date('Y-M-d',strtotime($reg_date));


if($webischedopt == 2 && $_GET['timezone'] == '') {

echo '<script language="javascript" src="js/detect_timezone.js"></script>

<script>
  var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == "undefined") {
        response_text = "UTC";
    }
    else {
        response_text = tz_info.timezone.display();
    }




window.location.href = "usertz.php?timezone=" + response_text + "&url='.base64_encode(full_url()).'";


</script>';

    exit;
}




/** double checking form data if JS validation fails  **/
    if($name == '' || $email == '' || $webtime == '' || $reg_date == '') {
        exit;
    }


/** for immediate webinar **/
    $is_rn = '0';
    setcookie('rn_date',$reg_date,time()+31622400);
    setcookie('rn_time',$webtime,time()+31622400);

    if($webtime == 'rn' && $reg_date == 'rn') {
        if($webischedopt == 1) {
            $rntz = $webi_tz;
        } else {
            $rntz = $tz;
        }

        $webtime = getrntime($rntz);
        $reg_date = getrndate($rntz);
        $gdif = getrndif($rntz,$reg_date,$webtime);
        setcookie('rn_dif',$gdif,time()+31622400);
        $is_rn = 1;

    }


    /** strtotime for tz admin datetime equivalent **/
        $tz_admin_dateset = strtotime_date($reg_date." ".$webtime.":00:00",$webi_tz);
        /** strtotime for tz local datetime equivalent **/
        $tz_local_dateset = strtotime_date($reg_date." ".$webtime.":00:00",$tz);

        if($is_rn == 1) {

            $tz_admin_dateset = strtotime_date($reg_date." ".convertTime($webtime),$webi_tz);
            /** strtotime for tz local datetime equivalent **/
            $tz_local_dateset = strtotime_date($reg_date." ".convertTime($webtime),$tz);
        }


    /** also used in the ie cookie inside iframe issue **/
    setcookie('user_name',$name,time()+31622400);
    setcookie('user_date',$reg_date,time()+31622400);
    setcookie('user_time',$webtime,time()+31622400);
    setcookie('user_email',$email,time()+31622400);


    $sms=unserialize(stripslashes($sms));
    if($sms[$page]==1) $smsnumber=trim($_GET["phone"]);

        //creating sessions to be use to TY page
    $_SESSION['wname'] = $_POST['name'];


    $_SESSION['wemail'] = $email;
    $_SESSION['wdate'] = $reg_date;
    $_SESSION['wtime'] = $webtime;
    $_SESSION['wlive'] = $domainname."/".$webifolder."/webinar-live.php?user=".base64_encode($name)."&email=".base64_encode($email)."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($webtime)."&webidate=".base64_encode($reg_date)."&tz=".base64_encode($tz)."";
    $_SESSION['wreplay'] = $domainname."/".$webifolder."/webinar-replay.php?user=".base64_encode($name)."&email=".base64_encode($email)."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($webtime)."&webidate=".base64_encode($reg_date)."&tz=".base64_encode($tz)."";

    $orig_url_live = $domainname."/".$webifolder."/webinar-live.php?user=".base64_encode($name)."&email=".base64_encode($email)."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($webtime)."&webidate=".base64_encode($reg_date)."&tz=".base64_encode($tz)."";
    $orig_url_replay = $domainname."/".$webifolder."/webinar-replay.php?user=".base64_encode($name)."&email=".base64_encode($email)."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($webtime)."&webidate=".base64_encode($reg_date)."&tz=".base64_encode($tz)."";

    if($hostingtype==1) $unsubscribe=$domainname.'/'.$webifolder.'/unsubscribe.php?m='.base64_encode($memberid).'&w='.base64_encode($webid).'&e='.base64_encode($email);
    else $unsubscribe=$domainname.'/'.$memberid.'/'.$webid.'/unsubscribe.php?m='.base64_encode($memberid).'&w='.base64_encode($webid).'&e='.base64_encode($email);

    $webi_presenter=unserialize(stripslashes($presenter));
    $get_npresenter =  unserialize(stripslashes($npresenter));
    if($get_npresenter[$page] != '') {
        $presentersname = $webi_presenter[$page].' & '.$get_npresenter[$page];
    }else{
        $presentersname = $webi_presenter[$page];
    }

    $get_presenter =  unserialize(stripslashes($presenter));
    $get_admin_email =  unserialize(stripslashes($admin_email));
    $webi_topic=unserialize(stripslashes($topic));

     $msg_0 = stripslashes($welcome_message);
    $msg_0 = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $msg_0);
    $msg_0 = str_replace("#WEBINAR_PRESENTER#",$presentersname, $msg_0);
    if($is_rn == 1) {
        $msg_0 = str_replace("#WEBINAR_DATE#",$lang['REG_FORM_TXT_RN_2'], $msg_0);
        $msg_0 = str_replace("#WEBINAR_TIME#",$lang['REG_FORM_TXT_RN_2'], $msg_0);
    }else{
        $msg_0 = str_replace("#WEBINAR_DATE#",convertdatelang($reg_date,'wc'), $msg_0);
        $msg_0 = str_replace("#WEBINAR_TIME#",get12hr($webtime).' '.makeoffsetcity($webischedopt,$webi_tz), $msg_0);
    }

    $msg_0 = str_replace("#USER_NAME#",$name, $msg_0);
    $msg_0 = str_replace("#USER_EMAIL#",$email, $msg_0);
    $msg_0 = str_replace("#AFFILIATE#",$af, $msg_0);



    $_SESSION['wtitle']=$webi_topic[$page];
    $_SESSION['whost']=$webi_presenter[$page];
    $to = $email;
    $subject_0 = $welcome_subject;
    $subject =  stripslashes($subject_0);
    $subject = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $subject);
    $subject = str_replace("#WEBINAR_PRESENTER#",$presentersname, $subject);
    if($is_rn == 1) {
        $subject = str_replace("#WEBINAR_DATE#",$lang['REG_FORM_TXT_RN_2'], $subject);
        $subject = str_replace("#WEBINAR_TIME#",$lang['REG_FORM_TXT_RN_2'], $subject);
    }else{
        $subject = str_replace("#WEBINAR_DATE#",convertdatelang($reg_date,'wc'), $subject);
        $subject = str_replace("#WEBINAR_TIME#",get12hr($webtime).' '.makeoffsetcity($webischedopt,$webi_tz), $subject);
    }
    $subject = str_replace("#USER_NAME#",$name, $subject);
    $subject = str_replace("#USER_EMAIL#",$email, $subject);
    $subject = str_replace("#AFFILIATE#",$af, $subject);

    $message = stripslashes($msg_0)."<br><div style='margin-top:50px;font-size:8px;'><a href='".$unsubscribe."'>Click to stop receiving notifications for this webinar</a></div>";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Additional headers
    $headers .= 'From: '.$from_name.' <do_not_reply@eWebinars.com>' . "\r\n" .
   'Reply-To: '.$from_name.' <do_not_reply@eWebinars.com>' . "\r\n" .
   'Return-Path: '.$from_name.' <do_not_reply@eWebinars.com>' . "\r\n";

    $_SESSION['autoresponder_name']=$name;
    $_SESSION['autoresponder_email']=$email;
    $ip = $_SERVER['REMOTE_ADDR'];
    $regdate = date('Y-M-d');


if($autoresponderopt==0 || $autoresponderopt==2 || $autoresponderopt==1) {
        if($sms==0) $sql="INSERT INTO users (memberid,webid,name,email,webtime,dateset,tz_admin_dateset,tz_local_dateset,tz_opt,tz_admin,tz,ip_address,registration_date,affid,rn,registered) VALUES (".$memberid.",'".$webid."','".addslashes($name)."','$email','$webtime','$reg_date','$tz_admin_dateset','$tz_local_dateset','$webischedopt','$webi_tz','$tz','$ip','$regdate','".$af."','".$is_rn."',1)";
        else $sql="INSERT INTO users (memberid,webid,name,email,webtime,dateset,tz_admin_dateset,tz_local_dateset,country_code,cellphone,phone_number,tz_opt,tz_admin,tz,ip_address,registration_date,affid,rn,registered) VALUES (".$memberid.",'".$webid."','".addslashes($name)."','$email','$webtime','$reg_date','$tz_admin_dateset','$tz_local_dateset','".$cc."','".substr($smsnumber,strlen($cc),(strlen($smsnumber) - strlen($cc)))."','$smsnumber','$webischedopt','$webi_tz','$tz','$ip','$regdate','".$af."','".$is_rn."',1)";

        if($webtime=="Select your date first"){
            exit;
        }
        else{
            //if (!mysql_query($sql,$conn)){ die('Error: ' . mysql_error()); }

            //$uid =  mysql_insert_id();
            $testsql="SELECT * FROM users WHERE email = '$email' AND dateset= '$reg_date' AND webtime = '$webtime'";
            $testresult=mysql_query($testsql,$conn) or die ('Error: ' . mysql_error());
            if($testrow=mysql_fetch_assoc($testresult)){
                $uid =0;

            }
            else{
                if (!mysql_query($sql,$conn)){ die('Error: ' . mysql_error()); }
                $uid =  mysql_insert_id();
            }
        }

                /** short links db insert **/
        if($uid > 0) {

            $su = $domainname.'/'.$webifolder.'/go/index.php?'.$webid;
            $query="INSERT INTO links (webid,user_id,short_url,orig_url,type) VALUES ('".$webid."','".$uid."','".$su."','".$orig_url_live."','live')";
            if (!mysql_query($query,$conn)){ die('Error: ' . mysql_error()); }
            $nxtid1 =  mysql_insert_id();

            $query="INSERT INTO links (webid,user_id,short_url,orig_url,type) VALUES ('".$webid."','".$uid."','".$su."','".$orig_url_replay."','replay')";
            if (!mysql_query($query,$conn)){ die('Error: ' . mysql_error()); }
            $nxtid2 =  mysql_insert_id();

            $su1 = $domainname.'/'.$webifolder.'/go/index.php?'.$webid.'-'.$nxtid1;
            $result= mysql_query("UPDATE links SET  short_url = '".$su1."' WHERE link_id = '".$nxtid1."'") or die('Error: ' . mysql_error());
            $su2 = $domainname.'/'.$webifolder.'/go/index.php?'.$webid.'-'.$nxtid2;
            $result= mysql_query("UPDATE links SET  short_url = '".$su2."' WHERE link_id = '".$nxtid2."'") or die('Error: ' . mysql_error());

        }
        else
        {
            $uid=   $testrow['userid'];
            $query=mysql_query("SELECT * FROM links WHERE webid='$webid' AND type='live' AND user_id=".$testrow['userid']);
            if ($roww=mysql_fetch_assoc($query)){
                $su1 = $roww['short_url'];
            }
            $query=mysql_query("SELECT * FROM links WHERE webid='$webid' AND type='replay' AND user_id=".$testrow['userid']);
            if ($roww=mysql_fetch_assoc($query)){
                $su2 = $roww['short_url'];
            }
        }
        /** short links end **/

        /** Start Infusionsoft Modifications **/
            $sc_webdate = date("l, F jS",strtotime($reg_date));
            $sc_webtime = get12hr($webtime).' '.makeoffsetcity($webischedopt,$webi_tz);

            $arrinf = array('registered', $email, $name,$sc_webdate,$sc_webtime,$webi_topic[$page],$webi_presenter[$page],$su1,$su2,$name,$email);
            $_SESSION['actionsetreg'] = implode("||", $arrinf);
       /** End Modifications **/

       /** Office autopilot **/
        if($oap_appid!="" && $oap_apikey!=""){
        $webi_topic=unserialize(stripslashes($topic));
        $webi_presenter=unserialize(stripslashes($presenter));
        $oapresult=curlURL($domainname."/$webifolder/oa_functions.php?a=$oap_appid&k=$oap_apikey&webi=".urlencode($webiname)."&c=addContact&userid=$uid&name=".urlencode($name)."&email=$email&webidate=$reg_date&webitime=$webtime&webipresenter=".urlencode($webi_presenter[0])."&webitopic=".urlencode($webi_topic[0])."&livelink=".urlencode($su1)."&replaylink=".urlencode($su2));
        }
        /** Office autopilot**/

       if($autoresponderopt==0 || $autoresponderopt==1 || $autoresponderopt==2) {

            $su1 = $su1 ? $su1 : $_SESSION['wlive'];
            $su2 = $su2 ? $su2 : $_SESSION['wreplay'];
            $subject = str_replace("#WEBINAR_LINK#",$su1, $subject);
            $subject = str_replace("#WEBINAR_REPLAY#",$su2, $subject);
            $message = str_replace("#WEBINAR_LINK#",$su1 , $message);
            $message = str_replace("#WEBINAR_REPLAY#",$su2 , $message);

            $_SESSION['slink_live'] = $su1;
            $_SESSION['slink_replay'] = $su2;

            if($is_welcome == 1) {

                /** send welcome notification  **/
                if($autoresponderopt==0) {
                // if own autoresponder, use builtin notification

                    if($emailtype == 1) {
                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                            $mailsuccess = true;
                        }
                    }
                    elseif($emailtype == 2) {
                        if(sendmail_smtp($arrsmtp,$to,$subject, $message)) {
                            $mailsuccess = true;
                        }
                    }
                    else {
                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                            $mailsuccess = true;
                        }
                    }
                }
                elseif($autoresponderopt==2 && $autorespondertype==1 && $via_infusion==1 || ($autoresponderopt==1 && $autorespondertype==1)) {
                    /**  writing a file that contains the email template intended for infusionsoft welcome email   **/
                    $_SESSION['regid'] = $uid; // user registrant id
                    $_SESSION['emailtplsubj'] = $subject; // email subj
                    $myFile = 'emailtpl_'.$uid.'.txt';
                    $fh = fopen($myFile, 'w') or die("can't open file");
                    $stringData = $message;
                    fwrite($fh, $stringData);
                    fclose($fh);
                    /** end file writing**/
                } else {
                    if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                        $mailsuccess = true;
                    }
                }
            }
        }
    }
    /*
    echo "wname: ".$_SESSION['wname']."<br>";
    echo "wemail: ".$_SESSION['wemail']."<br>";
    echo "wdate: ".$_SESSION['wdate']."<br>";
    echo "wtime: ".$_SESSION['wtime']."<br>";
    echo "wtitle: ".$_SESSION['wtitle']."<br>";
    echo "whost: ".$_SESSION['whost']."<br>";
    */
    $rregpage=$domainname.'/'.$webifolder."/webinar-register-thankyou.php";
    echo '<script type="text/javascript"> top.window.location = "'.$rregpage.'";</script>';





?>