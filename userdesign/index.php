<?php
/**
 * register page with user's own html code
 */
//$userhtmlcode = unserialize(stripslashes(base64_decode($htmldesign)));
$userhtmlcode = unserialize(base64_decode($htmldesign));
$usercode = $userhtmlcode[$page];
$usercode = str_replace('</HEAD>','</head>',$usercode);


$exitpage='<div id="webinar-register-exit" style="display:none;height:0;"   ></div>
<div id="iframeholder" style="visibility:hidden; overflow:hidden; height:0px;">
<iframe src="webinar-register-exit.php" scrolling="no"  align="middle" frameborder="0" width="100%" id = "resize"  onload = "setIframeHeight( this.id )" style="visibility:hidden;overflow:hidden;"  height="0" name="frame1"></iframe>
</div>

<script type="text/javascript">

function setIframeHeight( iframeId ) /** IMPORTANT: All framed documents *must* have a DOCTYPE applied **/
{
 var ifDoc, ifRef = document.getElementById( iframeId );

 try
 {
  ifDoc = ifRef.contentWindow.document.documentElement;
 }
 catch( e )
 {
  try
  {
   ifDoc = ifRef.contentDocument.documentElement;
  }
  catch(ee)
  {
  }
 }

 if( ifDoc )
 {
  ifRef.height = 1;
  ifRef.height = ifDoc.scrollHeight;

  /* For width resize, enable below.  */

  // ifRef.width = 1;
  // ifRef.width = ifDoc.scrollWidth;
 }
}

</script>

    <div id="exitimg" style="display:none; position:fixed; right:10px; top:10px; z-index:10000"><img id="image" name="image" border="0" src="webinar-files/exitimg.gif" /></div>';

if($get_splash[$page]!=1) $exitpage="";
$regex = '/<body([\w\W]*?)>/i';
preg_match_all($regex,$usercode,$match);
$divcode = '<div id="ajaxloader" style="display:none;background-color: rgba(0, 0, 0, 0.8);     color: #FFFFFF;          font-family: Tahoma,Arial,Helvetica,sans-serif;     height: 50px;     padding-top: 10px;     position: fixed;     text-align: center;     top: 0;     width: 100%;     z-index: 100;"><div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span>Registering To Webinar...</span></div></div><div id="currentPage">';

$usercode = str_replace($match[0][0],$match[0][0].$divcode,$usercode);
$usercode = str_replace('</body>','</div>'.$exitpage.stripslashes($tracker_reg).'</body>'.stripslashes($google_analytics_footer),$usercode);

 // head code setup
 $p = explode("</head>",$usercode);
 if(isset($p[1])) {
echo $p[0];
    include 'userdesign/headcode.php';

    echo '</head>';
    // reg form setup
    $pp = explode("#WEBINAR_REGISTRATION_FORM#",$p[1]);
    if(isset($pp[1])) {
        echo $pp[0];
        include 'userdesign/formcode.php';
        echo $pp[1];
    } else {
        echo $p[1];
    }

// no head tag code setup
 } else {
    include 'userdesign/headcode.php';
    // reg form setup
    $pp = explode("#WEBINAR_REGISTRATION_FORM#",$p[0]);
    if(isset($pp[1])) {
        echo $pp[0];
        include 'userdesign/formcode.php';
        echo $pp[1];
    } else {
        echo $p[0];
    }

 }

?>