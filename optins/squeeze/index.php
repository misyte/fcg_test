<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>

<meta property="og:title" content="<?php echo $get_topic[$page];?>" />
<meta property="og:type" content="activity" />
<meta property="og:url" content="<?php echo $domain_name.'webinar-register.php';?>" />
<meta property="og:image" content="<?php echo $domain_name.'webinar-files/but-ical.png';?>" />
<meta property="og:site_name" content="<?php echo $get_topic[$page];?>" />
<meta property="fb:admin" content="1639423099" />
<meta property="og:description" content="<?php echo $get_presenter[$page];?> is conducting a great webinar on <?php echo $get_topic[$page];?>.  I have already signed up! You can register here: <?php echo $domain_name.'webinar-register.php';?>"/>

<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $tpltype.'/'.$get_regtplcolor[$page].'/';?>style.css" media="screen" />
<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script language="javascript" src="js/dynajax.js" type="text/javascript"></script>
<script language="javascript" src="js/gen_validatorv31.js"></script>
<script language="javascript" src="js/detect_timezone.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function(){
    $('#sms_slide').slideUp();
    $('#sms_on').click(function(){
        if($("#sms_on").is(':checked'))
        {
            $('#sms_slide').slideDown();
        }
        else
        {
            $('#sms_slide').slideUp();
        }
    });

    var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == 'undefined') {
        response_text = 'UTC';
    }
    else {
        response_text = tz_info.timezone.display();
    }

    document.myform.timezone.value = response_text;

                $.ajax({
                        type: "POST",
                        url: "modules/regdatetime/index.php",
                        data: "tzone=" + response_text + "&id=<?php echo $memberid;?>" + "&show=dates",
                        success: function(data){
                            $("#date").html(data);

                    }
                });

                $.ajax({
                        type: "POST",
                        url: "modules/regdatetime/index.php",
                        data: "tzone=" + response_text + "&id=<?php echo $memberid;?>" + "&show=times",
                        success: function(data){
                            $("#time").html(data);

                    }
                });



        $('#frmsubmit').click(function() {

        var sdates = $("#dates").val();
        var stimes = $("#times").val();

        if(sdates == 'Select desired date'){
            alert('please select date');
            return false;
        }


        if(stimes == 'Select desired time'){
            alert('please select time');
            return false;
        }




});

});




function isNumberKey(evt) {
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}

function setCountryCode() {
    document.myform.phonenumber.value = document.myform.cc.options[document.myform.cc.selectedIndex].value;
    return true;
}


</script>

</head>
<body>

    <div id="header">
        <?php if($get_regheader[$page] == 1){ ?>
         <center><img src="<?php echo 'webinar-files/'.renamefilereg($get_headerimg[$page], $memberid);?>" alt="Live Webinar Broadcast"/></center>
        <?php }else{ ?>
        <h1><?php echo $get_regtxtheader1[$page]; ?></h1>
        <h2><?php echo $get_regtxtheader2[$page]; ?></h2>
        <?php } ?>
    </div> <!-- end header -->

    <div id="wrapper">
        <div id="sq_intro">
        <div style="width: 920px; text-align:center; padding-top: 20px; font-size: 36px; color: rgb(206, 0, 0); margin-bottom: -10px; font-family: impact,tahoma">
                <?php echo $get_headerline[$page]; ?>
        </div>
        <div id="sq_intro-text">
        <?php if($get_reginsertopt[$page] == 1){ ?>
            <div id="photo" style="width:300px;">
                <img src="resize.php?url=<?php echo 'webinar-files/'.renamefilereg($get_regimg[$page], $memberid);?>&width=280&height=330" alt="Forex Rep" />
            </div> <!-- end photo -->
            <?php }else{ ?>
            <div id="video" style="width:480px;background-color:#000; margin-top: 10px;">
                <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
                <object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="player" width="480" height="385">
                <param name="movie" value="player/player.swf" />
                <param name="allowfullscreen" value="true" />
                <param name="allowscriptaccess" value="always" />
                <param name="flashvars" value="file=<?php echo $get_regvid[$page];?>&feature=related" />
                <embed
                type="application/x-shockwave-flash"
                id="player"
                name="player"
                src="player/player.swf"
                width="480"
                height="385"
                allowscriptaccess="always"
                allowfullscreen="true"
                allowNetworking="internal"
                flashvars="file=<?php echo $get_regvid[$page];?>&autostart=<?php echo $get_regvidauto[$page];?>&controlbar=bottom"
                />
                </object>
                <!-- END OF THE PLAYER EMBEDDING -->
                <div id="player"></div>
            </div>
            <?php } ?>




                    <?php  echo $get_webidesc[$page]; ?>
            </div> <!-- end intro-text -->


                    <form action="loadregister.php" method="post" name="myform">
            <div id="sq_form">
                <div id="sq_form-header">
                    <p id="line1">Webinar Registration</p>
                    <p id="line2">Reserve your Spot!</p>
                </div> <!-- end form-header -->

                <div id="sq_form-bg">



                    <p id="local-time">Your Local Time:

                    <script type="text/javascript">
                    <!--
                    var currentTime = new Date();
                    var hours = currentTime.getHours();
                    var minutes = currentTime.getMinutes();
                    var secs = currentTime.getSeconds();
                    var orighours = hours;
                    var suffix = "AM";

                     if (hours >= 12) {
                        suffix = "PM";
                        hours = hours - 12;
                    }
                    if (hours == 0) {
                        hours = 12;
                    }

                    if (minutes < 10){
                        minutes = "0" + minutes;
                    }
                    if (secs < 10){
                        secs = "0" + secs;
                    }

                    document.write(hours + ":" + minutes + " " + suffix );

                    //-->
                    </script>

                    </p>

                        <div id="date"></div> <!-- end date -->

                        <div id="time"></div> <!-- end time -->


                        <p id="sq_register">REGISTER TO THE WEBINAR</p>

                        <!--- form fields -->
                        <input type="text" name="name" id="name" class="entry" value="Enter Your Name Here..." onFocus="if(this.value=='Enter Your Name Here...')this.value='';" onBlur="if(this.value=='')this.value='Enter Your Name Here...';" />
                        <br />
                        <input type="text" name="email" id="email" class="entry" value="Enter Your Email Here..." onFocus="if(this.value=='Enter Your Email Here...')this.value='';" onBlur="if(this.value=='')this.value='Enter Your Email Here...';" />
                        <!--- end form fields -->
                        <br />

                    <?php if($get_sms[$page] == 1){ include_once('modules/sms/index.php');  } ?>

<input type="hidden" name="timezone" id="timezone">
<input type="hidden" name="submit" value="submit">
<input type="hidden" name="page" value="<?php echo $page;?>">
<input type="image" class="submit" id="frmsubmit" src="<?php echo $tpltype.'/'.$get_regtplcolor[$page].'/';?>images/register_button.png" height="57" width="212" border="0" alt="Submit Form" />
                </div> <!-- end form-bg -->

                <div id="sq_form-footer"></div>
            </div> <!-- form -->
            </form>
<script language="javascript" type="text/javascript">
 var frmvalidator = new Validator("myform");
  frmvalidator.addValidation("name","req");
 frmvalidator.addValidation("email","maxlen=50");
 frmvalidator.addValidation("email","req");
 frmvalidator.addValidation("email","email");

 </script>

            <div class="clear"></div>

        </div> <!-- end intro -->



    </div> <!-- end wrapper -->

    <div id="footer">

    </div> <!-- end footer -->

        <!-- google analytics code -->


    <?php echo stripslashes($google_analytics); ?>

    <!-- end google analytics code -->


</body>
</html>
