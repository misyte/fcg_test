<?php

function getoptin($optin_code, $ret){

    $inputnames[0] = 'name';
    $inputnames[1] = 'subscriber_name';
    $inputnames[2] = 'fields_fname';
    $inputnames[3] = 'first_name';
    $inputnames[4] = 'NAME';
    $inputnames[5] = 'Name';
    $inputnames[6] = 'Contact0FirstName';
    $inputnames[7] = 'MERGE2';
    $inputnames[8] = 'MERGE3';
    $inputnames[9] = 'FNAME';

    $inputemails[0] = 'from';
    $inputemails[1] = 'subscriber_email';
    $inputemails[2] = 'fields_email';
    $inputemails[3] = 'email_address';
    $inputemails[4] = 'email';
    $inputemails[5] = 'ea';
    $inputemails[6] = 'EMAIL';
    $inputemails[7] = 'Email1';
    $inputemails[8] = 'Contact0Email';
    $inputemails[9] = 'MERGE0';

    $optin_code = strip_tags($optin_code, '<input><form>');
    $optin_code = html_entity_decode($optin_code);
    $optin_code = replace_newline($optin_code);



    $regex2 =  '/>([\w\W]*?)</';
    preg_match_all($regex2,$optin_code,$match2);
    foreach ($match2 as $v1) {
        foreach ($v1 as $v2) {
            $res =  "$v2";
            $find = array("<",">");
            $rep = str_replace($find,'',$res);
            $optin_code = str_replace($rep, '', $optin_code);
        }
    }

    $regex2_1 =  '/([\w\W]*?)<form/';
    preg_match_all($regex2_1,$optin_code,$match2_1);

    $rmatch2_1 = trim($match2_1[1][0]);
    $optin_code = str_replace($rmatch2_1, '', $optin_code);

    $regex2_2 =  '/.*?<\/form>/';
    if(preg_match_all($regex2_2,$optin_code,$match2_2) == 1){
        $optin_code = $match2_2[0][0];
    }

    $regex2_3 = '/style="([\w\W]*?)"/';
    if(preg_match_all($regex2_3,$optin_code,$match2_3)){
        foreach ($match2_3 as $v1) {
            foreach ($v1 as $v2) {
                $res =  "$v2";
                $optin_code = str_replace($res,'',$optin_code);
            }
        }
    }



    $pieces = explode("<", $optin_code);


    $hiddenfields = array();
    foreach($pieces as $vars){


        $find = array('"','\'',' ');

        $stripquotes =  str_replace($find, '', $vars);


            if(substr_count($stripquotes, 'form')> 0 && substr_count($stripquotes, 'action=')> 0)
            {

                $form = '&lt;'.$vars;
                    $find1 = '/>';
                    $find2 = '>';
                    $form = str_replace($find1, ' id="optinfrm" style="background:none;padding:0; margin: 0; border:none;" >', $form);
                    $form = str_replace($find2, ' id="optinfrm" style="background:none;padding:0; margin: 0; border:none;" >', $form);
            }

            if(substr_count($stripquotes, 'type=hidden')> 0 )
            {
                $hiddenfields[] = '&lt;'.$vars;

            }


            foreach($inputnames as $vname){
                if(substr_count($stripquotes, 'type=text')> 0 && substr_count($stripquotes, 'name='.$vname.'')> 0 )
                {
                                $namebox = '<input type="text" name="'.$vname.'"  value="Enter Your Name Here..."     onFocus="if(this.value==\'Enter Your Name Here...\')this.value=\'\';" onBlur="if(this.value==\'\')this.value=\'Enter Your Name Here...\';" class="entry" /><br />';
                }

                //for verticalresponse

                elseif($vname == 'first_name' && substr_count($stripquotes, 'name='.$vname.'')> 0 )
                {
                                $namebox = '<input type="text" name="'.$vname.'"value="Enter Your Name Here..."     onFocus="if(this.value==\'Enter Your Name Here...\')this.value=\'\';" onBlur="if(this.value==\'\')this.value=\'Enter Your Name Here...\';" class="entry" /><br />';
                }
                else{

                }


            }

            foreach($inputemails as $vemail)
            {

                if(substr_count($stripquotes, 'type=text')> 0 && substr_count($stripquotes, 'name='.$vemail.'')> 0 ){
                                $emailbox = '<input type="text" name="'.$vemail.'" value="Enter Your Email Here..." onFocus="if(this.value==\'Enter Your Email Here...\')this.value=\'\';" onBlur="if(this.value==\'\')this.value=\'Enter Your Email Here...\';" class="entry"  /><br />';
                }

                //for verticalresponse
                elseif($vemail == 'email_address' && substr_count($stripquotes, 'name='.$vemail.'')> 0 ){
                                $emailbox = '<input type="text" name="'.$vemail.'" value="Enter Your Email Here..." onFocus="if(this.value==\'Enter Your Email Here...\')this.value=\'\';" onBlur="if(this.value==\'\')this.value=\'Enter Your Email Here...\';" class="entry"  /><br />';
                }else{

                }
            }
    }


    $optinform  = $form;
    $hidden = implode(" ", $hiddenfields);
    $finaloptin = '';
    $finaloptin .= $hidden;
    $finaloptin .= $namebox;
    $finaloptin .= $emailbox;

    if($ret == 1){
        return $optinform ;
    }
    if($ret == 2){
        return $finaloptin;
    }

}

function replace_newline($string) {
  return (string)str_replace(array("\r", "\r\n", "\n"), '', $string);
}


?>