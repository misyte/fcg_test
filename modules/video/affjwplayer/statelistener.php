
var player = obj.id;

if( obj.newstate == 'BUFFERING'){
     $('#ajaxloader').css({'display': 'block'});
    $('#ajaxloader').html('<div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo addslashes($lang['LIVE_TXT_LOADING']);?></span></div>');
}

if( obj.newstate != 'BUFFERING'){
    $('#txt').text('').css("text-decoration", "none");
    $('#ajaxloader').css({'display': 'none'});
}


// when the user late arrived, the video will play the video 2
<?php if($dif < 0 && abs($dif) >= 60 || $a_intro_vid_opt == 0){ ?>

    $("#player1").css({visibility: "hidden"});
    $("#player2").css({visibility: "visible"});
    $("#themodules").css({display: "block"});

    $('#player2').jwPlayer('volume', 100);
    <?php if( $a_closing_vid_opt == 1){ ?>
        $('#player3').jwPlayer('volume', 0);
    <?php } ?>
    <?php  if($pagetype != 4) { ?>
    $('#player2').jwPlayer('play');
    <?php } ?>

<?php } ?>


// state of the players when completed
if( obj.newstate == 'COMPLETED' && player == 'jwPlayer1'){
    // hiding back delayed event divs
     $(".eventdivs").css({display: "none"});

    // hiding back poll divs
     $(".divpoll").css({display: "none"});

    $("#" + player).css({visibility: "hidden"});
    $("#player2").css({visibility: "visible"});
    $("#themodules").css({display: "block"});
    $('#player2').jwPlayer('seek', 0);
    $('#player2').jwPlayer('volume', 100);
    $('#player2').jwPlayer('play');
   // $('#player1').jwPlayer('destroy');

}

else if(obj.newstate == 'COMPLETED' && player == 'jwPlayer2'){
// if closing video disabled

     //$("#themodules").css({display: "none"});
     $("#" + player).css({visibility: "hidden"});
     $("#player3").css({visibility: "visible"});
     //$("#themodules").css({display: "none"});
     <?php if( $a_closing_vid_opt == 1){ ?>
        $('#player3').jwPlayer('volume', 100);
        $('#player3').jwPlayer('play');
     <?php } ?>

     $('#player2').jwPlayer('destroy');

     <?php
        if( $a_closing_vid_opt == 0){
            if($get_isautodir[$page] == 1) {
                $get_urlredirect[$page] = str_replace('#AFFILIATE#',$affid, $get_urlredirect[$page]);
     ?>
        is_exit=1;
        window.location.replace("<?php echo $get_urlredirect[$page];?>");

    <?php  } else ?>

   $("#" + player).css({visibility: "hidden"});
    $("#player3").css({visibility: "hidden"});
    $('#tot_loggedin').css({'display' : 'none'});
    $('#tot_loggedin2').css({'display' : 'inline'});
    <?php if($broadcast_design == 2){ ?>

    if (Browser.IE) {
        dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    } else {
    /*
        dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    */

    dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=notie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');

    }

     <?php
     }elseif($broadcast_design == 3){
     ?>

     if (Browser.IE) {
        dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    } else {

    /*
        dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    */

    dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=notie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');


    }

     <?php
     }

}
?>


}

<?php if( $a_closing_vid_opt == 1){ ?>
else if(obj.newstate == 'COMPLETED' && player == 'jwPlayer3'){

    <?php if($get_isautodir[$page] == 1) { $get_urlredirect[$page] = str_replace('#AFFILIATE#',$affid, $get_urlredirect[$page]);  ?>
        is_exit=1;
       window.location.replace("<?php echo $get_urlredirect[$page];?>");

    <?php } ?>

    $("#" + player).css({visibility: "hidden"});
    $("#themodules").css({display: "none"});
    $('#tot_loggedin').css({'display' : 'none'});
    $('#tot_loggedin2').css({'display' : 'inline'});
   <?php if($broadcast_design == 2){ ?>

   dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');

   /*
    if (Browser.IE) {
        dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie; resultloc: tot_loggedin2; refreshtime: 10000;');
    } else {
        dynajax.request('respfile: random-users.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    }

    */

     <?php
     }elseif($broadcast_design == 3){
     ?>

     dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie&langtype=<?php echo $langtype;?>; resultloc: tot_loggedin2; refreshtime: 10000;');


     /*
     if (Browser.IE) {
        dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>&namelist=<?php echo $namelist;?>&presenter=<?php echo $_SESSION['presentersss'];?>&browser=ie; resultloc: tot_loggedin2; refreshtime: 10000;');
    } else {
        dynajax.request('respfile: random-users3.php?&mode=minus&replay=1&uid=<?php echo $id;?>&userlog=<?php echo $userlog;?>&page=<?php echo $page;?>&dif=<?php echo $dif;?>; resultloc: tot_loggedin2; refreshtime: 10000;');
    }

    */



     <?php
     }
     ?>


}

<?php } ?>

