<?php
if (!isset($_COOKIE['split_ip'])) {
    setcookie('split_ip',$_SERVER['REMOTE_ADDR'],time()+31622400);
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
else{
    $ip_address = $_COOKIE['split_ip'];
}


/** video setings **/
$get_vidurl= unserialize(stripslashes($vidurl));
$get_isautodir = unserialize(stripslashes($isautodir));
$get_urlredirect = unserialize(stripslashes($urlredirect));
$get_showoffer = unserialize(stripslashes($showoffer));
$get_jump_opt   = unserialize(stripslashes($jump_opt));
$get_jumpvid  = unserialize(stripslashes($jumpvid));
$get_disp_livefullscreen = unserialize(stripslashes($disp_livefullscreen));


/** poll settings **/

$get_pollselect = unserialize(stripslashes($pollselect));
$pollnum = $get_pollselect[$page];
$get_showpollopt = unserialize(stripslashes($showpollopt));
$get_hidepollopt = unserialize(stripslashes($hidepollopt));
$get_showpollres = unserialize(stripslashes($showpollres));
$get_hidepollres = unserialize(stripslashes($hidepollres));


/** event settings **/
$get_delayedevents = unserialize(stripslashes($delayedevents));
$get_dtext = unserialize(stripslashes(base64_decode($dtext)));
$get_dbutton = unserialize(stripslashes($dbutton));
$get_dbuttonimgs = unserialize(stripslashes($dbutonimgs));
$get_dbuttonlink =  unserialize(stripslashes($dbuttonlink));

$get_vid_width =  unserialize(stripslashes($vid_width));
$get_vid_height =  unserialize(stripslashes($vid_height));

$eventnum = $get_delayedevents[$page];
$get_appear_dlink =  unserialize(stripslashes($appear_dlink));
$get_disappear_dlink =  unserialize(stripslashes($disappear_dlink));

include 'modules/chatlines/mboard.php';
$islate = 0;

if($dif < 0 && abs($dif) > $get_jumpvid[$page] && $get_jump_opt[$page] == 1 && $get_jumpvid[$page] > 0) {
    include 'jump-feature.php';
}


/** affiliate settings **/

    if(!isset($_COOKIE['affiliate_id']))
    {
        $getaffiliate=file_get_contents($affiliate_url."getaffiliate.php?email_address=".$email."&webinar_name=".urlencode(base64_decode($_GET['webiname']))."&webinar_date=".base64_decode($_GET['webidate'])."&webinar_time=".base64_decode($_GET['webitime']));
        setcookie('affiliate_id',$getaffiliate,time()+31622400);
        $affid = $getaffiliate;
    }
    else
    {
        $affid = $_COOKIE['affiliate_id'];
    }

        $a_intro_vid_opt=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affid.'&q=io');
        $a_closing_vid_opt=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affid.'&q=co');
        $a_intro_vid_url=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affid.'&q=ivu');
        $a_closing_vid_url=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affid.'&q=cvu');

?>


<script type="text/javascript" language="javascript">


/**
 * Browser detection
 */

    var userAgent = navigator.userAgent.toLowerCase();

    // Figure out what browser is being used.
    var Browser = {
        Version: (userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        Chrome: /chrome/.test(userAgent),
        Safari: /webkit/.test(userAgent),
        Opera: /opera/.test(userAgent),
        IE: /msie/.test(userAgent) && !/opera/.test(userAgent),
        Mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent),
        Check: function() { alert(userAgent); }
    };





jQuery(function ($) {

$.ajax({
                type: "GET",
                url: "userupdate.php",
                data: "action=attended&memberid=<?php echo $member_id;?>&webid=<?php echo $webid;?>&webtime=<?php echo $webitime;?>&dateset=<?php echo $webidate;?>&email=<?php echo $email;?>",

                success: function(data){
                    //$("#splitoffer").html(data);

                }
        });






    <?php

          include 'trackers.php';
          if($get_vid_width[$page] == '' ||  $get_vid_height[$page] == '') {
              $vwidth = 640;
              $vheight = 480;
          } else {

              $getvsize = $get_vid_width[$page] / $get_vid_height[$page];

              if($getvsize > 1.5){
                  $vwidth = 640;
                  $vheight = 360;
              } else {
                  $vwidth = 640;
                  $vheight = 480;
              }
          }

          /**  height update  **/
    if($get_disp_livefullscreen[$page] == 1) {
        $vheight = $vheight + 30;
    }

    ?>


   <?php if($dif <= 0 && abs($dif) <= 60 && $a_intro_vid_opt == 1){ ?>
    var playintro = 1;
    $('#player1').jwPlayer({
        //debug: 'console',
        swf: 'js/player.swf',
        autostart:'true',
        file: '<?php echo makeurl($a_intro_vid_url);?>',
        width: '<?php echo $vwidth;?>',
        height: '<?php echo $vheight;?>',
        wmode: 'opaque',
        wmode: 'transparent',
        mute: 'false',
        volume : 100,
        <?php if($get_disp_livefullscreen[$page] == 1) { ?>
        skin: 'skins/modieus/modieus.xml',
        controlbar: 'bottom',
        <?php }else{ ?>
        controlbar: 'false',
        <?php } ?>
        displayclick: 'link',
        menu: 'false',
        volume : 100,
        icons: 'false',

        id: 'jwPlayer1',
        playerready: playerReady1,
        timelistener: timeListener,
        statelistener: stateListener,
        playlistlistener: playlistListener
    });

    <?php } ?>

    var playmain = 1;
    $('#player2').jwPlayer({
        //debug: 'console',
        swf: 'js/player.swf',
        autostart:'true',
        file: '<?php echo makeurl($get_vidurl[$page]);?>',
        width: '<?php echo $vwidth;?>',
        height: '<?php echo $vheight;?>',
        wmode: 'opaque',
        wmode: 'transparent',
        <?php if($get_disp_livefullscreen[$page] == 1) { ?>
        skin: 'skins/modieus/modieus.xml',
        controlbar: 'bottom',
        <?php }else{ ?>
        controlbar: 'false',
        <?php } ?>
        displayclick: 'link',
        menu: 'false',
        mute: 'false',
        icons: 'false',
        volume: 100,
        id: 'jwPlayer2',
        playerready: playerReady2,
        timelistener: timeListener,
        statelistener: stateListener,
        playlistlistener: playlistListener
    });


    <?php if($a_closing_vid_opt == 1) { ?>
    $('#player3').jwPlayer({
        //debug: 'console',
        swf: 'js/player.swf',
        autostart:'true',
        file: '<?php echo makeurl($a_closing_vid_url);?>',
        width: '<?php echo $vwidth;?>',
        height: '<?php echo $vheight;?>',
        wmode: 'opaque',
        wmode: 'transparent',
        mute: 'false',
        <?php if($get_disp_livefullscreen[$page] == 1) { ?>
        skin: 'skins/modieus/modieus.xml',
        controlbar: 'bottom',
        <?php }else{ ?>
        controlbar: 'false',
        <?php } ?>
        displayclick: 'link',
        menu: 'false',
        icons: 'false',
        id: 'jwPlayer3',
        playerready: playerReady3,
        timelistener: timeListener,
        statelistener: stateListener,
        playlistlistener: playlistListener
    });

    <?php } ?>



     function playerReady1(obj)
     {
        $('#preview1').css({'display' : 'none'});
        $('#ajaxloader').css({'display': 'block'});
        $('#ajaxloader').html('<div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo addslashes($lang['LIVE_TXT_LOADING']);?></span></div>');

     }

     function playerReady2(obj)
     {
        $('#preview1').css({'display' : 'none'});
        $('#ajaxloader').css({'display': 'block'});
        $('#ajaxloader').html('<div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo addslashes($lang['LIVE_TXT_LOADING']);?></span></div>');
        //$('#player2').jwPlayer('volume', 0);
        //$('#player3').jwPlayer('volume', 0);
     }

     function playerReady3(obj)
     {
        $('#preview1').css({'display' : 'none'});
        $('#ajaxloader').css({'display': 'block'});
        $('#ajaxloader').html('<div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo addslashes($lang['LIVE_TXT_LOADING']);?></span></div>');
        //$('#player3').jwPlayer('volume', 0);
     }



      <?php include 'modules/chatlines/ajaxpost.php';   ?>
      function timeListener(obj)
      {
          <?php if(1){ ?>

           if(typeof playintro !== 'undefined'){
               var introstate =  $('#player1').jwPlayer('get', 'state');
               var mainstate =  $('#player2').jwPlayer('get', 'state');

              if(introstate == 'COMPLETED' && mainstate == 'PLAYING') {

                  <?php include 'modules/chatlines/index.php'; ?>
              }
           } else if(playmain == 1) {

               var mainstate =  $('#player2').jwPlayer('get', 'state');
              if(mainstate == 'PLAYING') {

                  <?php include 'modules/chatlines/index.php'; ?>
              }
           } else {

           }

        <?php } ?>

         <?php include 'timelistener.php'; ?>
      }


      function stateListener(obj)
      {
         <?php include 'statelistener.php'; ?>
      }

      function playlistListener(obj)
      {

      }






});
</script>

