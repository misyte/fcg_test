<?php
/**
 * override the video position set for all events
 */
$get_latevidurl= unserialize(stripslashes($latevidurl));
$get_vidlength =  unserialize(stripslashes($vidlength));
$get_latevidlength =  unserialize(stripslashes($latevidlength));

$vidcutsecs = $get_vidlength[$page] - $get_latevidlength[$page];
if($vidcutsecs < 0) {
    $vidcutsecs=0;
}


$islate=1;
/** show offer  **/
array_walk_recursive($get_showoffer[$page],'calclatevideo',$vidcutsecs);

/** delayed events  **/
array_walk_recursive($get_appear_dlink[$page],'calclatevideo',$vidcutsecs);
array_walk_recursive($get_disappear_dlink[$page],'calclatevideo',$vidcutsecs);

/** polls **/
array_walk_recursive($get_showpollopt[$page],'calclatevideo',$vidcutsecs);
array_walk_recursive($get_hidepollopt[$page],'calclatevideo',$vidcutsecs);
array_walk_recursive($get_showpollres[$page],'calclatevideo',$vidcutsecs);
array_walk_recursive($get_hidepollres[$page],'calclatevideo',$vidcutsecs);

/** chatlines **/
array_walk_recursive($get_chatlineshow[$page],'calclatevideo',$vidcutsecs);
$arrchatlineshow = implode(",", $get_chatlineshow[$page]);

/** video url and video length  **/
$get_latevidurl= unserialize(stripslashes($latevidurl));
$get_latevid_length = unserialize(stripslashes($latevidlength));

    /** overrride main video with late video  **/
    $get_vidurl[$page] = $get_latevidurl[$page];
    $get_vid_length[$page] = $get_latevidlength[$page];


?>