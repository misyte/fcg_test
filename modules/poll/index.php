<?php

$get_pollselect = unserialize(stripslashes($pollselect));
$get_polltitle = unserialize(stripslashes($polltitle));
$get_pollquestion = unserialize(stripslashes($pollquestion));
$get_pollopt = unserialize(stripslashes($pollopt));
$get_polltext = unserialize(stripslashes($polltext));
$get_polloptpercent = unserialize(stripslashes($polloptpercent));

$pollnum = $get_pollselect[$page];
$getpolltitle = array_slice($get_polltitle[$page], 0, $pollnum);
$getpollquestion = array_slice($get_pollquestion[$page], 0, $pollnum);
$getpollopt = array_slice($get_pollopt[$page], 0, $pollnum);
$getpolltext = $get_polltext;
$getpolloptpercent = $get_polloptpercent;


?>

<script language="javascript" src="js/jquery.js" type="text/javascript"></script>

<script language="javascript">

    $(document).ready(function(){
    <?php
    $a = 0;
    while ($a < $pollnum) {
    ?>
        $('#pollform<?php echo $page;?>-<?php echo $a;?>').submit(function() {
            if($('input[name=pollradio<?php echo $page;?>-<?php echo $a;?>]:checked').val()) {
                data = '<div class="polltitle"><?php echo htmlentities($getpolltitle[$a], ENT_QUOTES, "UTF-8");?></div>';
                data += '<br><div class="ty" style="font-size:20px; font-weight:bold; color:#C00; text-align:center"><?php echo addslashes($lang['LIVE_POLL_TXT2']);?><br><br><span style="color:#333; font-size:16px;"><?php echo addslashes($lang['LIVE_POLL_TXT3']);?></span></div>';
                $("#divpoll<?php echo $page;?>-<?php echo $a;?>").html(data);
            }

            return false;
        });
    <?php
    $a++;
    }
    ?>
    });

</script>

<?php

    $a = 0;
    while ($a < $pollnum) {
?>

<form id="pollform<?php echo $page;?>-<?php echo $a;?>">
    <div class="divpoll" id="divpoll<?php echo $page;?>-<?php echo $a;?>" style="display:none;">
        <div class="polltitle"><?php echo $getpolltitle[$a]; ?></div>
        <div class="pollquestion"><?php echo $getpollquestion[$a]; ?></div>
        <?php
        for ($b = 0; $b < $getpollopt[$a]; $b++) {

                $opttext = $get_polltext[$page][$a][$b];


            if($opttext != ''){
        ?>
                <div class="divopts">
                    <dl class="graph">
                        <dt class="bar-opts" style="text-align:left; padding-left:30px; width: 100%; line-height: 15px;">
                        <input name="pollradio<?php echo $page;?>-<?php echo $a;?>" type="radio" value="<?php echo $b;?>">&nbsp;&nbsp;<?php echo htmlentities($opttext, ENT_QUOTES, "UTF-8"); ?></dt>
                    </dl>
                </div>
        <?php } ?>
<?php } ?>

        <input type="hidden" value="<?php echo $a;?>" name="pollid<?php echo $page;?>-<?php echo $a;?>" id="pollid<?php echo $page;?>-<?php echo $a;?>" />
        <input type="submit" class="pollsubmit" value="submit" id="pollsub<?php echo $page;?>-<?php echo $a;?>">
    </div>

    </form>
<?php
        $a++;
   }

?>