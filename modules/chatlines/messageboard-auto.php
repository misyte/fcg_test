<?php

    error_reporting(E_ALL);
    ini_set("display_errors", 0);

    $webid = $_POST['webid'];
    $memberid = $_POST['memberid'];

    include '../../config.php';  /** loadinfo.php included here **/
    $page = $_POST['page'];      /** livepage split version **/
    $vidpos = $_POST['vidpos'];  /** video position in seconds */
    $mboard = $_POST['mboard'];  /** messageboard type **/


    $_POST['user'] = urldecode($_POST['user']);
    $_POST['owner'] = urldecode($_POST['owner']);
    $_POST['to'] = urldecode($_POST['to']);
    $chat_user = base64_decode($_POST['chat_user']);

    $get_chatlineopt  = unserialize(stripslashes($chatlineopt));
    $get_chatlinename = unserialize(stripslashes($chatlinename));
    $get_chatlinetext = unserialize(stripslashes($chatlinetext));
    $get_chatlineshow = unserialize(stripslashes($chatlineshow));
    $get_chatlinemark = unserialize(stripslashes($chatlinemark));


    if($_POST['islate'] == 1) {
        $get_vidlength =  unserialize(stripslashes($vidlength));
        $get_latevidlength =  unserialize(stripslashes($latevidlength));

        $vidcutsecs = $get_vidlength[$page] - $get_latevidlength[$page];
        if($vidcutsecs < 0) {
            $vidcutsecs=0;
        }
        array_walk_recursive($get_chatlineshow[$page],'calclatevideo',$vidcutsecs);

    }
    $chatpos =  $get_chatlineshow[$page];

    $chat_type=unserialize($chatbox);
    if($chat_type[$page]==1) $_POST['chat_type']="Open";
    elseif($chat_type[$page]==2) $_POST['chat_type']="Blind";

    if($_POST['mdate']=="") $mdate="admin"; else $mdate=$_POST['mdate'];
    if($_POST['mtime']=="") $mtime=0; else $mtime=$_POST['mtime'];

    if($vidpos>= 0)
    {
        $key = array_keys($chatpos, $vidpos);

        if(is_array($key) && sizeof($key)>0)
        {
            foreach($key as $key1)
            {
                $_POST['mymessage'] = $get_chatlinetext[$page][$key1];
                if($get_chatlinemark[$page][$key1] == 'on')
                {
                    $_POST['user'] = '<span style="color:#970202;">';
                    $_POST['user'].= $get_chatlinename[$page][$key1].'</span>';
                }
                else
                {
                    $_POST['user'] = $get_chatlinename[$page][$key1];
                }

                /** inserting chatline to DB **/
                if($_POST['chat_type']=='Open')
                {
                    if(trim($_POST['mymessage'])!="")
                    {
                        $sql="INSERT INTO messageboard (memberid,webid,m_date,m_time,name,message,fromuser,touser,is_chatline,chat_user)
                        VALUES(".$memberid.",'".$webid."','".$mdate."',".$mtime.",'".addslashes($_POST['user'])."',
                        '".addslashes($_POST['mymessage'])."','".addslashes($_POST['user'])."','".addslashes($_POST['to'])."','1','".addslashes($chat_user)."')";
                        mysql_query($sql) or die('Query failed inserting1');
                    }
                }

                elseif($_POST['chat_type']=='Blind')
                {
                    if(trim($_POST['mymessage'])!="")
                    {
                        $sql="INSERT INTO messageboard (memberid,webid,m_date,m_time,name,message,fromuser,touser,is_chatline,chat_user)
                        VALUES(".$memberid.",'".$webid."','".$mdate."',".$mtime.",'".addslashes($_POST['user'])."',
                        '".addslashes($_POST['mymessage'])."','".addslashes($_POST['user'])."','".addslashes($_POST['to'])."','1','".addslashes($chat_user)."')";
                        mysql_query($sql) or die('Query failed inserting2');
                    }
                }
            }
        }
        else
        {
            echo 'empty';
            exit;
        }


        /* DISPLAY CHATBOX WITH CHAT TEXT LINES
         * - set $_POST['mymessage'] into null to bypass insert query
         * - and perform the displaying of chatbox with chatlines
         */

        $_POST['mymessage'] = '';
        include "'".$mboard."'";

        /** end display **/

    }

?>