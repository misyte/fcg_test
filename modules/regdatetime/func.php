<?php

error_reporting(0);

function getnext3dates($type, $multimes, $block, $holidays, $daysched, $selecteddate) {

    $muldates = array();
    switch ($type) {

    case 1: $muldates = everyday($multimes, $block, $holidays); break;
    case 2: if(is_array($selecteddate)) {
                $muldates = $selecteddate;
            } else {
                $muldates = array($selecteddate);
            }
            break;

    case 3: $muldates = selecteddays($multimes, $block, $holidays, $daysched); break;
    case 4: $muldates = selecteddays($multimes, $block, $holidays, $daysched); break;
    default: ;

    }
    return $muldates;
}

function everyday($multimes, $block, $holidays) {

    /**
     *  checking if the shceduled time of current date is in the past
     *  if yes, add 1 day to the current date
    **/

    sort($multimes);
    $datenow = date('Y-M-d');
    $currdate = date('Y-M-d');
    $curtimeStamp = StrToTime($currdate);
    $timeStampnow = StrToTime($currdate);
    $getlasthr = date('H', $curtimeStamp);

    if (end($multimes) >= $getlasthr) {
        $currdate = date('Y-M-d');
    } else {
        $curplus1day = StrToTime('+1 day', $curtimeStamp);
        $currdate = date('Y-M-d', $curplus1day);
    }

    $arrdates = array();
    $cntdates = count($arrdates);

    /** block days  **/
    $blockdays = array();
    $block = $block ? $block : 0;
    if ($block > 0) {
        $blockdays[0] = $datenow;
    }

    for ($a = 1; $a < $block; $a++) {
        $curplus1day = StrToTime("+$a day", $timeStampnow);
        $blockdate = date('Y-M-d', $curplus1day);
        $blockdays[] = $blockdate;
    }

    $blockdays = array_values($blockdays);

    /** combining blockdays with holidays **/
    $holidays = array_merge($holidays, $blockdays);
    $holidays = array_unique($holidays);

    /** block holidays **/
    $holidays = $holidays ? $holidays : array();
    $timeStamp = StrToTime($currdate);

    $ctr = 0;
    while ($ctr < 10) {
        if (!in_array($currdate,$holidays) && $ctr == 0) {
            $dates[$ctr] = $currdate;
            $ctr++;
        } else {
            $plus1day = StrToTime('+1 day', $timeStamp);
            $nxtdate = date('Y-M-d', $plus1day);
            $timeStamp = StrToTime($nxtdate);
            if (!in_array($nxtdate,$holidays)) {
                $dates[$ctr] = $nxtdate;
                $ctr++;
            }
        }
    }

    /** get the first 6 elements and return **/
    $dates = array_slice($dates, 0, 6);
    return $dates;
}



function selecteddays($multimes, $block, $holidays, $daysched) {

    sort($multimes);
    $dates = array();
    $currdate = date('Y-M-d');
    $curtimeStamp = StrToTime($currdate);
    $getday = date('D', $curtimeStamp);
    $gethr = date('H', $curtimeStamp);

    /** block days  **/
    $blockdays = array();
    $block = $block ? $block : 0;

    if ($block > 0) {
        $blockdays[0] = $currdate;
    }

    for ($a = 1; $a < $block; $a++) {
        $curplus1day = StrToTime("+$a day", $curtimeStamp);
        $blockdate = date('Y-M-d', $curplus1day);
        $blockdays[] = $blockdate;
    }

    $blockdays = array_values($blockdays);

    /** combining blockdays with holidays **/
    $holidays = array_merge($holidays, $blockdays);
    $holidays = array_unique($holidays);
    $timeStampdays = array();
    $selecteddays = array();


    $cntdays = count($daysched);
    for ($a=0; $a < $cntdays; $a++) {
        for ($b=0; $b < 5; ) {
            if (@$timeStampdays[$a] == '') {
                if(trim($getday) == trim($daysched[$a])) {
                    $timeStampdays[$a] = $curtimeStamp;
                } else {
                    $timeStampdays[$a] = StrToTime('next '.$daysched[$a]);
                }
                $sday = date('Y-M-d', $timeStampdays[$a]);
            } else {
                $sday = StrToTime('next '.$daysched[$a], $timeStampdays[$a]);
                $sday = date('Y-M-d', $sday);
            }
            if (!in_array($sday, $holidays)) {
                $selecteddays[$a][$b] =  $sday;
                $timeStampdays[$a] = strtotime($sday);
                $b++;
            } else {
                $timeStampdays[$a] = strtotime($sday);

            }
        }
    }

    $dates = array();
    $c = 0;
    foreach ($selecteddays as $v1) {
        foreach ($v1 as $v2) {
            $dates[$c] = $v2;
            $c++;
        }
    }


    /** if current date is in schedule and time is not yet pass
         prepend it to the available dates **/
    $dates = array_values_recursive($selecteddays);
    if(in_array($getday, $holidays) && $gethr > max(multimes)) {
        array_unshift($dates, $currdate);
    }


    /** sorting dates **/
    $orderByDate = array();
    foreach ($dates as $val) {
        $orderByDate[]  = strtotime($val);
    }

    array_multisort($orderByDate, SORT_ASC, $dates);
    $orderByDate = array_unique($orderByDate);



    /** get the first 6 elements and return **/
    $orderByDate = array_slice($orderByDate, 0, 6);
    foreach($orderByDate as $var) {
        $finaldates[] = date('Y-M-d', $var);
    }

    return $finaldates;

}

function array_values_recursive($ary)  {

    $lst = array();
    foreach( array_keys($ary) as $k ) {
        $v = $ary[$k];
        if (is_scalar($v)) {
            $lst[] = $v;
        } elseif (is_array($v)) {
            $lst = array_merge($lst,array_values_recursive($v));
        }
    }
    return $lst;

}


function oneday($multimes, $block, $holidays, $daysched) {

    sort($multimes);
    $dates = array();
    $currdate = date('Y-M-d');
    $curtimeStamp = StrToTime($currdate);
    $getday = date('D', $curtimeStamp);
    $getlasthr = date('H', $curtimeStamp);


    /** block days  **/
    $blockdays = array();
    $block = $block ? $block : 0;

    if ($block > 0) {
        $blockdays[0] = $currdate;
    }

    for ($a = 1; $a < $block; $a++) {
        $curplus1day = StrToTime("+$a day", $curtimeStamp);
        $blockdate = date('Y-M-d', $curplus1day);
        $blockdays[] = $blockdate;
    }

    $blockdays = array_values($blockdays);

    /** combining blockdays with holidays **/
    $holidays = array_merge($holidays, $blockdays);
    $holidays = array_unique($holidays);


    /** block holidays **/
    $ctr = 0;
    $dates = array();
    $holidays = $holidays ? $holidays : array();

    while ($ctr < 10) {

        if (end($multimes) >= $getlasthr && in_array($getday,$daysched) && $ctr == 0 && !in_array($currdate,$holidays)) {
            $dates[$ctr] = $currdate;
            $timeStamp = $curtimeStamp;
            $ctr++;
        } else {
            $resdate = StrToTime('next '.$daysched[0], $timeStamp);
            $nxtday = date('Y-M-d', $resdate);
                if (!in_array($nxtday,$holidays)) {
                $dates[$ctr] = date('Y-M-d', $resdate);
                $ctr++;
            }
            $timeStamp = $resdate;
        }
    }

    /** get the first 6 elements and return **/
    $dates = array_slice($dates, 0, 6);
    return $dates;
}


#get user offset timezone
function getOffsetByIP($ip) {

    $offset = false;
    $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];

    if($_COOKIE['webioffset'] != '') {
        $offset = $_COOKIE['webioffset'];
    }
    else {

        $tags="";
        $lon=0;
        //$tags = get_meta_tags('http://www.geobytes.com/IpLocator.htm?GetLocation&template=php3.txt&IpAddress='.$ip);

        //if($tags['longitude'])
        //$lon=$tags['longitude'];
        if ($geodata = curlURL('https://www.geoiptool.com/?IP='.$ip))  {
            $geodata = str_replace(' ', '', $geodata);
            $geodata = str_replace(array("\r", "\r\n", "\n"), '', $geodata);
            if (preg_match_all('/Longitude:<\/span>([\w\W]*?)<\/div>/i', $geodata, $match)) {
                if (preg_match_all('/<span>([\w\W]*?)<\/span>/i', trim($match[1][0]), $match2)) {
                    $lon = trim($match2[1][0]);
                }

            }
            $offset = round($lon / 15);

        }
        $offset = round($lon / 15);
    setcookie("webioffset", $offset, time()+3600);
    }
    return $offset;
}


#get equivalent time of user by his timezone based on webinar timezone
function getUserEquivtime($useroffset, $webioffset, $webitime = false){

    $webitime = $webitime ? $webitime : array();
    $usertime = array();

    foreach($webitime as $utime) {
        if ($webioffset < 0) {
            $utc = ($utime + abs($webioffset)) % 24;
        } else {
            $utc = ($utime - abs($webioffset)) % 24;
        }

        if ($useroffset < 0) {
            $convertedtime = $utc - abs($useroffset);
        } else {
            $convertedtime = $utc + abs($useroffset);
        }

        $usertime[] = $convertedtime;
    }

    return $usertime;

}


function get12hourtime($usertime){
    $gettime = array();
    foreach($usertime as $var) {
        $gettime[] = get12hour($var);


        return $gettime;
    }
}


function get12hour($hr){
$hr = trim($hr);
    if($hr >=1 && $hr < 12){
        $hr12 = $hr.':00 am';
    }
    elseif($hr == 0){
        $hr12 = '12:00 am';
    }
    elseif($hr == 12){
        $hr12 = $hr.':00 pm';
    }
    else{
        $hr12 = $hr -12;
        $hr12 = $hr12.':00 pm';
    }

return $hr12;



}

function get_utimeval($multimes, $utime,$webischedopt,$offset,$tz) {

    global $lang;
    $utimeval = '';
    $tz1 = $tz;
    if(is_array($utime)) {
        $ctr = 0;
        foreach($utime as $var) {
            $vartime = get12hour($var);
            if($webischedopt == 1) {
                if($offset>=0) {
                    $sign = '+';
                }
                else {
                    $sign = '';
                }



                $tzlist = file_get_contents('tzoption.php');

                $tz = str_replace('/','\/',$tz);

                if(@preg_match_all('/<option value="'.$tz.'">([\w\W]*?)<\/option>/i', $tzlist, $match)){

                $city = trim(@$match[1][0]);
                $city = makeoffset2($tz1).' '.$city;

                }

                if($city != ''){
                    $vartime = $vartime.' '.$city;
                }
                else{
                    $vartime = $vartime.' (GMT'.' '.$sign.$offset.')';
                }


            }
            if($webischedopt == 2) {
                $vartime = $vartime.' ('.$lang['REG_FORM_TXT_LOCALTIME'].')';
            }
            $utimeval .= '<option value="'.$multimes[$ctr].'">'.$vartime.'</option>';
            $ctr++;
        }
    }

    return $utimeval;
}

function get_udateval($origdate, $udate) {

   global $langtype;
   global $langmonths;
   global $langdays;

   $origmonths = array('');



    $udateval = '';
    if(is_array($udate)) {
        $ctr = 0;
        foreach($udate as $var) {
            if($langtype=="hebrew") $datestyle =  date('l, F j',strtotime($var));
            else $datestyle =  date('l, F jS',strtotime($var));
            $dmonth = date('F',strtotime($var));
            $dday = trim(date('l',strtotime($var)));

            if(is_array($langmonths) && is_array($langdays)) {

                $datestyle = @str_ireplace($dmonth,$langmonths[strtolower($dmonth)],$datestyle);
                $datestyle = @str_ireplace($dday,$langdays[strtolower($dday)],$datestyle);
            }

            $udateval .= '<option value="'.@$origdate[$ctr].'">'.$datestyle.'</option>';
            $ctr++;
        }
    }
    return $udateval;
}


function curlURL2($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,4);
    curl_setopt($curl,CURLOPT_TIMEOUT,4);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)');
    $output = curl_exec($curl);

    return $output;
}

function convertdates($udate, $utime, $tz, $useroffset, $timeopt) {

    $offset = $useroffset;
    $datetime = array();
    $dtime = array();
    for($a=0;$a < count($udate);$a++) {
        for($b=0;$b < count($utime);$b++) {
            $timestamp = strtotime("$udate[$a] $utime[$b]:00");
            $datetime[] = gmdate('Y-m-d', $timestamp + ($offset * 3600));
            $dtime[] = $udate[$a].' '.$utime[$b];
        }
    }

    $datetime = array_unique($datetime);
    /** get the first 6 elements and return **/
    $datetime = array_slice($datetime, 0, 6);
    if($timeopt == 0) {
        return $datetime;
    } else {
        return $dtime;
    }


}

function filtertime ($udate, $utime) {
    if(is_array($udate) && is_array($utime)) {
        $tmp = $utime;
        global $type;
        global $vidlength;

        /** get the biggest video legnth to add into webinar time as a final expired time **/
        $t = unserialize($vidlength);
        $vidhours = array();
        if(is_array($t)) {
            foreach($t as $ob)
            {
                if(is_array($ob)) {
                    foreach($ob as $ob2) {
                        $vidhours[] = $ob2;
                    }
                } else {
                        $vidhours[] = $ob;
                }
            }
        }

        $maxvidhour = max($vidhours)/ 3600;



        //test time
        for($a=0; $a<count($utime);$a++) {
            $datenow = date('Y-M-d H:i:s');

            if($type != 2) {
                $resdate = strtotime("$udate[0] $utime[$a]:00:00") - strtotime("$datenow");
            } else {
                $thetime = $utime[$a] + $maxvidhour;
                $thetime2 =  convertTime($thetime);
                $resdate = strtotime("$udate[0] $thetime2") - strtotime("$datenow");
            }


            if($resdate < 1) {
                unset($tmp[$a]);
            }
        }

        $tmp = array_values($tmp);

        return $tmp;

    }


}

function nofiltertime ($udate, $utime) {
    if(is_array($udate) && is_array($utime)) {
        $tmp = $utime;
        global $type;
        global $vidlength;

        /** get the biggest video legnth to add into webinar time as a final expired time **/
        $t = unserialize($vidlength);
        $vidhours = array();
        if(is_array($t)) {
            foreach($t as $ob)
            {
                if(is_array($ob)) {
                    foreach($ob as $ob2) {
                        $vidhours[] = $ob2;
                    }
                } else {
                        $vidhours[] = $ob;
                }
            }
        }

        $maxvidhour = max($vidhours)/ 3600;



        //test time
        for($a=0; $a<count($utime);$a++) {
            $datenow = date('Y-M-d H:i:s');


                $thetime = $utime[$a] + $maxvidhour;
                $thetime2 =  convertTime($thetime);
                $resdate = strtotime("$udate[0] $thetime2") - strtotime("$datenow");


            if($resdate < 1) {
                unset($tmp[$a]);
            }
        }

        $tmp = array_values($tmp);

        return $tmp;

    }


}


function filterdate($udate, $utime, $avdates) {
    if(is_array($udate) && is_array($utime)) {

        $tmp2 = $udate;
        global $type;
        global $vidlength;

        /** get the biggest video legnth to add into webinar time as a final expired time **/
        $t = unserialize($vidlength);
        $vidhours = array();
        if(is_array($t)) {
            foreach($t as $ob)
            {
                if(is_array($ob)) {
                    foreach($ob as $ob2) {
                        $vidhours[] = $ob2;
                    }
                } else {
                        $vidhours[] = $ob;
                }
            }
        }

        $maxvidhour = max($vidhours)/ 3600;

        //test time
    $countme=0;
    foreach($udate as $udates) {
        $tmp = $utime;
        for($a=0; $a<count($utime);$a++) {
            $datenow = date('Y-M-d H:i:s');
            if($type != 2) {
                $resdate = strtotime("$udates $utime[$a]:00:00") - strtotime("$datenow");
            } else {
                $thetime = $utime[$a] + $maxvidhour;
                $thetime2 =  convertTime($thetime);
                $resdate = strtotime("$udates $thetime2") - strtotime("$datenow");
            }

            if($resdate < 1) {
                unset($tmp[$a]);

            }
        }

        $tmp = array_values($tmp);
        if(count($tmp) < 1){
            unset($udate[$countme]);

        }
    $countme++;
    }
        $udate = array_values($udate);
        $udate = array_slice($udate, 0, $avdates);

        return $udate;

    }

}




function nofilterdate($udate, $utime, $avdates) {
    if(is_array($udate) && is_array($utime)) {

        $tmp2 = $udate;
        global $type;
        global $vidlength;

        /** get the biggest video legnth to add into webinar time as a final expired time **/
        $t = unserialize($vidlength);
        $vidhours = array();
        if(is_array($t)) {
            foreach($t as $ob)
            {
                if(is_array($ob)) {
                    foreach($ob as $ob2) {
                        $vidhours[] = $ob2;
                    }
                } else {
                        $vidhours[] = $ob;
                }
            }
        }

        $maxvidhour = max($vidhours)/ 3600;

        //test time
    $countme=0;
    foreach($udate as $udates) {
        $tmp = $utime;
        for($a=0; $a<count($utime);$a++) {
                $datenow = date('Y-M-d H:i:s');
                $thetime = $utime[$a] + $maxvidhour;
                $thetime2 =  convertTime($thetime);
                $resdate = strtotime("$udates $thetime2") - strtotime("$datenow");


            if($resdate < 1) {
                unset($tmp[$a]);

            }
        }

        $tmp = array_values($tmp);
        if(count($tmp) < 1){
            unset($udate[$countme]);

        }
    $countme++;
    }
        $udate = array_values($udate);
        $udate = array_slice($udate, 0, $avdates);

        return $udate;

    }

}





function getTimeZoneDateTime($GMT) {
    $timezones = array(
        '-12'=>'Pacific/Kwajalein',
        '-11'=>'Pacific/Samoa',
        '-10'=>'Pacific/Honolulu',
        '-9'=>'America/Juneau',
        '-8'=>'America/Los_Angeles',
        '-7'=>'America/Denver',
        '-6'=>'America/Mexico_City',
        '-5'=>'America/New_York',
        '-4'=>'America/Caracas',
        '-3.5'=>'America/St_Johns',
        '-3'=>'America/Argentina/Buenos_Aires',
        '-2'=>'Atlantic/Azores',// no cities here so just picking an hour ahead
        '-1'=>'Atlantic/Azores',
        '0'=>'Europe/London',
        '1'=>'Europe/Paris',
        '2'=>'Europe/Helsinki',
        '3'=>'Europe/Moscow',
        '3.5'=>'Asia/Tehran',
        '4'=>'Asia/Baku',
        '4.5'=>'Asia/Kabul',
        '5'=>'Asia/Karachi',
        '5.5'=>'Asia/Calcutta',
        '6'=>'Asia/Colombo',
        '7'=>'Asia/Bangkok',
        '8'=>'Asia/Singapore',
        '9'=>'Asia/Tokyo',
        '9.5'=>'Australia/Darwin',
        '10'=>'Pacific/Guam',
        '11'=>'Asia/Magadan',
        '12'=>'Asia/Kamchatka'
    );
   return $timezones[$GMT];
}

function makeoffset2($webi_tz) {
    $tz = $webi_tz;
    $timezone = new DateTimeZone("$tz");
    $offset   = $timezone->getOffset(new DateTime);

    $offset =  $offset/3600;
    $pieces = explode(".", $offset);

    if($pieces[1] > 0 && strlen($pieces[1]) == 1) {
        $goffset = $pieces[0].':'.$pieces[1].'0';
    }
    elseif($pieces[1] > 0 && strlen($pieces[1]) == 2) {
        $goffset = $pieces[0].':'.$pieces[1];
    }
    else {
        $goffset = $pieces[0].':00';
    }

    if(strlen($pieces[0]) == 1) {
        $goffset = '0'.$pieces[0];
    }

    if($pieces[0] >= 0) {
        $goffset = '+'.$goffset;
    }


    if("Europe/Moscow" == $webi_tz)
        $goffset = '(GMT+04)';
    else
        $goffset = '(GMT'.$goffset.')';

    return $goffset;

}


?>