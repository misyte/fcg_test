<?php
/**
 * @param webischedopt : webinar timezone to use.
 *   1 = based on webinar admin's selected timezone
 *   2 = based on registrants' own time zone
 * @param getdate    : default first date of the sched
 * @param type       : type of sched e.g everyday, selecteddays, perday or specific date
 * @pram block       : block date schedules
 * @param multimes   : list of time sched
 * @pram selecteddays: list of sched days
 * @webioffset       : webinar timezone offset
 **/

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

include('modules/regdatetime/func.php');



//$useroffset = filteroffset($tzone);
$holiday_list = unserialize($holidaylist);
$selected_days = unserialize($selecteddays);
$mul_times = unserialize($multimes);

$multi = array();
foreach($mul_times as $var){
    $multi[] = get24hour($var);
}
$multi = array_unique($multi);
if($webischedopt == 1){

    $tz = $webi_tz;
    $timezone = new DateTimeZone("$tz");
    $offset   = $timezone->getOffset(new DateTime);
    $offset =  $offset/3600;
}
else{

    $t = getOffsetByIP($_SERVER['REMOTE_ADDR']);
    $tz = getTimeZoneDateTime($t) ;


}
if(!is_array($selected_days) && $type == 3){
    $selected_days = array($selected_days);
}
sort($multi);
date_default_timezone_set($tz);

$get_selecteddate = unserialize(stripslashes($selecteddate));
$udate = getnext3dates($type, $multi, $block, $holiday_list, $selected_days, $get_selecteddate);

$utime = $multi;


$avdates = $avdates ? $avdates : 3;
if($immediate == 1) {
    $avdates = $avdates-1;
}
$filterdate = filterdate($udate, $utime, $avdates);
$filtertime = filtertime($filterdate, $utime);


$udateval = get_udateval($filterdate, $filterdate);
$utimeval = get_utimeval($filtertime, $filtertime, $webischedopt,$offset,$tz);

/** for immediate webinar  **/
if($immediate == 1) {
    $udateval=  "<option value='rn'>".$lang['REG_FORM_TXT_RN_1']."</option>".$udateval;
}



?>

<!--  DISPLAYING THE DATE AND TIME DROPDOWN  -->



                        <div id="date">
                            <div id="date-title"><?php echo $lang['REG_FORM_TXT_SELECTDATE'];?></div>
                            <div id="date-select">
                                <select name="date" id="dates">
                                    <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELDATE'];?></option>
                                    <?php echo $udateval; ?>
                                </select>
                            </div> <!-- end date-select -->
                                <div class="clear"></div>
                        </div> <!-- end date -->



                        <div id="time">
                            <div id="time-title"><?php echo $lang['REG_FORM_TXT_SELECTTIME'];?></div>
                            <div id="time-select">
                                <select name="time" id="times">
                                    <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELTIME'];?></option>
                                    <?php //echo $utimeval; ?>
                                </select>
                            </div> <!-- end time-select -->
                            <div class="clear"></div>
                      </div>



<script language="javascript">
$(document).ready(function(){

    $("#dates").change(function() {


        var typeval = $('#dates').val();
         $.ajax({
                    type: "POST",
                    url: "modules/regdatetime/functime.php",
                    <?php if($webischedopt == '1'){ ?>
                    data: "tzone=<?php echo $webi_tz;?>&id=<?php echo $id;?>" + "&sdate=" + typeval + "&udate=<?php echo base64_encode(serialize($filterdate));?>"+ "&utime=<?php echo base64_encode(serialize($utime));?>" + "&schedopt=<?php echo $webischedopt;?>" + "&offset=<?php echo $offset;?>&memberid=<?php echo $_GET['memberid'];?>&webid=<?php echo $_GET['webid'];?>",
                    <?php } else { ?>
                    data: "tzone="+ response_text + "&id=<?php echo $id;?>" + "&sdate=" + typeval + "&udate=<?php echo base64_encode(serialize($filterdate));?>"+ "&utime=<?php echo base64_encode(serialize($utime));?>" + "&schedopt=<?php echo $webischedopt;?>" + "&offset=<?php echo $offset;?>&memberid=<?php echo $_GET['memberid'];?>&webid=<?php echo $_GET['webid'];?>",
                    <?php } ?>
                    success: function(data){

                    $("#time").html(data);

                    }
        });
    });
});
</script>