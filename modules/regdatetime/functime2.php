<?php

$webid      =  $_POST['webid'];
$memberid   =  $_POST['memberid'];
include "../../functions.php";
include "../../loadinfo.php";
include '../../languages/'.$langtype.'/lang.'.$langtype.'.php';
include 'func.php';

$id = $_POST['id'];
$sdate = $_POST['sdate'];
$udate = $_POST['udate'];
$utime = $_POST['utime'];
$offset = $_POST['offset'];
$schedopt = $_POST['schedopt'];
$tzone = $_POST['tzone'];
$udates = unserialize(base64_decode($udate));
$utimes = unserialize(base64_decode($utime));


date_default_timezone_set($tzone);

    if(is_array($udates)) {
        $tmp = $utimes;
        global $type;
        global $vidlength;

        /** get the biggest video legnth to add into webinar time as a final expired time **/
        $t = unserialize($vidlength);
        $vidhours = array();
        if(is_array($t)) {
            foreach($t as $ob)
            {
                if(is_array($ob)) {
                    foreach($ob as $ob2) {
                        $vidhours[] = $ob2;
                    }
                } else {
                        $vidhours[] = $ob;
                }
            }
        }

        $maxvidhour = max($vidhours)/ 3600;



            for($a=0; $a<count($utimes);$a++) {
                $datenow = date('Y-M-d H:i:s');
                //$resdate = strtotime("$sdate $utimes[$a]:00:00") - strtotime("$datenow");
                if($type != 2) {
                    $resdate = strtotime("$sdate $utimes[$a]:00:00") - strtotime("$datenow");
                } else {
                    $thetime = $utimes[$a] + $maxvidhour;
                    $thetime2 =  convertTime($thetime);
                    $resdate = strtotime("$sdate $thetime2") - strtotime("$datenow");
                }
                    if($resdate < 1) {
                        unset($tmp[$a]);
                    }
            }

        $tmp = array_values($tmp);
        $filtertime = get_utimeval($tmp,$tmp,$schedopt,$offset,$tzone);
    }



?>

        <select name="time" id="times">
        <?php if($filtertime == '') {  ?>
        <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELTIME'];?></option>
        <?php } else {  ?>
        <option selected="selected"><?php echo $lang['REG_FORM_TXT_SELTIME_2'];?></option>
        <?php } ?>
             <?php if($sdate == 'rn'){ ?>
            <option value="rn"><?php echo $lang['REG_FORM_TXT_RN_2'];?></option>
            <?php }else{ echo  $filtertime; } ?>
        </select>
