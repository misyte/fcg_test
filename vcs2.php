<?php session_start();

    $Filename = "webinar-alert.vcs";
    header("Content-Type: text/x-vCalendar");
    header("Content-Disposition: inline; filename=$Filename");
    include 'loadinfo.php';
?>
<?php
                $datetimereg=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00");
                if($webischedopt == 1) {
                    $yourtimelabel=makeoffsetcity2($webischedopt,$webi_tz);

                } else {
                    $yourtimelabel='Your Local Time';

                }
                $yourtimelabel=$yourtimelabel." ".$_SESSION['yourtimelabel'];
            ?>
BEGIN:VCALENDAR
VERSION:1.0
BEGIN:VEVENT
CATEGORIES:MEETING
DTSTART:<?php echo date("Ymd",strtotime($_SESSION['wdate3'])); ?>T<?php if(strlen($_SESSION['wtime3'])==1) echo "0".$_SESSION['wtime3']; else echo $_SESSION['wtime3']; ?>0000
<?php $_SESSION['wtime3']=$_SESSION['wtime3']+1; ?>
DTEND:<?php echo date("Ymd",strtotime($_SESSION['wdate3'])); ?>T<?php if(strlen($_SESSION['wtime3'])==1) echo "0".$_SESSION['wtime3']; else echo $_SESSION['wtime3']; ?>0000
SUMMARY:WEBINAR ALERT
DESCRIPTION;ENCODING=QUOTED-PRINTABLE:WEBINAR ALERT=0D=0A=0D=0A- Webinar: <?php echo $_SESSION['wtitle']; ?>=0D=0A- Host: <?php echo $_SESSION['whost']; ?>=0D=0A- Starting time: <?php echo $_SESSION['wdate']; ?> at <?php echo date("g:00 a",$datetimereg)." ".$yourtimelabel; ?>=0D=0A- Access the webinar room here: <?php echo $_SESSION['wlive']; ?>
CLASS:PRIVATE
END:VEVENT
END:VCALENDAR