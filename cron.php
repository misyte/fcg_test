<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

include 'config.php';
include 'loadinfo.php';
$iscron = 1;
include 'split-register.php';
include 'mailer.php';
include 'languages/'.$langtype.'/lang.'.$langtype.'.php';

/** Start cron Infusionsoft Modifications **/
$infsoft_actionset['registered_notattended'] =  $infsoft_as_registered_notattended;
$infsoft_actionset['attended_notsawoffer'] =    $infsoft_as_attended_notsawoffer;
$infsoft_actionset['sawoffer_notpurchased'] =   $infsoft_as_sawoffer_notpurchased;
$infsoft_actionset['attended_startedoffer'] =   $infsoft_as_attended_startedoffer;
$infsoft_actionset['attended_stayed_point_1'] =   $infsoft_as_attended_stayed_point_1;

$infsoft_actionlist = array(
    'registered_notattended',
    'attended_stayed_point_1',
    'attended_notsawoffer',
    'attended_startedoffer',
    'sawoffer_notpurchased'
);
/** End Modifications **/



if($autoresponderopt==0 || $autoresponderopt==1  || $autoresponderopt==2)
{

    /**  vid length **/
    $get_vid_lengthz = unserialize(stripslashes($vidlength));
    $totalvidlength = $get_vid_lengthz[0];

    /** get the max post hour **/
    $postmax="SELECT MAX(post_hour) as max_post_hour FROM post_email WHERE memberid=".$memberid." AND webid='".$webid."'";
    $postmaxresult=mysql_query($postmax);
    if($postmaxrow=mysql_fetch_object($postmaxresult)) {
        $postmaxhour = $postmaxrow->max_post_hour;
    } else {
        if($totalvidlength > 3600) {
            $postmaxhour = @ceil(round(($totalvidlength/3600),1));
        } else {
            $postmaxhour = 1;
        }
    }
    if($postmaxhour=="") {
        if($totalvidlength > 3600) {
            $postmaxhour = @ceil(round(($totalvidlength/3600),1));
        } else {
            $postmaxhour = 1;
        }
    }
    /** end **/

    if($postmaxhour=="") $postmaxhour = 0;
    /** get all timezones per user  **/
    if($webischedopt == 1) {
        $sql="SELECT tz_admin FROM users WHERE memberid=".$memberid." AND webid='".$webid."' GROUP BY tz_admin";
    } else {
        $sql="SELECT tz FROM users WHERE memberid=".$memberid." AND webid='".$webid."' GROUP BY tz";
    }

    $resulta=mysql_query($sql);


    while($row=mysql_fetch_assoc($resulta))
    {


        if($webischedopt == 1) {
            $tz = $webi_tz;
        } else {
            $tz =$row['tz'];
        }

        date_default_timezone_set($tz);
        $datenow = date('Y-M-d H:i:s');
        $datenowtime = date('Y-M-d H:00:00');
        $datenowtime2 = date('Y-M-d');

        $datenowset = strtotime($datenowtime);
        $postdateset = strtotime(date('Y-M-d H:00:00') . ' -'.$postmaxhour.' hours');
        $zerotzdate =  strtotime(date('Y-M-d H:00:00') . ' -'.$postmaxhour.' hours');
        $postdate = date('Y-M-d',$zerotzdate);
        $postdate2 = date('Y-M-d',$zerotzdate);

        echo 'post date: '. date('Y-M-d H:00:00',$postdateset);

        if($webischedopt == 1) {
            $query = "SELECT dateset,webtime,email,tz_admin FROM users WHERE memberid=".$memberid."
            AND webid='".$webid."' AND tz_admin='".$tz."' AND (tz_admin_dateset >= $datenowset OR (tz_admin_dateset <= $datenowset AND tz_admin_dateset >= $postdateset))


            GROUP BY dateset,webtime";

        } else {
            $query = "SELECT dateset,webtime,email,tz FROM users WHERE memberid=".$memberid."
            AND webid='".$webid."'  AND tz='".$tz."' AND (tz_local_dateset >= $datenowset OR (tz_local_dateset <= $datenowset AND tz_local_dateset >= $postdateset))

            GROUP BY dateset,webtime";
        }



        $qresult=mysql_query($query);
        while($qrow=mysql_fetch_object($qresult)) {

            if($webischedopt == 1) {
                $tz = $qrow->tz_admin;
            } else {
                $tz =$qrow->tz;
            }

            $datesets = $qrow->dateset;
            $hourset = $qrow->webtime;
            $email = $qrow->email;
            $tz = $tz;

            #echo $query.'<br><br>';
            #echo $postdateset.'<br><br>';
            echo 'email: '.$email.' date: '.$datesets.' time: '.$hourset.' timezone: '.$tz.'<br>';


            /** if the date and time are in immediate schedule **/
            if(is_true_float($hourset) && $immediate== 1 && $hourset != 0.25){
                $pieces = explode('.',$hourset);
                $pmin =  ('.'.$pieces[1]) * 60;
                $dateset = "$datesets $pieces[0]:$pmin:00";
            } else {
                $dateset = "$datesets $hourset:00:00";
            }
            /** end **/

            if($tz=="Europe/Moscow") $dif = strtotime("$dateset") - (strtotime("$datenow") + 3600);
            else $dif = strtotime("$dateset") - (strtotime("$datenow"));



/**----------------------------------------------------**/
# PRE EMAIL NOTIFICATION
/**----------------------------------------------------**/

            $sql0="SELECT * FROM pre_email WHERE memberid=".$memberid." AND webid='".$webid."'";
            $result0=mysql_query($sql0);
            while($row0=mysql_fetch_assoc($result0)) {

                $set_hour=$row0['pre_hour']*3600;
                /**  is_15minutes **/

                if($is_15mins == 0) {
                    if($row0['pre_column_name'] == 'pre15min') {
                        $set_hour = 0;
                        continue;
                    }
                } /** end **/

                if($set_hour == 900){ $set_hour = 1800; }
                # echo ($set_hour-1800)."<= $dif AND $set_hour >= $dif<br>";
                if(($set_hour-1800)<=$dif && $set_hour>=$dif) {

                    $webi_presenter=unserialize(stripslashes($presenter));
                    $get_npresenter =  unserialize(stripslashes($npresenter));
                    if($get_npresenter[$page] != '') {
                        $presentersname = $webi_presenter[$page].' & '.$get_npresenter[$page];
                    }else{
                        $presentersname = $webi_presenter[$page];
                    }

                    $get_presenter =  unserialize(stripslashes($presenter));
                    $get_admin_email =  unserialize(stripslashes($admin_email));

                    $message=stripslashes(stripslashes(stripslashes(stripslashes(stripslashes($row0['pre_message'])))));
                    $subject=stripslashes(stripslashes(stripslashes(stripslashes(stripslashes($row0['pre_subject'])))));

                    $webi_topic=unserialize(stripslashes($topic));
                    $webi_presenter=unserialize(stripslashes($presenter));

                    $subject = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $subject);
                    $subject = str_replace("#WEBINAR_PRESENTER#",$presentersname, $subject);
                    $subject = str_replace("#WEBINAR_DATE#",convertdatelang($datesets,'wc'), $subject);
                    $subject = str_replace("#WEBINAR_TIME#",get12hr($hourset).' '.makeoffsetcity($webischedopt,$webi_tz), $subject);

                    $message = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $message);
                    $message = str_replace("#WEBINAR_PRESENTER#",$presentersname, $message);
                    $message = str_replace("#WEBINAR_DATE#",convertdatelang($datesets,'wc'), $message);
                    $message = str_replace("#WEBINAR_TIME#",get12hr($hourset).' '.makeoffsetcity($webischedopt,$webi_tz), $message);

                    $origpresubj = $subject;
                    $origpremsg = $message;

                    $from=$from_email;
                    $sendername=$from_name;
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                    $headers .= 'From: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n" .
                                'Reply-To: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n" .
                                'Return-Path: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n";



                    if($webischedopt == 1) {
                        $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND ".$row0['pre_column_name']."=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                    } else {
                        $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND ".$row0['pre_column_name']."=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz='".$tz."'";
                    }
                    $result1=mysql_query($sql1);

                    while($row1=mysql_fetch_assoc($result1))
                    {

                        $isrn = $row1['rn'];
                        if($isrn == 1)
                            continue;

                        $subject = $origpresubj;
                        $message = $origpremsg;
                        $subject = str_replace("#USER_NAME#",$row1['name'], $subject);
                        $subject = str_replace("#USER_EMAIL#",$row1['email'], $subject);
                        $subject = str_replace("#AFFILIATE#",$row1['affid'], $subject);
                        $message = str_replace("#USER_NAME#",$row1['name'], $message);
                        $message = str_replace("#USER_EMAIL#",$row1['email'], $message);
                        $message = str_replace("#AFFILIATE#",$row1['affid'], $message);

                        $userid = $row1['userid'];
                        // getting the short url for live page
                        $qlive="SELECT short_url FROM links WHERE user_id=".$userid." AND type='live'";
                        $reslive=mysql_query($qlive);
                        if ($rlive=mysql_fetch_object($reslive)) {
                              $short_live = $rlive->short_url;
                        }

                        // getting the short url for replay page
                        $qreplay="SELECT short_url FROM links WHERE user_id=".$userid." AND type='replay'";
                        $resreplay=mysql_query($qreplay);

                        if ($rreplay=mysql_fetch_object($resreplay)) {
                              $short_replay = $rreplay->short_url;
                        }

                        if($hostingtype==1) {
                              $unsubscribe=$domainname.'/'.$webifolder.'/unsubscribe.php?m=';
                              $unsubscribe.=base64_encode($memberid).'&w='.base64_encode($webid).'&e='.base64_encode($row1['email']);
                        } else {
                              $unsubscribe=$domainname.'/'.$memberid.'/'.$webid.'/unsubscribe.php?m=';
                              $unsubscribe.=base64_encode($memberid).'&w='.base64_encode($webid).'&e='.base64_encode($row1['email']);
                        }

                        if($row1['unsubscribe']==0) {

                            $livepageURL=$domainname."/".$webifolder."/webinar-live.php?user=".base64_encode($row1['name'])."&email=";
                            $livepageURL.=base64_encode($row1['email'])."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($hourset)."&webidate=";
                            $livepageURL.=base64_encode($datesets)."&tz=".base64_encode($row1['tz'])."";
                            $replayURL=$domainname."/".$webifolder."/webinar-replay.php?user=".base64_encode($row1['name'])."&email=";
                            $replayURL.=base64_encode($row1['email'])."&webiname=".base64_encode($webiname)."&webitime=".base64_encode($hourset)."&webidate=";
                            $replayURL.=base64_encode($datesets)."";
                            $message=str_replace("#WEBINAR_LINK#",$short_live, $message);
                            $message=str_replace("#WEBINAR_REPLAY#",$short_replay, $message);
                            $message.= "<br><div style='margin-top:50px;font-size:8px;'><a href='".$unsubscribe."'>";
                            $message.="Click to stop receiving notifications for this webinar</a></div>";

                            $to=$row1['email'];
                            $mailsuccess = false;

                            /** send pre notification  **/
                            if(trim($subject) != '') {
                                if($autoresponderopt==0) {
                                // if own autoresponder, use builtin notification

                                    if($emailtype == 1) {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    elseif($emailtype == 2) {
                                        if(sendmail_smtp($arrsmtp,$to,$subject, $message)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    else {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }

                                }
                                elseif($autoresponderopt==1 && $autorespondertype==1) {
                                // if own autoresponder and autoresponder type is infusionsoft, use infusionsoft notif
                                    if(infusionsoft_sendmail($to,$from,$from_name,$subject,$message)) {
                                        $mailsuccess = true;
                                    }
                                }
                                elseif($autoresponderopt==2 && $autorespondertype==1 && $via_infusion==1) {
                                // if both autorresponder and autorespondertype is infusionsoft and notificationtype is infusionsoft, use infusionsoft notif
                                    if(infusionsoft_sendmail($to,$from,$from_name,$subject,$message)) {
                                        $mailsuccess = true;
                                    }
                                } else {
                                // use builtin notification
                                    if($emailtype == 1) {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    elseif($emailtype == 2) {
                                        if(sendmail_smtp($arrsmtp,$to,$subject, $message)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    else {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }

                                }

                                if($mailsuccess) {
                                    $result= mysql_query("UPDATE users SET ".$row0['pre_column_name']." = '1'
                                    WHERE memberid=".$memberid." AND webid='".$webid."' AND  webtime = '".$hourset."'
                                    AND  dateset='".$datesets."' AND email='".$row1['email']."' AND userid='".$row1['userid']."'")
                                    or die('Error: ' . mysql_error());
                                }
                            }   /** end send pre notification  **/
                        }
                    }
                }
            }



/**----------------------------------------------------**/
# SMS/VOICE QUERY
/**----------------------------------------------------**/

            $is_sms=unserialize(stripslashes($sms));
            $is_voice=unserialize(stripslashes($voice));
            if($is_sms[$page]==1)
            {
                $webi_topic=unserialize(stripslashes($topic));
                $webi_presenter=unserialize(stripslashes($presenter));
                $get_npresenter =  unserialize(stripslashes($npresenter));
                if($get_npresenter[$page] != '') {
                    $presentersname = $webi_presenter[$page].' & '.$get_npresenter[$page];
                }else{
                    $presentersname = $webi_presenter[$page];
                }
                $sms_msg1=unserialize(stripslashes($sms_msg));

                $sms_msg1[$page] = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $sms_msg1[$page]);
                $sms_msg1[$page] = str_replace("#WEBINAR_PRESENTER#",$presentersname, $sms_msg1[$page]);
                $sms_msg1[$page] = str_replace("#WEBINAR_DATE#",convertdatelang($datesets,'wc'), $sms_msg1[$page]);
                $sms_msg1[$page] = str_replace("#WEBINAR_TIME#",get12hr($hourset).' '.makeoffsetcity($webischedopt,$webi_tz), $sms_msg1[$page]);

                if(0<=$dif && 1800>=$dif)
                {
                    $country_code=array();
                    $cellphone=array();

                    if($webischedopt == 1) {
                        $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."' AND
                        sms_notification=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                    } else {
                        $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."' AND
                        sms_notification=0  AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz='".$tz."'";
                    }

                    $result1=mysql_query($sql1);
                    $voice_audio=unserialize(stripslashes($voice_audio));
                    $token=unserialize(stripslashes($token));
                    $voice_token=unserialize(stripslashes($voice_token));

                    while($row1=mysql_fetch_assoc($result1))
                    {

                        $userid = $row1['userid'];
                        // getting the short url for live page
                        $qlive="SELECT short_url FROM links WHERE user_id=".$userid." AND type='live'";
                        $reslive=mysql_query($qlive);
                        if ($rlive=mysql_fetch_object($reslive)) {
                            $short_live = $rlive->short_url;
                        }

                        // getting the short url for replay page
                        $qreplay="SELECT short_url FROM links WHERE user_id=".$userid." AND type='replay'";
                        $resreplay=mysql_query($qreplay);
                        if ($rreplay=mysql_fetch_object($resreplay)) {
                            $short_replay = $rreplay->short_url;
                        }

                        $livepageURL=$domainname."/".$webifolder."/webinar-live.php?user=";
                        $livepageURL.=base64_encode($row1['name'])."&email=".base64_encode($row1['email'])."&webiname=";
                        $livepageURL.=base64_encode($webiname)."&webitime=".base64_encode($hourset)."&webidate=".base64_encode($datesets)."&tz=";
                        $livepageURL.=base64_encode($row1['tz'])."";
                        $replayURL=$domainname."/".$webifolder."/webinar-replay.php?user=";
                        $replayURL.=base64_encode($row1['name'])."&email=".base64_encode($row1['email'])."&webiname=".base64_encode($webiname)."&webitime=";
                        $replayURL.=base64_encode($hourset)."&webidate=".base64_encode($datesets)."";
                        $sms_msg1[$page] = str_replace("#WEBINAR_LINK#",$short_live, $sms_msg1[$page]);
                        $sms_msg1[$page] = str_replace("#WEBINAR_REPLAY#",$short_replay, $sms_msg1[$page]);

                        $url="https://api.tropo.com/1.0/sessions?action=create&token=".$token[$page]."&numberToDial=";
                        $url.=$row1['phone_number']."&msg=".urlencode($sms_msg1[$page]);


                        $curl_handle=curl_init();

                        curl_setopt($curl_handle,CURLOPT_URL,$url);
                        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
                        curl_exec($curl_handle);
                        curl_close($curl_handle);

                        if($is_voice[$page]==1){
                            $url="https://api.tropo.com/1.0/sessions?action=create&token=".$voice_token[$page]."&numberToDial=";
                            $url.=$row1['phone_number']."&voice_audio=".urlencode($voice_audio[$page]);
                            $curl_handle=curl_init();
                            curl_setopt($curl_handle,CURLOPT_URL,$url);
                            curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
                            curl_exec($curl_handle);
                            curl_close($curl_handle);
                        }

                        $country_code[]=$row1['country_code'];
                        $cellphone[]=$row1['cellphone'];
                        $sql2="UPDATE users SET sms_notification=1 WHERE userid=".$row1['userid'];
                        $result2=mysql_query($sql2);
                    }
                }
            }



/**----------------------------------------------------**/
# POST EMAIL QUERY
/**----------------------------------------------------**/

            $dif = (strtotime("$datenow")) - strtotime("$dateset") ;
            $sql0="SELECT * FROM post_email WHERE memberid=".$memberid." AND webid='".$webid."'";
            $result0=mysql_query($sql0);
            while($row0=mysql_fetch_assoc($result0))
            {

                $webi_presenter=unserialize(stripslashes($presenter));
                $get_npresenter =  unserialize(stripslashes($npresenter));
                if($get_npresenter[$page] != '') {
                    $presentersname = $webi_presenter[$page].' & '.$get_npresenter[$page];
                }else{
                    $presentersname = $webi_presenter[$page];
                }

                $get_presenter =  unserialize(stripslashes($presenter));
                $get_admin_email =  unserialize(stripslashes($admin_email));
                $message=stripslashes(stripslashes(stripslashes(stripslashes(stripslashes($row0['post_message'])))));
                $subject=stripslashes($row0['post_subject']);

                $webi_topic=unserialize(stripslashes($topic));
                //$webi_presenter=unserialize(stripslashes($presenter));

                $subject = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $subject);
                $subject = str_replace("#WEBINAR_PRESENTER#",$presentersname, $subject);

                $message = str_replace("#WEBINAR_TOPIC#",$webi_topic[$page], $message);
                $message = str_replace("#WEBINAR_PRESENTER#",$presentersname, $message);

                $origpostsubj = $subject;
                $origpostmsg = $message;

                $from=$from_email;
                $sendername=$from_name;
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                $headers .= 'From: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n" .
                            'Reply-To: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n" .
                            'Return-Path: '.$sendername.' <do_not_reply@eWebinars.com>' . "\r\n";

                $set_hour=$row0['post_hour']*3600;
                #echo ($set_hour-1800)."<= $dif AND $set_hour >= $dif <br>";
                if(($set_hour)<=$dif && ($set_hour+1800)>=$dif)
                {
                    if($webischedopt == 1) {

                        if($row0['post_sendto']==1) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==2) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==3) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=1 AND saw_offer=0 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==4) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND saw_offer=1 AND purchased=0 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==5) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND purchased=1 AND webtime = '".$hourset."' AND dateset='".$datesets."'
                            AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==6) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND purchased=0 AND webtime = '".$hourset."' AND dateset='".$datesets."'
                            AND tz_admin='".$tz."'";
                        }
                        elseif($row0['post_sendto']==7) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=1 AND webtime = '".$hourset."' AND dateset='".$datesets."'
                            AND tz_admin='".$tz."'";
                        } else {
                            // do nothing
                        }

                    } else {

                        if($row0['post_sendto']==1) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==2) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==3) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=1 AND saw_offer=0 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==4) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND saw_offer=1 AND purchased=0 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==5) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND purchased=1 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==6) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND purchased=0 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz='".$tz."'";
                        }
                        elseif($row0['post_sendto']==7) {
                            $sql1="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                            AND ".$row0['post_column_name']."=0 AND attended=1 AND webtime = '".$hourset."'
                            AND dateset='".$datesets."' AND tz='".$tz."'";
                        } else {
                            //do nothing
                        }
                    }

                    $result1=mysql_query($sql1);
                    while($row1=mysql_fetch_assoc($result1))
                    {

                        $subject = $origpostsubj;
                        $message = $origpostmsg;
                        $subject = str_replace("#USER_NAME#",$row1['name'], $subject);
                        $subject = str_replace("#USER_EMAIL#",$row1['email'], $subject);
                        $subject = str_replace("#AFFILIATE#",$row1['affid'], $subject);
                        $message = str_replace("#USER_NAME#",$row1['name'], $message);
                        $message = str_replace("#USER_EMAIL#",$row1['email'], $message);
                        $message = str_replace("#AFFILIATE#",$row1['affid'], $message);


                        if($row1['rn'] == 1) {
                            $subject = str_replace("#WEBINAR_DATE#",$lang['REG_FORM_TXT_RN_2'], $subject);
                            $subject = str_replace("#WEBINAR_TIME#",$lang['REG_FORM_TXT_RN_2'], $subject);
                        } else {
                            $subject = str_replace("#WEBINAR_DATE#",convertdatelang($datesets,'wc'), $subject);
                            $subject = str_replace("#WEBINAR_TIME#",get12hr($hourset).' '.makeoffsetcity($webischedopt,$webi_tz), $subject);
                        }

                        if($row1['rn'] == 1) {
                            $message = str_replace("#WEBINAR_DATE#",$lang['REG_FORM_TXT_RN_2'], $message);
                            $message = str_replace("#WEBINAR_TIME#",$lang['REG_FORM_TXT_RN_2'], $message);
                        } else {
                            $message = str_replace("#WEBINAR_DATE#",convertdatelang($datesets,'wc'), $message);
                            $message = str_replace("#WEBINAR_TIME#",get12hr($hourset).' '.makeoffsetcity($webischedopt,$webi_tz), $message);
                        }


                        $userid = $row1['userid'];
                       // getting the short url for live page
                        $qlive="SELECT short_url FROM links WHERE user_id=".$userid." AND type='live'";
                        $reslive=mysql_query($qlive);
                        if ($rlive=mysql_fetch_object($reslive)) {
                              $short_live = $rlive->short_url;
                        }

                      // getting the short url for replay page
                        $qreplay="SELECT short_url FROM links WHERE user_id=".$userid." AND type='replay'";
                        $resreplay=mysql_query($qreplay);
                        if ($rreplay=mysql_fetch_object($resreplay)) {
                              $short_replay = $rreplay->short_url;
                        }


                        if($hostingtype==1) {
                            $unsubscribe=$unsubscribe=$domainname.'/'.$webifolder.'/unsubscribe.php?m='.base64_encode($memberid).'&w=';
                            $unsubscribe.=base64_encode($webid).'&e='.base64_encode($row1['email']);
                        }   else {
                            $unsubscribe=$unsubscribe=$domainname.'/'.$memberid.'/'.$webid.'/unsubscribe.php?m='.base64_encode($memberid).'&w=';
                            $unsubscribe.=base64_encode($webid).'&e='.base64_encode($row1['email']);
                        }

                        if($row1['unsubscribe']==0  && !($row0['post_sendto'] == 2 && $row1['rn'] == 1)) {

                            $livepageURL=$domainname."/".$webifolder."/webinar-live.php?user=";
                            $livepageURL.=base64_encode($row1['name'])."&email=".base64_encode($row1['email'])."&webiname=";
                            $livepageURL.=base64_encode($webiname)."&webitime=".base64_encode(($hourset))."&webidate=".base64_encode($datesets);
                            $livepageURL.="&tz=".base64_encode($row1['tz'])."";
                            $replayURL=$domainname."/".$webifolder."/webinar-replay.php?user=";
                            $replayURL.=base64_encode($row1['name'])."&email=".base64_encode($row1['email'])."&webiname=".base64_encode($webiname);
                            $replayURL.="&webitime=".base64_encode(($hourset))."&webidate=".base64_encode($datesets)."";
                            $message=str_replace("#WEBINAR_LINK#",$short_live, $message);
                            $message=str_replace("#WEBINAR_REPLAY#",$short_replay, $message);
                            $message.="<br><div style='margin-top:50px;font-size:8px;'><a href='".$unsubscribe."'>";
                            $message.="Click to stop receiving notifications for this webinar</a></div>";

                            $to=$row1['email'];
                            $mailsuccess = false;

                          /** send post notification  **/
                            if(trim($subject) != '') {
                                if($autoresponderopt==0) {
                                // if own autoresponder, use builtin notification
                                    if($emailtype == 1) {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    elseif($emailtype == 2) {
                                        if(sendmail_smtp($arrsmtp,$to,$subject, $message)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    else {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }

                                }
                                elseif($autoresponderopt==1 && $autorespondertype==1) {
                                // if own autoresponder and autoresponder type is infusionsoft, use infusionsoft notif
                                    if(infusionsoft_sendmail($to,$from,$from_name,$subject,$message)) {
                                        $mailsuccess = true;
                                    }
                                }
                                elseif($autoresponderopt==2 && $autorespondertype==1 && $via_infusion==1) {
                                // if both autorresponder and autorespondertype is infusionsoft and notificationtype is infusionsoft, use infusionsoft notif
                                    if(infusionsoft_sendmail($to,$from,$from_name,$subject,$message)) {
                                        $mailsuccess = true;
                                    }
                                } else {
                                // use builtin notification
                                    if($emailtype == 1) {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    elseif($emailtype == 2) {
                                        if(sendmail_smtp($arrsmtp,$to,$subject, $message)) {
                                            $mailsuccess = true;
                                        }
                                    }
                                    else {
                                        if(sendmail($to, $from, $from_name, $subject, $message, $domainname, $from_email, $email_login_username, $email_login_password)) {
                                            $mailsuccess = true;
                                        }
                                    }

                                }

                                if($mailsuccess) {
                                     $result= mysql_query("UPDATE users SET ".$row0['post_column_name']." = '1'
                                     WHERE memberid=".$memberid." AND webid='".$webid."' AND  webtime = '".$hourset."'
                                     AND  dateset='".$datesets."' AND email='".$row1['email']."' AND userid='".$row1['userid']."'")
                                     or die('Error: ' . mysql_error());
                                }
                            }   /** end send post notification  **/
                        }
                    }
                }
            }/**  end post email   **/

/**----------------------------------------------------**/
# Office Autopilot implementation
/**----------------------------------------------------**/
if($oap_appid!="" && $oap_apikey!=""){
    if($autorespondertype == '3'){
            $oap_dif = strtotime("$dateset") - strtotime("$datenow");
            $get_vid_length = unserialize(stripslashes($vidlength));
            // minus 15 mins adv to trigger actionsets. First vidlength is use as time to send the infusionsoft post actionset
            $vidlen = $get_vid_length[0] * (-1);

            //echo $inf_dif .'< '.$vidlen;

                if($oap_dif < $vidlen) {
                    $oapflag = false;

                    $tz_sql="";
                    if($webischedopt == 1) {
                        $tz_sql=" AND tz_admin='".$tz."'";
                    }
                    else{
                        $tz_sql=" AND tz='".$tz."'";
                    }

                    $sql_oap="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                         AND attended=0 AND webtime = '".$hourset."' AND dateset='".$datesets."'  $tz_sql";

                    $sql_oap_result = mysql_query($sql_oap);
                        while($sql_oap_row = mysql_fetch_assoc($sql_oap_result)) {
                        $uid=$sql_oap_row['userid'];

                        $oap_curl=curlURL($domainname."/$webifolder/oa_functions.php?a=$oap_appid&k=$oap_apikey&c=registerednotattended&userid=$uid&webi=".urlencode($webiname));

                        }

                    $sql_oap="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                         AND attended=1 AND saw_offer=0 AND webtime = '".$hourset."' AND dateset='".$datesets."'  $tz_sql";

                    $sql_oap_result = mysql_query($sql_oap);
                        while($sql_oap_row = mysql_fetch_assoc($sql_oap_result)) {
                            $uid=$sql_oap_row['userid'];

                        $oap_curl=curlURL($domainname."/$webifolder/oa_functions.php?a=$oap_appid&k=$oap_apikey&c=attendednotsawoffer&userid=$uid&webi=".urlencode($webiname));
                        }
                }
        }
    }

/**----------------------------------------------------**/
# INFUSIONSOFT POST EMAIL QUERY
/**----------------------------------------------------**/

            $inf_dif = strtotime("$dateset") - strtotime("$datenow");
            $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'";
            $get_vid_length = unserialize(stripslashes($vidlength));
            // minus 15 mins adv to trigger actionsets. First vidlength is use as time to send the infusionsoft post actionset
            $vidlen = $get_vid_length[0] * (-1);

            //echo $inf_dif .'< '.$vidlen;

                if($inf_dif < $vidlen) {
                    $infflag = false;
                foreach($infsoft_actionlist as $action) {
                                $tz_sql="";
                    if($webischedopt == 1) {
                        $tz_sql=" AND tz_admin='".$tz."'";
                    }
                    else{
                        $tz_sql=" AND tz='".$tz."'";
                    }
                    if ($infsoft_actionset['registered_notattended'] > 0 && $action == 'registered_notattended') {
                        $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND infsoft_" . $action . " = 0 AND attended=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' $tz_sql";
                        $infflag = true;
                    }
                    elseif ($infsoft_actionset['attended_notsawoffer'] > 0 && $action == 'attended_notsawoffer') {
                        $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND infsoft_" . $action . " = 0 AND attended=1 AND saw_offer=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' $tz_sql";
                        $infflag = true;
                    }
                    elseif ($infsoft_actionset['sawoffer_notpurchased'] > 0 && $action == 'sawoffer_notpurchased') {
                        $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND infsoft_" . $action. " = 0 AND saw_offer=1 AND purchased=0 AND webtime = '".$hourset."' AND dateset='".$datesets."' $tz_sql";
                        $infflag = true;
                    }
                    elseif ($infsoft_actionset['attended_stayed_point_1'] > 0 && $action == 'attended_stayed_point_1') {
                        $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND infsoft_" . $action. " = 0 AND saw_offer=0 AND stayed_point_1=1 AND webtime = '".$hourset."' AND dateset='".$datesets."' $tz_sql";
                        $infflag = true;
                    }
                    elseif ($infsoft_actionset['attended_startedoffer'] > 0 && $action == 'attended_startedoffer') {
                        $sql_inf="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."'
                        AND infsoft_" . $action. " = 0 AND attended=1 AND started_offer=1 AND webtime = '".$hourset."' AND dateset='".$datesets."' $tz_sql";
                        $infflag = true;
                    } else {
                        $infflag = false;
                    }

                    if($infflag) {
                        $sql_inf_result = mysql_query($sql_inf);
                        while($sql_inf_row = mysql_fetch_assoc($sql_inf_result)) {
                            $sql_inf_result2 = infusionsoft_runactionset($action, $sql_inf_row['email'], $sql_inf_row['name']);
                            if(count($sql_inf_result2) > 0){
                                $sql_inf_result3 = mysql_query("UPDATE users SET infsoft_" . $action . " = '1' WHERE memberid=".$memberid."
                                AND webid='".$webid."' AND  webtime = '".$hourset."' AND  dateset='".$datesets."' AND email='".$sql_inf_row['email']."' $tz_sql")
                                or die('Error: ' . mysql_error());
                            }
                        }
                    }
                }
            }
        }
    }
}





?>