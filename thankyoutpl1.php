<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_600.font.js"></script>
<script type="text/javascript">
    <?php if($langtype == 'en') { ?>
        Cufon.replace('h1', { fontFamily: 'Myriad Pro', textShadow: '1px 1px rgba(0, 0, 0, 0.2)' });
    <?php } ?>
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
  $.ajax({
                        type: "POST",
                        url: "<?php echo $_SESSION['action_url']; ?>?",
                        data: "<?php echo $_SESSION['post_string']; ?>",
                        success: function(data){
                            $("#tot_loggedin").html(data);

                    }
                });





});
</script>
<?php echo stripslashes_deep($google_analytics_header); ?>

<style type="text/css">
body {
    margin:0;
    /*--background: #FFF url(webinar-files/bg3.jpg) repeat-x;--*/
    font: normal 14px Arial;
    color: #000000;
    overflow-x:hidden;
}


/*--container--*/
#wrapper{
    margin:0 auto;
    padding:0;
    width:900px;
}

#header{
    margin:0;
    padding:0;
    width:900px;
    text-align:left;
}

#header h1{
    text-align:left;
    color:#C00;
    font-size:40px;
    font-style:normal;
}
#header h2{
    text-align:left;
    color:#666;
    font-size:24px;
    font-style:italic;
    font-weight:normal;
    padding-top:15px;
}

#webinarinfo{
    padding:20px 0px 20px 0px;
    width:100%;
    text-align:center;
    background-color:#EFECE9;
    border-bottom:1px solid #333;
    border-top:1px solid #333;
    margin: 10px 0px 10px 0px;
}

#bottom{
    margin:0;
    padding:0;
    width:900px;
    height:24px;
}



/*--text--*/
p{
    margin:0;
}

h1{
    margin:0;
}

h2{
    margin:0;
}

h3{
    margin:0;
    padding:8px 0;
}

h4{
    margin:0;
    padding:8px 0;
}

h5{
    margin:0;
    padding:8px 0;
}

h6{
    margin:0;
    padding:8px 0;
}

ul{
    list-style:none;
    margin:0 0 1em 15px;
    padding: 0;
}
ul li{
    line-height:1.5em;
    margin: .25em 0;
    padding: 0 0 0 30px;
    background:url(webinar-files/tick4.png) no-repeat 0px 0px;
}
ul li{
    line-height:1.5em;
    margin: .25em 0;
    padding: 0 0 0 30px;
    background:url(webinar-files/tick4.png) no-repeat 0px 0px;
}

h7{
    line-height:1.5em;
    margin: .25em 0;
    padding: 10px 0 10px 60px;
    background:url(webinar-files/arrow1.png) no-repeat 0px 0px;
}

/*--video--*/
.video{
    width:640px;
    margin:0 auto;
    background:url(webinar-files/bottom.jpg) no-repeat;
}


/*--foot information--*/
.footinfo{
    margin:10px auto;
    padding:10px 0;
    width:640px;
    border-top:1px solid #cdcdcd;
    color:#666;
    font:normal 10px verdana. helvetica, arial, sans-serif;
    text-align:center;
}
.calendars{
    list-style-type:none;
    padding-left:0px;
    }
.calendars li img{
    border:none;
}

.calendars li{
    display:inline-block;background:none;
    padding:0 25px 0 0;
    }
    #navfooterlist {
    margin:0;
}
#navfooterlist li {
    background:none;padding:0px;
}
</style>
</head>

<body>
<div id="wrapper" style="padding-bottom:10px;">
    <div id="header">
    <h2><b><?php echo $lang['TY_PRESENTEDBY'];?>:</b> <?php echo stripslashes_deep($webi_presenter[$page]); ?></h2>
    <h1><?php echo stripslashes_deep($webi_topic[$page]); ?></h1>
    </div>

</div>

<!-- Buy Now/Add To Cart Button Start Here -->
<div id="webinarinfo">
<table style="margin:0 auto;" align="center" width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="420" rowspan="3"  align="left" valign="top">
    <div style="border:10px solid #E5E1DF; width:400px; height:225px;">
<!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
    <script type="text/javascript" src="player/jwplayer.js"></script>
    <!-- END OF THE PLAYER EMBEDDING -->
    <div id="player"></div>
    <script type="text/javascript">
        jwplayer('player').setup({
        'flashplayer': 'player/player.swf',
        'file': '<?php if($ty_vid == 1 && $ty_freebonus==1){ ?>http://s3.amazonaws.com/all-videos/webinarregisterthankyou1.mp4<?php }elseif($ty_vid == 1 && $ty_freebonus==0){ ?>http://s3.amazonaws.com/all-videos/webinarregisterthankyou2.mp4<?php }else{ echo $ty_vidurl; } ?>',
        'feature': 'related',
        'controlbar': 'over',
        'autostart': 'true',
        'width': '400',
        'height': '225',
        'plugins': ' '
    });
    </script>
<!-- VIDEO EMBED CODE END HERE -->
</div>
      </td>
  <tr>
    <td align="right" height="62" valign="top" style="padding-left:0px;"><a href="javascript:window.print()"><img src="<?php echo 'languages/'.$langtype.'/images/ty/print.png';?>" width="200" height="44" /></a></td>
  </tr>
      <td align="left" valign="bottom" style="padding-left:30px; padding-top:0px;">
    <span style="color:#C00; font-style:italic; font-weight:bold; font-family:Georgia, Arial, Tahoma; font-size:24px;"><?php echo $lang['TY_WEBINAR_DETAILS'];?>:</span>
 <ul style="margin-top:15px;">
 <?php

    $get_npresenter =  unserialize(stripslashes($npresenter));
    $is_affiliate=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=q');
    $a_affiliate_name=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$_COOKIE['affiliate_id'].'&q=n');

    if(trim($get_npresenter[$page]) != ''){
        $aff_new_presenter_name = $get_npresenter[$page];
    } else {
        $aff_new_presenter_name = $a_affiliate_name;
    }
 ?>
 <li style="padding-bottom:5px; font-size:14px;"><b><?php echo $lang['TY_WEBINAR'];?>:</b> <span style="color:#666;"><?php echo stripslashes_deep($webi_topic[$page]); ?></span></li>
    <li style="padding-bottom:5px; font-size:14px;"><b><?php echo $lang['TY_PRESENTER'];?>:</b> <span style="color:#666;"><?php echo stripslashes_deep($webi_presenter[$page]); ?> <?php if($is_affiliate==1 && $affiliate_info==1 && $affiliate_opt==1  || ($get_npresenter[$page] != '')){ ?> and <?php echo  $aff_new_presenter_name; ?> <?php } ?></span></li>
    <li style="padding-bottom:5px; font-size:14px;"><b><?php echo $lang['TY_SCHEDDATE'];?>:</b> <span style="color:#666;">

    <?php

    /** convert date language **/
    if(!$langmonths) {
        echo $datenowval =  date('jS \o\f F',strtotime( $_SESSION['wdate']));
    }
    elseif(is_array($langmonths) && is_array($langdays)) {
        echo $datenowval = convertdatelang($_SESSION['wdate'],'ty');

    } else {
        echo $datenowval =  date('jS \o\f F',strtotime( $_SESSION['wdate']));

    }


    ?>

    </span></li>
    <li style="padding-bottom:5px; font-size:14px;"><b><?php echo $lang['TY_SCHEDTIME'];?>:</b> <span style="color:#666;"><?php echo get12hr2($_SESSION['wtime']); echo makeoffsetcity($webischedopt,$webi_tz);?></span></li>
    </ul>
    </td>
  </tr>

</table>
</div>

<table style="margin:0 auto;" align="center" width="900" border="0" cellspacing="0" cellpadding="20" style="padding-top:10px;">
  <tr>
    <td colspan="2">
      <div style="margin-top:20px;background-color:#FFC; padding:15px 15px 15px 15px; border:2px dashed #F00; vertical-align:middle">

          <h7><b style="font-size:16px; "><?php echo $lang['TY_YOUR_WEBINAR_LINK'];?>:</b> <a href="<?php echo ($_SESSION['slink_live']); ?>" target="_blank"><?php echo ($_SESSION['slink_live']); ?></a></h7>

      </div>
    </td>
  </tr>
  <?php
        $datetimereg=strtotime($_SESSION['wdate']." ".$_SESSION['wtime'].":00");
        if($webischedopt == 1) {
            $yourtimelabel=makeoffsetcity2($webischedopt,$webi_tz);
        } else {
            $yourtimelabel=$lang['REG_FORM_TXT_LOCALTIME'];
        }
        $yourtimelabel=urlencode($yourtimelabel)." ".$_SESSION['yourtimelabel'];
    ?>
  <tr>
    <td width="450" align="center" valign="top" style="padding:30px;">
        <img src="<?php echo 'languages/'.$langtype.'/images/ty/3.gif';?>" width="310" height="60" /><br />
        <?php
                if (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== false)
                {
                    $outlook_topmargin="margin-top:-5px;";
                }
                else{
                    $outlook_topmargin="margin-top:-0px;";
                }
            ?>
        <ul class="calendars">
            <li><a href="vcs2.php?<?php echo "memberid=$memberid&webid=$webid"; ?>"><img src="webinar-files/calendar-ical.png" />
                <div style="font-size: 11px; color: rgb(63, 127, 253); line-height: 12px; position: absolute; margin-left: -10px; <?php echo $outlook_topmargin; ?>text-align: center; width: 85px;"><?php echo $lang['TY_TXT_I_CAL'];?></div>
                </a></li>
            <?php $time2=$_SESSION['wtime2']+1;  ?>
            <li><a target="_blank" href="http://www.google.com/calendar/event?action=TEMPLATE&text=WEBINAR ALERT&dates=<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($_SESSION['wtime2'])==1) echo "0".$_SESSION['wtime2']; else echo $_SESSION['wtime2']; ?>0000Z/<?php echo date("Ymd",strtotime($_SESSION['wdate2'])); ?>T<?php if(strlen($time2)==1) echo "0".$time2; else echo $time2; ?>0000Z&details=WEBINAR ALERT:%0A%0A- Webinar: <?php echo urlencode(addslashes($webi_topic[$page])); ?>%0A- Host: <?php echo addslashes($webi_presenter[$page]); ?>%0A- Starting time: <?php echo $_SESSION['wdate']; ?> at <?php echo date("g:00 a",$datetimereg)." ".$yourtimelabel; ?>%0A- Access the webinar room here: <?php echo urlencode($_SESSION['wlive']); ?>&location=&trp=true&sprop=<?php echo $domainname; ?>&sprop=name:<?php echo addslashes($from_name); ?>"><img src="webinar-files/calendar-google.png" />
            <div style="font-size: 11px; color: rgb(63, 127, 253); line-height: 12px; position: absolute; margin-left: -10px; <?php echo $outlook_topmargin; ?> text-align: center; width: 92px;"><?php echo $lang['TY_TXT_GOOGLE_CAL'];?></div>
            </a></li>
            <li><img src="webinar-files/calendar-outlook.png" />

            <div style="text-align: center; font-size: 11px; color: rgb(63, 127, 253); line-height: 12px; position: absolute; margin-left: -10px; <?php echo $outlook_topmargin; ?> width: 100px;"><?php echo $lang['TY_TXT_OUTLOOK_CAL'];?><br><?php echo $lang['TY_TXT_CLICK_VER'];?><br><a style="text-decoration:none;color:#3F7FFD;" href="vcs2.php?<?php echo "memberid=$memberid&webid=$webid"; ?>">2003</a>&nbsp; <a style="color:#3F7FFD;text-decoration:none" href="vcs.php?<?php echo "memberid=$memberid&webid=$webid"; ?>">2007</a>&nbsp;<a style="text-decoration:none;color:#3F7FFD;" href="vcs2.php?<?php echo "memberid=$memberid&webid=$webid"; ?>">2010</a></div></li>

        </ul>
    </td>
    <td width="450" align="center" valign="top" style="padding:30px;">
        <img src="<?php echo 'languages/'.$langtype.'/images/ty/4.gif';?>" width="310" height="60" /><br />

        <div style="width: 350px; background: none repeat scroll 0 0 #D0D0D0;    padding: 0 1px 1px;">
            <div style=" background: none repeat scroll 0 0 #EBEBEB;    border-top: 1px solid #F8F8F8;    padding: 9px 10px 6px;">

                <div id="fb-root"></div>
                <script src="http://connect.facebook.net/en_US/all.js#appId=203398326385631&amp;xfbml=1"></script>
                <fb:like href="<?php echo $domain_name;?>webinar-register.php" send="true" layout="box_count" width="50" show_faces="false" action="like" font=""></fb:like>

                <img src="webinar-files/facebook.jpg" class="socmed"  />
                <a href="#" target="_parent"><img class="socmed" src="webinar-files/twitter.jpg" onClick="window.open('http://twitter.com/share?url=<?php echo $domain_name;?>webinar-register.php&text=<?php echo addslashes($webi_presenter[$page]); ?> is conducting a great webinar on <?php echo addslashes($webi_topic[$page]); ?>. I have already signed up! You can register here: ','TwitterShare','width=600,height=400');" /></a>
                </div>
        </div>
    </td>
  </tr>
 <?php
if($ty_freebonus==1)
{ ?>
  <tr>
    <td colspan="2" style="padding-top:20px; padding-bottom:50px; border-top:1px solid #CCC;">
    <center><img src="<?php echo 'languages/'.$langtype.'/images/ty/refer.gif';?>" width="554" height="117" style="margin-bottom:20px;" /></center>
     <table width="100%" border="0" cellpadding="0" style="margin-top:10px;">

        <tr>
            <td style="width:584px;vertical-align:text-top;padding-right:20px;padding-left:30px;padding-bottom:30px;line-height:24px;"><br>
                    <?php


                            echo stripslashes_deep($freebonus_desc);

                    ?>
            </td>
            <td style="width:340px;vertical-align:text-top;text-align:center;">
                <br>
                    <?php
                    if($iv_opt==1)
                    {
                        $iv_img_rename=explode(".", $iv_img);
                        $imgsrc="webinar-files/".renamefilereg($iv_img,$memberid);
                        list($widths1, $heights1, $types1, $attrs1) = @getimagesize($imgsrc);


                        if($widths1>320) echo '<img src="'.$imgsrc.'" style="width:320px;height:auto;"  />';
                        else echo '<img src="'.$imgsrc.'" style="width:auto;height:auto;"  />';
                    }
                    else{ ?>


              <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
                                        <script type="text/javascript" src="player/jwplayer.js"></script>
                                        <!-- END OF THE PLAYER EMBEDDING -->
                                      <div style="text-align:center;"><div id="player2" style="background-color:#000000;"></div></div>
                                        <script type="text/javascript">
                                        jwplayer('player2').setup({
                                          'flashplayer': 'player/player.swf',
                                          'file': '<?php echo $iv_vid;?>',
                                          'feature': 'related',
                                          'controlbar': 'over',
                                          'autostart': 'false',
                                          'width': '320',
                                          'height': '265',
                                          'plugins': ' '
                                        });
                                        </script>







                <?php } ?>
            </td>
        </tr>
     </table>
    <?php
     include 'tellafriend/tellafriend.php';
     ?>

    </td>
  </tr>
 <?php
    }
 ?>
</table>
<!-- Buy Now/Add To Cart Button End Here -->


<div style="padding-top:20px; padding-top:20px; background-color:#EFECE9; border-top:3px solid #CCC; text-align:center; color:#999; font-size:12px;">
<!--<p>Disclaimer message</p>
<p style="color:#333; padding-top:20px;font-weight:bold;"></p>
<p style="padding-top:20px;padding-bottom:20px;">Affiliate link</p> -->
<?php  include 'footer.php'; ?>
</div>
<?php echo stripslashes_deep($google_analytics_body); ?>
</body>
<?php echo stripslashes_deep($google_analytics_footer); ?>
</html>