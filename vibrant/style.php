<style type="text/css">

@charset "utf-8";
*{
margin:0;
padding:0;
}


img, a img{
border:0 none;
}

img{
vertical-align:baseline;
}

a{
outline:0;
}

input:focus, textarea:focus, select:focus{
outline: none;
}

textarea {
resize: none;
}

/*img, div, input { behavior: url("iepngfix.htc") }*/


.imgCenter{
display:block;
margin:0 auto;
clear:both;
}

.imgRight{
float:right;
}

.imgLeft{
float:left;
}

.none{
display:none;
}

.underlined{
text-decoration:underline;
}

.textCenter{
text-align:center;
}

.textLeft{
text-align:left;
}

.textRight{
text-align:right;
}

.clear{
clear:both;
height:0;
line-height:0;
font-size:0;
}

body{
font-size:12px;
background:#ededed;
font-family:Arial, Helvetica, sans-serif;
}

/*-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: alpha(opacity=0);*/

#header{
    width:100%;
    height:116px;
    }

#header h1, #header h2{
    text-align:center;
    }

#header h1{
    font-size: 37px;
    line-height: 37px;
    padding-top: 25px;
    }

.whiteheadertext{
    color:#ededed;
    }

.blackheadertext{
    color:#272727;
    }

#header h2{
    font-size: 22px;
    line-height: 24px;;
    }

.h2{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'color:#3abeff;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color:#888888;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color:#777777;';
        if($get_regtplcolor[$page]=="yellow") echo 'color:#ffdc73;';
        if($get_regtplcolor[$page]=="green") echo 'color:#bae691;';
        if($get_regtplcolor[$page]=="red") echo 'color:#f66f6f;';
    ?>
    }


.header{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(vibrant/images/header-blue.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(vibrant/images/header-dark-grey.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(vibrant/images/header-light-gray.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(vibrant/images/header-yellow.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(vibrant/images/header-green.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(vibrant/images/header-red.jpg) repeat-x left top;';
    ?>

}

.wrapper{
    width:960px;
    margin:0 auto;
    display:block;
    }

.content{
    background:url(vibrant/images/content.jpg) repeat-y;
    margin-top:22px;
    }

.contenttop{
    background:url(vibrant/images/contenttop.jpg) no-repeat left top;
    }

.contentbottom{
    background:url(vibrant/images/contentbottom.jpg) no-repeat left bottom;
    padding-bottom: 25px;
    }

.leftbox{
    width:588px;
    float:left;
    margin:34px 0 0 31px;
    }

.leftbox h1{
    color: #CE0000;
    font-size: 29px;
    line-height: 30px;
    text-align: center;
    }

#videobox{
    width:560px;
    height:333px;
    margin:20px auto 22px;
    display:block;
    }

.video{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(vibrant/images/video-blue.gif) no-repeat left top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(vibrant/images/video-dark-grey.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(vibrant/images/video-light-grey.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(vibrant/images/video-yellow.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(vibrant/images/video-green.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(vibrant/images/video-red.jpg) no-repeat left top;';
    ?>

    }



.videocodebox{
    padding:10px;
    width:540px;
    height:300px;
    }

.reserve{
    float:right;
    width:289px;
    background:url(vibrant/images/reserve.jpg) repeat-y left top;
    margin:36px 31px 0 0;
    }

.inputbg{
    background:url(vibrant/images/inputbg.gif) no-repeat left top;
    width:241px;
    height:36px;
    margin:0 auto 15px;
    display:block;
    }

.inputbg input, .inputbg select{
    border: 0 none;
    color: #555555;
    font-size: 15px;
    margin: 8px 0 0 10px;
    width: 219px;
    }
.reserve h3{
    text-align:center;
    color:#222222;
    font-size:16px;
    margin-bottom:20px;
    }

.reserve h3 span{
    color:#ed0000;
    }

.sms{
    color: #555555;
    line-height: 20px;
    margin-bottom: 27px;
    text-align: center;
    }

input[type="image"]{
    margin:0 auto;
    display:block;
    }

.privacy{
    color:#d1ccbc;
    text-align:center;
    margin-bottom:10px;
    line-height:24px;
    font-size:12px;
    text-transform:uppercase;
    }

.privacy a{
    color:#d1ccbc;
    text-decoration:none;
    }

.privacy a:hover{
    text-decoration:underline;
    }

#footer{
    width:100%;
    height:auto;
    margin:23px 0 0 0;
    padding-top: 10px;
    padding-bottom: 15px;
    padding-left: 0;
    padding-right: 0;
    background-color: #232323;
    }

#footer p{
    text-align:center;
    color:#828282;
    line-height:47px;
    }

#footer p a {
    text-decoration:none;
    color:#828282;
    padding: 0 5px;
    }

#footer p a:hover{
    text-decoration:underline;
    }

.footer-black{
    background:url(vibrant/images/footer-black.jpg) repeat-x left top;
    }


.footer-dark-gray{
    background:url(vibrant/images/footer-dark-grey.jpg) repeat-x left top;
    }


/** footer css adds **/
#footer p{
    line-height: normal;
    }

#navfooterlist li
{
display: inline;
list-style-type: none;
padding-right:10px;

}
#navfooterlist li a{
    text-decoration:none;
    font-size: 12px;
}
/** end footer css adds **/

</style>