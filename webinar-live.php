<?php
error_reporting(E_ALL);
ini_set("display_errors", 0);
session_start();


include_once 'functions.php';
include 'loadinfo.php';
/** force url to load in http://domain.com  without www  **/
if (substr($_SERVER['HTTP_HOST'],0,3) == 'www' && substr($domainname,0,10) != 'http://www') {
    $shost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
    $actual_link = full_url();
    $actual_url = parse_url($actual_link);
    $ishttp = strtolower($actual_url['scheme']);

    header('HTTP/1.1 301 Moved Permanently');
    if( $ishttp == 'https') {
        header('Location: https://'.$shost.$_SERVER['REQUEST_URI']);
    } else {
        header('Location: http://'.$shost.$_SERVER['REQUEST_URI']);
    }
}/** end **/



include 'config.php';
include 'clear-chatbox.php';

$pagetype = 3;
//get affiliate ID if cookie is deleted or not set
if($affiliate_opt==1)
{

    if(!isset($_COOKIE['affiliate_id']))
    {
        $sql="SELECT userid, affid FROM users WHERE email = '".base64_decode($_GET['email'])."' AND webid = '".$_GET['webid']."' AND memberid ='".$_GET['memberid']."' AND webtime ='".base64_decode($_GET['webitime'])."' AND dateset = '".base64_decode($_GET['webidate'])."' ORDER BY userid DESC LIMIT 0,1";
        $result=@mysql_query($sql);
        if($row=mysql_fetch_assoc($result))
        {
            setcookie('affiliate_id',$row['affid'],time()+31622400);
            $_COOKIE['affiliate_id']=$row['affid'];
        }



    }

}


/**  user list  **/
$get_presenter =  unserialize(stripslashes($presenter));
$maxnum =  unserialize(stripslashes($maxnum_attendees));
$get_regheader = unserialize(stripslashes($regheader));
$get_regtxtheader1 = unserialize(stripslashes($reg_textheader1));
$get_regtxtheader2 = unserialize(stripslashes($reg_textheader2));
$get_headerimg = unserialize(stripslashes($reg_headerimg));
$get_real_fake = unserialize(stripslashes($real_fake));
$get_vid_length = unserialize(stripslashes($vidlength));
$admin_email = unserialize(stripslashes($admin_email));


include 'languages/'.$langtype.'/lang.'.$langtype.'.php';




/** condition1 for the direct access @webinar-live page **/
if(strtolower($_GET['access']) == '' || strtolower($_GET['access']) != 'ok') {
    include 'iscountdown.php';
} else {
    if($_GET['version'] >= 0 && $_GET['version'] <= $splitliveopt && $_GET['version'] != '') {
        $page = $_GET['version']-1;

    } else {

        $page =  rand(0,$splitliveopt-1);
        $page1 = rand(0,$splitregopt-1);
        $pageaccess = $page;
        $page1access = $page1;
    }
}
/** end **/

if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
    {
        if($maxnum[$page]>200){$temp_maxnum=$maxnum[$page]; $maxnum[$page]=200;
        }
    }
if($get_real_fake[$page]=="fake"){
    foreach($namelists[$page] as $namesss){
            $_SESSION['namelistsss'][]=urlencode($namesss);
    }
     $countername=$maxnum[$page];
}
elseif($get_real_fake[$page]=="real"){
    $namelist_real=array();
    $countername=0;
    $sqlr="SELECT * FROM users GROUP BY name ORDER BY userid DESC";
    $resultr=mysql_query($sqlr) or die('real names failed');
    while($row=mysql_fetch_assoc($resultr))
    {
        if($countername==$maxnum[$page]) break;

        $namelist_real[]=urlencode($row['name']);
        $countername++;
    }

    $_SESSION['namelistsss']=$namelist_real;
}
elseif($get_real_fake[$page]=="both"){
    $namelist_real=array();
    $countername=0;
    $sqlr="SELECT * FROM users GROUP BY name ORDER BY userid DESC";
    $resultr=mysql_query($sqlr) or die('real names failed');
    while($row=mysql_fetch_assoc($resultr))
    {
        if($countername==$maxnum[$page]) break;

        $namelist_real[]=urlencode($row['name']);
        $countername++;
    }

    if($countername!=$maxnum[$page])
    {
        $counterlist=0;
        $listss=array();
        $listss=$namelists[$page];
        while($countername!=$maxnum[$page])
        {
            $namelist_real[]=urlencode($listss[$counterlist]);

            $countername++;
            $counterlist++;
        }
    }

    $_SESSION['namelistsss']=$namelist_real;
}

if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
    {
        $maxnum[$page]=$temp_maxnum;
         if($maxnum[$page]>200){
             $countername=$temp_maxnum;
        }
    }

/** condition2 for the direct access @live page **/
if(strtolower($_GET['access']) != '' && strtolower($_GET['access']) == 'ok') {
    include 'split-live.php';
    $userlog="Me";
    if($broadcast_design == 0 || $broadcast_design == 1){
                include('broadcast/broadcast.php');
            }
            elseif($broadcast_design == 2)
            {   include('broadcast/broadcast2.php'); }
            elseif($broadcast_design ==3)
            {   include('broadcast/broadcast3.php'); }
    exit;
}
/** end */


//if($autoresponderopt==1 && empty($_POST['submit']) ){ /*header("Location: webinar-login.php");*/ }
//elseif(($autoresponderopt==1 || $autoresponderopt==0 || $autoresponderopt==2) && !empty($_POST['submit']))
if(($autoresponderopt==1 || $autoresponderopt==0 || $autoresponderopt==2) && !empty($_POST['submit']))
{
        $webidate = trim($_POST['date']);
        $webitime = trim($_POST['time']);
        $email = trim($_POST['email']);
        $userlog = trim($_POST['name']);
        $usertz = ($_POST['timezone']);
}
elseif($autoresponderopt==0 || $autoresponderopt==2 || $autoresponderopt==1)
{
    if(empty($_GET['email']) || empty($_GET['webidate']) || empty($_GET['webitime']) || empty($_GET['user']) )
    {
            header("Location: webinar-login.php");
    }


    $webidate = base64_decode($_GET['webidate']);
    $webitime = base64_decode($_GET['webitime']);
    $email = base64_decode($_GET['email']);
    $usertz = base64_decode($_GET['tz']);
    $userlog = addslashes(base64_decode($_GET['user']));


}

if($userlog == ''){
    $userlog = base64_decode($_GET['user']);
}
$usertest_sql="SELECT * FROM users WHERE memberid=".$memberid." AND webid='".$webid."' AND webtime='".$webitime."' AND  dateset='".$webidate."' AND email='".$email."' ";


$usertest_result=mysql_query($usertest_sql) or die('Error usertest.');
if($row=mysql_fetch_assoc($usertest_result))
{ ?>

<?php
        /** office autopilot **/
        $uid=$row['userid'];



        if($webischedopt == 1) {
            $tz = $webi_tz;
        } else {

            $tz = $usertz;
        }

        date_default_timezone_set($tz);

        $datenow = date('Y-M-d H:i:s');
        $resdate = strtotime("$webidate $webitime:00:00") - strtotime("$datenow");
        $dif = $resdate;
        $min=00;
        $sec=00;

        /** $dif value is 0 if RN is 1 in the database  **/
        if($row['rn']==1) $dif=0;


        $_SESSION['s_page'] = $page;
        $_SESSION['s_dif'] = $dif;
        $_SESSION['s_maxnum'] = $maxnum[$page];
        $_SESSION['s_userlog'] = $userlog;


        $query_string = $_SERVER["QUERY_STRING"];

       if( $dif <= 0 && abs($dif) < $get_vid_length[$page]) {
            if($broadcast_design == 0 || $broadcast_design == 1){
                include('broadcast/broadcast.php');
            }
            elseif($broadcast_design == 2)
            {   include('broadcast/broadcast2.php'); }
            elseif($broadcast_design ==3)
            {   include('broadcast/broadcast3.php'); }
        }

        elseif ( $dif < 0 && abs($dif) >= $get_vid_length[$page]) {
            include('broadcast/endbroadcast.php');

        }

        else {
            include('broadcast/countdown.php');

        }


}
elseif($email == $admin_email[0] && $userlog==$get_presenter[0])
{
     if($webischedopt == 1) {
            $tz = $webi_tz;
        } else {

            $tz = $usertz;
        }

        date_default_timezone_set($tz);

        $datenow = date('Y-M-d H:i:s');
        $resdate = strtotime("$webidate $webitime:00:00") - strtotime("$datenow");
        $dif = $resdate;
        $min=00;
        $sec=00;

        /** $dif value if immediate feaure(RN) is being used  **/
        if($webidate == 'rn' && $webitime == 'rn') {
            $dif = 0;
        }

        $_SESSION['s_page'] = $page;
        $_SESSION['s_dif'] = $dif;
        $_SESSION['s_maxnum'] = $maxnum[$page];
        $_SESSION['s_userlog'] = $userlog;


        $query_string = $_SERVER["QUERY_STRING"];


          if($broadcast_design == 0 || $broadcast_design == 1){
                include('broadcast/broadcast.php');
            }
            elseif($broadcast_design == 2)
            {   include('broadcast/broadcast2.php'); }
            elseif($broadcast_design ==3)
            {   include('broadcast/broadcast3.php'); }


}
else
{
     //   echo "A C C E S S   D E N I E D ! ! !";
      //  exit;
        header("Location: webinar-login.php");
}
/*echo "dif= ".$dif;
echo "<br>dif = $webidate $webitime:00:00 ==== $datenow";
echo "<div id='position'></div>";*/

function addslashes_custom($str){
    $str= stripslashes(stripslashes(stripslashes(stripslashes(stripslashes(stripslashes($str))))));
    $str = addslashes($str);
    return $str;

}
?>



