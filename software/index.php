<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo htmlentities($page_title); ?></title>

<meta property="og:title" content="<?php echo $get_topic[$page];?>" />
<meta property="og:type" content="activity" />
<meta property="og:url" content="<?php echo $domain_name.'webinar-register.php';?>" />
<meta property="og:image" content="<?php echo $domain_name.'webinar-files/but-ical.png';?>" />
<meta property="og:site_name" content="<?php echo $get_topic[$page];?>" />
<meta property="fb:admin" content="1639423099" />
<meta property="og:description" content="<?php echo $get_presenter[$page];?> is conducting a great webinar on <?php echo $get_topic[$page];?>.  I have already signed up! You can register here: <?php echo $domain_name.'webinar-register.php';?>"/>

<meta name="keywords" content="<?php echo $page_keywords; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta http-equiv="Pragma" content="no-cache">

<link rel="stylesheet" type="text/css" href="<?php echo $tpltype.'/'.$get_regtplcolor[$page].'/';?>style.css" media="screen" />
<script language="javascript" src="js/jquery.js" type="text/javascript"></script>
<script language="javascript" src="js/cookie.js" type="text/javascript"></script>
<script language="javascript" src="js/dynajax.js" type="text/javascript"></script>
<!--<script language="javascript" src="js/gen_validatorv31.js"></script>-->
<script language="javascript" src="js/detect_timezone.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function(){
    $('#sms_slide').slideUp();
    $('#sms_on').click(function(){
        if($("#sms_on").is(':checked'))
        {
            $('#sms_slide').slideDown();
        }
        else
        {
            $('#sms_slide').slideUp();
        }
    });


    var tz_info = jzTimezoneDetector.determine_timezone();

    if (typeof(tz_info.timezone) == 'undefined') {
        response_text = 'UTC';
    }
    else {
        response_text = tz_info.timezone.display();
    }

    document.myform.timezone.value = response_text;


     $('#frmsubmit').click(function() {

        var sdates = $("#dates").val();
        var stimes = $("#times").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var sname =  $("#name").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');
        var semail = $("#email").val().replace(/(^[\s\xA0]+|[\s\xA0]+$)/g, '');

        if(sdates == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELDATE']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_DATE']);?>');
            return false;
        }

        if(stimes == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELTIME']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_TIME']);?>');
            return false;
        }

        if(stimes == '<?php echo trim(addslashes($lang['REG_FORM_TXT_SELTIME_2']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_TIME']);?>');
            return false;
        }

        if(sname == '' || sname == '<?php echo trim(addslashes($lang['REG_FORM_TXTBOX_NAME']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_NAME']);?>');
            return false;
        }
        if(semail == '' || semail == '<?php echo trim(addslashes($lang['REG_FORM_TXTBOX_EMAIL']));?>'){
            alert('<?php echo addslashes($lang['REG_FORM_ALERT_EMAIL']);?>');
            return false;
        }

});

});



function validate()
{
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(document.myform.time.options[document.myform.time.selectedIndex].value=='')
    {
        alert('Please Choose A Time For The Broad Cast'); return false;
    }

    if(document.myform.date.options[document.myform.date.selectedIndex].value==0)
    {
        alert('Please Choose A Date For The Broad Cast'); return false;
    }
    if((document.myform.name.value=='Enter Your Name Here...') || (document.myform.name.value==''))
    {
        alert('Please Enter Your Name'); return false;
    }

    if((document.myform.email.value=='Enter Valid Email Here...') || (document.myform.email.value==''))
    {
        alert('Please Enter A Valid Email Address'); return false;
    }

    if (!filter.test(document.myform.email.value)) {alert('<?php echo addslashes($lang['REG_FORM_ALERT_EMAIL_2']);?>'); return false;}

    copy_fields();

    return true;
}

var rname="";
var remail="";
var is_infusion=0;
function copy_fields(){

    rdateset=document.getElementById('dates').value;
    rwebitime=document.getElementById('times').value;
    rname=document.getElementById('name').value;
    remail=document.getElementById('email').value;
    rtimezone=document.getElementById('timezone').value;
    $.cookie("dateset", rdateset, { path: '/' });
    $.cookie("webitime", rwebitime, { path: '/' });
    $.cookie("name", rname, { path: '/' });
    $.cookie("email", remail, { path: '/' });
    $.cookie("timezone", rtimezone, { path: '/' });
    $('#ajaxloader').css({'display': 'block'});
        <?php
    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        document.getElementById('name1').value = document.getElementById('name').value;
        document.getElementById('email1').value = document.getElementById('email').value;
        <?php
    }


?>

}
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}

<?php
    if($autoresponderopt==1 || $autoresponderopt==2) {
        ?>
        function submitForm2(){
            if(is_infusion)
                ajax_posttt();
            else
                document.getElementById("form2").submit();
            <?php $rregpage=$domainname."/".$webifolder."/webinar-register-thankyou.php"; ?>
            setTimeout('window.location="<?php echo $rregpage; ?>";',5000);

        }
        <?php
    }


?>

function isNumberKey(evt) {
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}

function setCountryCode() {
    document.myform.phonenumber.value = document.myform.cc.options[document.myform.cc.selectedIndex].value;
    return true;
}


</script>

<?php
if($get_splash[$page]==1){
 ?>
<script type="text/javascript">
var the_height=0;
var is_exit=0;

function closeIt()
{
    if(is_exit==0){
        is_exit=1;
        document.getElementById("iframeholder").style.visibility='visible';
        document.getElementById("iframeholder").style.overflow='';
        document.getElementById("iframeholder").style.height='auto';
        document.getElementById("exitimg").style.display='block';
        document.getElementById("webinar-register-exit").style.display='block';
        document.getElementById("currentPage").style.display='none';
        document.getElementById("resize").style.visibility='visible';
        $('#player_wrapper').remove();
        var myplayer = '<object style="height: 0px; width: 0px;" type="application/x-shockwave-flash" data="player_mp3.swf" width="1" height="1"><param name="movie" value="player_mp3.swf" /><param name="FlashVars" value="mp3=webinar-files/<?php if($get_audio_file[$page]==1) echo renamefilereg($get_own_audio[$page], $memberid); else echo $get_builtin_audio[$page]; ?>&autoplay=1" /></object>';
        $('#webinar-register-exit').append(myplayer);


        document.body.style.background = 'white';
        setTimeout('document.getElementById("exitimg").style.display="none"',5000);

        return "<?php echo $get_popup_message[$page]; ?>";
    }
    is_exit=0;
}
window.onbeforeunload = closeIt;



</script>
<?php } ?>

<?php echo stripslashes($google_analytics_header); ?>
<style type="text/css">
 #player_wrapper{
     float: right;
 }

</style>
</head>
<body>
<div id="ajaxloader" style="display:none;background-color: rgba(0, 0, 0, 0.8);     color: #FFFFFF;          font-family: Tahoma,Arial,Helvetica,sans-serif;     height: 50px;     padding-top: 10px;     position: fixed;     text-align: center;     top: 0;     width: 100%;     z-index: 100;"><div class="loading"><img src="webinar-files/loader2.gif" style="border:none;"><br><span><?php echo $lang['REG_LOADING'];?></span></div></div>
<div id="currentPage">
    <div id="header">
        <?php if($get_regheader[$page] == 1){ ?>
         <center><img src="<?php echo 'webinar-files/'.renamefilereg($get_headerimg[$page], $memberid);?>" alt="Live Webinar Broadcast"/></center>
        <?php }else{ ?>
        <h1><?php echo $get_regtxtheader1[$page]; ?></h1>
        <h2><?php echo $get_regtxtheader2[$page]; ?></h2>
        <?php } ?>
    </div> <!-- end header -->

    <div id="wrapper">
        <div id="intro">

            <div style="width: 845px; text-align:center;padding-top: 0px; font-size: 36px; color: rgb(206, 0, 0); margin-bottom: 15px; font-family: impact,tahoma;">
                <?php echo $get_headerline[$page]; ?>
            </div>



                <div id="intro-text" style="width:500px;">
                    <?php
                    if($get_intro_box[$page]==1){
                        $is_affiliate=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=q');
                        $a_affiliate_name=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=n');
                        $a_affiliate_picture=curlURL('http://eWebinars.com/affiliate/a_getaffiliate.php?member_id='.$memberid.'&a_affiliate_id='.$affiliate_id.'&q=i');
                    include 'presentation-box/presentersboxr.php';
                    ?>

                    <?php } ?>

                <?php  echo $get_webidesc[$page]; ?>
            </div> <!-- end intro-text -->

            <?php if($get_reginsertopt[$page] == 1){ ?>
            <div id="photo">
                <img src="resize.php?url=<?php echo 'webinar-files/'.renamefilereg($get_regimg[$page], $memberid);?>&width=320&height=320"  />
            </div> <!-- end photo -->
            <?php }else{ ?>
            <div id="video">
                  <!-- START OF THE PLAYER EMBEDDING TO COPY-PASTE -->
              <script type="text/javascript" src="player/jwplayer.js"></script>
              <!-- END OF THE PLAYER EMBEDDING -->
              <div id="player" align="right"></div>
              <script type="text/javascript">
              jwplayer('player').setup({
                'flashplayer': 'player/player.swf',
                'file': '<?php echo $get_regvid[$page];?>',
                'feature': 'related',
                'controlbar': 'over',
                'autostart': '<?php echo $get_regvidauto[$page];?>',
                'width': '320',
                'height': '265'

              });
              </script>
            </div>
            <?php } ?>

            <div class="clear"></div>

        </div> <!-- end intro -->

        <div id="divider"></div>

        <div id="sign-up">

            <div id="banner">
                 <img src="<?php echo 'languages/'.$langtype.'/images/software/'.$get_regtplcolor[$page].'/register_arrow.png';?>" alt="<?php echo $lang['REG_FORM_BUTTON_TXT'];?>" />
            </div> <!-- end banner -->

            <?php

            ################EXPIRED STICKER####################
            include 'get_form_expire.php';
            $show=1;
            if($dif <= 0 && $type==2){
            echo "<a href='webinar-replay.php?access=ok' onClick='is_exit=1;'><img src='http://evergreenbusinesssystem.com/webinarfiles/expiredsticker1.png'  /></a>";
            $show=0;
            }
            if($show==1 && $dif2 <= 0)
            {
                echo "<a href='webinar-replay.php?access=ok' onClick='is_exit=1;'><img src='http://evergreenbusinesssystem.com/webinarfiles/expiredsticker1.png' style='margin-top:30px;' /></a>";
                $show=0;
            }
            ?>
            <div style="display: <?php if($show) echo 'block;'; else echo 'none;'; ?>">
        <form action="loadregister.php" method="post" name="myform" target="iframe2" onsubmit="return validate();">
            <div id="form">
                <div id="form-header">
                    <p id="line1" style="font-size:22px;"><?php echo $lang['REG_FORM_H1'];?></p>
                    <p id="line2" style="font-size:22px;"><?php echo $lang['REG_FORM_H2'];?></p>
                </div> <!-- end form-header -->

                <div id="form-bg">
                    <p id="local-time"><?php echo $lang['REG_FORM_TXT_LOCALTIME'];?>:

                    <script type="text/javascript">
                    <!--
                    var currentTime = new Date();
                    var hours = currentTime.getHours();
                    var minutes = currentTime.getMinutes();
                    var secs = currentTime.getSeconds();
                    var orighours = hours;
                    var suffix = "AM";

                     if (hours >= 12) {
                        suffix = "PM";
                        hours = hours - 12;
                    }
                    if (hours == 0) {
                        hours = 12;
                    }

                    if (minutes < 10){
                        minutes = "0" + minutes;
                    }
                    if (secs < 10){
                        secs = "0" + secs;
                    }

                    document.write(hours + ":" + minutes + " " + suffix );

                    //-->
                    </script>

                    </p>

                        <?php include 'modules/regdatetime/index.php'; ?>

                        <div id="line"></div>


                        <p id="register" style="font-size:19px;"><?php echo $lang['REG_FORM_H3'];?></p>

                        <!--- form fields -->
                        <input type="text" name="name" id="name" class="entry" value="<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>" onFocus="if(this.value=='<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>')this.value=''; is_exit=1;" onBlur="if(this.value=='')this.value='<?php echo $lang['REG_FORM_TXTBOX_NAME'];?>';" />
                        <br />
                        <input type="text" name="email" id="email" class="entry" value="<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>" onFocus="if(this.value=='<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>')this.value=''; is_exit=1;" onBlur="if(this.value=='')this.value='<?php echo $lang['REG_FORM_TXTBOX_EMAIL'];?>';"

/>

                        <!--- end form fields -->

                        <br />

                        <?php if($get_sms[$page] == 1 || $get_voice[$page] == 1){ include_once('modules/sms/index.php');  } ?>
<input type="hidden" name="timezone" id="timezone">
<input type="hidden" name="submit" value="submit">
<input type="hidden" name="page" value="<?php echo $page;?>">



<input onClick='is_exit=1;' type="image" class="submit" id="frmsubmit" src="<?php echo 'languages/'.$langtype.'/images/software/'.$get_regtplcolor[$page].'/register_button.png';?>" height="57" width="212" border="0" alt="Submit Form" />

                </div> <!-- end form-bg -->

                <div id="form-footer"></div>
            </div> <!-- form -->
            </form>

            <?php include 'autoresponder.php'; ?>
            <iframe sandbox="allow-same-origin allow-forms allow-scripts" name="iframe3" style="display:none"></iframe>
            <iframe name="iframe2" style="display:none"></iframe>

            </div>


            <div class="clear"></div>

        </div> <!-- end sign-up -->

    </div> <!-- end wrapper -->

    <div id="footer">
        <?php  include 'footer.php'; ?>
    </div> <!-- end footer -->

    <!-- google analytics code -->


    <?php echo stripslashes($google_analytics_body); ?>

    <!-- end google analytics code -->


</div>

<?php
if($get_splash[$page]==1){
 ?>
<div id="webinar-register-exit" style="display:none;height:0;"   ></div>
<div id="iframeholder" style="visibility:hidden; overflow:hidden; height:0px;">
<iframe src="webinar-register-exit.php" scrolling="no"  align="middle" frameborder="0" width="100%" id = "resize"  onload = "setIframeHeight( this.id )" style="visibility:hidden;overflow:hidden;"  height="0" name="frame1"></iframe>
</div>

<script type='text/javascript'>

function setIframeHeight( iframeId ) /** IMPORTANT: All framed documents *must* have a DOCTYPE applied **/
{
 var ifDoc, ifRef = document.getElementById( iframeId );

 try
 {
  ifDoc = ifRef.contentWindow.document.documentElement;
 }
 catch( e )
 {
  try
  {
   ifDoc = ifRef.contentDocument.documentElement;
  }
  catch(ee)
  {
  }
 }

 if( ifDoc )
 {
  ifRef.height = 1;
  ifRef.height = ifDoc.scrollHeight;

  /* For width resize, enable below.  */

  // ifRef.width = 1;
  // ifRef.width = ifDoc.scrollWidth;
 }
}

</script>

<div id="exitimg" style="display:none; position:fixed; right:10px; top:10px; z-index:10000"><img id="image" name="image" border="0" src="webinar-files/exitimg.gif" /></div>
<?php } ?>
<?php echo stripslashes($tracker_reg); ?>
</body>
<?php echo stripslashes($google_analytics_footer); ?>
</html>