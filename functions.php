<?php
function convertdatelang($thedate,$p){

   global $langtype;
   global $langmonths;
   global $langdays;


   if(trim($p) == 'ty')
   {
        $datestyle =  date('F jS',strtotime($thedate));
        $dmonth = date('F',strtotime($thedate));
        if(is_array($langmonths)) {
            $datestyle = @str_ireplace($dmonth,$langmonths[strtolower($dmonth)],$datestyle);
        }
   }
   if(trim($p) == 'cd')
   {
        $datestyle =  date("l, jS F Y",strtotime($thedate));
        $dmonth = date('F',strtotime($thedate));
        $dday = trim(date('l',strtotime($thedate)));
        if(is_array($langmonths)) {
            $datestyle = @str_ireplace($dmonth,$langmonths[strtolower($dmonth)],$datestyle);
            $datestyle = @str_ireplace($dday,$langdays[strtolower($dday)],$datestyle);
        }
   }

   if(trim($p) == 'wc')
   {
        $datestyle =  date("l, jS F",strtotime($thedate));
        $dmonth = date('F',strtotime($thedate));
        $dday = trim(date('l',strtotime($thedate)));

        if(is_array($langmonths)) {
            $datestyle = @str_ireplace($dmonth,$langmonths[strtolower($dmonth)],$datestyle);
            $datestyle = @str_ireplace($dday,$langdays[strtolower($dday)],$datestyle);
        }
   }



    return $datestyle;
}

function full_url()
{
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
}

// array_walk_recursive pass-by-reference example
function calclatevideo(&$item,$key,$minus){
        $item-=$minus; // Do This!
}


if(!function_exists('is_true_float')){
    function is_true_float($mVal)
    {
      return ( is_float($mVal)
               || ( (float) $mVal != round($mVal)
                    || strlen($mVal) != strlen( (int) $mVal) )
                  && $mVal != 0 );
    }
}
function convertTime($dec)
{
    // start by converting to seconds
    $seconds = $dec * 3600;
    // we're given hours, so let's get those the easy way
    $hours = floor($dec);
    // since we've "calculated" hours, let's remove them from the seconds variable
    $seconds -= $hours * 3600;
    // calculate minutes left
    $minutes = floor($seconds / 60);
    // remove those from seconds as well
    $seconds -= $minutes * 60;
    // return the time formatted HH:MM:SS
    return lz($hours).":".lz($minutes).":".lz(floor($seconds));
}

    // lz = leading zero
function lz($num)
{
    return (strlen($num) < 2) ? "0{$num}" : $num;
}



function strtotime_date($webidate,$tz) {
    date_default_timezone_set($tz);
    $dt = strtotime("$webidate");
    return $dt;
}



function getrntime($tz){
    date_default_timezone_set($tz);
    $time = date('G');
    $min = date('i');
    if($time == 24) {
        $time = 0;
    }
    $min =  round($min/60,1);
    $time = $time + $min;
    return $time;
}
function getrndate($tz){
    date_default_timezone_set($tz);
    $date = date('Y-M-d');
    return $date;
}
function getrndif($tz,$webidate,$webitime){
    date_default_timezone_set($tz);
    //$datenow1 = date('Y-M-d');
    //$datenow2 = date('Y-M-d H:i:s');
    //$dif = strtotime("$datenow2") - strtotime("$datenow1 00:00:00");
    $datenow = date('Y-M-d H:i:s');
    $dif = strtotime("$webidate $webitime:00:00") - strtotime("$datenow");
    return $dif;
}




function detect_mobile()
{
    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';

    $mobile_browser = '0';

    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);

    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', $agent))
        $mobile_browser++;

    if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
        $mobile_browser++;

    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
        $mobile_browser++;

    if(isset($_SERVER['HTTP_PROFILE']))
        $mobile_browser++;

    $mobile_ua = substr($agent,0,4);
    $mobile_agents = array(
                        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
                        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
                        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
                        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
                        'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
                        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
                        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
                        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
                        'wapr','webc','winw','xda','xda-'
                        );

    if(in_array($mobile_ua, $mobile_agents))
        $mobile_browser++;

    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
        $mobile_browser++;

    // Pre-final check to reset everything if the user is on Windows
    if(strpos($agent, 'windows') !== false)
        $mobile_browser=0;

    // But WP7 is also Windows, with a slightly different characteristic
    if(strpos($agent, 'windows phone') !== false)
        $mobile_browser++;

    if($mobile_browser>0)
        return true;
    else
        return false;
}



function makeoffsetcity($webischedopt,$tz){
global $lang;
    if($webischedopt == 1) {
        $tz1 = $tz;
        $tzlist = file_get_contents('tzoption.php');
        $tz = str_replace('/','\/',$tz);

        if(@preg_match_all('/<option value="'.$tz.'">([\w\W]*?)<\/option>/i', $tzlist, $match)){

            $city = trim(@$match[1][0]);
            $city = makeoffset($tz1).' '.$city;

        }

        if($city != ''){
            $vartime = $city;
        }else{
            $vartime = ' (GMT'.' '.$sign.$offset.')';
        }

    }
    if($webischedopt == 2) {
        $vartime = ' ('.$lang['REG_FORM_TXT_LOCALTIME'].')';
    }


        return $vartime;

}

function makeoffsetcity2($webischedopt,$tz){
global $lang;
    if($webischedopt == 1) {
        $tz1 = $tz;
        $tzlist = file_get_contents('tzoption.php');
        $tz = str_replace('/','\/',$tz);

        if(@preg_match_all('/<option value="'.$tz.'">([\w\W]*?)<\/option>/i', $tzlist, $match)){

            $vartime= trim(@$match[1][0]);


        }

    }
    if($webischedopt == 2) {
        $vartime = ' ('.$lang['REG_FORM_TXT_LOCALTIME'].')';
    }


        return $vartime;

}
function makeoffset($webi_tz) {
    $tz = $webi_tz;
    $timezone = new DateTimeZone("$tz");
    $offset   = $timezone->getOffset(new DateTime);

    $offset =  $offset/3600;
    $pieces = explode(".", $offset);

    if($pieces[1] > 0 && strlen($pieces[1]) == 1) {
        $goffset = $pieces[0].':'.$pieces[1].'0';
    }
    elseif($pieces[1] > 0 && strlen($pieces[1]) == 2) {
        $goffset = $pieces[0].':'.$pieces[1];
    }
    else {
        $goffset = $pieces[0].':00';
    }

    if(strlen($pieces[0]) == 1) {
        $goffset = '0'.$pieces[0];
    }

    if($pieces[0] >= 0) {
        $goffset = '+'.$goffset;
    }



    if("Europe/Moscow" == $webi_tz)
        $goffset = '(GMT+04)';
    else
        $goffset = '(GMT'.$goffset.')';

    return $goffset;

}



function stripslashes_deep ($text) {

 $times = 4;
    $i = 0;

    // loop will execute $times times.
    while (strstr($text, '\\') && $i != $times) {

        $text= stripslashes($text);
        $i++;

    }

    return $text;

}

function makeurl($str) {

$findme1 =  'http://www.';
$findme2 =  'http://';
$findme4 =  'https://www.';
$findme5 =  'https://';


    if (strlen(stristr($str,$findme1))>0 || strlen(stristr($str,$findme2))>0 || strlen(stristr($str,$findme4))>0 || strlen(stristr($str,$findme5))>0) {
        $res_url = $str;
    }
    else{
        $res_url = 'http://'.$str;
    }


    return $res_url;

}



function curlURL($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,4);
    curl_setopt($curl,CURLOPT_TIMEOUT,4);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)');
    $output = curl_exec($curl);

    return $output;
}

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

function curPageURL2() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"];
 }
 return $pageURL;
}


function renamefilereg($file, $memberid){
    global $webid;
    $info = pathinfo($file);
    $file_name =  basename($file,'.'.$info['extension']);

    return $file_name.'_'.$webid.'_'.$memberid.'.'.$info['extension'];

}
/* Takes a GMT offset (in hours) and returns a timezone name */
function tz_offset_to_name($offset) {
        $offset *= 3600; // convert hour offset to seconds
        $abbrarray = timezone_abbreviations_list();
        foreach ($abbrarray as $abbr) {
                foreach ($abbr as $city) {
                        if ($city['offset'] == $offset){
                                return $city['timezone_id'];
                        }
                }
        }

        return FALSE;
}


#get user offset timezone
function getOffsetByIP2($ip = false) {

    $offset = false;
    $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];

    if(@$_COOKIE['webioffset'] != '') {
        $offset = $_COOKIE['webioffset'];
    }
    else {

        $tags="";
        $lon=0;
        $tags = get_meta_tags('http://www.geobytes.com/IpLocator.htm?GetLocation&template=php3.txt&IpAddress='.$ip);

        if($tags['longitude'])
                $lon=$tags['longitude'];
        /*if ($geodata = curlURL('http://www.geoiptool.com/?IP='.$ip))  {
            $geodata = str_replace(' ', '', $geodata);
            $geodata = str_replace(array("\r", "\r\n", "\n"), '', $geodata);
            if (preg_match_all('/Longitude:<\/span><\/td><tdalign="left"class="arial_bold">([\w\W]*?)<\/td>/i', $geodata, $match)) {
                $lon = trim($match[1][0]);
            }
            $offset = round((($lon / 15)*2),0)/2;

        }*/
        $offset = round((($lon / 15)*2),0)/2;
    setcookie("webioffset", $offset, time()+3600);
    }
    return $offset;
}




function get24hour($hr){

    $pieces = explode("-", $hr);

    if(strtolower($pieces[1]) == 'am'){
        if($pieces[0] == 12){
        $hr24 = 0;
        }
        else{
        $hr24 = $pieces[0];
        }
    }
    else{
        if($pieces[0] == 12){
        $hr24 = 12;
        }
        else{
        $hr24 = $pieces[0] + 12;
        }
    }

    return $hr24;

}

function get12hr($hr){

    if($hr >=1 && $hr < 12){
        $hr12 = $hr.'am';
    }
    elseif($hr == 0){
        $hr12 = '12am';
    }
    elseif($hr == 12){
        $hr12 = $hr.'pm';
    }
    else{
        $hr12 = $hr -12;
        $hr12 = $hr12.'pm';
    }

return $hr12;

}
function get12hr2($hr){

    if($hr >=1 && $hr < 12){
        $hr12 = $hr.':00 am';
    }
    elseif($hr == 0){
        $hr12 = '12:00 am';
    }
    elseif($hr == 12){
        $hr12 = $hr.':00 pm';
    }
    else{
        $hr12 = $hr -12;
        $hr12 = $hr12.':00 pm';
    }

return $hr12;

}

function multimes($time, $ampm) {

    $tmp = array();
    $multitimes = array();
    $time = $time ? $time : array();
    $ampm = $ampm ? $ampm : array();

        $ctr = 0;
        foreach($time as $val){
            if($val != ''){
                $val = trim($val);
                $multitimes[] = trim($val.'-'.$ampm[$ctr]);         }
        $ctr++;
        }

        foreach($multitimes as $val){
            $tmp[] = get24hour($val);
        }

        return $tmp;
}

function getholidays($holidays){
    $ctr = 0;
    $subpiece = array();
    $getholidays = array();
    $pieces = explode("\r\n", $holidays);

    foreach($pieces as $val) {
        if(trim($val) != '') {
            $subpiece =  explode(" ", $val);
            $getholidays[$ctr] =  $subpiece[2].'-'.$subpiece[0].'-'.$subpiece[1];
            $ctr++;
        }
    }
   return $getholidays;


}

function getselecteddate($mm, $yy, $dd){

        if($mm != '' && $dd != '' && $yy != ''){

            $month_name = date( 'M', mktime(0, 0, 0, $mm) );
            $specificdate = $yy.'-'.$month_name.'-'.$dd;
            return $specificdate;

        }

}

function encode_array($args)
{
  if(!is_array($args)) return false;
  $c = 0;
  $out = '';
  foreach($args as $name => $value)
  {
    if($c++ != 0) $out .= '&';
    $out .= urlencode("$name").'=';
    if(is_array($value))
    {
      $out .= urlencode(serialize($value));
    }else{
      $out .= urlencode("$value");
    }
  }
  return $out . "\n";
}


function arrayToQstr($array,$name)
{
   if(is_array($array))
   {
      $ret = array();
      for( $i = 0; $i < count($array);$i++)
      {
         $ret[] = $name.'['.$i.']='.$array[$i];
      }
      return '&'.implode('&',$ret);
   }
}

function filteroffset($tzone){

$p_tzone = explode(":", $tzone);
$p_tzone0 = $p_tzone[0];
$p_tzone1 = $p_tzone[1];


    if($p_tzone0 < 10){
        $p_tzone0 = str_replace('0','',$p_tzone0);
    }
    if($p_tzone1 < 10){
        $p_tzone1 = str_replace('0','',$p_tzone1);
    }
    if($p_tzone1 > 0){
        $useroffset =$p_tzone0.'.'.$p_tzone1;
    }
    else{
        $useroffset = $p_tzone0;
    }

    return  $useroffset;

}

?>