<?php



    $inputnames[0] = 'name';
    $inputnames[1] = 'subscriber_name';
    $inputnames[2] = 'fields_fname';
    $inputnames[3] = 'first_name';
    $inputnames[4] = 'NAME';
    $inputnames[5] = 'Name';
    $inputnames[6] = 'Contact0FirstName';
    $inputnames[7] = 'MERGE2';
    $inputnames[8] = 'MERGE3';
    $inputnames[9] = 'FNAME';
    $inputnames[10] = 'inf_field_FirstName';
    $inputnames[11] = 'FirstName';
    $inputnames[12] = 'FullName';
    $inputnames[13] = 'firstname';
    $inputnames[14] = 'GRcategory2';
    $inputnames[15] = 'MMERGE2';
    $inputnames[16] = 'CustomFields[2]';
    $inputnames[17] = 'q3';
    $inputnames[18] = 'inf_custom_Name';
    $inputnames[19] = 'MMERGE1';
    $inputnames[20] = 'full_name';
    $inputnames[21] = 'fields_fname';
    $inputnames[22] = 'CustomFields[21]';
    $inputnames[23] = 'fname';
    $inputnames[24] = 'Last Name';
    $inputnames[25] = 'FormValue_Fields[CustomField12086]';
    $inputnames[26] = 'frm1314field_1';
    $inputnames[27] = 'CustomFields[31]';
    $inputnames[28] = 'u_firstname';
    $inputnames[29] = 'CustomFields[12]';
    $inputnames[30] = 'CustomFields[19]';
    $inputnames[31] = 'field_name_first';
    $inputnames[32] = 's_first_name';
    $inputnames[33] = 'fullname';
    $inputnames[34] = 'CustomFields[138]';
    $inputnames[35] = 'fldfirstname';
    $inputnames[36] = 'lead_name';
    $inputnames[37] = 'category2';
    $inputnames[38] = 'YMP1';
    $inputnames[39] = 'sName';
    $inputnames[40] = 'CustomFields[15]';
    $inputnames[41] = 'mc-firstname';

    $inputemails[0] = 'from';
    $inputemails[1] = 'subscriber_email';
    $inputemails[2] = 'fields_email';
    $inputemails[3] = 'email_address';
    $inputemails[4] = 'email';
    $inputemails[5] = 'ea';
    $inputemails[6] = 'EMAIL';
    $inputemails[7] = 'Email1';
    $inputemails[8] = 'Contact0Email';
    $inputemails[9] = 'MERGE0';
    $inputemails[10] = 'inf_field_Email';
    $inputemails[11] = 'Email1';
    $inputemails[12] = 'GRcategory3';
    $inputemails[13] = 'q14';
    $inputemails[14] = 'Email';
    $inputemails[15] = 'fields_email';
    $inputemails[16] = 'FormValue_Fields[EmailAddress]';
    $inputemails[17] = 'frm1314field_2';
    $inputemails[18] = 'u_email';
    $inputemails[19] = 'field_email';
    $inputemails[20] = 's_email';
    $inputemails[21] = 'fldEmail';
    $inputemails[22] = 'lead_email';
    $inputemails[23] = 'category3';
    $inputemails[24] = 'YMP0';
    $inputemails[25] = 'sEmail';
    $inputemails[26] = 'email1';
$aweber=stripslashes_deep($optincode);


//get action url
if(preg_match_all(  '/action="([\w\W]*?)"/', $aweber, $m)){
   $action_url=$m[1][0];
}
elseif(preg_match_all(  '/action=([\w\W]*?)\s/', $aweber, $m)){
    $action_url=$m[1][0];
}
//autoresponder form template
$formtpl="<form target='iframe3' name='form2' id='form2' method='post' action='%ACTION_URL%' style='display:none;'>%INPUT_FIELDS%</form>";
//replace form template action url to the autoresponder action url
$formtpl = str_replace('%ACTION_URL%',$action_url, $formtpl);
//strip autoresponders INPUT FIELDS
if(preg_match_all(  '/<input([\w\W]*?)>/', $aweber, $m)){
    #print_r($m);
}

$input_fields='';  //input fields

foreach($m[0] as $key=>$input)
{

        if(preg_match_all(  '/name="([\w\W]*?)"/', $input, $m)){ $replace_type1=0; }
        elseif(preg_match_all(  '/name=([\w\W]*?)\s/', $input, $m)){ $replace_type1=1; }
        $name=$m[1][0];
        //check if field name for the name field is present in the name name filter
        $hit=0;
        for($i=0;$i<sizeof($inputnames);$i++)
        {
            if($inputnames[$i]==$name) $hit=1;
        }
         if($hit){
            if(preg_match_all(  '/id="([\w\W]*?)"/', $input, $n)){
               $idvalue=$n[1][0];
               $replace_type=0;
            }
            elseif(preg_match_all(  '/id=([\w\W]*?)\s/', $input, $n)){
                $idvalue=$n[1][0];
                $replace_type=1;
            }
            if(isset($replace_type)){
                if($replace_type) $input=str_replace('id='.$idvalue,"id=name1",$input);
                else $input=str_replace('id="'.$idvalue.'"',"id=\"name1\"",$input);
            }
            else
            {
                if($replace_type1){
                    $input=str_replace('name='.$name,"name=".$name." id=name1",$input);
                }
                else
                    $input=str_replace('name="'.$name.'"',"name=\"".$name."\" id=\"name1\"",$input);
            }

         }

        //check if field name for the email field is present in the email name filter
        $hit=0;
        for($i=0;$i<sizeof($inputemails);$i++)
        {
            if($inputemails[$i]==$name) $hit=1;
        }
         if($hit){
            if(preg_match_all(  '/id="([\w\W]*?)"/', $input, $n)){
               $idvalue=$n[1][0];
               $replace_type=0;
            }
            elseif(preg_match_all(  '/id=([\w\W]*?)\s/', $input, $n)){
                $idvalue=$n[1][0];
                $replace_type=1;
            }
            if(isset($replace_type)){
            if($replace_type) $input=str_replace('id='.$idvalue,"id=email1",$input);
            else $input=str_replace('id="'.$idvalue.'"',"id=\"email1\"",$input);
            }
            else
            {
                if($replace_type1){
                    $input=str_replace('name='.$name,"name=".$name." id=email1",$input);
                }
                else
                    $input=str_replace('name="'.$name.'"',"name=\"".$name."\" id=\"email1\"",$input);
            }
         }

         //check if field name for the redirect field is present in the redirect name filter
        $hit=0;
        for($i=0;$i<sizeof($inputredirects);$i++)
        {
            if($inputredirects[$i]==$name) $hit=1;
        }
         if($hit){
            if(preg_match_all(  '/value="([\w\W]*?)"/', $input, $n)){
               $idvalue=$n[1][0];
            }
            elseif(preg_match_all(  '/value=([\w\W]*?)\s/', $input, $n)){
                $idvalue=$n[1][0];
            }
            $input=str_replace($idvalue,"http://ontowebinar.com/1/gb24ey5meh/webinar-register-thankyou.php?trackingID1=XXXXXXXX&trackingID2=YYYYYYYYY",$input);
         }


         //check if field name for the submit field is not submit

            if(preg_match_all(  '/name="([\w\W]*?)"/', $input, $n)){
               $submit=$n[1][0];
            }
            elseif(preg_match_all(  '/name=([\w\W]*?)\s/', $input, $n)){
                $submit=$n[1][0];
            }

            if($name=="submit" || $name=="Submit"){
                $input=str_replace("name=\"".$submit."\"","name='btnSubmit'",$input); }




        $input_fields.=$input;




}
//replace form template action url to the autoresponder action url
$formtpl = str_replace('%INPUT_FIELDS%',$input_fields, $formtpl);


#========================   start AJAX EXTRACTION =======================#





$name=array();
$value=array();
if(preg_match_all(  '/<input([\w\W]*?)>/', $aweber, $m)){
    #print_r($m);
}
foreach($m[1] as $key=>$input)
{

        if(preg_match_all(  '/name="([\w\W]*?)"/', $input, $m)){}
        elseif(preg_match_all(  '/name=([\w\W]*?)\s/', $input, $m)){}
        $tmp_name=$m[1][0];
        if($m[1][0]!='')
            $name[]=$m[1][0];

         if(preg_match_all(  '/value="([\w\W]*?)"/', $input, $m)){}
        elseif(preg_match_all(  '/value=([\w\W]*?)/', $input, $m)){}
        if($tmp_name!='')
            $value[]=$m[1][0];
}

for($i=0;$i<sizeof($name);$i++)
{
    #echo $name[$i] ."=". $value[$i] ."<br>";
}

//create array of data to be posted
for($j=0;$j<2;$j++)
    for($i=0;$i<sizeof($name);$i++)
    {
        if($j==0) $post_data[$i][$j] = $name[$i];
        else $post_data[$i][$j] = $value[$i];

    }



//traverse array and prepare data for posting (key1=value1)
$post_items=array();
for($i=0;$i<sizeof($name);$i++)
    {
        $hit1=0;
        foreach($inputnames as $field){
            if($field == $post_data[$i][0]){
                $hit1=1;
                $name_name=$field;
            }
        }
        $hit2=0;
        foreach($inputemails as $field){
            if($field == $post_data[$i][0]){
                $hit2=1;
                $name_email=$field;
            }
        }
        if($hit1==0 && $hit2==0) $post_items[] = $post_data[$i][0]  . '=' . $post_data[$i][1] ;

    }

//create the final string to be posted using implode()
$post_string = implode ('&', $post_items);

$_SESSION['post_string']=$post_string;
$_SESSION['action_url']=$action_url;

$ajaxtpl='
    <script type="text/javascript">
        is_infusion=1;

        function ajax_posttt(){
        a_name=rname;
        a_email=remail;

        $.ajax({
            type: "POST",
            url: "curlhttps.php",
            data: "'.$post_string.'&action_url='.$action_url.'&'.$name_name.'=" + a_name + "&'.$name_email.'=" + a_email + "",
            success: function(data){
            $("#tot_loggedin").html(data);

            }
        });


        }

    </script>
';

#========================   end AJAX EXTRACTION =======================#










if(strpos($action_url,"infusion")){
    echo  ($ajaxtpl);
}
else{
    echo  ($formtpl);
}















?>