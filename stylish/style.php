<style type="text/css">

@charset "utf-8";
body, h1, h2, h3, h4, h5, p, form, ul, ol, li, fieldset {
margin:0;
padding:0;
}


img, a img{
border:0 none;
}

img{
vertical-align:baseline;
}

a{
outline:0;
}

input:focus, textarea:focus, select:focus{
outline: none;
}

textarea {
resize: none;
}

/*img, div, input { behavior: url("iepngfix.htc") }*/


.imgCenter{
display:block;
margin:0 auto;
clear:both;
}

.imgRight{
float:right;
}

.imgLeft{
float:left;
}

.none{
display:none;
}

.underlined{
text-decoration:underline;
}

.textCenter{
text-align:center;
}

.textLeft{
text-align:left;
}

.textRight{
text-align:right;
}

.clear{
clear:both;
height:0;
line-height:0;
font-size:0;
}

body{
font-size:12px;
font-family:Arial, Helvetica, sans-serif;
}


/*-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
filter: alpha(opacity=0);*/

.body-stylish{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(stylish/images/blue-bg.jpg) repeat;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(stylish/images/dark-gray-bg.jpg) repeat;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(stylish/images/gray-bg.jpg) repeat;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(stylish/images/yellow-bg.jpg) repeat;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(stylish/images/green-bg.jpg) repeat;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(stylish/images/red-bg.jpg) repeat;';
    ?>


    }


#header{
    width:100%;
    height:116px;
    font-family:Georgia, "Times New Roman", Times, serif;
    padding-top:10px;
    }

#header h1, #header h2{
    text-align:center;
    }

#header h1{
    font-size: 37px;
    font-weight: normal;
    letter-spacing: 0.08em;
    line-height: 38px;
    padding-top: 38px;
    }

.whiteheadertext{
    color:#ededed;
    }

.blackheadertext{
    color:#272727;
    }

#header h2{
    font-size: 22px;
    line-height: 24px;;
    color:#fff;
    font-weight:normal;
    }

.h1-stylish{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'color:#00396c;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color:#151515;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color:#545454;';
        if($get_regtplcolor[$page]=="yellow") echo 'color:#a67c00;';
        if($get_regtplcolor[$page]=="green") echo 'color:#054950;';
        if($get_regtplcolor[$page]=="red") echo 'color:#9a0000;';
    ?>

    }


.header-stylish{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(stylish/images/header-blue.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(stylish/images/header-dark-gray.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(stylish/images/header-gray.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(stylish/images/header-yellow.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(stylish/images/header-green.jpg) repeat-x left top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(stylish/images/header-red.jpg) repeat-x left top;';
    ?>

}


.wrapper{
    width:960px;
    margin:0 auto;
    display:block;
    }

.content{
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    background:#fff;
    }

.content-stylish{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:#fff url(stylish/images/content-blue.jpg) repeat-y right top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:#fff url(stylish/images/content-dark-gray.jpg) repeat-y right top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:#fff url(stylish/images/content-gray.jpg) repeat-y right top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:#fff url(stylish/images/content-yellow.jpg) repeat-y right top;';
        if($get_regtplcolor[$page]=="green") echo 'background:#fff url(stylish/images/content-green.jpg) repeat-y right top;';
        if($get_regtplcolor[$page]=="red") echo 'background:#fff url(stylish/images/content-red.jpg) repeat-y right top;';
    ?>


    }



.contenttop{
    background:url(stylish/images/contenttop.jpg) no-repeat left top;
    }

.contentbottom{
    background:url(stylish/images/contentbottom.jpg) no-repeat left bottom;
    padding-bottom: 25px;
    }

.leftbox{
    width:588px;
    float:left;
    margin:34px 0 0 31px;
    margin-bottom:30px;
    }

.leftbox h1{
    color: #CE0000;
    font-family:Georgia, "Times New Roman", Times, serif;
    font-weight:normal;
    font-size: 29px;
    line-height: 30px;
    text-align: center;
    padding-bottom:30px;
    }

#videobox{
    width:560px;
    height:358px;
    margin:20px auto 0;
    display:block;
    }

.video-stylish{

    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(stylish/images/video-blue.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(stylish/images/video-dark-gray.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(stylish/images/video-dark-gray.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(stylish/images/video-yellow.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(stylish/images/video-green.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(stylish/images/video-red.jpg) no-repeat left top;';
    ?>

    }



.videocodebox{
    padding:10px;
    width:540px;
    height:300px;
    }

.reserve{
    float: right;
    margin: 10px 0 0;
    padding: 173px 12px 0 0;
    width: 289px;
    }

.optin-stylish{
    <?php


        if($get_regtplcolor[$page]=="blue") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-blue.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-dark-gray.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-gray.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-yellow.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-green.jpg) no-repeat center top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(languages/'.$langtype.'/images/stylish/optin-red.jpg) no-repeat center top;';
    ?>



    }

.optin-stylish .inputbg{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'background:url(stylish/images/input-blue.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'background:url(stylish/images/input-dark-gray.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'background:url(stylish/images/input-gray.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="yellow") echo 'background:url(stylish/images/input-yellow.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="green") echo 'background:url(stylish/images/input-green.jpg) no-repeat left top;';
        if($get_regtplcolor[$page]=="red") echo 'background:url(stylish/images/input-red.jpg) no-repeat left top;';
    ?>

    }


.inputbg{
    width:241px;
    height:36px;
    margin:0 auto 15px;
    display:block;
    }

.inputbg input, .inputbg select{
    border: 0 none;
    color: #555555;
    font-size: 15px;
    margin: 8px 0 0 10px;
    width: 219px;
    }
.reserve h3{
    text-align:center;
    color:#222222;
    font-size:16px;
    margin-bottom:20px;
    }

.reserve h3 span{
    color:#ed0000;
    }

.sms{
    color: #555555;
    line-height: 20px;
    margin-bottom: 27px;
    text-align: center;
    }

input[type="image"]{
    margin:0 auto;
    display:block;
    }

.privacy{
    color:#d1ccbc;
    text-align:center;
    margin-bottom:10px;
    line-height:24px;
    font-size:12px;
    text-transform:uppercase;
    }

.privacy a{
    color:#d1ccbc;
    text-decoration:none;
    }

.privacy a:hover{
    text-decoration:underline;
    }

#footer{
    width:100%;
    height:48px;
    margin:5px 0 0 0;
    }

#footer p{
    text-align:center;
    line-height:47px;
    }

#footer p a {
    text-decoration:none;
    padding: 0 5px;
    }

#footer p a:hover{
    text-decoration:underline;
    }

.footer-stylish, .footer-stylish a{
    <?php
        if($get_regtplcolor[$page]=="blue") echo 'color:#5888b3;';
        if($get_regtplcolor[$page]=="darkgrey") echo 'color:#999999;';
        if($get_regtplcolor[$page]=="lightgrey") echo 'color:#999999;';
        if($get_regtplcolor[$page]=="yellow") echo 'color:#bea14d;';
        if($get_regtplcolor[$page]=="green") echo 'color:#789a9f;';
        if($get_regtplcolor[$page]=="red") echo 'color:#d58f8f;';
    ?>

    }


/** footer css adds **/
#footer {
    height: auto;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 0;
    padding-right: 0;
    background: url(images/footer2_bg.png);
}
#footer p{
    line-height: normal;
    }

#navfooterlist li
{
display: inline;
list-style-type: none;
padding-right: 10px;

}
#navfooterlist li a{
    text-decoration:none;
    font-size: 12px;

}
/** end footer css adds **/



</style>