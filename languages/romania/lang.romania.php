<?php
# -----------------------------------------------
# LANGUAGE: ROMANIA                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='înregistrare pentru webinar în proces';
//presentation-box
$lang['REG_PBOX_H1'] ='Prezentarea Webinarului';
$lang['REG_PBOX_TOPIC'] ='Subiect';
$lang['REG_PBOX_PRESENTER'] ='Prezentator';
$lang['REG_PBOX_HOST'] ='Ziua şi Ora';
$lang['REG_PBOX_DATETIME'] ='Ziua şi Ora';
$lang['REG_PBOX_DATETIME_VAL'] ='Selectează din Meniu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Codul Ţării Pentru Mobil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Introdu Numărul Tău De Telefon Aici...';
$lang['REG_FORM_SMS_TXT1'] ='Notificări Text Prin SMS: Da, trimite-mi o notificare prin SMS înainte de pornirea webinarului (opţional dar recomandat).';
$lang['REG_FORM_SMS_TXT2'] ='Selectează Codul Ţării Tale';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Te rog completează câmpul zilei';
$lang['REG_FORM_ALERT_TIME'] ='Te rog completează câmpul pentru oră';
$lang['REG_FORM_ALERT_NAME'] ='Te rog completează câmpul pentru nume';
$lang['REG_FORM_ALERT_EMAIL'] ='Te rog completează câmpul pentru email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Te rog introdu o adresă de email validă';
$lang['REG_FORM_TXTBOX_NAME'] ='Introdu Numele Tău Aici......';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Introdu Emailul Tău Aici...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Fusul Tău Orar';
$lang['REG_FORM_TXT_SELDATE'] ='Selectează Ziua Dorită';
$lang['REG_FORM_TXT_SELTIME'] ='Selectează-ţi ziua întâi';
$lang['REG_FORM_TXT_SELTIME_2'] ='Selectează ora dorită';
$lang['REG_FORM_TXT_SELECTDATE'] ='Selectează Ziua Dorită';
$lang['REG_FORM_TXT_SELECTTIME'] ='Selectează Ora Dorită';
$lang['REG_FORM_TXT_RN_1'] = 'Uita-te la reluare ieri webinar ACUM!';
$lang['REG_FORM_TXT_RN_2'] = 'Chiar acum!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='ÎNREGISTRARE PENTRU WEBINAR';
$lang['REG_FORM_H2'] ='REZERVĂ-ŢI LOCUL!';
$lang['REG_FORM_H3'] ='ÎNREGISTREAZĂ-TE PENTRU WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Doresc Să Primesc Un SMS De Notificare Înainte De Pornirea Evenimentului';
$lang['REG_FORM_SMS_TXT2'] ='(Opţional dar foarte recomandat) Selectează Codul Ţării Tale şi Introdu Numărul Tău De Telefon Mobil Pentru A Primi O Notificare Text De Aducere Aminte Cu 15 Minute Înainte De Începerea Webinarului';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='REZERVĂ-ŢI LOCUL!';
$lang['REG_FORM_H2'] ='ÎNREGISTRARE PENTRU WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] =' În Ce Zi Doreşti Să Participi?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Ce Oră Este Potrivită Pentru Tine?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Unde Doreşti Să Trimitem Învitaţia?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notificări';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='ÎNREGISTREAZĂ-TE ACUM!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Prezentarea Webinarului';
$lang['PBOX_TXT_TOPIC'] ='Subiect';
$lang['PBOX_TXT_HOST'] ='Host';
$lang['PBOX_TXT_PRESENTER'] ='Prezentator';
$lang['PBOX_TXT_DATEANDTIME'] ='Ziua şi Ora';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='PORNIT!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Prezentat de';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Detaliile Webinarului';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Prezentator';
$lang['TY_SCHEDDATE'] ='Ziua Programată';
$lang['TY_SCHEDTIME'] ='Ora Programată';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Linkul tău unic pentru webinar';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Adaugă La Calendarul Tău iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Adaugă La Calendarul Tău Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Adaugă La Calendarul Tău Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Faceți clic pe Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Spune prietenilor tăi despre acest webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='ŞI DEBLOCHEAZĂ-ŢI BONUSUL GRATUIT!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Selectează Metoda Dorită De Recomandare';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Trimite Email La Prietenii Tăi';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Introdu numele tău';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Introdu emailul tău';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Numele Prietenului(ei)';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Email-ul Prietenului(ei)';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Acesta este emailul care va fi trimis prietenilor tăi';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Linia Subiect';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Corpul Mesajului';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Trimite un e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Postează Acest Mesaj Pe Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifică Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Postează pe Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verificați adresa URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Imediat ce ai postat mesajul pe Facebook, introdu mai jos linkul de Facebook unde ai postat pentru a verifica postarea. După verificare, bonusul tău va fi deblocat imediat!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Trimite Pe Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifică Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Imediat ce ai postat mesajul pe Twitter, introdu mai jos linkul contului tău de Twitter unde ai postat pentru a verifica postarea. După verificare, bonusul tău va fi deblocat imediat!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Acest mesaj';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Felicitări';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='AI DEBLOCAT BONUSUL TĂU!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DESCĂRCARE GRATUITĂ';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Click dreapta pe linkul de mai jos şi selectează "salvează ca.."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Click aici';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Numele este gol!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Adresa de Email Invalidă!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL-ul nu este verificat!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinarul Va Porni În Curand...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Prezentator';
$lang['COUNTDOWN_TXT_DATE'] ='Ziua Programată';
$lang['COUNTDOWN_TXT_TIME'] ='Ora Programată';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Se conectează la webinar, te rog aşteaptă...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Când trimiţi întrebarea ta, introdu numele şi emailul tău pentru a-ţi putea răspunde pe email la mesajul tău.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Numele Tău (Necesar)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Emailul Tău (Necesar)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Scrie Mesajul Tău';
$lang['LIVE_CBOX2_TXT_1'] ='Fereastră de Chat';
$lang['LIVE_CBOX2_TXT_2'] ='Scrie către';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Toţi';
$lang['LIVE_CBOX2_TXT_3'] ='Mesaj';
$lang['LIVE_CBOX2_TXT_4'] ='Trimiteti intrebarile';
$lang['LIVE_CBOX2_TXT_5'] ='Introduceți numele dvs. și e-mail astfel încât să putem trimite răspunsul nostru';

$lang['LIVE_CBOX2_TXT_6'] ='furnizeze toate domeniile complet';
$lang['LIVE_CBOX2_TXT_7'] ='Mesaj trimis!';
$lang['LIVE_CBOX2_TXT_8'] ='Ne pare rău, a trimis deja mesaje';

$lang['LIVE_CBOX2_SUBMIT'] ='Trimite mesaj';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Participanții înregistrați';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Replay Participanți';
$lang['LIVE_ATTENDEELIST_HOST'] ='gazdă';
//POLL
$lang['LIVE_POLL_TXT1'] ='REZULTATUL';
$lang['LIVE_POLL_TXT2'] ='Vă mulțumim pentru votul dvs.';
$lang['LIVE_POLL_TXT3'] ='Rezultatele sondajului finale vor fi disponibile în curând ...';

$lang['LIVE2_H1'] ='APLICAŢIA DE WEBINAR PORNITĂ';
$lang['LIVE2_TXT1'] ='Bine ai venit';
$lang['LIVE2_TXT2'] ='Setări';
$lang['LIVE2_TXT3'] ='Înregistrează-Te';
$lang['LIVE2_TXT4'] ='Ieşi';
$lang['LIVE2_TXT5'] ='Prezentare';
$lang['LIVE2_TXT6'] ='Membri';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Actiune Webinar Evenimente';
$lang['LIVE2_TXT9'] = 'Introduceti intrebari sau comentarii';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='PORNIT';
$lang['LIVE3_TXT3'] ='ID-ul Webinarului';
$lang['LIVE3_POPUP1'] ='Opţiunile au fost oprite de administratorul webinarului';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Cu siguranţă doreşti să pleci? este posibil să nu te mai poţi loga în camera de webinar!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'ianuarie';
$langmonths['february'] = 'februarie';
$langmonths['march'] = 'martie';
$langmonths['april'] = 'aprilie';
$langmonths['may'] = 'mai';
$langmonths['june'] = 'iunie';
$langmonths['july'] = 'iulie';
$langmonths['august'] = 'august';
$langmonths['september'] = 'septembrie';
$langmonths['october'] = 'octombrie';
$langmonths['november'] = 'noiembrie';
$langmonths['december'] = 'decembrie';

$langdays = array();
$langdays['monday'] = 'luni';
$langdays['tuesday'] = 'marți';
$langdays['wednesday'] = 'miercuri';
$langdays['thursday'] = 'joi';
$langdays['friday'] = 'vineri';
$langdays['saturday'] = 'sâmbătă';
$langdays['sunday'] = 'duminică';

?>