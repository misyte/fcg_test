<?php
# -----------------------------------------------
# LANGUAGE: ENGLISH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Registrering till Webbseminarie';
//presentation-box
$lang['REG_PBOX_H1'] ='Presentation av webbseminarie';
$lang['REG_PBOX_TOPIC'] ='Ämne';
$lang['REG_PBOX_PRESENTER'] ='Presentatör';
$lang['REG_PBOX_HOST'] ='Värd';
$lang['REG_PBOX_DATETIME'] ='Datum och tid';
$lang['REG_PBOX_DATETIME_VAL'] ='Välj från menyn';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landskod för mobilnummer';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Fyll i ditt mobilnummer här...';
$lang['REG_FORM_SMS_TXT1'] ='SMS-påminnelse: Ja tack, skicka en påminnelse via SMS strax innan Webbseminariet börjar.';
$lang['REG_FORM_SMS_TXT2'] ='Välj landskod och fyll i ditt mobilnummer för att få en SMS-påminnelse skickat till dig 15 minuter innan start';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Vänligen fyll i datumfältet';
$lang['REG_FORM_ALERT_TIME'] ='Vänligen fyll i tidsfältet';
$lang['REG_FORM_ALERT_NAME'] ='Vänligen fyll i namnfältet';
$lang['REG_FORM_ALERT_EMAIL'] ='Vänligen fyll i epost';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Vänligen fyll i en giltlig e-postadress';
$lang['REG_FORM_TXTBOX_NAME'] ='Fyll i ditt namn här...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Fyll i din e-postadress här...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Din lokala tid';
$lang['REG_FORM_TXT_SELDATE'] ='Välj önskat datum';
$lang['REG_FORM_TXT_SELTIME'] ='Välj önskat datum först';
$lang['REG_FORM_TXT_SELTIME_2'] ='Välj önskad tid';
$lang['REG_FORM_TXT_SELECTDATE'] ='Välj datum';
$lang['REG_FORM_TXT_SELECTTIME'] ='Välj tid';
$lang['REG_FORM_TXT_RN_1'] = 'Titta gårdagens webbseminarium repris NU!';
$lang['REG_FORM_TXT_RN_2'] = 'Just nu!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='REGISTRERING FÖR WEBBSEMINARIE';
$lang['REG_FORM_H2'] ='RESERVERA DIN PLATS';
$lang['REG_FORM_H3'] ='REGISTRERA DIG TILL WEBBSEMINARIET';
$lang['REG_FORM_SMS_TXT1'] ='Jag vill få en SMS-påminnelse strax innan webbseminariet börjar';
$lang['REG_FORM_SMS_TXT2'] ='(Valfritt, men rekommenderas) Välj landskod och fyll i ditt mobilnummer för att få en SMS-påminnelse strax innan Webbseminariet börjar';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='RESERVERA DIN PLATS!';
$lang['REG_FORM_H2'] ='REGISTRERING FÖR WEBBSEMINARIE';
$lang['REG_FORM_TXT_SELECTDATE'] ='Vilken dag vill du deltaga?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Vilken tidpunkt passar dig bäst?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Vart skall vi skicka inbjudan?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Uppdateringar';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTRERA DIG!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Presentation av webbseminarie';
$lang['PBOX_TXT_TOPIC'] ='Ämne';
$lang['PBOX_TXT_HOST'] ='Värd';
$lang['PBOX_TXT_PRESENTER'] ='Presentatör';
$lang['PBOX_TXT_DATEANDTIME'] ='Datum och tid';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='På';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Presenteras av';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Beskrivning av webbseminarie';
$lang['TY_WEBINAR'] ='Webbseminarie';
$lang['TY_PRESENTER'] ='Presentatör';
$lang['TY_SCHEDDATE'] ='Bokad datum';
$lang['TY_SCHEDTIME'] ='Bokad tid';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Din unika länk till webbseminariet';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Lägg till i din iCal-kalender';
$lang['TY_TXT_GOOGLE_CAL'] = 'Lägg till i din Google-kalender';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Lägg till i din Outlook-kalender';
$lang['TY_TXT_CLICK_VER'] = 'Klicka på version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Berätta för dina vänner/bekanta om detta Webbseminarie';
$lang['TY_TXT_FREEBONUS_H2'] ='OCH FÅ TILLGÅNG TILL EXTRAMATERIAL';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Välj sätt att dela';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Skicka e-post till dina vänner';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Fyll i namn';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Fyll i din e-post';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Din väns namn';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Din väns e-post';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Detta är det meddelande som skickas till dina vänner';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Ämnestitel';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Innehåll i meddelande';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Skicka e-post!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Posta detta meddelande på Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifiera Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Posta på Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifiera URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='När du har delat ditt meddelande på Facebook, fyll i din länk till detta inlägg på Facebook för att vi skall kunna verifiera det, och ge dig omedelbar tillgång till extramaterialet!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Dela på Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifiera Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='När du har delat ditt meddelande på Twitter, fyll i din länk till detta inlägg på Twitter för att verifiera det, och ge dig omedelbar tillgång till extramaterialet!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet meddelande';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Grattulerar';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='DU HAR FÅTT TILLGÅNG TILL DITT EXTRAMATERIAL';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='KOSTNADSFRI NEDLADDNING';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Högerklicka på länken nedan och välj "Spara som..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klicka här';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Namnfält är tomt!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Felaktig e-postadress';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='Länk är inte verifierad!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webbseminarie börjar snart...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presentatör';
$lang['COUNTDOWN_TXT_DATE'] ='Bokad datum';
$lang['COUNTDOWN_TXT_TIME'] ='Bokad tid';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Ansluter till webbseminarie';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='När du ställer en fråga bifoga även ditt namn och e-postadress så vi kan nå dig om vi inte hinner svara på din fråga under pågående webbseminarie';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Ditt namn (obligatoriskt)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Din e-post (obligatoriskt)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Skriv din fråga';
$lang['LIVE_CBOX2_TXT_1'] ='Chattfönster';
$lang['LIVE_CBOX2_TXT_2'] ='Skriv till';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Samtliga';
$lang['LIVE_CBOX2_TXT_3'] ='Meddelande';
$lang['LIVE_CBOX2_TXT_4'] ='Skicka dina frågor';
$lang['LIVE_CBOX2_TXT_5'] ='Fyll i ditt namn och e-post så att vi kan skicka dig vårt svar';

$lang['LIVE_CBOX2_TXT_6'] ='ge alla fält fullständigt';
$lang['LIVE_CBOX2_TXT_7'] ='Meddelandet skickat!';
$lang['LIVE_CBOX2_TXT_8'] ='Tyvärr Meddelande skickat redan';

$lang['LIVE_CBOX2_SUBMIT'] ='Skicka meddelande';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Registrerade deltagare';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Deltagare för repris';
$lang['LIVE_ATTENDEELIST_HOST'] ='Värd';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTAT';
$lang['LIVE_POLL_TXT2'] ='Tack för din röst';
$lang['LIVE_POLL_TXT3'] ='Undersökningen slutliga resultaten kommer att finnas tillgängliga inom kort ...';

$lang['LIVE2_H1'] ='Applikation för webbseminarie har startat';
$lang['LIVE2_TXT1'] ='Välkommen';
$lang['LIVE2_TXT2'] ='Inställningar';
$lang['LIVE2_TXT3'] ='Logga in';
$lang['LIVE2_TXT4'] ='Logga ut';
$lang['LIVE2_TXT5'] ='Presentation';
$lang['LIVE2_TXT6'] ='Medlemmar';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar åtgärder händelser';
$lang['LIVE2_TXT9'] = 'Skriv in dina frågor eller kommentarer';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='På';
$lang['LIVE3_TXT3'] ='ID för Webbseminarie';
$lang['LIVE3_POPUP1'] ='Allternativ har stängts av av admin';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Vill du verkligen avsluta? Det är inte säkert att att du kan logga in på nytt';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'januari';
$langmonths['february'] = 'februari';
$langmonths['march'] = 'mars';
$langmonths['april'] = 'april';
$langmonths['may'] = 'maj';
$langmonths['june'] = 'juni';
$langmonths['july'] = 'juli';
$langmonths['august'] = 'augusti';
$langmonths['september'] = 'september';
$langmonths['october'] = 'oktober';
$langmonths['november'] = 'november';
$langmonths['december'] = 'december';

$langdays = array();
$langdays['monday'] = 'Måndag';
$langdays['tuesday'] = 'tisdag';
$langdays['wednesday'] = 'onsdag';
$langdays['thursday'] = 'Torsdag';
$langdays['friday'] = 'fredag';
$langdays['saturday'] = 'Lördag';
$langdays['sunday'] = 'Söndag';

?>