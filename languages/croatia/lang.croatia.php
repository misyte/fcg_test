<?php
# -----------------------------------------------
# LANGUAGE: CROTIA                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Predbilježba za webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar predbilježba';
$lang['REG_PBOX_TOPIC'] ='Tema';
$lang['REG_PBOX_PRESENTER'] ='Predavačr';
$lang['REG_PBOX_HOST'] ='Domaćin';
$lang['REG_PBOX_DATETIME'] ='Datum i vrijeme';
$lang['REG_PBOX_DATETIME_VAL'] ='Odaberite u izborniku:';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Pozivni broj zemlje';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Upišite svoj telefonski broj';
$lang['REG_FORM_SMS_TXT1'] ='SMS upozorenja: Da, želim primati SMS podsjetnik prije početka webinara (preporučeno, ali nije obvezno)';
$lang['REG_FORM_SMS_TXT2'] ='SMS upozorenja: Da, želim primati SMS podsjetnik prije početka webinara (preporučeno, ali nije obvezno)';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Upišite datum';
$lang['REG_FORM_ALERT_TIME'] ='Upišite vrijeme';
$lang['REG_FORM_ALERT_NAME'] ='Upišite  ime';
$lang['REG_FORM_ALERT_EMAIL'] ='Upišite  e-mail adresu';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Upišite valjanu e-mail adresu';
$lang['REG_FORM_TXTBOX_NAME'] ='Upišite svoje ime';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Upišite svoju e-mail adresu';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Lokalno vrijeme';
$lang['REG_FORM_TXT_SELDATE'] ='Odaberite željeni datum';
$lang['REG_FORM_TXT_SELTIME'] ='Najprije odaberite datum';
$lang['REG_FORM_TXT_SELTIME_2'] ='Odaberite željeno vrijeme';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Odaberite svoj datum';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Odaberite svoje vrijeme';
$lang['REG_FORM_TXT_RN_1'] = 'Pogledajte jučerašnju replay webinar ODMAH!';
$lang['REG_FORM_TXT_RN_2'] = 'odmah!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='PREDBILJEŽBA  ZA WEBINAR';
$lang['REG_FORM_H2'] ='REZERVIRAJTE SVOJE MJESTO';
$lang['REG_FORM_H3'] ='REGISTER TO THE WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Želim primati SMS upozorenja prije početka programa';
$lang['REG_FORM_SMS_TXT2'] ='(preporuka) Upišite pozivni broj zemlje i broj mobitela, kako biste mogli primati SMS podsjetnike 15 minuta prije početka webinara';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Rezervirajte svoje mjesto';
$lang['REG_FORM_H2'] ='PREDBILJEŽBA  ZA WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] ='Kojega biste dana željeli sudjelovati';
$lang['REG_FORM_TXT_SELECTTIME'] ='Koje Vam vrijeme najviše odgovara';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Where To Send The Invitation?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Obavijesti';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='Predbilježite se sada';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Predstavljanje preko webinara';
$lang['PBOX_TXT_TOPIC'] ='Tema';
$lang['PBOX_TXT_HOST'] ='Domaćin';
$lang['PBOX_TXT_PRESENTER'] ='Predavač';
$lang['PBOX_TXT_DATEANDTIME'] ='Date and Time';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='Uključeno!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Predavač';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Pojedinosti webinara';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Predavač';
$lang['TY_SCHEDDATE'] ='Zakazani datum';
$lang['TY_SCHEDTIME'] ='Zakazano vrijeme';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Vaš jedinstveni link za webinar';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Dodajte u svoj ICal kalendar';
$lang['TY_TXT_GOOGLE_CAL'] = 'Dodajte u svoj Google kalendar';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Dodajte u svoj Outlook kalendar';
$lang['TY_TXT_CLICK_VER'] = 'Kliknite na verziji';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Obavijestite prijatelje o webinaru';
$lang['TY_TXT_FREEBONUS_H2'] ='I OTKLJUČAJTE SVOJU BESPLATNU NAGRADU!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Odaberite način izlaganja';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Pošaljite e-mail prijateljima';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Upišite svoje ime';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Upišite svoju e-mail adresu';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Ime Vašega prijatelja';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='E-mail adresa Vašega prijatelja';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Ovo je e-mail koji će biti poslan Vašim prijateljima';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Osobni pristup';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Sadržaj e-maila';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Pošaljite e-poštu!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Stavite ovu poruku na Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Potvrdite Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post Dana Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Provjerite URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Nakon što stavite poruku na Facebook,unesite na kraju svoj Facebook URL, kako biste to potvrdili. Nakon potvrde, Vaš skriveni bonus odmah će se  otključati!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Twitajte ovo na Twitteru';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Potvrdite Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Nakon što stavite poruku na Twitter,unesite na kraju  svoj Twitter URL, kako biste to potvrdili. Nakon potvrde, Vaš skriveni bonus odmah će se  otključati!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Cvrkut Ovaj Poruka';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Čestitamo!';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='OTKLJUČALI STE SVOJ BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='Besplatno učitavanje!';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Kliknite desnom tipkom miša na donji link, te odaberite spremi kao/save as';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Kliknite ovdje';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Nedostaje ime!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Pogrešna e-mail adresa!Nedostaje ime!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL nije potvđen!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinar uskoro započinje';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Predavač';
$lang['COUNTDOWN_TXT_DATE'] ='Zakazani datum';
$lang['COUNTDOWN_TXT_TIME'] ='Zakazano vrijeme';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Spajanje na webinar, molimo pričekajte...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Uz postavljeno pitanje, upišite svoje ime i e-mail adresu, kako bismo Vam odgovorili u slučaju da zbog  bilo kog razloga to nismo mogli tijekom webinara';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Vaše ime (obvezno)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Vaš e-mail (obvezno)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Napišite svoje pitanje';
$lang['LIVE_CBOX2_TXT_1'] ='Rubrika za chat';
$lang['LIVE_CBOX2_TXT_2'] ='Pišite';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Sve';
$lang['LIVE_CBOX2_TXT_3'] ='Poruka';
$lang['LIVE_CBOX2_TXT_4'] ='Pošaljite vaša pitanja';
$lang['LIVE_CBOX2_TXT_5'] ='Unesite svoje ime i e-mail, tako možemo vam poslati naš odgovor';
$lang['LIVE_CBOX2_TXT_6'] ='osigurati sva polja potpunosti';
$lang['LIVE_CBOX2_TXT_7'] ='Poruka je poslana!';
$lang['LIVE_CBOX2_TXT_8'] ='Nažalost, poruka je već poslana';


$lang['LIVE_CBOX2_SUBMIT'] ='Pošaljite poruku';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Predbilježeni sudionici';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Odgovor sudionika';
$lang['LIVE_ATTENDEELIST_HOST'] ='Domaćin';
//POLL
$lang['LIVE_POLL_TXT1'] ='Rezultat';
$lang['LIVE_POLL_TXT2'] ='Hvala vam za vaše glasova';
$lang['LIVE_POLL_TXT3'] ='U anketi je konačni rezultati će uskoro biti dostupna ...';

$lang['LIVE2_H1'] ='Započinje webinar aplikacija';
$lang['LIVE2_TXT1'] ='Dobrodošli!';
$lang['LIVE2_TXT2'] ='Postavke';
$lang['LIVE2_TXT3'] ='Upišite se';
$lang['LIVE2_TXT4'] ='Ispišite se';
$lang['LIVE2_TXT5'] ='Prezentacija';
$lang['LIVE2_TXT6'] ='Sudionici';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Akcija Događanja';
$lang['LIVE2_TXT9'] = 'Unesite svoja pitanja ili komentare';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='Uključeno';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Opcije isključila webinar administracija';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Sigurni ste da želite otići? Možda se nećete moći  ponovno logirati!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'siječanj';
$langmonths['february'] = 'veljača';
$langmonths['march'] = 'ožujak';
$langmonths['april'] = 'travanj';
$langmonths['may'] = 'svibanj';
$langmonths['june'] = 'lipanj';
$langmonths['july'] = 'srpanj';
$langmonths['august'] = 'kolovoz';
$langmonths['september'] = 'rujan';
$langmonths['october'] = 'listopad';
$langmonths['november'] = 'studeni';
$langmonths['december'] = 'prosinac';

$langdays = array();
$langdays['monday'] = 'ponedjeljak';
$langdays['tuesday'] = 'utorak';
$langdays['wednesday'] = 'srijeda';
$langdays['thursday'] = 'četvrtak';
$langdays['friday'] = 'petak';
$langdays['saturday'] = 'subota';
$langdays['sunday'] = 'nedjelja';

?>