<?php
# -----------------------------------------------
# LANGUAGE: ITALY                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='registrazione al webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Registrazione Webinar';
$lang['REG_PBOX_TOPIC'] ='Argomento';
$lang['REG_PBOX_PRESENTER'] ='Presentatore';
$lang['REG_PBOX_HOST'] ='Ospite';
$lang['REG_PBOX_DATETIME'] ='Data e Ora';
$lang['REG_PBOX_DATETIME_VAL'] ='Seleziona dal menù';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Codice nazione per il Telefonino';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Inserisci il tuo Numero di Telefono Qui ...';
$lang['REG_FORM_SMS_TXT1'] ='Avvisi SMS: Si, inviami un avviso via SMS prima che il webinar abbia inizio (facoltativo ma consigliato).';
$lang['REG_FORM_SMS_TXT2'] ='Seleziona il tuo codice cliente e digita il tuo Numero di Telefono per ricevere un avviso via SMS 15 Minuti prima che il Webinar abbia inizio';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Compila il campo data';
$lang['REG_FORM_ALERT_TIME'] ='Compila il campo orario';
$lang['REG_FORM_ALERT_NAME'] ='Compila il campo nome';
$lang['REG_FORM_ALERT_EMAIL'] ='Compila il campo email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Inserisci un Indirizzo email';
$lang['REG_FORM_TXTBOX_NAME'] ='Inserisci il tuo Nome Qui ...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Inserisci la tua Email Qui ...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Orario Locale';
$lang['REG_FORM_TXT_SELDATE'] ='Seleziona una data desiderata';
$lang['REG_FORM_TXT_SELTIME'] ='Seleziona prima la tua data';
$lang['REG_FORM_TXT_SELTIME_2'] ='Seleziona l\'orario desiderato';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Seleziona la tua Data';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Seleziona il tuo Orario';
$lang['REG_FORM_TXT_RN_1'] = 'Guarda riproduzione webinar di ieri ORA!';
$lang['REG_FORM_TXT_RN_2'] = 'in questo momento!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Registrazione Webinar';
$lang['REG_FORM_H2'] ='Prenota il tuo Posto!';
$lang['REG_FORM_H3'] ='REGISTRATI AL WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Vorrei ricevere un avviso via SMS prima che l\'evento cominci';
$lang['REG_FORM_SMS_TXT2'] ='(Facoltativo ma consigliato) Seleziona il tuo Prefisso Nazionale e digita il tuo Numero di Telefono per ricevere un avviso via SMS 15 Minuti prima che il Webinar abbia inizio';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Prenota il tuo Posto!';
$lang['REG_FORM_H2'] ='Registrazione Webinar';
$lang['REG_FORM_TXT_SELECTDATE'] ='In che Giorno vuoi Partecipare?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Quale Orario è migliore per te?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Dove inviare l\'Invito:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notifiche';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTRATI ADESSO!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Presentazione Webinar';
$lang['PBOX_TXT_TOPIC'] ='Argomento';
$lang['PBOX_TXT_HOST'] ='Ospite';
$lang['PBOX_TXT_PRESENTER'] ='Presentatore';
$lang['PBOX_TXT_DATEANDTIME'] ='Data e Ora';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Presentato da';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Dettagli Webinar';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Presentatore';
$lang['TY_SCHEDDATE'] ='Data programmata';
$lang['TY_SCHEDTIME'] ='Orario programmato';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Il tuo link unico al Webinar';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Aggiungi al tuo Calendario iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Aggiungi al tuo Google Calendar';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Aggiungi al tuo Calendario Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Fare clic sulla versione';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Dì ai tuoi amici di questo webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='E SBLOCCA I TUOI BONUS IN REGALO!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Seleziona il tuo Metodo di Comunicazione';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Invia un\'eMail ai tuoi Amici ';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Inserisci il tuo nome';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Inserisci la tua email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='I nomi dei tuoi amici';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Le email dei tuoi amici';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Questa è l\'email che verrà inviata ai tuoi amici';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Oggetto dell\'email';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Corpo del messaggio';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Invia email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Posta questo Messaggio su Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifica Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Pubblica su Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifica URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Una volta postato il messaggio su Facebook, digita il link della tua pagina Facebook qui sotto per verificare il post. Dopo la verifica, i tuoi bonus in regalo verranno sbloccati!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet This On Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifica Tweeter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Una volta postato il messaggio su Tweeter, digita il link della tua pagina Tweeter qui sotto per verificare il Tweet. Una volta verificato, il tuo bonus in regalo verrà sbloccato!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet questo messaggio';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Congratulazioni';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='HAI SBLOCCATO IL TUO BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DOWNLOAD GRATIS';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Clicca con il tasto destro del mouse e seleziona "salva come ...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Click here';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Il campo Nome è vuoto!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Indirizzo Email non Valido!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL not verified!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='il webinar inizierà a breve ...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presentatore';
$lang['COUNTDOWN_TXT_DATE'] ='Data Programmata';
$lang['COUNTDOWN_TXT_TIME'] ='Orario Programmato';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Connessione al webinar, per favore attendi...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Quando invii la tua domanda, inserisci anche il tuo nome ed email così che possiamo risponderti via email nel caso non riusciamo a farlo durante il webinar per una qualsiasi ragione.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Il tuo Nome (Obbligatorio)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='La tua Email (Obbligatorio)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Digita la tua Domanda';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Scrivi a';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Tutti';
$lang['LIVE_CBOX2_TXT_3'] ='Messaggio';
$lang['LIVE_CBOX2_TXT_4'] ='Inviare le vostre domande';
$lang['LIVE_CBOX2_TXT_5'] ='Inserisci il tuo nome ed e-mail in modo che possiamo inviare la nostra risposta';
$lang['LIVE_CBOX2_TXT_6'] ='fornire tutti i campi completamente';
$lang['LIVE_CBOX2_TXT_7'] ='Messaggio inviato!';
$lang['LIVE_CBOX2_TXT_8'] ='Spiacenti, Messaggio già inviato';
$lang['LIVE_CBOX2_SUBMIT'] ='Invia Messaggio';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Partecipanti Registrati';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Partecipanti Replica';
$lang['LIVE_ATTENDEELIST_HOST'] ='Ospite';
//POLL
$lang['LIVE_POLL_TXT1'] ='RISULTATI';
$lang['LIVE_POLL_TXT2'] ='Grazie per il tuo voto';
$lang['LIVE_POLL_TXT3'] ='I risultati del sondaggio finale sarà disponibile a breve ...';

$lang['LIVE2_H1'] ='IL WEBINAR HA INIZIO';
$lang['LIVE2_TXT1'] ='Benvenuto';
$lang['LIVE2_TXT2'] ='Impostazioni';
$lang['LIVE2_TXT3'] ='Login';
$lang['LIVE2_TXT4'] ='Logout';
$lang['LIVE2_TXT5'] ='Presentazione';
$lang['LIVE2_TXT6'] ='Utenti';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar azione Eventi';
$lang['LIVE2_TXT9'] = 'Inserisci le tue domande o commenti';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Le Opzioni sono state disattivate dall\'amministratore del Webinar';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Sei sicuro di voler andare via? Potresti non essere più in grado di accedere al Webinar!';




// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'gennaio';
$langmonths['february'] = 'febbraio';
$langmonths['march'] = 'marzo';
$langmonths['april'] = 'aprile';
$langmonths['may'] = 'maggio';
$langmonths['june'] = 'giugno';
$langmonths['july'] = 'luglio';
$langmonths['august'] = 'agosto';
$langmonths['september'] = 'settembre';
$langmonths['october'] = 'ottobre';
$langmonths['november'] = 'novembre';
$langmonths['december'] = 'dicembre';

$langdays = array();
$langdays['monday'] = 'lunedi';
$langdays['tuesday'] = 'martedì';
$langdays['wednesday'] = 'mercoledì';
$langdays['thursday'] = 'giovedi';
$langdays['friday'] = 'venerdì';
$langdays['saturday'] = 'sabato';
$langdays['sunday'] = 'domenica';


?>