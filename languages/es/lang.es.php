<?php
# -----------------------------------------------
# LANGUAGE: SPANISH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Regístrate Para El Seminario Online';
//presentation-box
$lang['REG_PBOX_H1'] ='Presentación del Seminario Online';
$lang['REG_PBOX_TOPIC'] ='Tema';
$lang['REG_PBOX_PRESENTER'] ='Presentador';
$lang['REG_PBOX_HOST'] ='Anfitrión';
$lang['REG_PBOX_DATETIME'] ='Día y hora';
$lang['REG_PBOX_DATETIME_VAL'] ='Selecciona del menú';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Código del país para teléfono móvil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Escribe aquí tu número de teléfono';
$lang['REG_FORM_SMS_TXT1'] ='Alerta por SMS: Sí, envíenme un recordatorio por SMS antes de que comience el Seminario Online (opcional, pero recomendable)';
$lang['REG_FORM_SMS_TXT2'] ='Elige tu código de país y escribe tu número de teléfono móvil y 15 minutos antes de que comience el Seminario Online recibirás un mensaje de texto como recordatorio.';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Por favor elige la fecha';
$lang['REG_FORM_ALERT_TIME'] ='Por favor elige la hora';
$lang['REG_FORM_ALERT_NAME'] ='Por favor escribe tu nombre';
$lang['REG_FORM_ALERT_EMAIL'] ='Por favor escribe tu correo electrónico';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Escribe una dirección de correo electrónico válida';
$lang['REG_FORM_TXTBOX_NAME'] ='Escribe Tu Nombre Aquí';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Escribe Tu Email Aquí';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Tu hora local';
$lang['REG_FORM_TXT_SELDATE'] ='Selecciona La Fecha';
$lang['REG_FORM_TXT_SELTIME'] ='Primero Elige Una Fecha';
$lang['REG_FORM_TXT_SELTIME_2'] ='Elige La Hora';
$lang['REG_FORM_TXT_SELECTDATE'] ='Selecciona La Fecha';
$lang['REG_FORM_TXT_SELECTTIME'] ='Selecciona La Hora';
$lang['REG_FORM_TXT_RN_1'] = 'ver la repetición del webinar ahora!';
$lang['REG_FORM_TXT_RN_2'] = 'Ahora!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Inscripción Al Seminario Online';
$lang['REG_FORM_H2'] ='Reserva Tu Lugar';
$lang['REG_FORM_H3'] ='REGÍSTRATE PARA EL SEMINARIO ONLINE';
$lang['REG_FORM_SMS_TXT1'] ='Quiero Recibir Un SMS Antes De Que Empiece El Evento';
$lang['REG_FORM_SMS_TXT2'] ='(Opcional pero muy recomendable) Elige tu código de País y escribe tu número de teléfono Móvil para recibir un recordatorio 15 minutos antes del inicio del Seminario Online';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Reserva Tu Lugar';
$lang['REG_FORM_H2'] ='Inscripción Al Seminario Online';
$lang['REG_FORM_TXT_SELECTDATE'] ='¿Qué Día Te Gustaría Asistir?';
$lang['REG_FORM_TXT_SELECTTIME'] ='¿Qué Hora Prefieres?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='¿A dónde enviamos la invitación?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notificaciones';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='¡INSCRIBETE YA!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Presentación del Seminario Online';
$lang['PBOX_TXT_TOPIC'] ='Tema';
$lang['PBOX_TXT_HOST'] ='Anfitrión';
$lang['PBOX_TXT_PRESENTER'] ='Presentador';
$lang['PBOX_TXT_DATEANDTIME'] ='Día y hora';
$lang['PBOX_TXT_STATUS'] ='ESTADO';
$lang['PBOX_TXT_STATUS_VAL'] ='TRANSMITIENDO!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Presentado por';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Detalles del Seminario Online';
$lang['TY_WEBINAR'] ='Seminario Online';
$lang['TY_PRESENTER'] ='Presentador';
$lang['TY_SCHEDDATE'] ='Fecha programada';
$lang['TY_SCHEDTIME'] ='Hora programada';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Tu enlace exclusivo para el Seminario Online';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Añadir a tu calendario iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Añadir a tu calendario de Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Añadir a tu calendario de Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Haga clic en la versión';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Invita a tus amigos a este Seminario Online';
$lang['TY_TXT_FREEBONUS_H2'] ='¡Y RECIBE UN REGALO GRATIS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Elige tu forma de recomendación';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Envíales Un Correo Electrónico A Tus Amigos';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Escribe Tu Nombre';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Escribe Tu Correo Electrónico';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Nombre de tu amigo';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Email de tus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Este es el email que le enviaremos a tus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Asunto';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Cuerpo del Email';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Enviar correo!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Publicar este mensaje en Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verificar Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Publicar en Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verificar URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Una vez que hayas publicado el mensaje en Facebook, copia en la casilla de abajo la URL de la página de Facebook para verificar la publicación. Una vez verificada, ¡recibirás el regalo gratis inmediatamente!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweetea esto en Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verificar Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Una vez que hayas publicado el mensaje en Twitter, copia en la casilla de abajo la URL de la página de Twitter para poder verificarlo. Una vez verificado, ¡recibirás el regalo gratis inmediatamente!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet este mensaje';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Felicidades';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='¡HAS DESBLOQUEADO TU REGALO!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DESCARGA GRATUITA';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Click derecho del mouse sobre el siguiente enlace y selecciona "Guardar como…"';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='¡Falta el Nombre!';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='¡Falta el Nombre!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='¡Tu Email es invalido!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='¡Tu dirección de URL no ha sido verificada!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='El Seminario Online comenzará en unos momentos...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presentador';
$lang['COUNTDOWN_TXT_DATE'] ='Fecha programada';
$lang['COUNTDOWN_TXT_TIME'] ='Hora programada';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Conectando con el Seminario Online, por favor espere...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Además de tu pregunta, también escribe tu nombre y dirección de correo electrónico para que podamos enviarte la respuesta en caso de que por algún motivo no te podamos contestar durante el seminario.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Tu Nombre (Requerido)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Tu email (Requerido)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Escribe Tu Pregunta';
$lang['LIVE_CBOX2_TXT_1'] ='Chat';
$lang['LIVE_CBOX2_TXT_2'] ='Escribir a';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Todos';
$lang['LIVE_CBOX2_TXT_3'] ='Mensaje';
$lang['LIVE_CBOX2_TXT_4'] ='Envíe sus preguntas';
$lang['LIVE_CBOX2_TXT_5'] ='Introduce tu nombre y correo electrónico para que podamos enviarle nuestra respuesta';
$lang['LIVE_CBOX2_TXT_6'] ='proporcionar todos los campos completamente';
$lang['LIVE_CBOX2_TXT_7'] ='Mensaje enviado!';
$lang['LIVE_CBOX2_TXT_8'] ='Lo sentimos, Mensaje enviado ya';
$lang['LIVE_CBOX2_SUBMIT'] ='Enviar mensaje';

//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Participantes  Inscritos';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Participantes en la grabación';
$lang['LIVE_ATTENDEELIST_HOST'] ='Anfitrión';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTADO';
$lang['LIVE_POLL_TXT2'] ='Gracias por su voto';
$lang['LIVE_POLL_TXT3'] ='Los resultados finales de la encuesta estará disponible en breve...';

$lang['LIVE2_H1'] ='SEMINARIO EN PROCESO';
$lang['LIVE2_TXT1'] ='Bienvenido';
$lang['LIVE2_TXT2'] ='Configuración';
$lang['LIVE2_TXT3'] ='Ingresar';
$lang['LIVE2_TXT4'] ='Salir';
$lang['LIVE2_TXT5'] ='Presentación';
$lang['LIVE2_TXT6'] ='Miembros';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Acción Eventos Webinar';
$lang['LIVE2_TXT9'] = 'Ingrese sus preguntas o comentarios';
$lang['LIVE3_TXT1'] ='ESTADO';
$lang['LIVE3_TXT2'] ='TRANSMITIENDO';
$lang['LIVE3_TXT3'] ='ID del Seminario Online';
$lang['LIVE3_POPUP1'] ='Las opciones han sido desactivas por el administrador del Seminario';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='¿Estás seguro que deseas salir? ¡No podrás volver a ingresar a esta sala!';




// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'enero';
$langmonths['february'] = 'febrero';
$langmonths['march'] = 'marzo';
$langmonths['april'] = 'abril';
$langmonths['may'] = 'mayo';
$langmonths['june'] = 'junio';
$langmonths['july'] = 'julio';
$langmonths['august'] = 'agosto';
$langmonths['september'] = 'septiembre';
$langmonths['october'] = 'octubre';
$langmonths['november'] = 'noviembre';
$langmonths['december'] = 'diciembre';

$langdays = array();
$langdays['monday'] = 'lunes';
$langdays['tuesday'] = 'martes';
$langdays['wednesday'] = 'miércoles';
$langdays['thursday'] = 'jueves';
$langdays['friday'] = 'viernes';
$langdays['saturday'] = 'sábado';
$langdays['sunday'] = 'domingo';

?>