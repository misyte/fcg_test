<?php
# -----------------------------------------------
# LANGUAGE: FRENCH                    UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='S\'inscrire au webinaire';
//presentation-box
$lang['REG_PBOX_H1'] ='Inscription au webinaire';
$lang['REG_PBOX_TOPIC'] ='Sujet';
$lang['REG_PBOX_PRESENTER'] ='Présentateur';
$lang['REG_PBOX_HOST'] ='Hôte';
$lang['REG_PBOX_DATETIME'] ='Date et heure';
$lang['REG_PBOX_DATETIME_VAL'] ='Sélectionnez dans le menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Code pays de votre téléphone mobile';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Veuillez saisir votre numéro de téléphone...';
$lang['REG_FORM_SMS_TXT1'] ='Alerte SMS : veuillez m\'envoyer un rappel par SMS avant le début du webinaire (facultatif mais recommandé)..';
$lang['REG_FORM_SMS_TXT2'] ='Choisissez le code de votre pays et inscrivez votre numéro de téléphone mobile pour recevoir une alerte SMS 15 minutes avant le début du webinaire.';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Veuillez indiquer la date';
$lang['REG_FORM_ALERT_TIME'] ='Veuillez indiquer l\'heure';
$lang['REG_FORM_ALERT_NAME'] ='Veuillez indiquer votre prénom';
$lang['REG_FORM_ALERT_EMAIL'] ='Veuillez indiquer votre adresse e-mail';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Veuillez sasir une adresse e-mail valide';
$lang['REG_FORM_TXTBOX_NAME'] ='Indiquez votre prénom ici...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Indiquez votre adresse e-mail ici...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Votre heure locale';
$lang['REG_FORM_TXT_SELDATE'] ='Sélectionnez la date désirée';
$lang['REG_FORM_TXT_SELTIME'] ='Choisissez votre date';
$lang['REG_FORM_TXT_SELTIME_2'] ='Choisissez l\'heure';
$lang['REG_FORM_TXT_SELECTDATE'] ='Choisissez votre date';
$lang['REG_FORM_TXT_SELECTTIME'] ='Choisissez votre heure';
$lang['REG_FORM_TXT_RN_1'] = 'Regarder rediffusion webinaire hier MAINTENANT!';
$lang['REG_FORM_TXT_RN_2'] = 'maintenant';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Inscription au webinaire';
$lang['REG_FORM_H2'] ='Réservez votre place !';
$lang['REG_FORM_H3'] ='INSCRIVEZ-VOUS AU WEBINAIRE';
$lang['REG_FORM_SMS_TXT1'] ='J\'aimerais recevoir une alerte SMS avant le début du webinaire';
$lang['REG_FORM_SMS_TXT2'] ='(Facultatif mais très recommandé) Choisissez le code de votre pays et inscrivez votre numéro de téléphone mobile pour recevoir un rappel SMS 15 minutes avant le début du webinaire.';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='RÉSERVEZ VOTRE PLACE !';
$lang['REG_FORM_H2'] ='Inscription au webinaire';
$lang['REG_FORM_TXT_SELECTDATE'] ='Quel jour voulez-vous participer ?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Quelle heure vous convient le mieux ?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Où envoyer l\'invitation?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notifications';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='INSCRIVEZ-VOUS MAINTENANT!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Présentation du webinaire';
$lang['PBOX_TXT_TOPIC'] ='Sujet';
$lang['PBOX_TXT_HOST'] ='Hôte';
$lang['PBOX_TXT_PRESENTER'] ='Présentateur';
$lang['PBOX_TXT_DATEANDTIME'] ='Date et heure';
$lang['PBOX_TXT_STATUS'] ='ÉTAT';
$lang['PBOX_TXT_STATUS_VAL'] ='Actif!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Présenté par';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Détails du webinaire';
$lang['TY_WEBINAR'] ='Webinaire';
$lang['TY_PRESENTER'] ='Présentateur';
$lang['TY_SCHEDDATE'] ='Date prévue';
$lang['TY_SCHEDTIME'] ='Heure prévue';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Votre lien personnalisé pour accéder au webinaire';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Ajoutez cette date à votre calendrier iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Ajoutez cette date à votre calendrier Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Ajoutez cette date à votre calendrier Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Cliquez sur Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Invitez vos amis à ce webinaire';
$lang['TY_TXT_FREEBONUS_H2'] ='ET ACCÉDEZ À VOS CADEAUX GRATUITS !';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Choisissez la méthode de référence';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Envoyez un e-mail à vos amis';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Indiquez votre prénom';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Indiquez votre adresse e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Le prénom de votre ami(e)';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='L\'adresse de courriel de votre ami(e)';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Voici le courriel qui sera envoyé à vos amis';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Objet';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Message';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Envoyer un email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Publiez ce message sur Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Confirmez l\'accès à Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Publier sur Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Vérifiez l\'URL';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Une fois le message publié sur Facebook, entrez l\'adresse Web de votre page Facebook ci-dessous pour en vérifier l\'affichage. Dès que la vérification aura été validée, vous aurez accès à vos cadeaux !';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Publiez ce message sur Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Confirmez l\'accès à Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Une fois le message publié sur Twitter, entrez l\'adresse Web de votre page Twitter ci-dessous pour en confirmer l\'affichage. Dès que la vérification aura été validée, vous aurez accès à vos cadeaux !';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet ce message';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Félicitations !';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='VOUS AVEZ MAINTENANT ACCÈS À VOS CADEAUX !';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='TÉLÉCHARGEMENT GRATUIT';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Cliquez du bouton droit de la souris et choisissez "Enregistrez sous...""';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='cliquez ici';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Le champ du prénom est vide !';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Adresse e-mail non valide !';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='L\'adresse Web n\'a pas été confirmée !';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Le webinaire commencera sous peu...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Présentateur';
$lang['COUNTDOWN_TXT_DATE'] ='Date prévue';
$lang['COUNTDOWN_TXT_TIME'] ='Heure prévue';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Connection au webinaire, veuillez patienter...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Lorsque vous posez votre question, veuillez également entrer votre nom et votre adresse e-mail pour que nous puissions vous répondre par e-mail au cas où nous ne pourrions pas pendant le webinaire.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Votre prénom (obligatoire)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Votre adresse de e-mail (obligatoire)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Tapez votre question';
$lang['LIVE_CBOX2_TXT_1'] ='Boîte de discussion';
$lang['LIVE_CBOX2_TXT_2'] ='Écrire à';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Tous';
$lang['LIVE_CBOX2_TXT_3'] ='Message';
$lang['LIVE_CBOX2_TXT_4'] ='Envoyez vos questions';
$lang['LIVE_CBOX2_TXT_5'] ='Entrez votre nom et votre email afin que nous puissions vous envoyer notre réponse';
$lang['LIVE_CBOX2_TXT_6'] ='fournir tous les domaines tout à fait';
$lang['LIVE_CBOX2_TXT_7'] ='Message envoyé!';
$lang['LIVE_CBOX2_TXT_8'] ='Désolé, Message déjà envoyé';

$lang['LIVE_CBOX2_SUBMIT'] ='Envoyez le message';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Participants inscrits';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Participants à la rediffusion';
$lang['LIVE_ATTENDEELIST_HOST'] ='Hôte';
//POLL
$lang['LIVE_POLL_TXT1'] ='RÉSULTAT';
$lang['LIVE_POLL_TXT2'] ='Merci pour votre vote';
$lang['LIVE_POLL_TXT3'] ='Les résultats du sondage finaux seront disponibles sous peu ...';

$lang['LIVE2_H1'] ='L\'APPLICATION DU WEBINAIRE EST LANCÉE';
$lang['LIVE2_TXT1'] ='Bienvenue';
$lang['LIVE2_TXT2'] ='Réglages';
$lang['LIVE2_TXT3'] ='Ouvrez une session';
$lang['LIVE2_TXT4'] ='Fermez la session';
$lang['LIVE2_TXT5'] ='Présentation';
$lang['LIVE2_TXT6'] ='Members';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Evénements Webinar d\'action';
$lang['LIVE2_TXT9'] = 'Entrez vos commentaires ou questions';
$lang['LIVE3_TXT1'] ='ÉTAT';
$lang['LIVE3_TXT2'] ='EN MARCHE';
$lang['LIVE3_TXT3'] ='Identification du webinaire';
$lang['LIVE3_POPUP1'] ='Les options ont été désactivées par l\'administrateur du webinaire';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Êtes-vous sûr de vouloir quitter ? Il se peut que vous ne puissiez plus vous reconnecter à ce webinaire !';




// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'janvier';
$langmonths['february'] = 'février';
$langmonths['march'] = 'mars';
$langmonths['april'] = 'avril';
$langmonths['may'] = 'mai';
$langmonths['june'] = 'juin';
$langmonths['july'] = 'juillet';
$langmonths['august'] = 'août';
$langmonths['september'] = 'septembre';
$langmonths['october'] = 'octobre';
$langmonths['november'] = 'novembre';
$langmonths['december'] = 'décembre';

$langdays = array();
$langdays['monday'] = 'lundi';
$langdays['tuesday'] = 'mardi';
$langdays['wednesday'] = 'mercredi';
$langdays['thursday'] = 'jeudi';
$langdays['friday'] = 'vendredi';
$langdays['saturday'] = 'samedi';
$langdays['sunday'] = 'dimanche';


?>