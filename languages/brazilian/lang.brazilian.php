<?php
# -----------------------------------------------
# LANGUAGE: BRAZILLIAN                  UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Registrando para uma conferência';
//presentation-box
$lang['REG_PBOX_H1'] ='Apresentação da Conferência';
$lang['REG_PBOX_TOPIC'] ='Tema';
$lang['REG_PBOX_PRESENTER'] ='Palestrante';
$lang['REG_PBOX_HOST'] ='Anfitrião';
$lang['REG_PBOX_DATETIME'] ='Data e Hora';
$lang['REG_PBOX_DATETIME_VAL'] ='Selecione do menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Código de País Para Celular';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Digite o Número do Seu Telefone Aqui...';
$lang['REG_FORM_SMS_TXT1'] ='Alerta de Texto SMS: Sim, me envie um aviso antes da conferência começar (opcional mas recomendável).';
$lang['REG_FORM_SMS_TXT2'] ='Selecione Seu Código de País e Digite o Número do Seu Celular Para Receber um Aviso Por Mensagem de Texto 15 Minutos Antes do Início da Conferência';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Por favor, preencha o campo da data';
$lang['REG_FORM_ALERT_TIME'] ='Por favor, preencha o campo do horário';
$lang['REG_FORM_ALERT_NAME'] ='Por favor, preencha o campo do nome';
$lang['REG_FORM_ALERT_EMAIL'] ='Por favor, preencha o campo do e-mail';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Por favor, digite um endereço de e-mail válido';
$lang['REG_FORM_TXTBOX_NAME'] ='Digite Seu Nome Aqui...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Digite Seu E-mail Aqui...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Sua Hora Local';
$lang['REG_FORM_TXT_SELDATE'] ='Selecione a data desejada';
$lang['REG_FORM_TXT_SELTIME'] ='Selecione sua data primeiro';
$lang['REG_FORM_TXT_SELTIME_2'] ='Selecione o horário desejado';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Selecione Sua Data';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Selecione Seu Horário';
$lang['REG_FORM_TXT_RN_1'] = 'Assista AGORA ao replay do webinar de ontem!';
$lang['REG_FORM_TXT_RN_2'] = 'RIGHT NOW!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Registro para Conferência';
$lang['REG_FORM_H2'] ='Reserve seu Lugar!';
$lang['REG_FORM_H3'] ='REGISTRE-SE PARA A CONFERÊNCIA';
$lang['REG_FORM_SMS_TXT1'] ='Eu Gostaria de Receber Um Aviso Por SMS Antes Que o Evento Comece';
$lang['REG_FORM_SMS_TXT2'] ='(Opcional mas altamente recomendável)  Selecione Seu Código de País e Digite o Número do Seu Celular Para Receber um Aviso Por Mensagem de Texto 15 Minutos Antes do Início da Conferência';
}
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Reserve seu Lugar!';
$lang['REG_FORM_H2'] ='Registro para Conferência';
$lang['REG_FORM_TXT_SELECTDATE'] ='Qual Dia Você Quer Assistir?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Qual O Melhor Horario Para Você?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Para Onde Enviar o Convite:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Avisos';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='INSCREVA-SE JÁ!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Apresentação da Conferência';
$lang['PBOX_TXT_TOPIC'] ='Tema';
$lang['PBOX_TXT_HOST'] ='Anfitrião';
$lang['PBOX_TXT_PRESENTER'] ='Palestrante';
$lang['PBOX_TXT_DATEANDTIME'] ='Data e Hora';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='LIGADO';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Apresentado por';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Informações Sobre a Conferência';
$lang['TY_WEBINAR'] ='Seminário Online';
$lang['TY_PRESENTER'] ='Palestrante';
$lang['TY_SCHEDDATE'] ='Data Agendada';
$lang['TY_SCHEDTIME'] ='Horário Agendado';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Seu link individual de acesso á conferência';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Adicione ao  seu Calendário iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Adicione ao seu Calendário do Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Adicione ao seu Calendário do Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Adicione ao seu Calendário do Outlook';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Avise seus amigos sobre essa conferência';
$lang['TY_TXT_FREEBONUS_H2'] ='E LIBERE SEU BÔNUS GRÁTIS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Selecione a Forma de Aviso';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Envie um E-mail Para Seus Amigos';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Digite seu nome';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Digite seu e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Nome dos seus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='E-mail dos seus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Esse é o e-mail que será enviado para seus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Assunto do e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Mensagem do e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Enviar e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Publique Essa Mensagem No Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verificar Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post no Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifique a URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Após enviar sua mensagem ao Facebook, digite abaixo a URL da página do Facebook para confirmar o envio. Assim, seu bônus será liberado imediatamente!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Enviar Pelo Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verificar Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Após enviar a mensagem pelo Twitter, digite abaixo a URL de endereço do seu Twitter para verificar seu envio. Após a verificação, seu bônus será liberado imediatamente!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Mensagem Tweet This';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Parabéns';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='VOCÊ LIBEROU SEU BÔNUS!!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DOWNLOAD GRATUITO';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Clique com o botão direito no link abaixo e selecione "Salvar Como...""';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Clique aqui';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Nome está em branco!!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Endereço de E-mail Inválido!!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL não verificada!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Esta Conferência Começará Em Alguns Instantes...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Palestrante';
$lang['COUNTDOWN_TXT_DATE'] ='Data Agendada';
$lang['COUNTDOWN_TXT_TIME'] ='Horário Agendado';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Conectando á conferência. Por favor, aguarde...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Enquanto envia sua pergunta, digite também seu nome e e-mail para que nós possamos respondê-la por email caso nós não possamos fazê-lo durante a conferência, por qualquer razão.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Seu Nome (Obrigatório))';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Seu Email (Obrigatório)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Digite Sua Pergunta';
$lang['LIVE_CBOX2_TXT_1'] ='Caixa de Bate-Papo';
$lang['LIVE_CBOX2_TXT_2'] ='Escrever para';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Todos';
$lang['LIVE_CBOX2_TXT_3'] ='Mensagem';
$lang['LIVE_CBOX2_TXT_4'] ='Envie suas perguntas';
$lang['LIVE_CBOX2_TXT_5'] ='Digite seu nome e e-mail para que possamos lhe enviar nossa resposta';
$lang['LIVE_CBOX2_TXT_6'] ='fornecer todos os campos completamente';
$lang['LIVE_CBOX2_TXT_7'] ='Mensagem enviada!';
$lang['LIVE_CBOX2_TXT_8'] ='Desculpe, Mensagem já enviada';
$lang['LIVE_CBOX2_SUBMIT'] ='Enviar Mensagem';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Participantes Inscritos';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Participantes do Replay';
$lang['LIVE_ATTENDEELIST_HOST'] ='Anfitrião';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTADO';
$lang['LIVE_POLL_TXT2'] ='Obrigado por seu voto';
$lang['LIVE_POLL_TXT3'] ='Os resultados da pesquisa finais estarão disponíveis em breve ...';

$lang['LIVE2_H1'] ='APLICATIVO DA CONFERÊNCIA INICIADO';
$lang['LIVE2_TXT1'] ='Bem Vindo';
$lang['LIVE2_TXT2'] ='Configurações';
$lang['LIVE2_TXT3'] ='Entrar';
$lang['LIVE2_TXT4'] ='Sair';
$lang['LIVE2_TXT5'] ='Apresentação';
$lang['LIVE2_TXT6'] ='Membros';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Ação Eventos Webinar';
$lang['LIVE2_TXT9'] = 'Insira as suas perguntas ou comentários';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='LIGADO';
$lang['LIVE3_TXT3'] ='ID da Conferência';
$lang['LIVE3_POPUP1'] ='Opções foram desligadas pelo administrador da conferência';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Tem certeza que deseja sair? Você pode não conseguir voltar para a sala!!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'janeiro';
$langmonths['february'] = 'fevereiro';
$langmonths['march'] = 'março';
$langmonths['april'] = 'abril';
$langmonths['may'] = 'maio';
$langmonths['june'] = 'junho';
$langmonths['july'] = 'julho';
$langmonths['august'] = 'agosto';
$langmonths['september'] = 'setembro';
$langmonths['october'] = 'outubro';
$langmonths['november'] = 'novembro';
$langmonths['december'] = 'dezembro';

$langdays = array();
$langdays['monday'] = 'segunda-feira';
$langdays['tuesday'] = 'terça-feira';
$langdays['wednesday'] = 'quarta-feira';
$langdays['thursday'] = 'quinta-feira';
$langdays['friday'] = 'sexta-feira';
$langdays['saturday'] = 'sábado';
$langdays['sunday'] = 'domingo';


?>