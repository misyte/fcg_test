<?php
# -----------------------------------------------
# LANGUAGE: ENGLISH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='registering to a webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar Presentation';
$lang['REG_PBOX_TOPIC'] ='Topic';
$lang['REG_PBOX_PRESENTER'] ='Presenter';
$lang['REG_PBOX_HOST'] ='Host';
$lang['REG_PBOX_DATETIME'] ='Date and Time';
$lang['REG_PBOX_DATETIME_VAL'] ='Select from menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Country Code For Mobile';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Enter Your Phone Number Here...';
$lang['REG_FORM_SMS_TXT1'] ='SMS Text Alert: Yes, send me a SMS reminder before the webinar starts (optional but recommended).';
$lang['REG_FORM_SMS_TXT2'] ='Select Your Country Code and Enter Your Mobile Phone Number To Receive a Text Alert Reminder 15 Minutes Before Webinar Starts';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Please fill out the date field';
$lang['REG_FORM_ALERT_TIME'] ='Please fill out the time field';
$lang['REG_FORM_ALERT_NAME'] ='Please fill out the name field';
$lang['REG_FORM_ALERT_EMAIL'] ='Please fill out the email field';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Please enter a valid email address';
$lang['REG_FORM_TXTBOX_NAME'] ='Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Enter Your Email Here...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Your Local Time';
$lang['REG_FORM_TXT_SELDATE'] ='Select desired date';
$lang['REG_FORM_TXT_SELTIME'] ='Select your date first';
$lang['REG_FORM_TXT_SELTIME_2'] ='Select desired time';
$lang['REG_FORM_TXT_SELECTDATE'] ='Select Your Date';
$lang['REG_FORM_TXT_SELECTTIME'] ='Select Your Time';
$lang['REG_FORM_TXT_RN_1'] = 'Watch yesterday\'s webinar replay NOW!';
$lang['REG_FORM_TXT_RN_2'] = 'Right Now!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Webinar Registration';
$lang['REG_FORM_H2'] ='Reserve your Spot!';
$lang['REG_FORM_H3'] ='REGISTER TO THE WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='I Would Like To Receive A SMS Text Alert Before The Event Starts';
$lang['REG_FORM_SMS_TXT2'] ='(Optional but highly recommended)<br>Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>
Reminder 15 Minutes Before Webinar Starts';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Reserve your Spot!';
$lang['REG_FORM_H2'] ='Webinar Registration';
$lang['REG_FORM_TXT_SELECTDATE'] ='Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Where To Send The Invitation?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notifications';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTER NOW!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinar Presentation';
$lang['PBOX_TXT_TOPIC'] ='Topic';
$lang['PBOX_TXT_HOST'] ='Host';
$lang['PBOX_TXT_PRESENTER'] ='Presenter';
$lang['PBOX_TXT_DATEANDTIME'] ='Date and Time';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Presented By';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinar Details';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Presenter';
$lang['TY_SCHEDDATE'] ='Scheduled date';
$lang['TY_SCHEDTIME'] ='Scheduled time';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Your unique webinar link';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Add To Your iCal Calendar';
$lang['TY_TXT_GOOGLE_CAL'] = 'Add To Your Google Calendar';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Add To Your Outlook Calendar';
$lang['TY_TXT_CLICK_VER'] = 'Click on Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Tell your friends about this webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='AND UNLOCK YOUR FREE BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Select Your Referring Method';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Send Email To Your Friends';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Enter your name';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Enter your email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Your friends\' name';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Your friends\' email';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='This is the email that will be sent to your friends';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Subject line';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Email body';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Send Email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Post This Message On Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verify Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post On Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verify URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Once you\'ve posted the message on Facebook, enter your Facebook page URL below to verify the post. Upon verification, your hidden bonus will be unlocked right away!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet This On Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verify Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Once you\'ve twitted the message on Twitter, enter your Twitter page URL below to verify the twit. Upon verification, your hidden bonus will be unlocked right away!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet This Message';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Congratulations';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='YOU HAVE UNLOCKED YOUR BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='FREE DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Right click on link below and select "save as..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Click here';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='name is empty!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='invalid Email Address!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL not verified!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='The Webinar Will Begin Shortly...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presenter';
$lang['COUNTDOWN_TXT_DATE'] ='Scheduled date';
$lang['COUNTDOWN_TXT_TIME'] ='Scheduled time';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Connecting to a webinar, please wait...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='While sending your question, also enter your name and email so we can answer by email in case we cannot do it during the webinar for any reason.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Your Name (Required)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Your Email (Required)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Type Your Question';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Write to';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='All';
$lang['LIVE_CBOX2_TXT_3'] ='Message';
$lang['LIVE_CBOX2_TXT_4'] ='Send Your Questions';
$lang['LIVE_CBOX2_TXT_5'] ='Enter your name and email so we can send you our reply';

$lang['LIVE_CBOX2_TXT_6'] ='provide all fields completely';
$lang['LIVE_CBOX2_TXT_7'] ='Message Sent!';
$lang['LIVE_CBOX2_TXT_8'] ='Sorry, Message already sent';

$lang['LIVE_CBOX2_SUBMIT'] ='Send Message';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Registered Participants';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Replay Attendees';
$lang['LIVE_ATTENDEELIST_HOST'] ='Host';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULT';
$lang['LIVE_POLL_TXT2'] ='Thank you for your vote';
$lang['LIVE_POLL_TXT3'] ='The poll final results will be available shortly...';

$lang['LIVE2_H1'] ='WEBINAR APP STARTED';
$lang['LIVE2_TXT1'] ='Welcome';
$lang['LIVE2_TXT2'] ='Settings';
$lang['LIVE2_TXT3'] ='Sign In';
$lang['LIVE2_TXT4'] ='Sign out';
$lang['LIVE2_TXT5'] ='Presentation';
$lang['LIVE2_TXT6'] ='Members';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Action Events';
$lang['LIVE2_TXT9'] = 'Enter your questions or comments';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Options have been turned off by webinar admin';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Are you sure you want to leave? you might not be able to log back into the room!';

?>