<?php
# -----------------------------------------------
# LANGUAGE: PORTUGESE                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='registar-se para um webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Registo para o Webinar';
$lang['REG_PBOX_TOPIC'] ='Assunto';
$lang['REG_PBOX_PRESENTER'] ='Apresentador';
$lang['REG_PBOX_HOST'] ='Assunto';
$lang['REG_PBOX_DATETIME'] ='Data e Hora';
$lang['REG_PBOX_DATETIME_VAL'] ='Seleccione no menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Código do País para Tlelmóvel';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Introduza o seu telefone aqui...';
$lang['REG_FORM_SMS_TXT1'] ='Texto de Alerta SMS: Sim, envie-me um SMS de alerta antes do webinar começar (opcional mas recomendado)';
$lang['REG_FORM_SMS_TXT2'] ='Seleccione o Código do seu País e Introduza o Número do seu Telemóvel para receber uma aviso 15 minutos antes do início do Webinar';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Por favor preencha o campo da data';
$lang['REG_FORM_ALERT_TIME'] ='Por favor preencha o campo da hora';
$lang['REG_FORM_ALERT_NAME'] ='Por favor preencha o campo do nome';
$lang['REG_FORM_ALERT_EMAIL'] ='Por favor preencha o campo do email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Por favor introduza um email valido';
$lang['REG_FORM_TXTBOX_NAME'] ='Introduza o Seu Nome Aqui...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Introduza o Seu Email Aqui...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='A Sua Hora Local';
$lang['REG_FORM_TXT_SELDATE'] ='Seleccione a data desejada';
$lang['REG_FORM_TXT_SELTIME'] ='Seleccione primeiro a sua data';
$lang['REG_FORM_TXT_SELTIME_2'] ='Seleccione a hora desejada';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Seleccione a Sua Data';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Seleccione a Sua Hora';
$lang['REG_FORM_TXT_RN_1'] = 'Assista repetição de ontem webinar AGORA!';
$lang['REG_FORM_TXT_RN_2'] = 'agora mesmo!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Registo para o Webinar';
$lang['REG_FORM_H2'] ='Reserve o seu Lugar';
$lang['REG_FORM_H3'] ='REGISTE-SE PARA O WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Gostaria de receber um SMS de alerta antes do evento começar';
$lang['REG_FORM_SMS_TXT2'] ='(Opcional mas recomendado) Seleccione o seo Código de País e introduza o seu número de telemóvel para receber um SMS de aviso 15 minutos antes de o Webinar começar';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Reserve o seu Lugar!';
$lang['REG_FORM_H2'] ='Registo para o Webinar';
$lang['REG_FORM_TXT_SELECTDATE'] ='Em que dia quer participar?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Qual é a hora mais conveniente?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Para onde enviar o Convite:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notificações';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTE-SE AGORA!';

}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Apresentação do Webinar';
$lang['PBOX_TXT_TOPIC'] ='Assunto';
$lang['PBOX_TXT_HOST'] ='Convidado';
$lang['PBOX_TXT_PRESENTER'] ='Apresentador';
$lang['PBOX_TXT_DATEANDTIME'] ='Data e Hora';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Apresentado por:';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Detalhes sobre o Webinar';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Apresentador';
$lang['TY_SCHEDDATE'] ='Data de realização';
$lang['TY_SCHEDTIME'] ='Hora';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='O seu link para o webinar';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Adicione ao seu Calendário iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Adicione ao seu Calendário Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Adicione ao seu Calendário Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Clique na versão';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Divulgue este webinar junto dos seus amigos';
$lang['TY_TXT_FREEBONUS_H2'] ='E OBTENHA O SEU BONUS GRATUITO!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Seleccione o seu método de referência';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Envie um Email para os seus Amigos';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Introduza o seu nome';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Introduza o seu email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Nome do seu amigo';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Email do seu amigo';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Este é o email que será enviado aos seus amigos';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Assunto';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Corpo do email';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Enviar e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Poste esta mensagem no Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifique o Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Mensagem no Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifique a URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Uma vez que a mensagem seja postada no Facebook, introduza o URL da sua página de Facebook em baixo para verificar o post. Uma vez verificado, o seu bónus surpresa será disponibilizado imediatamente!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet isto no Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifique o Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Uma vez que tenha posto a mensagem no Twitter, introduza o URL da sua página Twitter em baixo para verificar o twit. Uma vez verificado, o seu bónus surpresa será disponibilizado imediatamente';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Mensagem Tweet This';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Parabéns';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='GANHOU O SEU BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DOWNLOAD GRATUITO';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Clique com o botão direito do rato e selecciona \'gravar---';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Clique aqui';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='None não preenchido!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='invalid Email Address!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL não verificado!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='O Webinar começará em brebe...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Apresentador';
$lang['COUNTDOWN_TXT_DATE'] ='Data de realização';
$lang['COUNTDOWN_TXT_TIME'] ='Hora';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='A ligar ao webinar, por favor aguarde...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Ao enviar a sua pergunta, introduza também o seu nome e email para que possamos responder por email caso não haja tempo durante o webinar';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Nome (obrigatório)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Your Email (Required)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Type Your Question';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Escreva para';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Todos';
$lang['LIVE_CBOX2_TXT_3'] ='Mensagem';
$lang['LIVE_CBOX2_TXT_4'] ='Envie mensagem';
$lang['LIVE_CBOX2_TXT_5'] ='Digite seu nome e e-mail para que possamos lhe enviar nossa resposta';
$lang['LIVE_CBOX2_TXT_6'] ='fornecer todos os campos completamente';
$lang['LIVE_CBOX2_TXT_7'] ='Mensagem enviada!';
$lang['LIVE_CBOX2_TXT_8'] ='Desculpe, Mensagem já enviada';

$lang['LIVE_CBOX2_SUBMIT'] ='Envie mensagem';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Participantes Registados';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Participantes no Replay';
$lang['LIVE_ATTENDEELIST_HOST'] ='Convidado';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTADO';
$lang['LIVE_POLL_TXT2'] ='Obrigado por seu voto';
$lang['LIVE_POLL_TXT3'] ='Os resultados da pesquisa finais estarão disponíveis em breve ...';

$lang['LIVE2_H1'] ='APP DO WEBINAR INICIADA';
$lang['LIVE2_TXT1'] ='Bem vindo(a)';
$lang['LIVE2_TXT2'] ='Settings';
$lang['LIVE2_TXT3'] ='Sign In';
$lang['LIVE2_TXT4'] ='Sign out';
$lang['LIVE2_TXT5'] ='Apresentação';
$lang['LIVE2_TXT6'] ='Membros';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar eventos de ação';
$lang['LIVE2_TXT9'] = 'Insira as suas perguntas ou comentários';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='ID do Webinar';
$lang['LIVE3_POPUP1'] ='As Opções foram desligadas pelo administrador do webinar';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Tem a certeza de que quer sair? pode não ter lugar para voltar a entrar!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'janeiro';
$langmonths['february'] = 'fevereiro';
$langmonths['march'] = 'março';
$langmonths['april'] = 'abril';
$langmonths['may'] = 'maio';
$langmonths['june'] = 'junho';
$langmonths['july'] = 'julho';
$langmonths['august'] = 'agosto';
$langmonths['september'] = 'setembro';
$langmonths['october'] = 'outubro';
$langmonths['november'] = 'novembro';
$langmonths['december'] = 'dezembro';

$langdays = array();
$langdays['monday'] = 'segunda-feira';
$langdays['tuesday'] = 'terça-feira';
$langdays['wednesday'] = 'quarta-feira';
$langdays['thursday'] = 'quinta-feira';
$langdays['friday'] = 'sexta-feira';
$langdays['saturday'] = 'sábado';
$langdays['sunday'] = 'domingo';

?>