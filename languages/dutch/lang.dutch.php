<?php
# -----------------------------------------------
# LANGUAGE: DUTCH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='registreren voor een webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar Presentatie';
$lang['REG_PBOX_TOPIC'] ='Onderwerp';
$lang['REG_PBOX_PRESENTER'] ='Presentator';
$lang['REG_PBOX_HOST'] ='Gastheer';
$lang['REG_PBOX_DATETIME'] ='Datum en Tijd';
$lang['REG_PBOX_DATETIME_VAL'] ='Selecteer uit het menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landcode Voor Mobiel';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Vul Je Telefoonnummer Hier In...';
$lang['REG_FORM_SMS_TXT1'] ='SMS Herinnering: Ja, stuur mij een SMS herinnering voordat het webinat begint. (Optioneel, maar anbevolen)';
$lang['REG_FORM_SMS_TXT2'] ='Selecteer Je Landcode en Vul Je 06 Nummer In om 15 Minuten Voor De Webinar Start een Text Alert ter Herinnering te Ontvangen.';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Gelieve de datum in te vullen';
$lang['REG_FORM_ALERT_TIME'] ='Gelieve de tijd in te vullen';
$lang['REG_FORM_ALERT_NAME'] ='Gelieve je naam in te vullen';
$lang['REG_FORM_ALERT_EMAIL'] ='Gelieve je e-mail in te vullen';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Gelieve een geldig e-mail adres in te vullen';
$lang['REG_FORM_TXTBOX_NAME'] ='Vul Hier Je Naam In...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Vul Hier Je E-mail In...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Jouw Lokale Tijd';
$lang['REG_FORM_TXT_SELDATE'] ='Selecteer de gewenste datum';
$lang['REG_FORM_TXT_SELTIME'] ='Selecteer eerst je datum';
$lang['REG_FORM_TXT_SELTIME_2'] ='Selecteer de gewenste tijd';
$lang['REG_FORM_TXT_SELECTDATE'] ='Selecteer Je Datum';
$lang['REG_FORM_TXT_SELECTTIME'] ='Selecteer Je Tijd';
$lang['REG_FORM_TXT_RN_1'] = 'Nu kijken gisteren webinar replay!';
$lang['REG_FORM_TXT_RN_2'] = 'Right Now!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Webinar Registratie';
$lang['REG_FORM_H2'] ='Reserveer Je Plaats!';
$lang['REG_FORM_H3'] ='REGISTREER VOOR HET WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Ik Wens Een SMS Te Ontvangen Voordat Het Event Begint';
$lang['REG_FORM_SMS_TXT2'] ='(Optioneel maar aanbevolen) Selecteer Je Landcode En Geef Je GSM Nummer Om Een SMS Herinnering Te Ontvangen 15 Minuten Voor Het Webinar Start';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Reserveer Je Plaats!';
$lang['REG_FORM_H2'] ='Webinar Registratie';
$lang['REG_FORM_TXT_SELECTDATE'] ='Welke Dag Wil Je Deelnemen?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Welke Tijd Past Jou Het Beste?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Waar De Uitnodiging Naar Gestuurd Moet Worden';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Berichten';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTREER NU!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinar Presentatie';
$lang['PBOX_TXT_TOPIC'] ='Onderwerp';
$lang['PBOX_TXT_HOST'] ='Gastheer';
$lang['PBOX_TXT_PRESENTER'] ='Presentator';
$lang['PBOX_TXT_DATEANDTIME'] ='Datum en Tijd';
$lang['PBOX_TXT_STATUS'] ='STATUS';
$lang['PBOX_TXT_STATUS_VAL'] ='AAN!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Gepresenteerd door';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinar Details';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Presentator';
$lang['TY_SCHEDDATE'] ='Geselecteerde datum';
$lang['TY_SCHEDTIME'] ='Geselecteerde tijd';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Je unieke Webinar link';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Voeg toe aan je iCal Kalender';
$lang['TY_TXT_GOOGLE_CAL'] = 'Voeg toe aan je Google Kalender';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Voeg toe aan je Outlook Kalender';
$lang['TY_TXT_CLICK_VER'] = 'Klik op Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Vertel jouw vrienden over dit webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='EN ONTVANG EEN GRATIS BONUS';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Selecteer je referentie methode';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Stuur een email naar je vrienden';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Vul je naam in';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Vul je email in';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='De naam van je vriend of vriendin';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Het emailadres van je vriend of vriendin';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Dit is de email die naar je vrienden wordt gestuurd';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Onderwerp';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Email';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='E-mail verzenden!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Post dit bericht op Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifieer Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Publiceren op Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Controleer URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Wanneer je je bericht op Facebook hebt ge-POST , vul dan je Facebook Pagina URL hieronder in om je bericht te verifieren. Na een succesvolle verificatie zal je verborgen bonus onmiddelijk worden vrijgeven!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet dit op Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifieer Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Wanneer je je bericht op Twitter hebt ge-POST , vul dan je Twitter Pagina URL hieronder in om je Tweet te verifieren. Na een succesvolle verificatie zal je verborgen bonus onmiddelijk worden vrijgeven!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet dit bericht';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gefeliciteerd';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='JE HEBT JE GRATIS BONUS VERDIEND';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='GRATIS DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Druk op de rechtermuisknop op onderstaande link en selecteer \'opslaan als..\'';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='click here';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Naam is leeg!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Ongeldig Emailadres!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL niet geverifieerd';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Het  webinar begint binnenkort...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presentator';
$lang['COUNTDOWN_TXT_DATE'] ='Geselecteerde datum';
$lang['COUNTDOWN_TXT_TIME'] ='Geselecteerde tijd';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Verbinding aan het maken met het webinar, even geduld alstjeblieft...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Voordat je jou vraag verstuurd, vul je naam en email in zodat we de vraag per email kunnen beantwoorden indien we je vraag niet tijdens de webinar kunnen beantwoorden, om welke reden dan ook.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Je Naam (Verplicht)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Je Email (Verplicht)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Vul hier je vraag in';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Schrijf naar';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Allen';
$lang['LIVE_CBOX2_TXT_3'] ='Bericht';
$lang['LIVE_CBOX2_TXT_4'] ='Stuur uw vragen';
$lang['LIVE_CBOX2_TXT_5'] ='Voer uw naam en e-mail zodat we kunnen sturen u onze reactie';

$lang['LIVE_CBOX2_TXT_6'] ='volledig voorzien van alle velden';
$lang['LIVE_CBOX2_TXT_7'] ='Bericht verzonden!';
$lang['LIVE_CBOX2_TXT_8'] ='Sorry, bericht al verzonden';

$lang['LIVE_CBOX2_SUBMIT'] ='Stuur Bericht';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Geregistreerde Deelnemers';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Replay Deelnemers';
$lang['LIVE_ATTENDEELIST_HOST'] ='Gastheer';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTAAT';
$lang['LIVE_POLL_TXT2'] ='Bedankt voor uw stem';
$lang['LIVE_POLL_TXT3'] ='De poll uiteindelijke resultaten zullen binnenkort beschikbaar zijn ...';

$lang['LIVE2_H1'] ='WEBINAR APP IS OPGESTART';
$lang['LIVE2_TXT1'] ='Welkom';
$lang['LIVE2_TXT2'] ='Instellingen';
$lang['LIVE2_TXT3'] ='Inschrijven';
$lang['LIVE2_TXT4'] ='Uitschrijven';
$lang['LIVE2_TXT5'] ='Presentatie';
$lang['LIVE2_TXT6'] ='Leden';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar actie events';
$lang['LIVE2_TXT9'] = 'Vul uw vragen of opmerkingen';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='AAN';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Opties zijn uitgeschakeld door de Webinar admin';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Weet je zeker dat je ons nu wilt verlaten? Je zal waarschijnlijk niet meer kunnen inloggen tijdens het webinar!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'januari';
$langmonths['february'] = 'februari';
$langmonths['march'] = 'maart';
$langmonths['april'] = 'april';
$langmonths['may'] = 'mei';
$langmonths['june'] = 'juni';
$langmonths['july'] = 'juli';
$langmonths['august'] = 'augustus';
$langmonths['september'] = 'september';
$langmonths['october'] = 'oktober';
$langmonths['november'] = 'november';
$langmonths['december'] = 'december';

$langdays = array();
$langdays['monday'] = 'maandag';
$langdays['tuesday'] = 'dinsdag';
$langdays['wednesday'] = 'woensdag';
$langdays['thursday'] = 'donderdag';
$langdays['friday'] = 'vrijdag';
$langdays['saturday'] = 'zaterdag';
$langdays['sunday'] = 'zondag';

?>