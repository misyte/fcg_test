<?php
# -----------------------------------------------
# LANGUAGE: Russian                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Регистрация на Вебинар';
//presentation-box
$lang['REG_PBOX_H1'] ='Регистрация Вебинара';
$lang['REG_PBOX_TOPIC'] ='Тема';
$lang['REG_PBOX_PRESENTER'] ='Ведущий';
$lang['REG_PBOX_HOST'] ='Организатор';
$lang['REG_PBOX_DATETIME'] ='Дата и время';
$lang['REG_PBOX_DATETIME_VAL'] ='Выберите из меню';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Код страны для мобильного телефона';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Введите номер Вашего телефона ...';
$lang['REG_FORM_SMS_TXT1'] ='SMS текстовое сообщение: Да, пошлите мне SMS напоминание до начала вебинара (не обязательно, но рекомендуется)';
$lang['REG_FORM_SMS_TXT2'] ='Выберите Вашу страну и введите номер мобильного телефона на который будет выслано текстовое напоминание за 15 минут до начала вебинара.';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Пожалуйста, выберите дату';
$lang['REG_FORM_ALERT_TIME'] ='Пожалуйста, выберите время в соответствующем поле';
$lang['REG_FORM_ALERT_NAME'] ='Пожалуйста, впишите имя в соответствующем поле';
$lang['REG_FORM_ALERT_EMAIL'] ='Пожалуйста, впишите Email в соответствующем поле';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Пожалуйста, впишите правильный email';
$lang['REG_FORM_TXTBOX_NAME'] ='Введите Ваше имя...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Введите Ваш Email...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Ваше время';
$lang['REG_FORM_TXT_SELDATE'] ='Выберите дату';
$lang['REG_FORM_TXT_SELTIME'] ='Сначала выберите дату';
$lang['REG_FORM_TXT_SELTIME_2'] ='Выберите время';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Выберите дату';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Выберите время';
$lang['REG_FORM_TXT_RN_1'] = 'Смотреть запись сегодняшнего вебинара СЕЙЧАС!';
$lang['REG_FORM_TXT_RN_2'] = 'прямо сейчас';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Регистрация Вебинара';
$lang['REG_FORM_H2'] ='Забронируй Место!';
$lang['REG_FORM_H3'] ='РЕГИСТРАЦИЯ НА ВЕБИНАР';
$lang['REG_FORM_SMS_TXT1'] ='Я хочу получить SMS напоминание перед началом вебинара';
$lang['REG_FORM_SMS_TXT2'] ='(Не обязательно, но очень рекомендуется) Выберите Код Вашей Страны и введите номер мобильного телефона для получения напоминания за 15 минут до начала вебинара.';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Забронируй Место!';
$lang['REG_FORM_H2'] ='Регистрация Вебинара';
$lang['REG_FORM_TXT_SELECTDATE'] ='Какой день Вам подходит?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Какое время Вам удобнее?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Кому отправить приглашение?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Дополнительные напоминания';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='ЗАРЕГИСТРИРОВАТЬСЯ!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Вебинар Представляет';
$lang['PBOX_TXT_TOPIC'] ='Тема';
$lang['PBOX_TXT_HOST'] ='Организатор';
$lang['PBOX_TXT_PRESENTER'] ='Ведущий';
$lang['PBOX_TXT_DATEANDTIME'] ='Дата и время';
$lang['PBOX_TXT_STATUS'] ='СТАТУС';
$lang['PBOX_TXT_STATUS_VAL'] ='ВКЛЮЧЕН!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Представляет';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Детали Вебинара';
$lang['TY_WEBINAR'] ='Вебинар';
$lang['TY_PRESENTER'] ='Ведущий';
$lang['TY_SCHEDDATE'] ='Назначенная дата';
$lang['TY_SCHEDTIME'] ='Назначенное время';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Ваша уникальная ссылка на вебинар';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Добавить в iCal Календарь';
$lang['TY_TXT_GOOGLE_CAL'] = 'Добавить в Google Календарь';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Добавить в Outlook Календарь';
$lang['TY_TXT_CLICK_VER'] = 'нажмите на версию';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Расскажи друзьям о вебинаре';
$lang['TY_TXT_FREEBONUS_H2'] ='И ПОЛУЧИ БЕСПЛАТНЫЙ БОНУС!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Выберите Ваш метод рекомендации';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Послать Email Вашим друзьям';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Введите Ваше имя';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Введите Ваш Email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Введите имя друга';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Введите Email друга';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Вот такое письмо будет послано Вашим друзьям';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Тема';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Сообщение';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Написать письмо';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Опубликовать сообщение на Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Подтвердить Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'разместить на facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Проверка URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='После публикации сообщения на Facebook, введите адрес Вашей страницы Facebook ниже, для подтверждения публикации. После этого Вы получите мгновенный доступ к Вашему бонусу!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Твитнуть';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Подтвердить Твит';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='После публикации сообщения на Twitter, введите адрес Вашей страницы Twitter ниже, для подтверждения публикации. После этого Вы получите мгновенный доступ к Вашему бонусу!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet это сообщение';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Поздравляем!';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ВЫ ПОЛУЧИЛИ БОНУС!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='СКАЧАТЬ БЕСПЛАТНО';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Кликните правой мышкой на ссылку ниже и выберите "Сохранить Как..""';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Имя пусто!';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Имя пусто!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Невереный Email адрес!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='Адрес (URL) не подтвержден!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Вебинар скоро начнется...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Ведущий';
$lang['COUNTDOWN_TXT_DATE'] ='Назначенная дата';
$lang['COUNTDOWN_TXT_TIME'] ='Назначенное время';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Подключаемся к вебинару, пожалуйста, подождите...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='При отправке своего вопроса, также указывайте Ваше имя и Email, чтобы мы могли ответить Вам на эл. почту в случае, если по какой-то причине, мы не сможем сделать это во время вебина.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Ваше Имя (Обязательно)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Ваш Email (Обязательно)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Впишите Ваш вопрос';
$lang['LIVE_CBOX2_TXT_1'] ='Чат';
$lang['LIVE_CBOX2_TXT_2'] ='Написать (кому)';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Всем';
$lang['LIVE_CBOX2_TXT_3'] ='Сообщение';
$lang['LIVE_CBOX2_TXT_4'] ='Присылайте ваши вопросы';
$lang['LIVE_CBOX2_TXT_5'] ='Введите свое имя и электронной почты, чтобы мы могли отправить вам наш ответ';
$lang['LIVE_CBOX2_TXT_6'] ='обеспечить все поля полностью';
$lang['LIVE_CBOX2_TXT_7'] ='Сообщение отправлено!';
$lang['LIVE_CBOX2_TXT_8'] ='Извините, сообщение уже было отправлено';


$lang['LIVE_CBOX2_SUBMIT'] ='Отправить сообщение';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Зарегистрированные Участники';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Ответить Участникам';
$lang['LIVE_ATTENDEELIST_HOST'] ='Организатор';
//POLL
$lang['LIVE_POLL_TXT1'] ='РЕЗУЛЬТАТ';
$lang['LIVE_POLL_TXT2'] ='Спасибо за ваш голос';
$lang['LIVE_POLL_TXT3'] ='Опрос окончательные результаты будут доступны в ближайшее время ...';

$lang['LIVE2_H1'] ='ВЕБИНАР ИДЕТ';
$lang['LIVE2_TXT1'] ='Добро пожаловать!';
$lang['LIVE2_TXT2'] ='Настройки';
$lang['LIVE2_TXT3'] ='Войти';

$lang['LIVE2_TXT4'] ='Выйти';
$lang['LIVE2_TXT5'] ='Презентация';
$lang['LIVE2_TXT6'] ='Участники';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'События Вебинар действий';
$lang['LIVE2_TXT9'] = 'Введите свой вопрос или комментарий';
$lang['LIVE3_TXT1'] ='СТАТУС';
$lang['LIVE3_TXT2'] ='ВКЛЮЧЕН';
$lang['LIVE3_TXT3'] ='ID вебинара';
$lang['LIVE3_POPUP1'] ='Опции были отключены администратором вебинара';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Вы уверены, что хотите уйти? Вы не сможете войти обратно в комнату вебинара!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'январь';
$langmonths['february'] = 'февраль';
$langmonths['march'] = 'март';
$langmonths['april'] = 'апрель';
$langmonths['may'] = 'май';
$langmonths['june'] = 'июнь';
$langmonths['july'] = 'июль';
$langmonths['august'] = 'август';
$langmonths['september'] = 'сентябрь';
$langmonths['october'] = 'октябрь';
$langmonths['november'] = 'ноябрь';
$langmonths['december'] = 'декабрь';

$langdays = array();
$langdays['monday'] = 'понедельник';
$langdays['tuesday'] = 'вторник';
$langdays['wednesday'] = 'среда';
$langdays['thursday'] = 'четверг';
$langdays['friday'] = 'пятница';
$langdays['saturday'] = 'суббота';
$langdays['sunday'] = 'воскресенье';


?>