<?php
# -----------------------------------------------
# LANGUAGE: Norwegian                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='registrering til webseminar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webseminar Presentasjon';
$lang['REG_PBOX_TOPIC'] ='Tema';
$lang['REG_PBOX_PRESENTER'] ='Foreleser';
$lang['REG_PBOX_HOST'] ='Vert';
$lang['REG_PBOX_DATETIME'] ='Dato og tid';
$lang['REG_PBOX_DATETIME_VAL'] ='Velg fra menyen';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landskode for mobil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Skriv inn telefonnummer her...';
$lang['REG_FORM_SMS_TXT1'] ='SMS påminnelse: Ja, send meg en SMS påminnelse før seminaret starter. (valgfritt men anbefalt).';
$lang['REG_FORM_SMS_TXT2'] ='Velg din landskode og skriv inn ditt mobilnummer slik at du mottar en påminnelse 15 minutter før seminaret starter';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Vennligst fyll ut datofeltet';
$lang['REG_FORM_ALERT_TIME'] ='Vennligst fyll ut klokkeslett';
$lang['REG_FORM_ALERT_NAME'] ='Vennligst fyll ut navnefeltet';
$lang['REG_FORM_ALERT_EMAIL'] ='Vennligst fyll ut e-post feltet';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Vennligst bruk en gyldig e-post adresse';
$lang['REG_FORM_TXTBOX_NAME'] ='Skriv navnet her...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Skriv e-post adresse her...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Din lokale tid';
$lang['REG_FORM_TXT_SELDATE'] ='Velg ønsket dato';
$lang['REG_FORM_TXT_SELTIME'] ='Velg dato først';
$lang['REG_FORM_TXT_SELTIME_2'] ='Velg ønsket tidspunkt';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Velg dato';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Velg klokkeslett';
$lang['REG_FORM_TXT_RN_1'] = 'Se gårsdagens webinar nå!';
$lang['REG_FORM_TXT_RN_2'] = 'Akkurat NÅ!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Webseminar Registrering';
$lang['REG_FORM_H2'] ='Reserver din Plass!';
$lang['REG_FORM_H3'] ='REGISTRER DEG TIL WEBSEMINARET';
$lang['REG_FORM_SMS_TXT1'] ='Jeg ønsker å motta en SMS varsel før seminaret starter';
$lang['REG_FORM_SMS_TXT2'] ='(Valgfritt men anbefalt) Velg din landskode og skriv inn ditt mobilnummer slik at du mottar en påminnelse 15 minutter før seminaret starter';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Reserver din Plass!';
$lang['REG_FORM_H2'] ='Webseminar Registrering';
$lang['REG_FORM_TXT_SELECTDATE'] ='Hvilken dag ønsker du å delta';
$lang['REG_FORM_TXT_SELECTTIME'] ='Hvilket tidspunkt passer best for deg';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Hvor skal invitasjonen sendes?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Notater';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTRER NÅ';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webseminar Presentasjon';
$lang['PBOX_TXT_TOPIC'] ='Tema';
$lang['PBOX_TXT_HOST'] ='Vert';
$lang['PBOX_TXT_PRESENTER'] ='Foreleser';
$lang['PBOX_TXT_DATEANDTIME'] ='Dato og tid';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='PÅ!';



/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Presentert av';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Seminar detaljer';
$lang['TY_WEBINAR'] ='Webseminar';
$lang['TY_PRESENTER'] ='Foreleser';
$lang['TY_SCHEDDATE'] ='Planlagt dato';
$lang['TY_SCHEDTIME'] ='Planlagt tidspunkt';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Din unike seminarlink';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Legg til i din iCal Kalender';
$lang['TY_TXT_GOOGLE_CAL'] = 'Legg til i din Google kalender';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Legg til i din Outlook kalender';
$lang['TY_TXT_CLICK_VER'] = 'Klik Versi';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Fortell dine venner om dette seminaret';
$lang['TY_TXT_FREEBONUS_H2'] ='OG FÅ TILGANG TIL DIN GRATIS BONUS!!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Velg din refereringsmetode';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Send e-post til dine venner';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Skriv inn ditt navn';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Skriv inn din e-post';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Din venns navn';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Din venns e-post';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Dette er e-posten som sendes til dine venner';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Emnelinje';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='E-post tekst';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Kirim email';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Post dette på Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifiser Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'legge ut på facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifiser Facebook!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Når du har postet til Facebook, skriv inn adressen til din Facebook Side under for å verifisere posten. Når det er verifisert, vil din gratis BONUS bli tilgjengelig umiddelbart!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Del på Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifiser Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Når du har postet til Twitter, skriv inn adressen til din Twitter profil under for å verifisere tweeten. Når det er verifisert, vil din gratis BONUS bli tilgjengelig umiddelbart.';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet dette innlegget';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gratulerer';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='DU HAR NÅ TILGANG TIL DIN GRATIS BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='GRATIS NEDLASTNING';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Høyreklikk på linken under og velg "Lagre som..""';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Navn mangler!';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Navn mangler!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Ugyldig e-post adresse!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL ikke verifisert!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Seminaret starter straks';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Foreleser';
$lang['COUNTDOWN_TXT_DATE'] ='Planlagt dato';
$lang['COUNTDOWN_TXT_TIME'] ='Planlagt tid';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Kobler til seminaret, vennligst vent';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Mens du sender ditt spørsmål, skriv også inn ditt navn og din e-post adresse slik at vi kan svare deg via e-post dersom vi ikke kan svare under selve seminaret.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Ditt navn (obligatorisk)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Din e-post (obligatorisk)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Ditt spørsmål';
$lang['LIVE_CBOX2_TXT_1'] ='Chat boks';
$lang['LIVE_CBOX2_TXT_2'] ='Skriv til';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Alle';
$lang['LIVE_CBOX2_TXT_3'] ='Melding';
$lang['LIVE_CBOX2_TXT_4'] ='Send melding';
$lang['LIVE_CBOX2_TXT_5'] ='Skriv inn navn og e-post slik at vi kan sende deg våre svar';

$lang['LIVE_CBOX2_TXT_6'] ='gi alle felter helt';
$lang['LIVE_CBOX2_TXT_7'] ='Melding sendt!';
$lang['LIVE_CBOX2_TXT_8'] ='Beklager, Message allerede sendt';

$lang['LIVE_CBOX2_SUBMIT'] ='Send melding';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Registrerte deltakere';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Replay Deltakere';
$lang['LIVE_ATTENDEELIST_HOST'] ='Vert';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTAT';
$lang['LIVE_POLL_TXT2'] ='Takk for din stemme';
$lang['LIVE_POLL_TXT3'] ='Avstemningsmulighetene endelige resultatene vil foreligge om kort tid ...';

$lang['LIVE2_H1'] ='SEMINAR APP STARTET';
$lang['LIVE2_TXT1'] ='Velkommen';
$lang['LIVE2_TXT2'] ='Innstillinger';
$lang['LIVE2_TXT3'] ='Logg inn';
$lang['LIVE2_TXT4'] ='Logg ut';
$lang['LIVE2_TXT5'] ='Presentasjon';
$lang['LIVE2_TXT6'] ='Medlemmer';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Handling Hendelser';
$lang['LIVE2_TXT9'] = 'Skriv inn ditt spørsmål eller kommentarer';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='PÅ';
$lang['LIVE3_TXT3'] ='Seminar ID';
$lang['LIVE3_POPUP1'] ='Valgene er deaktivert av seminar administrator';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Er du sikker på du vil avslutte? Det kan hende du ikke vil kunne logge på denne rommet igjen.';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'januar';
$langmonths['february'] = 'februar';
$langmonths['march'] = 'mars';
$langmonths['april'] = 'april';
$langmonths['may'] = 'mai';
$langmonths['june'] = 'juni';
$langmonths['july'] = 'juli';
$langmonths['august'] = 'august';
$langmonths['september'] = 'september';
$langmonths['october'] = 'oktober';
$langmonths['november'] = 'november';
$langmonths['december'] = 'desember';

$langdays = array();
$langdays['monday'] = 'mandag';
$langdays['tuesday'] = 'tirsdag';
$langdays['wednesday'] = 'onsdag';
$langdays['thursday'] = 'Torsdag';
$langdays['friday'] = 'fredag';
$langdays['saturday'] = 'lørdag';
$langdays['sunday'] = 'søndag';


?>