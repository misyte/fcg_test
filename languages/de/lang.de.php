<?php
# -----------------------------------------------
# LANGUAGE: GERMAN                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Registrieren Sie sich für ein Webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar-Präsentation';
$lang['REG_PBOX_TOPIC'] ='Thema';
$lang['REG_PBOX_PRESENTER'] ='Moderator';
$lang['REG_PBOX_HOST'] ='Gastgeber';
$lang['REG_PBOX_DATETIME'] ='Datum und Zeit';
$lang['REG_PBOX_DATETIME_VAL'] ='Wählen Sie bitte aus';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landesvorwahl für Ihr Mobiltelefon';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Geben Sie hier Ihre Telefonnummer ein ...';
$lang['REG_FORM_SMS_TXT1'] ='SMS Alarm: Ja, senden sie mir eine SMS-Erinnerung, bevor das Webinar startet (Optional, aber empfohlen).';
$lang['REG_FORM_SMS_TXT2'] ='Bitte ihre Landes-Vorwahl auswählen und ihre Mobil-Telefonnummer eintragen, um einen SMS Alarm 15 Minuten vor Beginn des Webinars zu erhalten.';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Füllen Sie bitte das Feld "Termin" aus';
$lang['REG_FORM_ALERT_TIME'] ='Füllen Sie bitte das Feld "Zeit" aus';
$lang['REG_FORM_ALERT_NAME'] ='Füllen Sie bitte das Feld "Name" aus';
$lang['REG_FORM_ALERT_EMAIL'] ='Bitte füllen Sie das Feld "E-Mail" aus';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Bitte geben Sie eine gültige E-Mail-Adresse ein';
$lang['REG_FORM_TXTBOX_NAME'] ='Geben Sie hier Ihren Namen ein ...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Geben Sie hier Ihre E-Mail ein';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Ihre Ortszeit';
$lang['REG_FORM_TXT_SELDATE'] ='Wählen Sie Ihren Termin aus';
$lang['REG_FORM_TXT_SELTIME'] ='Wählen Sie Ihren ersten Termin';
$lang['REG_FORM_TXT_SELTIME_2'] ='Wählen Sie Ihre gewünschte Zeit';
$lang['REG_FORM_TXT_SELECTDATE'] ='Wählen Sie Ihr Datum aus';
$lang['REG_FORM_TXT_SELECTTIME'] ='Wählen Sie Ihre Zeit aus';
$lang['REG_FORM_TXT_RN_1'] = 'Aufzeichnung des letzten Webinars anschauen';
$lang['REG_FORM_TXT_RN_2'] = 'Right Now!';

//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Webinar-Anmeldung';
$lang['REG_FORM_H2'] ='Reservieren Sie Ihren Platz!';
$lang['REG_FORM_H3'] ='Registrierung für das Webinar';
$lang['REG_FORM_SMS_TXT1'] ='Ich möchte gerne vor dem Event eine SMS erhalten';
$lang['REG_FORM_SMS_TXT2'] ='(Optional aber empfohlen) Bitte ihre Landesvorwahl auswählen und ihre Mobile Telefonnummer eintragen, um einen SMS Alarm zu erhalten. Sie erhalten dann 15 Minuten vor Webinarstart eine Erinnerung.';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline') && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {



$lang['REG_FORM_H1'] ='Reservieren Sie Ihren Platz!';
$lang['REG_FORM_H2'] ='Webinar-Anmeldung';
/*
$lang['REG_FORM_TXT_SELECTDATE'] ='An welchem Tag wollen Sie teilnehmen?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Welche Zeit passt Ihnen am besten?';
*/
$lang['REG_FORM_TXT_SELECTDATE'] ='Wählen Sie Ihr Datum aus';
$lang['REG_FORM_TXT_SELECTTIME'] ='Wählen Sie Ihre Zeit aus';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Wohin sollen wir Ihre Einladung senden?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Benachrichtigungen';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='Registrieren Sie sich jetzt!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinar-Präsentation';
$lang['PBOX_TXT_TOPIC'] ='Thema';
$lang['PBOX_TXT_HOST'] ='Gastgeber';
$lang['PBOX_TXT_PRESENTER'] ='Moderator';
$lang['PBOX_TXT_DATEANDTIME'] ='Datum und Zeit';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='An!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Präsentiert von';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinar Details';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Moderator';
$lang['TY_SCHEDDATE'] ='Vorgesehenes Datum';
$lang['TY_SCHEDTIME'] ='Vorgesehene Zeit';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Ihr einmaliger Webinar-Link';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'In Ihren iCal hinzufügen';
$lang['TY_TXT_GOOGLE_CAL'] = 'In Ihren Google-Kalender hinzufügen';
$lang['TY_TXT_OUTLOOK_CAL'] = 'In Ihren Outlook-Kalendar hinzufügen';
$lang['TY_TXT_CLICK_VER'] = 'Klicken Sie auf Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Erzählen Sie Ihren Freunden über dieses Webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='und schalten Sie Ihren kostenlosen Bonus frei!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Wählen Sie Ihre Empfehlungsmethode aus';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Senden Sie Ihren Freunden eine E-Mail';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Geben Sie Ihren Namen ein';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Geben Sie Ihre E-Mail ein';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Den Namen Ihres Freundes / Freundin';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='E-Mail Ihres Freundes / Ihrer Freundin';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Dies ist die E-Mail, die an Ihre Freunde gesandt wird';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Betreff';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='E-Mail';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Senden Sie E-Mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Poste diese Nachricht auf Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifiziere Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post auf Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Überprüfen URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Sobald Sie die Nachricht auf Facebook gepostet haben, geben Sie bitte die URL Ihrer Facebook-Seite unten ein, damit Ihr Post verifiziert wird. Während der Verifizierung wird Ihr versteckter Bonus sofort freigeschalten.';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweete dies auf Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifiziere Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Sobald Sie die Nachricht auf Twitter gepostet haben, geben Sie bitte die URL Ihrer Twitter-Seite unten ein, damit Ihr Tweet verifiziert wird. Während der Verifizierung wird Ihr versteckter Bonus sofort freigeschalten.';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Diese Nachricht';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Glückwunsch';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='Sie haben Ihren Bonus freigeschalten!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='Kostenloser DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Klicken Sie mit der rechten Maustaste auf den Link unten und wählen Sie "Speichern unter..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klicken Sie hier';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Name ist leer!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Ungültige E-Mail-Adresse!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL nicht verifiziert!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Das Webinar wird in Kürze beginnen ...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Moderator';
$lang['COUNTDOWN_TXT_DATE'] ='Vorgesehenes Datum';
$lang['COUNTDOWN_TXT_TIME'] ='Vorgesehene Zeit';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Verbindung zum Webinar, bitte warten ...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Wenn Sie eine Frage stellen, geben Sie auch Ihren Namen und Ihre E-Mail ein, damit wir Ihnen per E-Mail antworten können, falls wir es nicht während dem Webinar aus irgend einem Grund nicht können.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Ihr Name (notwendig)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Ihre E-Mail (notwendig)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Schreiben Sie Ihre Frage';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Schreiben Sie an';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Alle';
$lang['LIVE_CBOX2_TXT_3'] ='Nachricht';
$lang['LIVE_CBOX2_TXT_4'] ='Senden Sie Ihre Fragen';
$lang['LIVE_CBOX2_TXT_5'] ='Geben Sie Ihren Namen und E-Mail, so können wir Ihnen unsere Antwort';

$lang['LIVE_CBOX2_TXT_6'] ='bieten alle Felder vollständig aus';
$lang['LIVE_CBOX2_TXT_7'] ='Nachricht gesendet!';
$lang['LIVE_CBOX2_TXT_8'] ='Sorry, Nachricht bereits gesendet';

$lang['LIVE_CBOX2_SUBMIT'] ='Nachricht senden';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Registrierte Teilnehmer';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Teilnehmer der Wiederholung';
$lang['LIVE_ATTENDEELIST_HOST'] ='Gastgeber';
//POLL
$lang['LIVE_POLL_TXT1'] ='Ergebnis';
$lang['LIVE_POLL_TXT2'] ='Vielen Dank für Ihre Stimme';
$lang['LIVE_POLL_TXT3'] ='Die Umfrage endgültigen Ergebnisse werden in Kürze verfügbar sein ...';

$lang['LIVE2_H1'] ='Die Webinar App startet';
$lang['LIVE2_TXT1'] ='Willkommen';
$lang['LIVE2_TXT2'] ='Einstellungen';
$lang['LIVE2_TXT3'] ='Anmeldung';
$lang['LIVE2_TXT4'] ='Abmeldung';
$lang['LIVE2_TXT5'] ='Präsentation';
$lang['LIVE2_TXT6'] ='Mitglieder';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Aktion Events';
$lang['LIVE2_TXT9'] = 'Geben Sie Ihre Fragen oder Kommentare';
$lang['LIVE3_TXT1'] ='Status';
$lang['LIVE3_TXT2'] ='An';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Optionen wurden vom Webinar-Administrator abgeschalten';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Wollen Sie uns wirklich verlassen? Sie können sich nicht mehr im Webinarraum einloggen!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'Januar';
$langmonths['february'] = 'Februar';
$langmonths['march'] = 'März';
$langmonths['april'] = 'April';
$langmonths['may'] = 'Mai';
$langmonths['june'] = 'Juni';
$langmonths['july'] = 'Juli';
$langmonths['august'] = 'August';
$langmonths['september'] = 'September';
$langmonths['october'] = 'Oktober';
$langmonths['november'] = 'November';
$langmonths['december'] = 'Dezember';

$langdays = array();
$langdays['monday'] = 'Montag';
$langdays['tuesday'] = 'Dienstag';
$langdays['wednesday'] = 'Mittwoch';
$langdays['thursday'] = 'Donnerstag';
$langdays['friday'] = 'Freitag';
$langdays['saturday'] = 'Samstag';
$langdays['sunday'] = 'Sonntag';

?>