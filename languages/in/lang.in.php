<?php
# -----------------------------------------------
# LANGUAGE: INDONESIA                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='mendaftar ke webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Pendaftaran Webinar';
$lang['REG_PBOX_TOPIC'] ='Topik';
$lang['REG_PBOX_PRESENTER'] ='Presenter';
$lang['REG_PBOX_HOST'] ='Pemandu Acara';
$lang['REG_PBOX_DATETIME'] ='Tanggal dan Jam0';
$lang['REG_PBOX_DATETIME_VAL'] ='Pilih dari menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Kode negara untuk telpon genggam';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Masukkan nomor telpon genggam di sini....';
$lang['REG_FORM_SMS_TXT1'] ='Pengingat SMS. Ya, kirimi saya SMS pengingat sebelum webinar dimulai (pilihan namun disarankan).';
$lang['REG_FORM_SMS_TXT2'] ='Pilih kode negara dan masukkan nomer telpon selular untuk mendapatkan SMS pengingat 15 menit sebelum webinar dimulai';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Mohon isikan tanggal';
$lang['REG_FORM_ALERT_TIME'] ='Mohon isikan jam';
$lang['REG_FORM_ALERT_NAME'] ='Mohon isikan nama';
$lang['REG_FORM_ALERT_EMAIL'] ='Mohon isikan alamat email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Mohon isikan alamat email yang benar';
$lang['REG_FORM_TXTBOX_NAME'] ='Masukkan nama anda di sini...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Masukkan email anda di sini...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Waktu Setempat Anda sekarang';
$lang['REG_FORM_TXT_SELDATE'] ='Pilih tanggal yang diinginkan';
$lang['REG_FORM_TXT_SELTIME'] ='Pilih tanggal terlebih dulu';
$lang['REG_FORM_TXT_SELTIME_2'] ='Pilih jam yang diinginkan';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Pilih tanggal anda';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Pilih waktu anda';
$lang['REG_FORM_TXT_RN_1'] = 'Melihat video rekaman webinar kemarin SEKARANG!';
$lang['REG_FORM_TXT_RN_2'] = 'sekarang juga';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) {
$lang['REG_FORM_H1'] ='Pendaftaran Webinar';
$lang['REG_FORM_H2'] ='DAPATKAN TEMPAT ANDA!';
$lang['REG_FORM_H3'] ='MENDAFTAR KE WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Saya ingin mendapatkan SMS pengingat sebelum acara dimulai';
$lang['REG_FORM_SMS_TXT2'] ='(Pilihan namun sangat disarankan) Pilih kode negara anda dan masukkan nomor telpon genggam untuk mendapatkan SMS pengingat 15 menit sebelum acara dimulai';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
else if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline') {
$lang['REG_FORM_H1'] ='PESAN TEMPAT ANDA!';
$lang['REG_FORM_H2'] ='PENDAFTARAN WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] ='Hari apa anda ingin hadir?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Waktu yang paling sesuai dengan anda?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Kemana undangan harus dikirimkan?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Pemberitahuan';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='DAFTAR SEKARANG!';

}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Presentasi webinar';
$lang['PBOX_TXT_TOPIC'] ='Topik';
$lang['PBOX_TXT_HOST'] ='Pemandu Acara';
$lang['PBOX_TXT_PRESENTER'] ='Presenter';
$lang['PBOX_TXT_DATEANDTIME'] ='Tanggal dan Jam';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Dipresentasikan oleh';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Rincian webinar';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Presenter';
$lang['TY_SCHEDDATE'] ='Tanggal terjadwal';
$lang['TY_SCHEDTIME'] ='Jam terjadwal';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Tautan unik webinar anda';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Tambahkan ke  iCal Calendar anda';
$lang['TY_TXT_GOOGLE_CAL'] = 'Tambahkan ke Google Calender anda';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Tambahkan ke Outlook Calendar anda';
$lang['TY_TXT_CLICK_VER'] = 'Klik pada Versi';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Ceritakan ke teman tentang webinar ini';
$lang['TY_TXT_FREEBONUS_H2'] ='DAN DAPATKAN BONUS GRATIS ANDA!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Pilih metode referensi anda';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Kirim email ke teman-teman anda';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Masukkan nama anda';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Masukkan email anda';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Nama teman anda';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Email teman anda';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Ini adalah email yang akan dikirimkan kapada teman anda';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Subject line';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Email body';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='kirim email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Postingkan pesan ini ke Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Periksa Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'posting di facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Verifikasi URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Setelah anda postingkan pesan ini ke Facebook, masukkan URL Halaman Facebook anda di bawah untuk verifikasi. Usai verifikasi, bonus anda akan segera tersedia!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet kan ke Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Periksa Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Setelah anda postingkan pesan ini ke Twitter, masukkan URL halaman Twitter anda berikut ini untuk memeriksanya. Setelah pemeriksaan, bonus anda akan segera tersedia!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Pesan ini';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Selamat';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ANDA TELAH DAPATKAN BONUS!!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='UNDUH GRATIS';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Klik kanan tautan berikut dan pilih "save as..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='klik di sini!';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Nama kosong!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='email address tidak sesuai!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL tidak sesuai!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinar akan segera mulai...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presenter';
$lang['COUNTDOWN_TXT_DATE'] ='Tanggal terjadwal';
$lang['COUNTDOWN_TXT_TIME'] ='Jam terjadwal';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Menghubungkan ke webinar, mohon tunggu....';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Sambil mengirimkan pertanyaan anda, tolong masukkan nama dan email anda supaya kami bisa menjawab lewat email apabila kami tidak bisa menjawab saat webinar.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Nama anda (diperlukan)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Alamat email anda (diperlukan)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Ketik pertanyaan anda';
$lang['LIVE_CBOX2_TXT_1'] ='Ketik pertanyaan anda';
$lang['LIVE_CBOX2_TXT_2'] ='Tulis ke';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Semua';
$lang['LIVE_CBOX2_TXT_3'] ='Pesan';
$lang['LIVE_CBOX2_TXT_4'] ='Pesan';
$lang['LIVE_CBOX2_TXT_5'] ='Masukkan nama dan email sehingga kami dapat mengirimkan balasan kami';
$lang['LIVE_CBOX2_TXT_6'] ='menyediakan semua bidang benar-benar';
$lang['LIVE_CBOX2_TXT_7'] ='Pesan Terkirim!';
$lang['LIVE_CBOX2_TXT_8'] ='Maaf, pesan sudah dikirim';
$lang['LIVE_CBOX2_SUBMIT'] ='Kirim Pesan';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Peserta terdaftar';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Peserta Pemutaran ulang';
$lang['LIVE_ATTENDEELIST_HOST'] ='Pemandu Acara';
//POLL
$lang['LIVE_POLL_TXT1'] ='HASIL';
$lang['LIVE_POLL_TXT2'] ='Terima kasih atas suara Anda';
$lang['LIVE_POLL_TXT3'] ='Hasil akhir jajak pendapat akan segera tersedia';

$lang['LIVE2_H1'] ='WEBINAR APP DIMULAI';
$lang['LIVE2_TXT1'] ='Selamat datang';
$lang['LIVE2_TXT2'] ='Pengaturan';
$lang['LIVE2_TXT3'] ='Masuk';
$lang['LIVE2_TXT4'] ='Keluar';
$lang['LIVE2_TXT5'] ='Presentasi';
$lang['LIVE2_TXT6'] ='Anggota';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Aksi Acara';
$lang['LIVE2_TXT9'] = 'Masukkan pertanyaan atau komentar';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='menghidupkan';
$lang['LIVE3_TXT3'] ='ID Webinar';
$lang['LIVE3_POPUP1'] ='Pilihan dimatikan oleh admin webinar';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Anda yakin ingin keluar? Anda mungkin tidak bisa masuk lagi ke ruangan!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'Januari';
$langmonths['february'] = 'Februari';
$langmonths['march'] = 'Maret';
$langmonths['april'] = 'April';
$langmonths['may'] = 'Mei';
$langmonths['june'] = 'Juni';
$langmonths['july'] = 'Juli';
$langmonths['august'] = 'Agustus';
$langmonths['september'] = 'September';
$langmonths['october'] = 'Oktober';
$langmonths['november'] = 'november';
$langmonths['december'] = 'Desember';

$langdays = array();
$langdays['monday'] = 'Senin';
$langdays['tuesday'] = 'Selasa';
$langdays['wednesday'] = 'Rabu';
$langdays['thursday'] = 'Kamis';
$langdays['friday'] = 'Jumat';
$langdays['saturday'] = 'Sabtu';
$langdays['sunday'] = 'Minggu';


?>