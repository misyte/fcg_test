<?php
# -----------------------------------------------
# LANGUAGE: TURKISH               UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='webinar\'a kayıt yapılıyor';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar Sunumu';
$lang['REG_PBOX_TOPIC'] ='Konu';
$lang['REG_PBOX_PRESENTER'] ='Konuşmacı';
$lang['REG_PBOX_HOST'] ='Organizatör';
$lang['REG_PBOX_DATETIME'] ='Tarih ve Saat';
$lang['REG_PBOX_DATETIME_VAL'] ='Menüden seçin';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Mobil İçin Ülke Kodu';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Telefon Numaranızı Girin...';
$lang['REG_FORM_SMS_TXT1'] ='SMS Hatırlatması: Evet, webinar başlamadan bana SMS gönderin (isteğe bağlı ama önerilir).';
$lang['REG_FORM_SMS_TXT2'] ='Webinar Başlamadan 15 Dakika Önce Hatırlatma SMS\'i Almak İçin Ülke Kodunuzu Seçin ve Cep Telefonu Numaranızı Yazın';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Lütfen tarih alanını doldurun';
$lang['REG_FORM_ALERT_TIME'] ='Lütfen saat alanını doldurun';
$lang['REG_FORM_ALERT_NAME'] ='Lütfen isim alanını doldurun';
$lang['REG_FORM_ALERT_EMAIL'] ='Lütfen email alanını doldurun';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Lütfen geçerli bir email adresi girin';
$lang['REG_FORM_TXTBOX_NAME'] ='İsminizi Yazın...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Email Adresinizi Yazın...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Yerel Saat';
$lang['REG_FORM_TXT_SELDATE'] ='Tarih seçin';
$lang['REG_FORM_TXT_SELTIME'] ='İlk önce tarih seçin';
$lang['REG_FORM_TXT_SELTIME_2'] ='Saat seçin';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Tarih Seçin';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Saat Seçin';
$lang['REG_FORM_TXT_RN_1'] = 'ŞİMDİ dünkü semineri tekrar izleyin!';
$lang['REG_FORM_TXT_RN_2'] = 'hemen şimdi!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Webinar Kayıt-p--pp';
$lang['REG_FORM_H2'] ='Yerinizi Ayırın!';
$lang['REG_FORM_H3'] ='WEBINAR\'A KAYIT OL';
$lang['REG_FORM_SMS_TXT1'] ='Webinar Başlamadan Hatırlatma SMS\'i Almak İstiyorum';
$lang['REG_FORM_SMS_TXT2'] ='(İsteğe bağlı ama kesinlikle önerilir) Webinar Başlamadan 15 Dakika Önce Hatırlatma SMS\'i Almak İçin Ülke Kodunuzu Seçin ve Cep Telefonu Numaranızı Yazın';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Yerinizi Ayırın!';
$lang['REG_FORM_H2'] ='Webinar Kayıt-p--pp';
$lang['REG_FORM_TXT_SELECTDATE'] ='Ne Gün Katılmak İstiyorsunuz?';
$lang['REG_FORM_TXT_SELECTTIME'] ='En Uygun Saat Hangisi?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Katılım Davetiyesi Nereye Gönderilsin:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Bilgilendirmeler';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='ŞİMDİ TIKLAYIN!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinar Sunumu';
$lang['PBOX_TXT_TOPIC'] ='Konu';
$lang['PBOX_TXT_HOST'] ='Organizatör';
$lang['PBOX_TXT_PRESENTER'] ='Konuşmacı';
$lang['PBOX_TXT_DATEANDTIME'] ='Date and Time';
$lang['PBOX_TXT_STATUS'] ='STATÜ';
$lang['PBOX_TXT_STATUS_VAL'] ='AÇIK!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Organizatör';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinar Detayları';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Konuşmacı';
$lang['TY_SCHEDDATE'] ='Planlanan tarih';
$lang['TY_SCHEDTIME'] ='Planlanan saat';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Size özel webinar bağlantı linkiniz';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'iCal Takviminize Ekleyin';
$lang['TY_TXT_GOOGLE_CAL'] = 'Google Takviminize Ekleyin';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Outlook Takviminize Ekleyin';
$lang['TY_TXT_CLICK_VER'] = 'Sürüm tıklayın';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Arkadaşlarınızı bu webinar\'dan haberdar edin';
$lang['TY_TXT_FREEBONUS_H2'] ='VE ÜCRETSİZ BONUS HEDİYENİZE ERİŞİN!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Tercih ettiğiniz haber verme şeklini seçin';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Arkadaşlarınıza Email Gönderin';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='İsminizi Yazın';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Emailinizi Yazın';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Arkadaşınızın ismi';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Arkadaşınızın email adresi';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Arkadaşınıza gönderilecek email bu şekildedir';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Konu başlığı';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Email İçeriği';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='e-posta gönderin!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Facebook\'da Paylaşın';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Facebook\'u doğrulayın';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'On Facebook Ver';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'URL doğrulayın!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Bu mesajı Facebook\'da paylaştıktan sonra, doğrulamak için Facebook sayfanızın linkini(URL) aşağıya yazın. Doğrulandığı zaman, ücretsiz bonus hediyenize anında erişeceksiniz!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Twitter\'da Tweetleyin';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Twitter\'i doğrulayın';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Bu mesajı tweet\'ledikten sonra, doğrulamak için Twitter sayfanızın linkini(URL) aşağıya yazın. Doğrulandığı zaman, ücretsiz bonus hediyenize anında erişeceksiniz!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Bu Mesaj';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Tebrikler';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='BONUS HEDİYENİZE ERİŞTİNİZ!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='ÜCRETSİZ DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Aşağıdaki linke sağ tıklayın ve dosyayı bilgisayarınıza indirin..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='buraya tıklayın';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='İsim alanı boş!!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Geçersiz Email Adresi!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL doğrulanamad!!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinar Kısa Bir Süre Sonra Başlayacak...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Konuşmacı';
$lang['COUNTDOWN_TXT_DATE'] ='Planlanan tarih';
$lang['COUNTDOWN_TXT_TIME'] ='Planlanan saat';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Webinar\'a bağlanıyor, lütfen bekleyin...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Sorunuzu gönderirken isim ve email adresinizi de girin. Böylece, herhangi bir sebepten dolayı webinar sırasında cevaplayamadığımız sorularınızı email üzerinden cevaplayabiliriz.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='İsim (Gerekli)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Email (Gerekli)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Sorunuzu Yazın';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Kutusu';
$lang['LIVE_CBOX2_TXT_2'] ='Kime';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Herkes';
$lang['LIVE_CBOX2_TXT_3'] ='Mesaj';
$lang['LIVE_CBOX2_TXT_4'] ='Sorularınızı Gönderin';
$lang['LIVE_CBOX2_TXT_5'] ='Adınızı ve e-posta girin yüzden siz cevap gönderebilir';
$lang['LIVE_CBOX2_TXT_6'] ='tamamen tüm alanları sağlamak';
$lang['LIVE_CBOX2_TXT_7'] ='Mesaj Gönderildi!';
$lang['LIVE_CBOX2_TXT_8'] ='Üzgünüm, Mesaj zaten gönderildi';
$lang['LIVE_CBOX2_SUBMIT'] ='Gönder';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Kayıtlı Katılımcılar';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Tekrar Katılımcıları';
$lang['LIVE_ATTENDEELIST_HOST'] ='Organizatör';
//POLL
$lang['LIVE_POLL_TXT1'] ='SONUÇ';
$lang['LIVE_POLL_TXT2'] ='Oy için teşekkür ederiz';
$lang['LIVE_POLL_TXT3'] ='Anket nihai sonuçları kısa süre içinde hazır olacak';

$lang['LIVE2_H1'] ='WEBINAR APP BAŞLADI';
$lang['LIVE2_TXT1'] ='Hoşgeldiniz';
$lang['LIVE2_TXT2'] ='Ayarlar';
$lang['LIVE2_TXT3'] ='Giriş';
$lang['LIVE2_TXT4'] ='Çıkış';
$lang['LIVE2_TXT5'] ='Sunum';
$lang['LIVE2_TXT6'] ='Üyeler';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Eylem Olaylar';
$lang['LIVE2_TXT9'] = 'Soru ya da yorumlarınızı girin';
$lang['LIVE3_TXT1'] ='STATÜ';
$lang['LIVE3_TXT2'] ='AÇIK';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Seçenekler webinar admini tarafından kapatılmıştır';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Çıkmak istediğinizden emin misiniz? Odaya tekrar geri dönüş yapamayabilirsiniz!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'Ocak';
$langmonths['february'] = 'Şubat';
$langmonths['march'] = 'Mart';
$langmonths['april'] = 'Nisan';
$langmonths['may'] = 'Mayıs';
$langmonths['june'] = 'Haziran';
$langmonths['july'] = 'Temmuz';
$langmonths['august'] = 'Ağustos';
$langmonths['september'] = 'Eylül';
$langmonths['october'] = 'Ekim';
$langmonths['november'] = 'Kasım';
$langmonths['december'] = 'Aralık';

$langdays = array();
$langdays['monday'] = 'Pazartesi';
$langdays['tuesday'] = 'Salı';
$langdays['wednesday'] = 'Çarşamba';
$langdays['thursday'] = 'Perşembe';
$langdays['friday'] = 'Cuma';
$langdays['saturday'] = 'Cumartesi';
$langdays['sunday'] = 'Pazar';




?>