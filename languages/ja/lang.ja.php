<?php
# -----------------------------------------------
# LANGUAGE: JAPAN                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='ウェビナーに登録';
//presentation-box
$lang['REG_PBOX_H1'] ='ウェビナーのプレゼンテーション';
$lang['REG_PBOX_TOPIC'] ='トピック';
$lang['REG_PBOX_PRESENTER'] ='プレゼンター';
$lang['REG_PBOX_HOST'] ='ホスト';
$lang['REG_PBOX_DATETIME'] ='日時';
$lang['REG_PBOX_DATETIME_VAL'] ='メニューから選択';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='モバイルの国コード';
$lang['REG_FORM_SMS_INPUTPHONE'] ='電話番号を入力してください...';
$lang['REG_FORM_SMS_TXT1'] ='SMSメッセージを送る.';
$lang['REG_FORM_SMS_TXT2'] ='（省略可能、ただし推奨）国コードを選択し、テキストアラートを受信するには携帯電話の番号を入力してください。 ウェビナーが開始される前にリマインダーが届きます。';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='日付フィールドに必要事項を記入してください';
$lang['REG_FORM_ALERT_TIME'] ='時間フィールドに必要事項を記入してください';
$lang['REG_FORM_ALERT_NAME'] ='名前欄にご記入ください';
$lang['REG_FORM_ALERT_EMAIL'] ='Eメールフィールドに必要事項を記入してください';
$lang['REG_FORM_ALERT_EMAIL_2'] ='有効なEメールアドレスを入力してください';
$lang['REG_FORM_TXTBOX_NAME'] ='あなたのお名前をここに入力してください...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='あなたのメールアドレスをここに入力してください...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='あなたの現地時間';
$lang['REG_FORM_TXT_SELDATE'] ='希望の日付を選択してください';
$lang['REG_FORM_TXT_SELTIME'] ='日付を選択';
$lang['REG_FORM_TXT_SELTIME_2'] ='時間を選択';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. 日付を選択してください';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. 時間を選択してください';
$lang['REG_FORM_TXT_RN_1'] = '過去のウェビナーのリプレイを見てください！';
$lang['REG_FORM_TXT_RN_2'] = '今すぐ';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || @$rp_regtpl == 0 || @$rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='ウェビナー登録';
$lang['REG_FORM_H2'] ='席を確保';
$lang['REG_FORM_H3'] ='ウェビナーへの登録';
$lang['REG_FORM_SMS_TXT1'] ='イベントが始まる前に私はSMSテキストアラートを受け取る';
$lang['REG_FORM_SMS_TXT2'] ='（省略可能、ただし推奨）国コードを選択し、テキストアラートを受信するには携帯電話の番号を入力してください。 ウェビナーが開始される前にリマインダーが届きます。';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || @$rp_regtpl == 3 || @$rp_regtpl == 4 || @$rp_regtpl == 5 || @$rp_regtpl == 6 || @$rp_regtpl == 7 || @$rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='無料WEBセミナーへの';
$lang['REG_FORM_H2'] ='ご参加はこちら';
$lang['REG_FORM_TXT_SELECTDATE'] ='どの日付が良いですか？';
$lang['REG_FORM_TXT_SELECTTIME'] ='どの時間が良いですか？';
$lang['REG_FORM_TXT_SENDINVITATION'] ='招待メールはどこに送りますか？';
$lang['REG_FORM_TXT_NOTIFICATION'] ='通知';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='今すぐ登録！';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='ウェビナーのプレゼンテーション';
$lang['PBOX_TXT_TOPIC'] ='トピック';
$lang['PBOX_TXT_HOST'] ='ホスト';
$lang['PBOX_TXT_PRESENTER'] ='プレゼンター';
$lang['PBOX_TXT_DATEANDTIME'] ='日時';
$lang['PBOX_TXT_STATUS'] ='状態';
$lang['PBOX_TXT_STATUS_VAL'] ='オン';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='プレゼンター';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='ウェビナーの詳細';
$lang['TY_WEBINAR'] ='ウェビナー';
$lang['TY_PRESENTER'] ='プレゼンター';
$lang['TY_SCHEDDATE'] ='日付';
$lang['TY_SCHEDTIME'] ='時間';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='ウェビナーへのリンク';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'iCalに追加';
$lang['TY_TXT_GOOGLE_CAL'] = 'Googleカレンダーに追加';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Outlookに追加';
$lang['TY_TXT_CLICK_VER'] = 'バージョンをクリックして';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='友人に伝える';
$lang['TY_TXT_FREEBONUS_H2'] ='無料ボーナスを受け取る';
$lang['TY_TXT_FREEBONUS_STEP1'] ='紹介方法を選択';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='友人にメールを送る';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='名前を入力';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='メールアドレスを入力';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='友人の名前を入力';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='友人のメールアドレスを入力';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='友人に送られるメール';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='件名';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='本文';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='メールを送信';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Facebookに投稿する';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Facebookで認証する';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Facebookに投稿する';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'URLを確認';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Facebookに投稿し、そのURLを下記に入力するとボーナスが受け取れます。';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Twitterでつぶやく';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Twitterで認証する';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Twitterに投稿し、そのURLを下記に入力するとボーナスが受け取れます。';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet このメッセージ';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='おめでとうございます';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ボーナスが受け取れます';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='無料ダウンロード';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='右クリックして名前をつけて保存ををしてください"';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='名前が空欄です';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='名前が空欄です!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='メールアドレスが正しくありません!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URLが正しくありません';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='まもなく始まります';
$lang['COUNTDOWN_TXT_PRESENTER'] ='プレゼンター';
$lang['COUNTDOWN_TXT_DATE'] ='日付';
$lang['COUNTDOWN_TXT_TIME'] ='時間';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='ウェビナーに接続しています';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='質問を送信する際には名前とメールアドレスを入力してください。ウェビナー中に回答できない場合、後ほど回答いたします。';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='名前（必須）';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='メールアドレス（必須）';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='質問を入力';
$lang['LIVE_CBOX2_TXT_1'] ='チャットボックス';
$lang['LIVE_CBOX2_TXT_2'] ='宛先';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='全て';
$lang['LIVE_CBOX2_TXT_3'] ='メッセージ';
$lang['LIVE_CBOX2_TXT_4'] ='あなたの質問を送る';
$lang['LIVE_CBOX2_TXT_5'] ='いただいたご質問にセミナー内でご回答いたします';

$lang['LIVE_CBOX2_TXT_6'] ='完全にすべてのフィールドを提供';
$lang['LIVE_CBOX2_TXT_7'] ='メッセージが送信されました！';
$lang['LIVE_CBOX2_TXT_8'] ='申し訳ありませんが、メッセージがすでに送信';


$lang['LIVE_CBOX2_SUBMIT'] ='送信する';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='参加者';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='リプレイ参加者';
$lang['LIVE_ATTENDEELIST_HOST'] ='ホスト';
//POLL
$lang['LIVE_POLL_TXT1'] ='結果';
$lang['LIVE_POLL_TXT2'] ='あなたの投票をありがとうございました';
$lang['LIVE_POLL_TXT3'] ='世論調査の最終結果は、すぐに利用できるようになります...';

$lang['LIVE2_H1'] ='ウェビナーが始まりました';
$lang['LIVE2_TXT1'] ='ようこそ';
$lang['LIVE2_TXT2'] ='設定';
$lang['LIVE2_TXT3'] ='サインイン';
$lang['LIVE2_TXT4'] ='サインアウト';
$lang['LIVE2_TXT5'] ='プレゼンテーション';
$lang['LIVE2_TXT6'] ='メンバー';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'ウェビナーアクションイベント';
$lang['LIVE2_TXT9'] = 'ご質問やコメントを入力してください';
$lang['LIVE3_TXT1'] ='状態';
$lang['LIVE3_TXT2'] ='オン';
$lang['LIVE3_TXT3'] ='ウェビナーID';
$lang['LIVE3_POPUP1'] ='オプションはオフにされました';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='ログアウトしますか？';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = '1月';
$langmonths['february'] = '2月';
$langmonths['march'] = '3月';
$langmonths['april'] = '4月';
$langmonths['may'] = '5月';
$langmonths['june'] = '6月';
$langmonths['july'] = '7月';
$langmonths['august'] = '8月';
$langmonths['september'] = '9月';
$langmonths['october'] = '10月';
$langmonths['november'] = '11月';
$langmonths['december'] = '12月';

$langdays = array();
$langdays['monday'] = '月曜日';
$langdays['tuesday'] = '火曜日';
$langdays['wednesday'] = '水曜日';
$langdays['thursday'] = '木曜日';
$langdays['friday'] = '金曜日';
$langdays['saturday'] = '土曜日';
$langdays['sunday'] = '日曜日';


?>