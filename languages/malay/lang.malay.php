<?php
# -----------------------------------------------
# LANGUAGE: MALAY                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='mendaftar ke webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Pendaftaran Webinar';
$lang['REG_PBOX_TOPIC'] ='Topik';
$lang['REG_PBOX_PRESENTER'] ='Presenter';
$lang['REG_PBOX_HOST'] ='Hos';
$lang['REG_PBOX_DATETIME'] ='Tarikh dan Masa';
$lang['REG_PBOX_DATETIME_VAL'] ='Pilih dari menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Kod negara untuk telefon mudah alih';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Sila isikan nombor telefon anda di sini....';
$lang['REG_FORM_SMS_TXT1'] ='Peringatan SMS. Ya, kirimkan saya SMS peringatan sebelum webinar dimulakan (pilihan namun disarankan).';
$lang['REG_FORM_SMS_TXT2'] ='Pilih kod negara dan masukkan nombor telefon mudah alih untuk mendapatkan SMS peringatan 15 minit sebelum webinar dimulakan';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Sila isikan tarikh';
$lang['REG_FORM_ALERT_TIME'] ='Sila isikan masa';
$lang['REG_FORM_ALERT_NAME'] ='Sila isikan nama';
$lang['REG_FORM_ALERT_EMAIL'] ='Sila isikan alamat emel';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Sila isikan alamat emel yang benar';
$lang['REG_FORM_TXTBOX_NAME'] ='Isikan nama anda di sini...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Isikan emel anda di sini...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Waktu Tempatan Anda sekarang';
$lang['REG_FORM_TXT_SELDATE'] ='Pilih tarikh yang diinginkan';
$lang['REG_FORM_TXT_SELTIME'] ='Pilih tarikh terlebih dulu';
$lang['REG_FORM_TXT_SELTIME_2'] ='Pilih masa yang diinginkan';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Pilih tarikh anda';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Pilih masa anda';
$lang['REG_FORM_TXT_RN_1'] = 'menonton ulangan Webinar semalam SEKARANG!';
$lang['REG_FORM_TXT_RN_2'] = 'sekarang!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) {
$lang['REG_FORM_H1'] ='Pendaftaran Webinar';
$lang['REG_FORM_H2'] ='Dapatkan tempat anda!';
$lang['REG_FORM_H3'] ='MENDAFTAR KE WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Saya ingin mendapatkan SMS peringatan sebelum acara bermula';
$lang['REG_FORM_SMS_TXT2'] ='(Pilihan namun sangat disarankan) Pilih kod negara anda dan isikan nombor telefon mudah alih anda untuk mendapatkan SMS peringatan 15 minit sebelum acara dimulakan';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
else if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline') {
$lang['REG_FORM_H1'] ='DAPATKAN TEMPAT ANDA!';
$lang['REG_FORM_H2'] ='PENDAFTARAN WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] ='Hari anda ingin hadir?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Masa yang paling sesuai untuk anda?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Kemana undangan harus dikirimkan?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Pemberitahuan';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='DAFTAR SEKARANG!';


}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Presentasi webinar';
$lang['PBOX_TXT_TOPIC'] ='Topik';
$lang['PBOX_TXT_HOST'] ='Hos';
$lang['PBOX_TXT_PRESENTER'] ='Presenter';
$lang['PBOX_TXT_DATEANDTIME'] ='Tarikh dan Masa';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Dipresentasikan oleh';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Matlumat webinar';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Presenter';
$lang['TY_SCHEDDATE'] ='Tarikh';
$lang['TY_SCHEDTIME'] ='Masa';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Pautan unik webinar anda';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Masukkan ke Kalendar iCal anda';
$lang['TY_TXT_GOOGLE_CAL'] = 'Masukkan ke Kalendar Google anda';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Masukkan ke Kalendar Outlook anda';
$lang['TY_TXT_CLICK_VER'] = 'Klik pada Versi';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Beritahu kawan anda mengenai webinar ini';
$lang['TY_TXT_FREEBONUS_H2'] ='DAN DAPATKAN BONUS PERCUMA ANDA!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Pilih opsyen pemberitahuan anda';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Kirim emel kepada kawan anda';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Isikan nama anda';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Isikan emel anda';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Nama kawan anda';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Alamat Emel kawan anda';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Ini adalah emel yang akan dikirimkan kapada kawan anda';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Subjek';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Isian Emel';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Hantar Email';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Poskan pesanan ini ke Facebook anda';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Pemeriksaan Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post Di Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Mengesahkan URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Setelah anda poskan pesanan ini ke Facebook, isikan URL Halaman Facebook anda di bawah untuk verifikasi. Selepas verifikasi, bonus anda akan segera tersedia!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet ke Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Pemeriksaan Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Setelah anda poskan pesanan ini ke Twitter, masukkan URL halaman Twitter anda berikut ini untuk memeriksanya. Setelah pemeriksaan, bonus anda akan segera tersedia!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Mesej Ini';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Tahniah';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ANDA TELAH DAPATKAN BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='DOWNLOAD PERCUMA';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Klik kanan pautan berikut dan pilih "save as...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klik Disini';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Nama kosong!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Alamat emel tidak sesuai!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL tidak sesuai!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinar akan bermula sebentar lagi...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Presenter';
$lang['COUNTDOWN_TXT_DATE'] ='Tarikh';
$lang['COUNTDOWN_TXT_TIME'] ='Masa';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Menghubungi webinar, sila tunggu....';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Sambil mengirimkan pertanyaan anda, sila masukkan nama dan alamat emel anda supaya kami masih boleh menjawab pertanyaan anda jikalau kami tidak sempat berbuat demikian ketika webinar ini sedang dijalankan.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Nama anda (diperlukan)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Alamat emel anda (diperlukan)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Taipkan soalan anda';
$lang['LIVE_CBOX2_TXT_1'] ='Chat Box';
$lang['LIVE_CBOX2_TXT_2'] ='Tulis ke';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Semua';
$lang['LIVE_CBOX2_TXT_3'] ='Mesej';
$lang['LIVE_CBOX2_TXT_4'] ='Hantar Soalan Anda';
$lang['LIVE_CBOX2_TXT_5'] ='Masukkan nama dan email anda supaya kami boleh menghantar jawapan kami';
$lang['LIVE_CBOX2_TXT_6'] ='menyediakan semua bidang sepenuhnya';
$lang['LIVE_CBOX2_TXT_7'] ='Mesej dihantar!';
$lang['LIVE_CBOX2_TXT_8'] ='Maaf, Mesej sudah dihantar';



$lang['LIVE_CBOX2_SUBMIT'] ='Kirim Mesej';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Peserta terdaftar';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Peserta Pemutaran ulang';
$lang['LIVE_ATTENDEELIST_HOST'] ='Hos';
//POLL
$lang['LIVE_POLL_TXT1'] ='HASIL';
$lang['LIVE_POLL_TXT2'] ='Terima kasih kerana undi anda';
$lang['LIVE_POLL_TXT3'] ='Keputusan undian muktamad akan disediakan sejurus ...';

$lang['LIVE2_H1'] ='APP WEBINAR DIMULAKAN';
$lang['LIVE2_TXT1'] ='Selamat datang';
$lang['LIVE2_TXT2'] ='Pengaturan';
$lang['LIVE2_TXT3'] ='Masuk';
$lang['LIVE2_TXT4'] ='Keluar';
$lang['LIVE2_TXT5'] ='Presentasi';
$lang['LIVE2_TXT6'] ='Member';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Peristiwa Tindakan';
$lang['LIVE2_TXT9'] = 'Masukkan soalan atau komen anda';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='ID Webinar';
$lang['LIVE3_POPUP1'] ='Pilihan dimatikan oleh admin webinar';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Anda yakin ingin keluar? Anda mungkin tidak dibenarkan masuk lagi ke halaman ini!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'januari';
$langmonths['february'] = 'februari';
$langmonths['march'] = 'Mac';
$langmonths['april'] = 'April';
$langmonths['may'] = 'Mei';
$langmonths['june'] = 'Jun';
$langmonths['july'] = 'Julai';
$langmonths['august'] = 'ogos';
$langmonths['september'] = 'September';
$langmonths['october'] = 'Oktober';
$langmonths['november'] = 'November';
$langmonths['december'] = 'Disember';

$langdays = array();
$langdays['monday'] = 'isnin';
$langdays['tuesday'] = 'Selasa';
$langdays['wednesday'] = 'rabu';
$langdays['thursday'] = 'Khamis';
$langdays['friday'] = 'Jumaat';
$langdays['saturday'] = 'Sabtu';
$langdays['sunday'] = 'Ahad';


?>