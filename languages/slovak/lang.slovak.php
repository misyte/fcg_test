<?php
# -----------------------------------------------
# LANGUAGE: SLOVAC                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='prihlasujem na webinár';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinár';
$lang['REG_PBOX_TOPIC'] ='Téma';
$lang['REG_PBOX_PRESENTER'] ='Prednáša';
$lang['REG_PBOX_HOST'] ='Organizuje';
$lang['REG_PBOX_DATETIME'] ='Dátum a čas';
$lang['REG_PBOX_DATETIME_VAL'] ='Vyberte si z ponuky';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Medzinárodná predvoľba na mobil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Zadajte svoje číslo na mobil...';
$lang['REG_FORM_SMS_TXT1'] ='SMS upozornenie: Áno, odošlite mi upozornenie cez SMS pred začiatkom webinára (nepovinné, ale doporučené)';
$lang['REG_FORM_SMS_TXT2'] ='Vyberte si predvoľbu krajiny a zadajte svoje mobilné číslo pre zaslanie SMS upozornenia 15 minút pred začiatkom webinára';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Prosím vyplňte položku dátum';
$lang['REG_FORM_ALERT_TIME'] ='Prosím vyplňte položku čas';
$lang['REG_FORM_ALERT_NAME'] ='Prosím vyplňte svoje meno';
$lang['REG_FORM_ALERT_EMAIL'] ='Prosím vyplňte svoj email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Prosím vložte fungujúcu email adresu';
$lang['REG_FORM_TXTBOX_NAME'] ='Zadajte svoje meno tu...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Zadajte svoj email tu...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Váš aktuálny čas';
$lang['REG_FORM_TXT_SELDATE'] ='Vyberte si dátum';
$lang['REG_FORM_TXT_SELTIME'] ='Vyberte si najprv dátum';
$lang['REG_FORM_TXT_SELTIME_2'] ='Vyberte si čas';
$lang['REG_FORM_TXT_SELECTDATE'] ='Vyberte si dátum';
$lang['REG_FORM_TXT_SELECTTIME'] ='Vyberte si čas';
$lang['REG_FORM_TXT_RN_1'] = 'Sledujte Včera Webinar záznam TERAZ!';
$lang['REG_FORM_TXT_RN_2'] = 'práve teraz!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Prihlásenie na webinár';
$lang['REG_FORM_H2'] ='Zarezervujte si miesto!';
$lang['REG_FORM_H3'] ='PRIHLÁSENIE NA WEBINÁR';
$lang['REG_FORM_SMS_TXT1'] ='Chcem zaslať SMS upozornenie pred začiatkom webinára';
$lang['REG_FORM_SMS_TXT2'] ='(Nepovinné ale doporučené) Vyberte si predvolbu krajiny a zadajte svoje mobilne číslo pre zaslalie SMS upozornenia 15 minút pred začiatkom webinára';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Zarezervujte si miesto!';
$lang['REG_FORM_H2'] ='Prihlásenie na webinár';
$lang['REG_FORM_TXT_SELECTDATE'] ='Ktorý deň sa chcete zúčastniť?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Ktorý čas vám najviac vyhovuje?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Kde chcete zaslať pozvánku?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Upozornenie';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='PRIHLÁSIŤ TERAZ!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinár';
$lang['PBOX_TXT_TOPIC'] ='Téma';
$lang['PBOX_TXT_HOST'] ='Organizuje';
$lang['PBOX_TXT_PRESENTER'] ='Prednáša';
$lang['PBOX_TXT_DATEANDTIME'] ='Dátum a čas';
$lang['PBOX_TXT_STATUS'] ='STAV';
$lang['PBOX_TXT_STATUS_VAL'] ='AKTÍVNY!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Prednáša';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='O webinári';
$lang['TY_WEBINAR'] ='Webinár';
$lang['TY_PRESENTER'] ='Prednáša';
$lang['TY_SCHEDDATE'] ='Dátum začiatku';
$lang['TY_SCHEDTIME'] ='Čas začiatku';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Váš špeciálny odkaz na pripojenie';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Pridať do vášho iCal kalendára';
$lang['TY_TXT_GOOGLE_CAL'] = 'Pridať do vášho Google kalendára';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Pridať do vášho Outlook kalendára';
$lang['TY_TXT_CLICK_VER'] = 'Kliknite na verziu';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Povedzte o tomto webinári svojm priateľom';
$lang['TY_TXT_FREEBONUS_H2'] ='A ZÍSKAJTE NÁŠ ŠPECIÁLNY BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Vyberte si spôsob doporučenia';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Odoslať email priateľom';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Zadajte svoje meno';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Zadajte svoj email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Meno vašeho priateľa';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='YEmail vašeho priateľa';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Toto je email, ktorý bude zaslaný vašim priateľom';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Subject line';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Text emailu';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Poslať e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Zverejnit túto správu na Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Overiť zverejnenie na Facebooku';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Pridať na Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Overte URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Po vložení správy na Facebook zadajte adresu vašej Facebook stránky pre overenie vloženia. Po overení sa vám ihneď sprístupní sľúbený bonus!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Odoslať správu na Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Overiť zverejnenie na Twittri';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Po vložení správy na Twitter zadajte adresu vašej Twitter stránky pre overenie vloženia. Po overení sa vám ihneď sprístupní sľúbený bonus!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Táto správa';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gratulujem';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ZÍSKALI STE SĽÚBENÝ BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='STIAHNUŤ ZADARMO';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Kliknite na odkaz pravým tlačítkom myši a zvoľte "Uložiť ako...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klikni tu';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Meno je prázdné!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Chybná emailová adresa!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='Adresa nie je overená!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinár začne za chvíľu...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Prednáška';
$lang['COUNTDOWN_TXT_DATE'] ='Dátum začiatku';
$lang['COUNTDOWN_TXT_TIME'] ='Čas začiatku';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Pripojujem sa na webinár, prosím vyčkajte...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Pred odoslaním otázky prosím zadajte vaše meno a email, aby sme vám mohli odpovedať emailom, ak nebude možné zodpovedať počas webinára.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Vaše meno (vyžadované)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Váš email  (vyžadované)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Zadajte svoju otázku';
$lang['LIVE_CBOX2_TXT_1'] ='Okno četu';
$lang['LIVE_CBOX2_TXT_2'] ='Komu';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Všetko';
$lang['LIVE_CBOX2_TXT_3'] ='Správa';
$lang['LIVE_CBOX2_TXT_4'] ='Poslať na vaše otázky';
$lang['LIVE_CBOX2_TXT_5'] ='Zadajte svoje meno a e-mail, takže môžeme vám zaslať odpoveď';

$lang['LIVE_CBOX2_TXT_6'] ='poskytnúť všetky polia úplne';
$lang['LIVE_CBOX2_TXT_7'] ='Správa bola odoslaná!';
$lang['LIVE_CBOX2_TXT_8'] ='Je nám ľúto, Správa už odoslané';

$lang['LIVE_CBOX2_SUBMIT'] ='Odoslať správu';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Prihlásený účastníci';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Účastníci zo záznamu';
$lang['LIVE_ATTENDEELIST_HOST'] ='Organizuje';
//POLL
$lang['LIVE_POLL_TXT1'] ='VÝSLEDOK';
$lang['LIVE_POLL_TXT2'] ='Ďakujeme za váš hlas';
$lang['LIVE_POLL_TXT3'] ='Prieskum verejnej mienky Konečné výsledky budú k dispozícii krátko ...';

$lang['LIVE2_H1'] ='APLIKÁCIA WEBINÁRA BEŽÍ';
$lang['LIVE2_TXT1'] ='Vitajte';
$lang['LIVE2_TXT2'] ='Nastavenie';
$lang['LIVE2_TXT3'] ='Prihlásit';
$lang['LIVE2_TXT4'] ='Odhlásit';
$lang['LIVE2_TXT5'] ='Prezentácia';
$lang['LIVE2_TXT6'] ='Členovia';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Akcia Webinar Akčné';
$lang['LIVE2_TXT9'] = 'Zadajte svoje otázky alebo pripomienky';
$lang['LIVE3_TXT1'] ='STAV';
$lang['LIVE3_TXT2'] ='AKTÍVNY';
$lang['LIVE3_TXT3'] ='ID webinára';
$lang['LIVE3_POPUP1'] ='Zmena nastavenia bola vypnutá administrátorom';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Ste si istý, že chcete odísť? Možno sa nebudete môcť pripojiť späť!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'január';
$langmonths['february'] = 'február';
$langmonths['march'] = 'marec';
$langmonths['april'] = 'apríl';
$langmonths['may'] = 'máj';
$langmonths['june'] = 'jún';
$langmonths['july'] = 'júl';
$langmonths['august'] = 'august';
$langmonths['september'] = 'septembra';
$langmonths['october'] = 'október';
$langmonths['november'] = 'november';
$langmonths['december'] = 'december';

$langdays = array();
$langdays['monday'] = 'pondelok';
$langdays['tuesday'] = 'utorok';
$langdays['wednesday'] = 'streda';
$langdays['thursday'] = 'štvrtok';
$langdays['friday'] = 'piatok';
$langdays['saturday'] = 'sobota';
$langdays['sunday'] = 'nedeľa';

?>