<?php
# -----------------------------------------------
# LANGUAGE: Danish                      UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='tilmelding til et webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinar præsentation';
$lang['REG_PBOX_TOPIC'] ='Emne';
$lang['REG_PBOX_PRESENTER'] ='Taler';
$lang['REG_PBOX_HOST'] ='Vært';
$lang['REG_PBOX_DATETIME'] ='Dato og tidspunkt';
$lang['REG_PBOX_DATETIME_VAL'] ='Vælg fra menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landekode – mobil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Indtast dit telefonnummer ...';
$lang['REG_FORM_SMS_TXT1'] ='SMS besked: Ja, send mig en SMS besked, inden webinaret starter (valgfrit - men anbefales).';
$lang['REG_FORM_SMS_TXT2'] ='Vælg din landekode, og indtast dit mobilnummer for at modtage en SMS besked 15 minutter før webinaret starter';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Du mangler at vælge dato';
$lang['REG_FORM_ALERT_TIME'] ='Du mangler at vælge tidspunkt';
$lang['REG_FORM_ALERT_NAME'] ='Skriv venligst dit navn';
$lang['REG_FORM_ALERT_EMAIL'] ='Skriv venligst din e-mail';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Skriv en gyldig e-mailadresse';
$lang['REG_FORM_TXTBOX_NAME'] ='Skriv dit navn her ...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Skriv din e-mail her ...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Din lokaltid (Tids Zone)';
$lang['REG_FORM_TXT_SELDATE'] ='Vælg dato';
$lang['REG_FORM_TXT_SELTIME'] ='Vælg først dato';
$lang['REG_FORM_TXT_SELTIME_2'] ='Vælg tidspunkt';
$lang['REG_FORM_TXT_SELECTDATE'] ='Vælg dato';
$lang['REG_FORM_TXT_SELECTTIME'] ='Vælg tidspunkt';
$lang['REG_FORM_TXT_RN_1'] = 'Se gårsdagens webinar replay NU!';
$lang['REG_FORM_TXT_RN_2'] = 'Lige nu!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Webinar tilmelding';
$lang['REG_FORM_H2'] ='Reservér din plads!';
$lang['REG_FORM_H3'] ='TILMELD DIG WEBINARET';
$lang['REG_FORM_SMS_TXT1'] ='Jeg vil gerne modtage en SMS besked, inden webinaret starter';
$lang['REG_FORM_SMS_TXT2'] ='(Valgfrit - men anbefales) Vælg din landekode, og indtast dit mobilnummer for at modtage en SMS besked ca. 15 minutter før webinaret starter';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Reservér din plads!';
$lang['REG_FORM_H2'] ='Webinar tilmelding';
$lang['REG_FORM_TXT_SELECTDATE'] = 'Hvilken dag ønsker du at deltage?';
$lang['REG_FORM_TXT_SELECTTIME'] = 'Hvilket tidspunkt passer dig bedst?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Send invitationen til';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Beskeder';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTRER NU!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinar præsentation';
$lang['PBOX_TXT_TOPIC'] ='Emne';
$lang['PBOX_TXT_HOST'] ='Vært';
$lang['PBOX_TXT_PRESENTER'] ='Taler';
$lang['PBOX_TXT_DATEANDTIME'] ='Dato og tidspunkt';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='I GANG!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Præsenteret af';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinar detaljer';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Taler';
$lang['TY_SCHEDDATE'] ='Planlagt dato';
$lang['TY_SCHEDTIME'] ='Planlagt tidspunkt';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Dit unikke webinar link';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Føj til din iCal kalender';
$lang['TY_TXT_GOOGLE_CAL'] = 'Føj til din Google kalender';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Føj til din Outlook kalender';
$lang['TY_TXT_CLICK_VER'] = 'Klik på Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Fortæl dine venner om dette webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='OG FÅ ADGANG TIL DIN GRATIS BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Vælg hvordan du vil anbefale';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Send e-mail til dine venner';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Skriv dit navn';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Skriv din e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Din vens navn';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Din vens e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Her er e-mailen, der bliver sendt til dine venner';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Emnelinje';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='E-mail besked';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Send Email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Post denne besked på Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verificér Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post på Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Bekræft URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Indtast URL\'en på din Facebook side herunder, når du har slået beskeden op på Facebook. Du får adgang til din skjulte bonus, så snart du har verificeret!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweet dette på Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verificér Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Indtast URL\'en på din Twitter side herunder, når du har tweetet beskeden. Du får adgang til din skjulte bonus, så snart du har verificeret!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet dette indlæg';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Tillykke';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='DU HAR NU ADGANG TIL DIN BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='GRATIS DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Højreklik på linket herunder og vælg "Gem som ..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klik her';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Navn er ikke udfyldt!!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Ugyldig e-mailadresse!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL ikke verificeret!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinaret starter om et øjeblik ...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Taler';
$lang['COUNTDOWN_TXT_DATE'] ='Planlagt dato';
$lang['COUNTDOWN_TXT_TIME'] ='Scheduled time';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Opretter forbindelse, vent et øjeblik ...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Udfyld dit navn og din e-mail, når du sender dit spørgsmål, så vi kan svare dig på e-mail, hvis vi ikke har mulighed for at gøre det under webinaret.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Dit navn (skal udfyldes)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Din e-mail (skal udfyldes)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Skriv dit spørgsmål';
$lang['LIVE_CBOX2_TXT_1'] ='Chat boks';
$lang['LIVE_CBOX2_TXT_2'] ='Skriv til';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Alle';
$lang['LIVE_CBOX2_TXT_3'] ='Besked';
$lang['LIVE_CBOX2_TXT_4'] ='Send dine spørgsmål';
$lang['LIVE_CBOX2_TXT_5'] ='Indtast dit navn og e-mail, så vi kan sende dig vores svar';
$lang['LIVE_CBOX2_TXT_6'] ='Give alle felter fuldstændigt';
$lang['LIVE_CBOX2_TXT_7'] ='Beskeden er sendt!';
$lang['LIVE_CBOX2_TXT_8'] ='undskyld, besked allerede sendt';
$lang['LIVE_CBOX2_SUBMIT'] ='Send besked';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Registrerede deltagere';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Replay deltagere';
$lang['LIVE_ATTENDEELIST_HOST'] ='Vært';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTAT';
$lang['LIVE_POLL_TXT2'] ='Tak for din stemme';
$lang['LIVE_POLL_TXT3'] ='Meningsmålingen endelige resultater vil foreligge meget snart ...';

$lang['LIVE2_H1'] ='WEBINAR APP ER STARTET';
$lang['LIVE2_TXT1'] ='Velkommen';
$lang['LIVE2_TXT2'] ='Indstillinger';
$lang['LIVE2_TXT3'] ='Log på';
$lang['LIVE2_TXT4'] ='Log ud';
$lang['LIVE2_TXT5'] ='Præsentation';
$lang['LIVE2_TXT6'] ='Medlemmer';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Action Events';
$lang['LIVE2_TXT9'] = 'Indtast dine spørgsmål eller kommentarer';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='I GANG';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Spærret af admin';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Er du sikker på, du vil forlade webinaret? Du kan muligvis ikke få adgang til webinaret igen!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'januar';
$langmonths['february'] = 'februar';
$langmonths['march'] = 'marts';
$langmonths['april'] = 'april';
$langmonths['may'] = 'maj';
$langmonths['june'] = 'juni';
$langmonths['july'] = 'juli';
$langmonths['august'] = 'august';
$langmonths['september'] = 'september';
$langmonths['october'] = 'oktober';
$langmonths['november'] = 'november';
$langmonths['december'] = 'december';

$langdays = array();
$langdays['monday'] = 'Mandag';
$langdays['tuesday'] = 'tirsdag';
$langdays['wednesday'] = 'Onsdag';
$langdays['thursday'] = 'Torsdag';
$langdays['friday'] = 'Fredag';
$langdays['saturday'] = 'lørdag';
$langdays['sunday'] = 'søndag';



?>