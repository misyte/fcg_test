<?php
# -----------------------------------------------
# LANGUAGE: GREEK                     UPDATE: 1
# -----------------------------------------------

$lang = array();



/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ="Εγγραφή για το ουέμπιναρ (Webinar)";
//presentation-box
$lang['REG_PBOX_H1'] ='Ηλεκτρονική Παρουσίαση';
$lang['REG_PBOX_TOPIC'] ='Θέμα';
$lang['REG_PBOX_PRESENTER'] ='Παρουσιαστής';
$lang['REG_PBOX_HOST'] ='Οικοδεσπότης';
$lang['REG_PBOX_DATETIME'] ='Ημερομηνία και ώρα';
$lang['REG_PBOX_DATETIME_VAL'] ='Διάλεξε από το μενού επιλογών';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Κωδικό τής Χώρας σας γιά το κινητό';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Συμπληρώσετε το κινητό σας';
$lang['REG_FORM_SMS_TXT1'] ='Ειδοποίηση με SMS: Ναι στείλε μου μία ειδοποίηση λίγο πριν ξεκινήσει το webinar (προαιρετικό αλλά συστήνεται)';
$lang['REG_FORM_SMS_TXT2'] ='Διάλεξε Τον Διεθνή Κωδικό Χώρας σου και Βάλε τον Αριθμό σου για να Λάβεις ένα Γραπτό Μήνυμα Υπενθύμισης SMS 15 λεπτά πριν την έναρξη του Webinar';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Παρακαλούμε να συμπληρώσετε την ημερομηνία';
$lang['REG_FORM_ALERT_TIME'] ='ούμε να συμπληρώσετε την ώρα';
$lang['REG_FORM_ALERT_NAME'] ='Παρακαλούμε να συμπληρώσετε το όνομα σας';
$lang['REG_FORM_ALERT_EMAIL'] ='Παρακαλούμε να συμπληρώσετε το έμαιλ (email) σας';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Συμπληρώσετε ένα έγκυρο έμαιλ (email)';
$lang['REG_FORM_TXTBOX_NAME'] ='Συμπληρώσετε το όνομα σας εδώ...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Συμπληρώσετε το έμαιλ (email) σας εδώ...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='τοπική ώρα σας';
$lang['REG_FORM_TXT_SELDATE'] ='Διαλέξετε την ημερομηνία σας';
$lang['REG_FORM_TXT_SELTIME'] ='Πρώτα διαλέξετε την ημερομηνία';
$lang['REG_FORM_TXT_SELTIME_2'] ='Διαλέξετε την ώρα σας';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Διαλέξετε την ημερομηνία σας';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Διαλέξετε την ώρα σας';
$lang['REG_FORM_TXT_RN_1'] = 'Παρακολουθήστε επανάληψη webinar χθεσινή ΤΩΡΑ!';
$lang['REG_FORM_TXT_RN_2'] = 'τώρα!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {

$lang['REG_FORM_H1'] ='Ουέμπιναρ (Webinar) Εγγραφή';
$lang['REG_FORM_H2'] ='Κρατίσετε την θέση σας';
$lang['REG_FORM_H3'] ='Εγγραφή για το ουέμπιναρ (Webinar)';
$lang['REG_FORM_SMS_TXT1'] ='Θέλω να λάβω ενα ΣΜΣ (SMS) μήνυμα πριν αρχινίσει το ουέμπιναρ (Webinar)';
$lang['REG_FORM_SMS_TXT2'] ='(Προαιρετικό αλλά συστήνεται ιδιαίτερα) Διάλεξε τον διεθνή κωδικό χώρας και βάλε τον αριθμό σου για να λάβεις μία υπενθύμιση 15 λεπτά πριν ξεκινήσει το webinar';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='ΚΑΤΟΧΥΡΩΣΕ ΤΗΝ ΘΕΣΗ ΣΟΥ!';
$lang['REG_FORM_H2'] ='ΕΓΓΡΑΦΗ ΣΤΟ WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] ='Ποια Μέρα Θέλεις να Παρακολουθήσεις;';
$lang['REG_FORM_TXT_SELECTTIME'] ='Ποια Ώρα σου Ταιριάζει Καλύτερα;';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Που να Στείλουμε την Πρόσκληση';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Ειδοποιήσεις';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='Εγγραφείτε τώρα!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Ηλεκτρονική Παρουσίαση';
$lang['PBOX_TXT_TOPIC'] ='Θέμα';
$lang['PBOX_TXT_HOST'] ='Οικοδεσπότης';
$lang['PBOX_TXT_PRESENTER'] ='Παρουσιαστής';
$lang['PBOX_TXT_DATEANDTIME'] ='Ημερομηνία και ώρα';
$lang['PBOX_TXT_STATUS'] ='ΚΑΤΑΣΤΑΣΗ';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Παρουσιάζεται από';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Λεπτομέρειες του σεμιναρίου';
$lang['TY_WEBINAR'] ='Ηλεκτρονικό Σεμινάριο';
$lang['TY_PRESENTER'] ='Παρουσιαστής';
$lang['TY_SCHEDDATE'] ='Προγραμματισμένη ημερομηνία';
$lang['TY_SCHEDTIME'] ='Προγραμματισμένη ώρα';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Ο δικός σου προσωπικός σύνδεσμος για το webinar';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Πρόσθεσέ το στο ημερολόγιο ical';
$lang['TY_TXT_GOOGLE_CAL'] = 'Πρόσθεσέ το στο ημερολόγιο ical';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Πρόσθεσέ το στο Google Calendar';
$lang['TY_TXT_CLICK_VER'] = 'Κάντε κλικ στο Version';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Ενημέρωσε τους φίλους σου για αυτό το webinar';
$lang['TY_TXT_FREEBONUS_H2'] ='ΞΕΚΛΕΙΔΩΣΕ ΤΟ ΔΩΡΕΑΝ BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Ενημέρωσε τους φίλους σου για αυτό το webinar';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Στείλε Email στους φίλους σου';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Συμπλήρωσε το όνομα σου';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Συμπλήρωσε το email σου';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Το όνομα του φίλου σου';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Το mail του φίλου σου';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Αυτό είναι το email που θα σταλεί στους φίλους σου';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Γραμμή Θέματος';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Κείμενο Email';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Στείλτε e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Δημοσίευση αυτό το μήνυμα στο Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Επαλήθευση Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Δημοσίευση στο Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Βεβαιωθείτε URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Μόλις δημοσιεύσεις το μήνυμα στο Facebook, συμπλήρωσε τον σύνδεσμο της σελίδας σου στο Facebook για να επαληθευτεί η δημοσίευσή του. Μετά την επαλήθευση το bonus σου θα ελευθερωθεί αμέσως!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Μήνυμα στο Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Επαλήθευση Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Μόλις δημοσιεύσεις το μήνυμα στο Twitter, συμπλήρωσε τον σύνδεσμο του προσωπικού σου Twitter για να επαληθευτεί η δημοσίευσή του. Μετά την επαλήθευση το bonus σου θα ελευθερωθεί αμέσως!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Αυτό το μήνυμα';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Συγχαρητήρια';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ΕΛΕΥΘΕΡΩΣΑΤΕ ΤΟ BONUS ΣΑΣ!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='ΔΩΡΕΑΝ DOWNLOAD';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Δεξί κλικ στον παρακάτω σύνδεσμο και επιλέξτε "Αποθήκευση ως…"';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Κάντε κλικ εδώ';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Το Όνομα είναι κενό';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Μη έγκυρη διεύθυνση Email!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='Δεν επαληθεύτηκε ο σύνδεσμος!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Το Webinar ξεκινάει σε λίγο….';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Παρουσιαστής';
$lang['COUNTDOWN_TXT_DATE'] ='Προγραμματισμένη ημερομηνία';
$lang['COUNTDOWN_TXT_TIME'] ='Προγραμματισμένη ώρα';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Συνδέεσαι με το webinar, παρακαλώ περίμενε…';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Μαζί με την ερώτησή σου, γράψε και το όνομα σου καθώς και το email σου για να μπορέσουμε να σου απαντήσουμε αν δεν τα καταφέρουμε κατά την διάρκεια του webinar για οποιονδήποτε λόγο';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Το Όνομα σου (απαραίτητο)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Το Email σου (απαραίτητο)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Γράψε την ερώτησή σου';
$lang['LIVE_CBOX2_TXT_1'] ='Κιβώτιο Συνομιλίας';
$lang['LIVE_CBOX2_TXT_2'] ='Γράψε προς';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Όλους';
$lang['LIVE_CBOX2_TXT_3'] ='Μήνυμα';
$lang['LIVE_CBOX2_TXT_4'] ='Στείλτε τις ερωτήσεις σας';
$lang['LIVE_CBOX2_TXT_5'] ='Πληκτρολογήστε το όνομα και το email σας ώστε να μπορέσουμε να σας στείλουμε την απάντησή μας';
$lang['LIVE_CBOX2_TXT_6'] ='παρέχει όλα τα πεδία εντελώς';
$lang['LIVE_CBOX2_TXT_7'] ='Μήνυμα εστάλη!';
$lang['LIVE_CBOX2_TXT_8'] ='Λυπούμαστε, ήδη στείλει μήνυμα';


$lang['LIVE_CBOX2_SUBMIT'] ='Στείλε μήνυμα';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Εγγεγραμμένοι συμμετέχοντες';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Απάντηση συμμετέχοντες';
$lang['LIVE_ATTENDEELIST_HOST'] ='Οικοδεσπότης';
//POLL
$lang['LIVE_POLL_TXT1'] ='ΑΠΟΤΕΛΕΣΜΑ';
$lang['LIVE_POLL_TXT2'] ='Σας ευχαριστούμε για την ψήφο σας';
$lang['LIVE_POLL_TXT3'] ='Τα τελικά αποτελέσματα δημοσκόπησης θα είναι διαθέσιμο σύντομα ...';

$lang['LIVE2_H1'] ='Η ΕΦΑΡΜΟΓΗ ΤΟΥ WEBINAR ΞΕΚΙΝΗΣΕ';
$lang['LIVE2_TXT1'] ='Καλωσήλθες';
$lang['LIVE2_TXT2'] ='Ρυθμίσεις';
$lang['LIVE2_TXT3'] ='Σύνδεση';
$lang['LIVE2_TXT4'] ='Αποσύνδεση';
$lang['LIVE2_TXT5'] ='Παρουσίαση';
$lang['LIVE2_TXT6'] ='Μέλη';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Εκδηλώσεις Webinar δράσης';
$lang['LIVE2_TXT9'] = 'Εισάγετε τις ερωτήσεις ή τα σχόλιά σας';
$lang['LIVE3_TXT1'] ='ΚΑΤΑΣΤΑΣΗ';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='Webinar ID';
$lang['LIVE3_POPUP1'] ='Οι επιλογές απενεργοποιήθηκαν από τον διαχειριστή του webinar';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Είσαι σίγουρος ότι θέλεις να φύγεις; Δεν θα μπορείς να ξανασυνδεθείς στο δωμάτιο του webinar!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'Ιανουάριος';
$langmonths['february'] = 'Φεβρουάριος';
$langmonths['march'] = 'Μάρτιος';
$langmonths['april'] = 'Απρίλιος';
$langmonths['may'] = 'Μάιος';
$langmonths['june'] = 'Ιούνιος';
$langmonths['july'] = 'Ιούλιος';
$langmonths['august'] = 'Αύγουστος';
$langmonths['september'] = 'Σεπτέμβριος';
$langmonths['october'] = 'Οκτώβριος';
$langmonths['november'] = 'Νοέμβριος';
$langmonths['december'] = 'Δεκέμβριος';

$langdays = array();
$langdays['monday'] = 'Δευτέρα';
$langdays['tuesday'] = 'Τρίτη';
$langdays['wednesday'] = 'Τετάρτη';
$langdays['thursday'] = 'Πέμπτη';
$langdays['friday'] = 'Παρασκευή';
$langdays['saturday'] = 'Σάββατο';
$langdays['sunday'] = 'Κυριακή';

