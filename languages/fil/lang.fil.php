<?php
#------------------
# Language: FILIPINO
#------------------


$lang = array();
$lang['REG_LOADING'] = 'Kasalukuyang Nag-rerehistro Sa Webinar';

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

if($tpl == 0 || $tpl == 1) { // software or squeeze
$lang['REG_ARROW'] = 'register_arrow.png';
$lang['REG_FORM_H1'] = 'Webinar Registration ';
$lang['REG_FORM_H2'] = 'Reserve your Spot! ';

$lang['REG_FORM_TXT_SELDATE'] = 'Pumili Ng Petsa';
$lang['REG_FORM_TXT_SELTIME'] = 'Pumili Muna ng Petsa';
$lang['REG_FORM_TXT_SELTIME_2'] ='Pumili ng Oras';
$lang['REG_FORM_ALERT_DATE'] = 'Paki Lagyan Ng Petsa';
$lang['REG_FORM_ALERT_TIME'] = 'Paki Lagyan Ng Oras';
$lang['REG_FORM_ALERT_NAME'] = 'Paki Lagyan Ng Name o Pangalan';
$lang['REG_FORM_ALERT_EMAIL'] = 'Paki Lagyan Ng Email Address';
$lang['REG_FORM_ALERT_EMAIL_2'] = 'Paki Lagay Ang Tamang Email Address Lang Po';
$lang['REG_FORM_SMS_SELCODE'] = 'Philippine Country Code Na Ginagamit Sa Cell Phone o Telephone';
$lang['REG_FORM_SMS_INPUTPHONE'] = 'Ilagay Ang Phone Number Dito';

$lang['REG_FORM_TXT_LOCALTIME'] = 'Ang Iyong Local Na Oras:';
$lang['REG_FORM_TXT_SELECTDATE'] = 'Pumili Ng Petsa';
$lang['REG_FORM_TXT_SELECTDATE_DEFAULT'] = 'Pumili Ng Petsa';

$lang['REG_FORM_TXT_SELECTTIME'] = 'Pumili Ng Oras';
$lang['REG_FORM_H3'] = 'MAG-REGISTER SA WEBINAR';
$lang['REG_FORM_TXTBOX_NAME'] = 'Ilagay Ang Iyong Name o Pangalan Dito';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Ilagay Ang Iyong Email Address Dito';
$lang['REG_FORM_SMS_TXT1'] = 'Gusto Kong Maka-Receive o Tangap Ng SMS Text Para I-remind Ako Bago Pa Mag-simula';
$lang['REG_FORM_SMS_TXT2'] = '(Optional but highly recommended)<br>Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>
Reminder 15 Minutes Before Webinar Starts ';
$lang['REG_FORM_BUTTON'] = 'register_button.png';

//presentation-box
$lang['REG_PBOX_H1'] = 'Webinar Presentation ';
$lang['REG_PBOX_TOPIC'] = 'Topic :';
$lang['REG_PBOX_PRESENTER'] = 'Presenter :';
$lang['REG_PBOX_HOST'] = 'Host :';
$lang['REG_PBOX_DATETIME'] = 'Date and Time :';
$lang['REG_PBOX_DATETIME_VAL'] = 'Select from menu ';



}
else if($tpl == 3) {  //vibrant
$lang['REG_FORM_IMG_TOP'] = 'reservetop.jpg';
$lang['REG_FORM_TXT_LOCALTIME'] = 'Your Local Time: ';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend? ';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You? ';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = '4. Notifications';
$lang['REG_FORM_SMS_TXT1'] = 'SMS Text Alert: Yes, send me a SMS reminder before the webinar starts (optional but recommended).';
$lang['REG_FORM_SMS_TXT2'] = 'Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>Reminder 15 Minutes Before Webinar Starts';
$lang['REG_FORM_BUTTON'] = 'registernow.jpg';
}
else if($tpl == 'stylish') {
$lang['REG_FORM_IMG_TOP'] = 'optin-blue.jpg';
$lang['REG_FORM_TXT_LOCALTIME'] = 'Your Local Time:';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = '4. Notifications';
$lang['REG_FORM_SMS_TXT1'] = 'SMS Text Alert: Yes, send me a SMS reminder before the webinar starts (optional but recommended).';
$lang['REG_FORM_SMS_TXT2'] = 'Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>Reminder 15 Minutes Before Webinar Starts';
$lang['REG_FORM_BUTTON'] = 'registernow.png';
}
else if($tpl == 'panorama') {
$lang['REG_FORM_H1'] = 'RESERVE YOUR SPOT! WEBINAR REGISTRATION';
$lang['REG_FORM_TXT_LOCALTIME'] = 'Your Local Time:';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = 'Notifications';
$lang['REG_FORM_SMS_TXT1'] = 'SMS Text Alert: Yes, send me a SMS reminder before the webinar starts (optional but recommended).';
$lang['REG_FORM_SMS_TXT2'] = 'Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>Reminder 15 Minutes Before Webinar Starts';
$lang['REG_FORM_BUTTON'] = 'registerNow.png';
}

else if($tpl == 'facebook' || $tpl == 'timeline') {
$lang['REG_FORM_H1'] = 'Reserve your Spot!';
$lang['REG_FORM_H2'] = 'Webinar Registration';
$lang['REG_FORM_TXT_LOCALTIME'] = 'Your Local Time:';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = 'Notifications';
$lang['REG_FORM_BUTTON_TXT'] = 'REGISTER NOW!';
}

else if($tpl == 'clasic') {
$lang['REG_ARROW'] = 'arrow.png';
$lang['REG_FORM_IMG_TOP'] = 'regtop2.jpg';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = 'Notifications';
$lang['REG_FORM_BUTTON'] = 'registernow.jpg';
}

else if($tpl == 'etherial') {
$lang['REG_ARROW'] = 'progressionbar.png';
$lang['REG_FORM_IMG_TOP'] = 'reservetop.jpg';
$lang['REG_FORM_TXT_LOCALTIME'] = 'Your Local Time:';
$lang['REG_FORM_TXT_SELECTDATE'] = '1. Which Day Do You Want to Attend?';
$lang['REG_FORM_TXT_SELECTTIME'] = '2. Which Time Works Best For You?';
$lang['REG_FORM_TXT_SENDINVITATION'] = '3. Where To Send The Invitation:';
$lang['REG_FORM_TXTBOX_NAME'] = 'Enter Your Name Here...';
$lang['REG_FORM_TXTBOX_EMAIL'] = 'Enter Your Email Here...';
$lang['REG_FORM_TXT_NOTIFICATION'] = '4. Notifications';
$lang['REG_FORM_SMS_TXT1'] = 'SMS Text Alert: Yes, send me a SMS reminder before the webinar starts (optional but recommended).';
$lang['REG_FORM_SMS_TXT2'] = 'Select Your Country Code and<br>Enter Your Mobile Phone Number To Receive a Text Alert<br>Reminder 15 Minutes Before Webinar Starts';
$lang['REG_FORM_BUTTON'] = 'registernow.jpg';
}


/**  PRESENTER-BOX PRE-DEFINED VARIABLES **/

$lang['PBOX_H1'] = 'Webinar Presentation';
$lang['PBOX_TXT_TOPIC'] = 'Topic:';
$lang['PBOX_TXT_HOST'] = 'Host:';
$lang['PBOX_TXT_PRESENTER'] = 'Presenter:';
$lang['PBOX_TXT_DATEANDTIME'] = 'Date and Time:';
$lang['PBOX_TXT_STATUS'] = 'Status:';
$lang['PBOX_TXT_STATUS_VAL'] = 'Broadcasting ON!';


/**  THANKYOU PAGE PRE-DEFINED VARIABLES **/
//header part
$lang['TY_PRESENTEDBY'] = 'Presented By :';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] = 'Webinar Details :';
$lang['TY_WEBINAR'] = 'Webinar :';
$lang['TY_PRESENTER'] = 'Presenter :';
$lang['TY_SCHEDDATE'] = 'Scheduled date :';
$lang['TY_SCHEDTIME'] = 'Scheduled time :';
$lang['TY_IMG_PRINT'] = 'print.png';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] = 'Your unique webinar link :';
//calendar details under weinar link
$lang['TY_IMG_SCHEDULE_YOUR_WEBINAR'] = '3.gif';
$lang['TY_IMG_SHARE_THIS_WEBINAR'] = '4.gif';
$lang['TY_IMG_I_CAL'] = 'ical.jpg';
$lang['TY_IMG_GOOGLE_CAL'] = 'googleCal.jpg';
$lang['TY_IMG_OUTLOOK_CAL'] = 'outlookCal.jpg';
$lang['TY_IMG_LIKEUS_ONFACEBOOK'] = 'facebook.jpg';
//Free bonus section
$lang['TY_IMG_DOWNLOAD_OUR_FREEBONUS'] = 'refer.gif';
$lang['TY_TXT_FREEBONUS_H1'] = 'Tell your friends about this webinar ';
$lang['TY_TXT_FREEBONUS_H2'] = 'AND UNLOCK YOUR FREE BONUS! ';
$lang['TY_TXT_FREEBONUS_STEP1'] = 'Select Your Referring Method';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] = 'Send Email To Your Friends';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] = 'Enter your name:';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] = 'Enter your email:';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] = 'Your friends\' name:';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] = 'Your friends\' email:';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] = 'This is the email that will be sent to your friends:';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] = 'Subject line:';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] = 'Email body:';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] = 'Post This Message On Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] = 'Verify Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] = 'Once you\'ve posted the message on Facebook, enter your Facebook page URL below to verify the post. Upon verification, your hidden bonus will be unlocked right away!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] = 'Tweet This On Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] = 'Verify Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] = 'Once you\'ve twitted the message on Twitter, enter your Twitter page URL below to verify the twit. Upon verification, your hidden bonus will be unlocked right away!';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] = 'Congratulations';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] = 'YOU HAVE UNLOCKED YOUR BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] = 'FREE DOWNLOAD:';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] = 'Right click on link below and select "save as...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] = 'Click here';



/** countdown variables **/
$lang['COUNTDOWN_TXT_H1'] = 'The Webinar Will Begin Shortly...';
$lang['COUNTDOWN_IMG_WEEKS'] = 'webinar-files/dark_weeks_dash.png';
$lang['COUNTDOWN_IMG_DAYS'] = 'webinar-files/dark_days_dash.png';
$lang['COUNTDOWN_IMG_HOURS'] = 'webinar-files/dark_hours_dash.png';
$lang['COUNTDOWN_IMG_MINUTES'] = 'webinar-files/dark_minutes_dash.png';
$lang['COUNTDOWN_IMG_SECONDS'] = 'webinar-files/dark_seconds_dash.png';

$lang['COUNTDOWN_TXT_PRESENTER'] = 'Presenter:';
$lang['COUNTDOWN_TXT_DATE'] = 'Scheduled date:';
$lang['COUNTDOWN_TXT_TIME'] = 'Scheduled time:';



/** webinar live **/

$lang['LIVE_TXT_LOADING'] = 'Connecting to a webinar, please wait...';
//CBOX
$lang['LIVE_CBOX1_TXT'] = 'While sending your question, also enter your name and email so we can answer by email in case we cannot do it during the webinar for any reason.';
$lang['LIVE_CBOX1_TXT_INPUT1'] = 'Your Name (Required)';
$lang['LIVE_CBOX1_TXT_INPUT2'] = 'Your Email (Required)';
$lang['LIVE_CBOX1_TXT_INPUT3'] = 'Type Your Question';
$lang['LIVE_CBOX1_IMG_BUTTON'] = 'broadcast/images/question.png';
$lang['LIVE_CBOX2_TXT_1'] = 'Chat Box';
$lang['LIVE_CBOX2_TXT_2'] = 'Write to:';
$lang['LIVE_CBOX2_TXT_2_INPUT'] = 'All';
$lang['LIVE_CBOX2_TXT_3'] = 'Mesage:';
$lang['LIVE_CBOX2_SUBMIT'] = 'Send Message';

$lang['LIVE_CBOX3_TXT_1'] = 'Chat Box';
$lang['LIVE_CBOX3_TXT_2'] = 'Write to:';
$lang['LIVE_CBOX3_TXT_2_INPUT'] = 'EBS PRESENTER';
$lang['LIVE_CBOX3_TXT_3'] = 'Mesage:';
$lang['LIVE_CBOX3_SUBMIT'] = 'Send Message';


//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] = 'Registered Participants:';
$lang['LIVE_ATTENDEELIST_HOST'] = 'Host:';
//POLL
$lang['LIVE_POLL_TXT_1'] = 'RESULT:';
$lang['LIVE_POLL_TXT_2'] = 'RESULT:';


$lang['LIVE2_H1'] = 'WEBINAR APP STARTED';
$lang['LIVE2_TXT1'] = 'Welcome';
$lang['LIVE2_TXT2'] = 'Settings';
$lang['LIVE2_TXT3'] = 'Sign In';
$lang['LIVE2_TXT4'] = 'Sign out';
$lang['LIVE2_TXT5'] = 'Presentation [ EBS PRESENTER ]';
$lang['LIVE2_TXT6'] = 'Members';
$lang['LIVE2_TXT7'] = 'EBS PRESENTER';
$lang['LIVE2_IMG1'] = 'broadcast/broadcast2/images/icon-presenter-sml2.png';


$lang['LIVE3_IMG1'] = 'http://www.evergreenbusinesssystem.com/members/webinar2/preview/broadcast/broadcast3/images/left-top.png';
$lang['LIVE3_IMG2'] = 'http://www.evergreenbusinesssystem.com/members/webinar2/preview/broadcast/broadcast3/images/right-top.png';
$lang['LIVE3_IMG3'] = 'http://www.evergreenbusinesssystem.com/members/webinar2/preview/broadcast/broadcast3/images/right-bottom.jpg';
$lang['LIVE3_TXT1'] = 'STATUS';
$lang['LIVE3_TXT2'] = 'ON';
$lang['LIVE3_TXT2'] = 'Webinar ID';
$lang['LIVE3_POPUP1'] = 'Options have been turned off by webinar admin';
//exit webinar
$lang['LIVE_EXIT_TXT'] = 'Are you sure you want to leave? you might not be able to log back into the room!';

?>