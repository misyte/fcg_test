<?php
# -----------------------------------------------
# LANGUAGE: AFRIKAANS                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='Registreer vir \'n webinaar';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinaar Registrasie';
$lang['REG_PBOX_TOPIC'] ='Onderwerp';
$lang['REG_PBOX_PRESENTER'] ='Aanbieder';
$lang['REG_PBOX_HOST'] ='Gasheer';
$lang['REG_PBOX_DATETIME'] ='Datum en Tyd';
$lang['REG_PBOX_DATETIME_VAL'] ='Kies uit die Spyskaart';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Landkode vir Mobiele foon';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Vul jou Telefoonnommer Hier In...';
$lang['REG_FORM_SMS_TXT1'] ='SMS Teksboodskap: Ja, stuur vir my \'n SMS boodskap voordat die webinaar begin (opsioneel, maar aan te beveel).';
$lang['REG_FORM_SMS_TXT2'] ='Kies Jou Landskode en Vul Jou Selfoonnommer In Om \'n Teksboodskap 15 Minute Voor Die Webinaar Begin Te Ontvang';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Voltooi asseblief die datumveld';
$lang['REG_FORM_ALERT_TIME'] ='Voltooi asseblief die tydveld';
$lang['REG_FORM_ALERT_NAME'] ='Voltooi asseblief die naamveld';
$lang['REG_FORM_ALERT_EMAIL'] ='Verstrek asseblief die e-posveld';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Vul asseblief \'n geldige e-posadres in';
$lang['REG_FORM_TXTBOX_NAME'] ='Vul Jou Naam Hier In ...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Vul Jou E-pos Hier In ...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Jou Plaaslike Tyd';
$lang['REG_FORM_TXT_SELDATE'] ='Kies jou gewensde datum';
$lang['REG_FORM_TXT_SELTIME'] ='Kies eers jou datum';
$lang['REG_FORM_TXT_SELTIME_2'] ='Kies jou gewensde tyd';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Kies Jou Datum';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Kies Jou Tyd';
$lang['REG_FORM_TXT_RN_1'] = 'Kyk na webinar herhaling NOU!';
$lang['REG_FORM_TXT_RN_2'] = 'nou';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1 || $rp_regtpl == 0 || $rp_regtpl == 1) {
$lang['REG_FORM_H1'] ='Webinaar Registrasie';
$lang['REG_FORM_H2'] ='Reserveer jou Plek!!';
$lang['REG_FORM_H3'] ='REGISTREER VIR DIE WEBINAAR';
$lang['REG_FORM_SMS_TXT1'] ='Ek Wil Graag \'n SMS Teksboodskap Ontvang Voordat Die Webinaar Begin';
$lang['REG_FORM_SMS_TXT2'] ='(Opsioneel, maar sterk aanbeveel) Kies Jou Landskode en Vul Jou Selfoonnommer In Om \'n Teksboodskap 15 Minute Voor Die Webinaar Begin Te Ontvang';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline' || $rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8 ) {
$lang['REG_FORM_H1'] ='Reserveer jou Plek!';
$lang['REG_FORM_H2'] ='Webinaar Registrasie';
$lang['REG_FORM_TXT_SELECTDATE'] ='Watter Dag Wil Jy Bywoon?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Watter Tyd Sal Die Beste Pas?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Waarheen Om Die Uitnodiging Te Stuur';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Kennisgewings';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISTREER NOU!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinaaraanbieding';
$lang['PBOX_TXT_TOPIC'] ='Onderwerp';
$lang['PBOX_TXT_HOST'] ='Gasheer';
$lang['PBOX_TXT_PRESENTER'] ='Aanbieder';
$lang['PBOX_TXT_DATEANDTIME'] ='Datum en Tyd';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='AAN!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Aangebied deur';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinaarbesonderhede';
$lang['TY_WEBINAR'] ='Webinaar';
$lang['TY_PRESENTER'] ='Aanbieder';
$lang['TY_SCHEDDATE'] ='Geskeduleerde datum';
$lang['TY_SCHEDTIME'] ='Geskeduleerde tyd';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Jou unieke webinaarskakel';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Voeg By Jou iCal Kalender';
$lang['TY_TXT_GOOGLE_CAL'] = 'Voeg By Jou Google Kalender';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Voeg By Jou Outlook Kalender';
$lang['TY_TXT_CLICK_VER'] = 'Kliek op Weergawe';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Vertel jou vriende van hierdie webinaar';
$lang['TY_TXT_FREEBONUS_H2'] ='EN ONTSLUIT JOU GRATIS BONUS!!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Kies Jou Verwysingsmetode';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Stuur E-pos Aan Jou Vriende';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Vul jou naam im';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Vul jou e-pos in';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Jou vriende se name';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Jou vriende se e-posse';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Hierdie is die e-pos wat aan jou vriende gestuur sal wordOndwer';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Onderwerplyn';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='E-pos hoofboodskap';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Stuur e-pos!
';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Pos Hierdie Boodskap op Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Verifieer Facebook';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Post op facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'verifieer URL';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Wanneer jy die boodskap op Facebook gepos het, vul jou Facebook bladsy URL hieronder in om die pos te verifieer. By verifikasie sal jou verborge bonus dadelik ontsluit word!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Twiet Dit Op Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Verifieer Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Wanneer jy die boodskap op Twitter getwiet het, vul jou Twitter bladsy URL hieronder in om die twiet te verifieer. By verifikasie sal jou verborge bonus dadelik ontsluit word!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet hierdie boodskap';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Geluk';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='JY JET JOU BONUS ONTSLUIT!!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='GRATIS AFLAAI';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Regs-klik op die skakel hieronder en kies "Berg as ...""';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='kliek hier';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='naam is leeg!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Ongeldige E-posadres!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL nie geverifieer nie!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Die Webinaar begin binnekort ...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Aanbieder';
$lang['COUNTDOWN_TXT_DATE'] ='Geskeduleerde datum';
$lang['COUNTDOWN_TXT_TIME'] ='Geskeduleerde tyd';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Konnekteer met \'n webinaar, wag asseblief ...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Wanneer jy jou vraag stuur, vul ook jou naam en e-pos in sodat ons per e-pos kan antwoord as ons vir een of ander rede dit nie gedurende die webinaar kan doen nie.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Jou Naam (Vereiste)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Jou E-pos (Vereiste)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Tik Jou Vraag';
$lang['LIVE_CBOX2_TXT_1'] ='Gesprekskassie';
$lang['LIVE_CBOX2_TXT_2'] ='Skryf aan';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Almal';
$lang['LIVE_CBOX2_TXT_3'] ='Boodskap';
$lang['LIVE_CBOX2_TXT_4'] ='Stuur jou vrae';
$lang['LIVE_CBOX2_TXT_5'] ='Tik jou naam en e-pos sodat ons u kan stuur ons antwoord';
$lang['LIVE_CBOX2_TXT_6'] ='Verskaf al die velde heeltemal';
$lang['LIVE_CBOX2_TXT_7'] ='Boodskap gestuur!';
$lang['LIVE_CBOX2_TXT_8'] ='Jammer, Boodskap al gestuur';


$lang['LIVE_CBOX2_SUBMIT'] ='Stuur Boodskap';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Geregistreerde Deelnemers';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Herspeel Webinaargangers';
$lang['LIVE_ATTENDEELIST_HOST'] ='Gasheer';
//POLL
$lang['LIVE_POLL_TXT1'] ='RESULTAAT';
$lang['LIVE_POLL_TXT2'] ='dankie vir jou stem';
$lang['LIVE_POLL_TXT3'] ='Die peiling finale resultate sal binnekort beskikbaar wees ...';

$lang['LIVE2_H1'] ='WEBINAAR APPLIKASIE BEGIN';
$lang['LIVE2_TXT1'] ='Welkom';
$lang['LIVE2_TXT2'] ='Instellings';
$lang['LIVE2_TXT3'] ='Teken in';
$lang['LIVE2_TXT4'] ='Tek uit';
$lang['LIVE2_TXT5'] ='Aanbieding';
$lang['LIVE2_TXT6'] ='Lede';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Aksie Events';
$lang['LIVE2_TXT9'] = 'Tik jou vrae of kommentaar';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='AAN';
$lang['LIVE3_TXT3'] ='Webinaar ID';
$lang['LIVE3_POPUP1'] ='Opsies is deur die webinaaradministrateur afgeskakel';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Is jy seker jy wil weggaan? Jy mag dalk nie weer inkom nie!!';




$langmonths = array();
$langmonths['january'] = 'Januarie';
$langmonths['february'] = 'Februarie';
$langmonths['march'] = 'Maart';
$langmonths['april'] = 'April';
$langmonths['may'] = 'Mei';
$langmonths['june'] = 'Junie';
$langmonths['july'] = 'Julie';
$langmonths['august'] = 'Augustus';
$langmonths['september'] = 'September';
$langmonths['october'] = 'Oktober';
$langmonths['november'] = 'November';
$langmonths['december'] = 'Desember';

$langdays = array();
$langdays['monday'] = 'Maandag';
$langdays['tuesday'] = 'Dinsdag';
$langdays['wednesday'] = 'Woensdag';
$langdays['thursday'] = 'Donderdag';
$langdays['friday'] = 'Vrydag';
$langdays['saturday'] = 'Saterdag';
$langdays['sunday'] = 'Sondag';





?>