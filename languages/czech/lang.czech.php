<?php
# -----------------------------------------------
# LANGUAGE: CZECH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='přihlašuji na webinář';
//presentation-box
$lang['REG_PBOX_H1'] ='Přihlášení na webinář';
$lang['REG_PBOX_TOPIC'] ='Téma';
$lang['REG_PBOX_PRESENTER'] ='Přednášející';
$lang['REG_PBOX_HOST'] ='Organizuje';
$lang['REG_PBOX_DATETIME'] ='Datum a čas';
$lang['REG_PBOX_DATETIME_VAL'] ='Vyberte si z nabídky';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Mezinárodní předvolba pro mobil';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Zadejte zde své číslo na mobil...';
$lang['REG_FORM_SMS_TXT1'] ='SMS upozornění: Ano, odešlete mi upozornění přes SMS před začátkem webináře (nepovinné, ale doporučené).';
$lang['REG_FORM_SMS_TXT2'] ='Vyberte si předvolbu krajiny a zadejte své mobilní číslo pro zaslání SMS upozornění 15 minut přes začátkem webináře';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Prosím vyplňte položku datum';
$lang['REG_FORM_ALERT_TIME'] ='Prosím vyplňte položku čas';
$lang['REG_FORM_ALERT_NAME'] ='Prosím vyplňte svoje jméno';
$lang['REG_FORM_ALERT_EMAIL'] ='Prosím vyplňte svůj email';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Prosím zadejte funkční emailovou adresu';
$lang['REG_FORM_TXTBOX_NAME'] ='Zdejte svoje jméno zde...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Zadejte svůj email zde...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Váš aktuální čas';
$lang['REG_FORM_TXT_SELDATE'] ='Vyberte si datum';
$lang['REG_FORM_TXT_SELTIME'] ='Nejprve si vyberte datum';
$lang['REG_FORM_TXT_SELTIME_2'] ='Vyberte si čas';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Vyberte si datum';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Vyberte si čas';
$lang['REG_FORM_TXT_RN_1'] = 'Sledujte včerejší webinar záznam NYNÍ!';
$lang['REG_FORM_TXT_RN_2'] = 'hned!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) { $lang['REG_FORM_H1'] ='Přihlášení na webinář';
$lang['REG_FORM_H2'] ='Zarezervujte si místo!';
$lang['REG_FORM_H3'] ='PŘIHLÁŠENÍ NA WEBINÁŘ';
$lang['REG_FORM_SMS_TXT1'] ='Chci zaslat SMS upozornění před začátkem webináře';
$lang['REG_FORM_SMS_TXT2'] ='(Nepovinné, ale doporučené) Vyberte si předvolbu krajiny a zadejte své mobilní číslo pro zaslání SMS upozornění 15 minut přes začátkem webináře';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Zarezervujte si místo!';
$lang['REG_FORM_H2'] ='Přihlášení na webinář';
$lang['REG_FORM_TXT_SELECTDATE'] ='Který den se chcete účastnit?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Který čas vám nejvíce vyhovuje?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Kam chcete zaslat pozvánku:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Upozornění';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='PŘIHLAŠTE SE NYNÍ!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinář';
$lang['PBOX_TXT_TOPIC'] ='Téma';
$lang['PBOX_TXT_HOST'] ='Organizuje';
$lang['PBOX_TXT_PRESENTER'] ='Přednášející';
$lang['PBOX_TXT_DATEANDTIME'] ='Datum a čas';
$lang['PBOX_TXT_STATUS'] ='STAV';
$lang['PBOX_TXT_STATUS_VAL'] ='AKTIVNÍ!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Prezentuje';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='O webináři';
$lang['TY_WEBINAR'] ='Webinář';
$lang['TY_PRESENTER'] ='Přednášející';
$lang['TY_SCHEDDATE'] ='Datum začátku';
$lang['TY_SCHEDTIME'] ='Čas začátku';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Váš speciální odkaz na připojení';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Přidat do vašeho iCal kalendáře';
$lang['TY_TXT_GOOGLE_CAL'] = 'Přidat do vašeho Google kalendáře';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Přidat do vašeho Outlook kalendáře';
$lang['TY_TXT_CLICK_VER'] = 'Klikněte na verzi';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Řekněte svým přátelům o tomto webináři';
$lang['TY_TXT_FREEBONUS_H2'] ='A ZÍSKEJTE NÁŠ SPECIÁLNÍ BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Vyberte si způsob doporučení';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Odeslat email přítelům';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Zadejte své jméno';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Zadejte svůj email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Jméno vašeho přítele';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Email vašeho přítele';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Toto je email, který bude zaslán vašim přátelům';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Věc';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Text emailu';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Odeslat e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Poslat tuto zprávu na Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Ověřit zveřejnění na Facebooku';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Přidat na Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Ověřte URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Po vložení zprávy na Facebook zadejte adresu vaší Facebookové stránky pro ověření vložení. Po ověření se vám ihned zpřístupní slíbený bonus!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Odeslat zprávu na Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Ověřit zveřejnění na Twitteru';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Po vložení zprávy na Twitter zadejte adresu vaší Twitter stránky pro ověření vložení. Po ověření se vám ihned zpřístupní slíbený bonus!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Tato zpráva';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gratuluji';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='ZÍSKALI JSTE SLÍBENÝ BONUS!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='STÁHNOUT ZDARMA';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Klikněte na odkaz níže pravým tlačítkem myši a zvolte "Uložit jako...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Klikněte zde';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Jméno je prázdné!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Chybný email!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL not verified!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinář začne za chvíli...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Přednášející';
$lang['COUNTDOWN_TXT_DATE'] ='Datum začátku';
$lang['COUNTDOWN_TXT_TIME'] ='Čas začátku';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Připojuji se na webinář, prosím čekejte...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Před odesláním dotazu prosím zadejte vaše jméno a email, abychom vám mohli odpovědět emailem, pokud to nebude možné během webináře.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Vaše jméno (vyžadováno)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Váš email (vyžadováno)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Zadejte váš dotaz';
$lang['LIVE_CBOX2_TXT_1'] ='Okno chatu';
$lang['LIVE_CBOX2_TXT_2'] ='Komu';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Všem';
$lang['LIVE_CBOX2_TXT_3'] ='Zpráva';
$lang['LIVE_CBOX2_TXT_4'] ='Poslat na vaše otázky';
$lang['LIVE_CBOX2_TXT_5'] ='Zadejte své jméno a e-mail, takže můžeme vám zaslat odpověď';
$lang['LIVE_CBOX2_TXT_6'] ='poskytnout všechna pole zcela';
$lang['LIVE_CBOX2_TXT_7'] ='Zpráva byla odeslána!';
$lang['LIVE_CBOX2_TXT_8'] ='Je nám líto, Zpráva již odesláno';


$lang['LIVE_CBOX2_SUBMIT'] ='Odeslat zprávu';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Přihlášení účastníci';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Účastníci ze záznamu';
$lang['LIVE_ATTENDEELIST_HOST'] ='Organizuje';
//POLL
$lang['LIVE_POLL_TXT1'] ='VÝSLEDEK';
$lang['LIVE_POLL_TXT2'] ='Děkujeme za váš hlas';
$lang['LIVE_POLL_TXT3'] ='Průzkum veřejného mínění Konečné výsledky budou k dispozici krátce ...';

$lang['LIVE2_H1'] ='APLIKACE WEBINÁŘE BĚŽÍ';
$lang['LIVE2_TXT1'] ='Vítejte';
$lang['LIVE2_TXT2'] ='Nastavení';
$lang['LIVE2_TXT3'] ='Přihlásit';
$lang['LIVE2_TXT4'] ='Odhlásit';
$lang['LIVE2_TXT5'] ='Prezentace';
$lang['LIVE2_TXT6'] ='Členové';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Akce Webinar Akční';
$lang['LIVE2_TXT9'] = 'Zadejte své dotazy nebo připomínky';
$lang['LIVE3_TXT1'] ='STAV';
$lang['LIVE3_TXT2'] ='AKTIVNÍ';
$lang['LIVE3_TXT3'] ='ID webináře';
$lang['LIVE3_POPUP1'] ='Změna nastavení byla vypnuta administrátorem';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Jste si jisti, že chcete odejít? Možná se již nebudete moci připojit zpět!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'leden';
$langmonths['february'] = 'únor';
$langmonths['march'] = 'březen';
$langmonths['april'] = 'duben';
$langmonths['may'] = 'květen';
$langmonths['june'] = 'červen';
$langmonths['july'] = 'červenec';
$langmonths['august'] = 'srpen';
$langmonths['september'] = 'září';
$langmonths['october'] = 'říjen';
$langmonths['november'] = 'listopad';
$langmonths['december'] = 'prosinec';

$langdays = array();
$langdays['monday'] = 'pondělí';
$langdays['tuesday'] = 'úterý';
$langdays['wednesday'] = 'středa';
$langdays['thursday'] = 'čtvrtek';
$langdays['friday'] = 'pátek';
$langdays['saturday'] = 'sobota';
$langdays['sunday'] = 'neděle';



?>