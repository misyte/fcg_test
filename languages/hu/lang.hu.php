<?php
# -----------------------------------------------
# LANGUAGE: HUNGARIAN                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='regisztráció webináriumra';
//presentation-box
$lang['REG_PBOX_H1'] ='Webinárium előadás';
$lang['REG_PBOX_TOPIC'] ='Téma';
$lang['REG_PBOX_PRESENTER'] ='Előadó';
$lang['REG_PBOX_HOST'] ='Házigazda';
$lang['REG_PBOX_DATETIME'] ='Dátum és idő';
$lang['REG_PBOX_DATETIME_VAL'] ='Válasszon a menüből';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Mobiltelefon országhívó kód';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Írja be ide a telefonszámát…';
$lang['REG_FORM_SMS_TXT1'] ='SMS értesítő: Igen, küldjön egy sms értesítőt a webinárium kezdete előtt (opcionális, de ajánlott).';
$lang['REG_FORM_SMS_TXT2'] ='Válassza ki az országhívó kódot és írja be a mobiltelefonszámát, hogy a szöveges üznetet megkaphassa 15 perccel a webinárium kezdete előtt)';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Kérem töltse ki a dátum mezőt';
$lang['REG_FORM_ALERT_TIME'] ='Kérem töltse ki az időpont mezőt';
$lang['REG_FORM_ALERT_NAME'] ='Kérem töltse ki a név mezőt';
$lang['REG_FORM_ALERT_EMAIL'] ='Kérem töltse ki az e-mail mezőt';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Írjon be egy érvényes e-mail címet';
$lang['REG_FORM_TXTBOX_NAME'] ='Írja be ide a nevét…';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Írja be ide az e-mail címét…';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Az Ön helyi ideje';
$lang['REG_FORM_TXT_SELDATE'] ='Válassza ki a kívánt dátumot!';
$lang['REG_FORM_TXT_SELTIME'] ='Válassza ki először a dátumot';
$lang['REG_FORM_TXT_SELTIME_2'] ='Válassza ki a kívánt időpontot';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Válassza ki a dátumot';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Válassza ki az időpontot';
$lang['REG_FORM_TXT_RN_1'] = 'Nézd meg a tegnapi webinar replay MOST!';
$lang['REG_FORM_TXT_RN_2'] = 'jelenleg';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='webinárium-regisztráció';
$lang['REG_FORM_H2'] ='Foglalja le a helyét!';
$lang['REG_FORM_H3'] ='REGISZTRÁLJON A WEBINÁRIUMRA';
$lang['REG_FORM_SMS_TXT1'] ='Szeretnék kérni egy sms figyelmeztetőt az esemény kezdete előtt';
$lang['REG_FORM_SMS_TXT2'] ='(opcionális, de ajánlott) Válassza ki az országhívó kódot és írja be a mobiltelefonszámát, hogy szöveges üzenetet kapjon 15 perccel a webinárium kezdete előtt.';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='Foglalja le a helyét!';
$lang['REG_FORM_H2'] ='webinárium-regisztráció';
$lang['REG_FORM_TXT_SELECTDATE'] ='Mely napon szeretne résztvenni?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Melyik időpont a legjobb az Ön számára?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Melyik időpont a legjobb az Ön számára?';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Megjegyzések';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='REGISZTRÁLJON MOST!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Webinárium előadás';
$lang['PBOX_TXT_TOPIC'] ='Téma';
$lang['PBOX_TXT_HOST'] ='Házigazda';
$lang['PBOX_TXT_PRESENTER'] ='Előadó';
$lang['PBOX_TXT_DATEANDTIME'] ='Dátum és idő';
$lang['PBOX_TXT_STATUS'] ='ÁLLAPOT';
$lang['PBOX_TXT_STATUS_VAL'] ='BE!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Előadó';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Webinárium részletei';
$lang['TY_WEBINAR'] ='Webinárium';
$lang['TY_PRESENTER'] ='Előadó';
$lang['TY_SCHEDDATE'] ='Kiválasztott dátum';
$lang['TY_SCHEDTIME'] ='Kiválasztott időpont';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Az Ön egyedi webinárium linkje';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Hozzáadás az iCal naptáramhoz';
$lang['TY_TXT_GOOGLE_CAL'] = 'Hozzáadás a Google Naptáramhoz';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Hozzáadás az Outlook Naptáramhoz';
$lang['TY_TXT_CLICK_VER'] = 'kattintson a verzió
';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Beszéljen barátainak erről a webináriumról';
$lang['TY_TXT_FREEBONUS_H2'] ='ÉS VÁLTSA BE AZ INGYENES BÓNUSZÁT!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Válassza ki az ajánlás módját';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Küldjön e-mailt a barátainak';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Írja be a nevét';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Írja be az e-mail címét';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='A barátja neve';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='A barátja e-mail címe';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Ezt az e-mailt fogjuk a barátjának elküldeni';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Tárgy';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='E-mail szövege';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='E-mail küldése';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Posztolja ezt az üzenetet a Facebookra';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Érvényesítse a Facebookon';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Elküldés Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'ellenőrizze URL';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Ha kiposztolta az üzenetet a Facebookra, az alábbi mezőbe írja be a Facebook oldaalának URL címét, hogy igazolja a posztot. Az igazolás után a rejtett bónusz azonnal megjelenik.';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweetelje a Twitteren';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Igazolja a Twitteren';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Ha kiposztolta az üzenetet a Twitterre, az alábbi mezőbe írja be a Twitter oldalának URL címét, hogy igazolja a posztot. Az igazolás után a rejtett bónusz azonnal megjelenik.';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Ez üzenet';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gratulálunk';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='MEGKAPTA A BÓNUSZÁT!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='INGYENES LETÖLTÉS';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Jobb kattintás az alábbi linkre, majd  válassza, hogy "Mentés másként"';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='kattintson ide';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Név üres!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Érvénytelen e-mail cím!!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='URL nem igazolt!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='A webinárium hamarosan kezdődik…';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Előadó';
$lang['COUNTDOWN_TXT_DATE'] ='Beállított dátum';
$lang['COUNTDOWN_TXT_TIME'] ='Beállított idő';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Kapcsolódás a webináriumhoz, kérem várjon…';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Mialatt a kérdést továbbítjuk, írja be a nevét és e-mail címét is, hogy e-mailen is  válaszolhassunk,';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Az Ön neve (kötelező)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='E-mail címe (kötelező)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Írja be a kérdését';
$lang['LIVE_CBOX2_TXT_1'] ='Csevegő ablak';
$lang['LIVE_CBOX2_TXT_2'] ='Címzett';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Mindenki';
$lang['LIVE_CBOX2_TXT_3'] ='Üzenet';
$lang['LIVE_CBOX2_TXT_4'] ='Küldje el kérdéseit';
$lang['LIVE_CBOX2_TXT_5'] ='Adja meg nevét és e-mail így tudjuk küldeni Önnek választ';

$lang['LIVE_CBOX2_TXT_6'] ='hogy minden területen teljesen';
$lang['LIVE_CBOX2_TXT_7'] ='Üzenet elküldve!';
$lang['LIVE_CBOX2_TXT_8'] ='Sajnáljuk, Üzenet Már elküldött';


$lang['LIVE_CBOX2_SUBMIT'] ='Üzenet küldése';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Regisztrált résztvevők';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Válasz a résztvevőknek';
$lang['LIVE_ATTENDEELIST_HOST'] ='Házigazda';
//POLL
$lang['LIVE_POLL_TXT1'] ='EREDMÉNY';
$lang['LIVE_POLL_TXT2'] ='Köszönjük, hogy a szavazás';
$lang['LIVE_POLL_TXT3'] ='A felmérés végeredménye hamarosan elérhető lesz ...';

$lang['LIVE2_H1'] ='WEBINÁRIUM ALKALMAZÁS ELINDULT';
$lang['LIVE2_TXT1'] ='Üdvözöljük';
$lang['LIVE2_TXT2'] ='Beállítások';
$lang['LIVE2_TXT3'] ='Bejelentkezés';
$lang['LIVE2_TXT4'] ='Kijelentkezés';
$lang['LIVE2_TXT5'] ='Előadás';
$lang['LIVE2_TXT6'] ='Résztvevők';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Webinar Action Események';
$lang['LIVE2_TXT9'] = 'Adja meg a kérdése vagy megjegyzése';
$lang['LIVE3_TXT1'] ='ÁLLAPOT';
$lang['LIVE3_TXT2'] ='BE';
$lang['LIVE3_TXT3'] ='Webinárium azonosító';
$lang['LIVE3_POPUP1'] ='Opciókat kikapcsolta a webinárium adminisztrátora';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Biztosan elhagyja az oldalt? Nem biztos, hogy vissza tud menni a terembe!!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'január';
$langmonths['february'] = 'február';
$langmonths['march'] = 'március';
$langmonths['april'] = 'április';
$langmonths['may'] = 'május';
$langmonths['june'] = 'június';
$langmonths['july'] = 'július';
$langmonths['august'] = 'augusztus';
$langmonths['september'] = 'szeptember';
$langmonths['october'] = 'október';
$langmonths['november'] = 'november';
$langmonths['december'] = 'december';

$langdays = array();
$langdays['monday'] = 'hétfő';
$langdays['tuesday'] = 'kedd';
$langdays['wednesday'] = 'szerda';
$langdays['thursday'] = 'csütörtök';
$langdays['friday'] = 'péntek';
$langdays['saturday'] = 'szombat';
$langdays['sunday'] = 'vasárnap';



?>