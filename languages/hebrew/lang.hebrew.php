<?php
# -----------------------------------------------
# LANGUAGE: HEBREW                     UPDATE: 1
# -----------------------------------------------

$lang = array();



/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ="רישום לוובינר";
//presentation-box
$lang['REG_PBOX_H1'] ='מציג הוובינר';
$lang['REG_PBOX_TOPIC'] ='נושא';
$lang['REG_PBOX_PRESENTER'] ='מציג';
$lang['REG_PBOX_HOST'] ='מארח';
$lang['REG_PBOX_DATETIME'] ='תאריך ושעה';
$lang['REG_PBOX_DATETIME_VAL'] ='בחר מהתפריט';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='קוד מדינה לטלפון נייד';
$lang['REG_FORM_SMS_INPUTPHONE'] ='הכנס את מספר הטלפון הנייד שלך כאן...';
$lang['REG_FORM_SMS_TXT1'] ='הודעת מסרון: כן! שלח לי הודעה טקסט כתזכורת לפני שהוובינר מתחיל (אופציונאלי אבל רצוי)';
$lang['REG_FORM_SMS_TXT2'] ='בחר את קוד המדינה שלך והכנס את מספר הטלפון הנייד שלך על מנת לקבל הודעת טקסט כתזכורת 15 דקות לפני שהוובינר מתחיל';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='אנא מלא את שדה התאריך';
$lang['REG_FORM_ALERT_TIME'] ='אנא מלא את שדה השעה';
$lang['REG_FORM_ALERT_NAME'] ='אנא מלא את שדה השם';
$lang['REG_FORM_ALERT_EMAIL'] ='אנא מלא את שדה האימייל';
$lang['REG_FORM_ALERT_EMAIL_2'] ='אנא הכנס אימייל חוקי';
$lang['REG_FORM_TXTBOX_NAME'] ='הכנס שם כאן...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='הכנס אימייל כאן...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='הזמן המקומי שלך';
$lang['REG_FORM_TXT_SELDATE'] ='בחר תאריך רצוי';
$lang['REG_FORM_TXT_SELTIME'] ='בחר את התאריך תחילה';
$lang['REG_FORM_TXT_SELTIME_2'] ='בחר שעה רצויה';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. בחר תאריך';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. בחר שעה';
$lang['REG_FORM_TXT_RN_1'] = 'צפה בשידור החוזר מאתמול עכשיו!';
$lang['REG_FORM_TXT_RN_2'] = 'עכשיו!';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {

$lang['REG_FORM_H1'] ='הרשמה לוובינר';
$lang['REG_FORM_H2'] ='מציג הוובינר';
$lang['REG_FORM_H3'] ='הרשמה לוובינר';
$lang['REG_FORM_SMS_TXT1'] ='אני רוצה לקבל הודעת טקסט (מסרון) לפני שהאירוע מתחיל';
$lang['REG_FORM_SMS_TXT2'] ='(אופציונאלי ומאוד מומלץ) בחר את קוד המדינה שלך והכנס את מספר הטלפון הנייד שלך על מנת לקבל הודעת טקסט כתזכורת 15 דקות לפני שהוובינר מתחיל';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='שריין מקומך!';
$lang['REG_FORM_H2'] ='הרשמה לוובינר';
$lang['REG_FORM_TXT_SELECTDATE'] ='באיזה יום הנך רוצה להשתתף?';
$lang['REG_FORM_TXT_SELECTTIME'] ='מהי השעה הכי מתאימה עבורך?';
$lang['REG_FORM_TXT_SENDINVITATION'] =' לאן לשלוח את ההזמנה:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='הודעות';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='הרשם עכשיו!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='מציג הוובינר';
$lang['PBOX_TXT_TOPIC'] ='נושא';
$lang['PBOX_TXT_HOST'] ='מארח';
$lang['PBOX_TXT_PRESENTER'] ='מציג';
$lang['PBOX_TXT_DATEANDTIME'] ='תאריך ושעה';
$lang['PBOX_TXT_STATUS'] ='סטטוס';
$lang['PBOX_TXT_STATUS_VAL'] ='בשידור';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='מוגש על ידי';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='פרטי הוובינר';
$lang['TY_WEBINAR'] ='וובינר';
$lang['TY_PRESENTER'] ='מציג';
$lang['TY_SCHEDDATE'] ='תאריך מתוכנן';
$lang['TY_SCHEDTIME'] ='שעה מתוכננת';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='קישור הוובינר האישי שלך';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'הוספת ליומן שלך';
$lang['TY_TXT_GOOGLE_CAL'] = 'הוסף ליומן גוגל';
$lang['TY_TXT_OUTLOOK_CAL'] = 'הוסף ליומן אוטלוק';
$lang['TY_TXT_CLICK_VER'] = 'לחץ על גרסה';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='המלץ לחבריך על הוובינר';
$lang['TY_TXT_FREEBONUS_H2'] ='וקבל מתנה בחינם';
$lang['TY_TXT_FREEBONUS_STEP1'] ='בחר את דרך ההפניה המועדפת עליך';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='שלח אימייל לחבריך';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='הכנס את שמך';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='הכנת את האימייל שלך';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='שם החבר';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='מייל החבר';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='זה המייל שישלח לחברים שלך';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='שורת הנושא';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='גוף המייל';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='שלח!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='פרסם הודעה זאת בפייסבוק';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='אמת את פייסבוק';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'פרסם הודעה זאת בפייסבוק';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'אמת את פייסבוק';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='לאחר פרסום הסטטוס בפייסבוק, אנא הכנס את כתובת הדף שלך בפייסבוק לצורך אימות הפרסום. ברגע שיאושר, המתנה שלך תתגלה עבורך במיידית!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='צייץ בטוויטר';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='אמת את טוויטר';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='לאחר פרסום הסטטוס בטוייטר, אנא הכנס את כתובת הדף שלך בטוייטר לצורך אימות הפרסום. ברגע שיאושר, המתנה שלך תתגלה עבורך במיידית!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='צייץ בטוויטר';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='ברכות';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='המתנה שלך נחשפה';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='הורדה בחינם';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='לחץ קליק ימני על הקישור ובחר שמור בשם...';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='לחץ כאן';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='שדה השם ריק';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='אימייל לא חוקי!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='כתובת הדף לא מאומתת';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='הוובינר יתחיל בקרוב';
$lang['COUNTDOWN_TXT_PRESENTER'] ='מציג';
$lang['COUNTDOWN_TXT_DATE'] ='תאריך מתוכנן';
$lang['COUNTDOWN_TXT_TIME'] ='שעה מתוכננת';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='מתחבר לוובינר, אנא המתן';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='בזמן כתיבת שאלה, אנא פרטו כתובת אימייל ושם כדי שנוכל לחזור אליכם במייל במידה ולא נספיק לענות לכם';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='השם שלך - חובה';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='המייל שלך - חובה';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='השאלה שלך';
$lang['LIVE_CBOX2_TXT_1'] ='תיבת שיחה';
$lang['LIVE_CBOX2_TXT_2'] ='כתוב ל';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='הכל';
$lang['LIVE_CBOX2_TXT_3'] ='הודעה';
$lang['LIVE_CBOX2_TXT_4'] ='שלח הודעה';
$lang['LIVE_CBOX2_TXT_5'] ='הזן את השם ואת הדוא"ל שלך כדי שנוכל לשלוח לך את התשובה שלנו';

$lang['LIVE_CBOX2_TXT_6'] ='לספק את כל השדות לחלוטין';
$lang['LIVE_CBOX2_TXT_7'] ='הודעה נשלחה!';
$lang['LIVE_CBOX2_TXT_8'] ='מצטער, כבר שלחה הודעה';

$lang['LIVE_CBOX2_SUBMIT'] ='שלח הודעה';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='משתתפים רשומים';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='משתתפים בשידור החוזר';
$lang['LIVE_ATTENDEELIST_HOST'] ='מארח';
//POLL
$lang['LIVE_POLL_TXT1'] ='תוצאה';
$lang['LIVE_POLL_TXT2'] ='תודה על הצבעתך';
$lang['LIVE_POLL_TXT3'] ='התוצאות הסופיות הסקר תהיינה זמינות בקרוב...';

$lang['LIVE2_H1'] ='אפליקציית הוובינר התחילה';
$lang['LIVE2_TXT1'] ='ברוכים הבאים';
$lang['LIVE2_TXT2'] ='הגדרות';
$lang['LIVE2_TXT3'] ='כניסה';
$lang['LIVE2_TXT4'] ='יציאה';
$lang['LIVE2_TXT5'] ='מצגת';
$lang['LIVE2_TXT6'] ='חבר';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'אירועי פעולת Webinar';
$lang['LIVE2_TXT9'] = 'הזן את השאלות או ההערות שלך';
$lang['LIVE3_TXT1'] ='סטטוס';
$lang['LIVE3_TXT2'] ='ב';
$lang['LIVE3_TXT3'] ='מספר וובינר';
$lang['LIVE3_POPUP1'] ='אפשרויות כבו על ידי מנהל הוובינר';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='אתה בטוח שאתה רוצה לעזוב? יכול להיות שלא תוכל להתחבר מחדש!';



// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'ינואר';
$langmonths['february'] = 'פברואר';
$langmonths['march'] = 'מרץ';
$langmonths['april'] = 'אפריל';
$langmonths['may'] = 'מאי';
$langmonths['june'] = 'יוני';
$langmonths['july'] = 'יולי';
$langmonths['august'] = 'אוגוסט';
$langmonths['september'] = 'ספטמבר';
$langmonths['october'] = 'אוקטובר';
$langmonths['november'] = 'נובמבר';
$langmonths['december'] = 'דצמבר';

$langdays = array();
$langdays['monday'] = 'יום שני';
$langdays['tuesday'] = 'יום שלישי';
$langdays['wednesday'] = 'יום רביעי';
$langdays['thursday'] = 'יום חמישי';
$langdays['friday'] = 'יום שישי';
$langdays['saturday'] = 'יום שבת';
$langdays['sunday'] = 'יום ראשון';

