<?php
# -----------------------------------------------
# LANGUAGE: POLISH                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='rejestracja na webinar';
//presentation-box
$lang['REG_PBOX_H1'] ='Prezentacja';
$lang['REG_PBOX_TOPIC'] ='Temat';
$lang['REG_PBOX_PRESENTER'] ='Prowadzący';
$lang['REG_PBOX_HOST'] ='Gospodarz';
$lang['REG_PBOX_DATETIME'] ='Data i Czas';
$lang['REG_PBOX_DATETIME_VAL'] ='Wybierz z menu';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='Numer kierunkowy kraju';
$lang['REG_FORM_SMS_INPUTPHONE'] ='Wpisz tutaj numer Twojego telefonu komórkowego...';
$lang['REG_FORM_SMS_TXT1'] ='Tekst Przypomnienia SMS: Tak, wyślij mi przypominający SMS przed rozpoczęciem webinaru (opcjonalne, lecz zalecane)';
$lang['REG_FORM_SMS_TXT2'] ='Wprowadź Nr Kierunkowy Kraju i Nr Twojego Telefonu Komórkowego Aby Otrzymać Przypomnienie o Webinarze Na 15 Minut Przed Jego Rozpoczęciem';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='Wypełnij pole datyWypełnij pole daty';
$lang['REG_FORM_ALERT_TIME'] ='Wypełnij pole czasu';
$lang['REG_FORM_ALERT_NAME'] ='Wpisz swoje imię';
$lang['REG_FORM_ALERT_EMAIL'] ='Wpisz swój adres e-mail';
$lang['REG_FORM_ALERT_EMAIL_2'] ='Podaj poprawny adres e-mail';
$lang['REG_FORM_TXTBOX_NAME'] ='Wpisz tutaj Twoje imię...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='Wpisz tutaj Twój adres e-mail...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='Twój Lokalny Czas';
$lang['REG_FORM_TXT_SELDATE'] ='Wybierz odpowiedni dzień';
$lang['REG_FORM_TXT_SELTIME'] ='Najpierw wybierz dzień';
$lang['REG_FORM_TXT_SELTIME_2'] ='Wybierz odpowiednią godzinę';
$lang['REG_FORM_TXT_SELECTDATE'] ='1. Wybierz Odpowiedni Dzień';
$lang['REG_FORM_TXT_SELECTTIME'] ='2. Wybierz Odpowiednią Godzinę';
$lang['REG_FORM_TXT_RN_1'] = 'Oglądaj powtórki wczorajszego seminarium internetowego TERAZ!';
$lang['REG_FORM_TXT_RN_2'] = 'już!';
//software or squeeze
if($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) {
$lang['REG_FORM_H1'] ='Rejestracja Na Webinar';
$lang['REG_FORM_H2'] ='Zarezerwuj Swoje Miejsce!';
$lang['REG_FORM_H3'] ='ZAREJESTRUJ SIĘ NA WEBINAR';
$lang['REG_FORM_SMS_TXT1'] ='Tak, chcę otrzymać przypominającą wiadomość SMS przed rozpoczęciem webinaru';
$lang['REG_FORM_SMS_TXT2'] ='(Opcjonalne, lecz zalecane) Wprowadź nr kierunkowy kraju i nr Twojego telefonu komórkowego aby otrzymać przypomnienie o webinarze na 15 minut przed jego rozpoczęciem';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
else if($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline') {
$lang['REG_FORM_H1'] ='ZAREZERWUJ SWOJE MIEJSCE!';
$lang['REG_FORM_H2'] ='REJESTRACJA NA WEBINAR';
$lang['REG_FORM_TXT_SELECTDATE'] ='Którego dnia chcesz wziąć udział?';
$lang['REG_FORM_TXT_SELECTTIME'] ='Która godzina najbardziej Ci odpowiada?';
$lang['REG_FORM_TXT_SENDINVITATION'] ='Gdzie wysłać zaproszenie:';
$lang['REG_FORM_TXT_NOTIFICATION'] ='Powiadomienia';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='ZAREJESTRUJ SIĘ TERAZ!';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='Prezentacja';
$lang['PBOX_TXT_TOPIC'] ='Temat';
$lang['PBOX_TXT_HOST'] ='Gospodarz';
$lang['PBOX_TXT_PRESENTER'] ='Prowadzący';
$lang['PBOX_TXT_DATEANDTIME'] ='Data i Czas';
$lang['PBOX_TXT_STATUS'] ='Status';
$lang['PBOX_TXT_STATUS_VAL'] ='WŁĄCZONY!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='Prowadzący';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='Szczegóły webinaru';
$lang['TY_WEBINAR'] ='Webinar';
$lang['TY_PRESENTER'] ='Prowadzący';
$lang['TY_SCHEDDATE'] ='Zaplanowany dzień';
$lang['TY_SCHEDTIME'] ='Zaplanowana godzina';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='Twój indywidualny link do webinaru...';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = 'Dodaj do swojego kalendarza iCal';
$lang['TY_TXT_GOOGLE_CAL'] = 'Dodaj do swojego kalendarza Google';
$lang['TY_TXT_OUTLOOK_CAL'] = 'Dodaj do swojego kalendarza Outlook';
$lang['TY_TXT_CLICK_VER'] = 'Kliknij na wersji';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='Poleć swoim znajomym ten webinar i...';
$lang['TY_TXT_FREEBONUS_H2'] ='I ODBLOKUJ TWÓJ DARMOWY BONUS!';
$lang['TY_TXT_FREEBONUS_STEP1'] ='Wybierz Metodę Polecenia';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='Wyślij Wiadomość E-mail Do Twoich Znajomych';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='Wpisz Twoje imię';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='Wpisz Twój adres e-mail';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='Imię Twojego znajomego';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='Adres e-mail Twojego znajomego';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='Do Twoich znajomych zostanie wysłana wiadomość e-mail o następującej treści';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='Tytuł wiadomości';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Treść wiadomości';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='Wyślij e-mail!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Opublikuj tą wiadomość na Facebooku';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Zweryfikuj konto na Facebooku';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Napisz na Facebooku';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = 'Zweryfikuj adres URL!';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='Po opublikowaniu wiadomości na Facebooku, wpisz poniżej adres URL Twojej strony na Facebooku w celu weryfikacji. Po pozytywnej weryfikacji natychmiast otrzymasz dostęp do bonusu!';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='Tweetuj to na Twitterze';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Zweryfikuj konto na Twitterze';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='Po przesłaniu wiadomości za pomocą Twittera, wpisz poniżej adres URL Twojej strony Twitter w celu weryfikacji. Po pozytywnej weryfikacji natychmiast otrzymasz dostęp do darmowego bonusu!';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet Ta wiadomość';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='Gratulacje';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='DOSTĘP DO BONUSU ODBLOKOWANY!';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='POBIERZ ZA DARMO';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='Kliknij link prawym przyciskiem myszy i wybierz opcję "zapisz jako..."';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='Kliknij tutaj';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='Pole "imię" jest puste!';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='Błędny adres e-mail!';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='Adres URL nie został poprawnie zweryfikowany!';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='Webinar wkrótce się rozpocznie...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='Prowadzący';
$lang['COUNTDOWN_TXT_DATE'] ='Zaplanowany dzień';
$lang['COUNTDOWN_TXT_TIME'] ='Zaplanowana godzina';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='Łączenie z webinarem, proszę czekać...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='Gdy wysyłasz zapytanie, podaj imię i adres e-mail, abyśmy mogli odpowiedzieć Ci drogą mailową (jeśli z jakichkolwiek przyczyn nie odpowiemy na Twoje pytanie podczas trwania konferencji)';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='Twoje Imię (wymagane)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='Twój Adres E-mail (wymagany)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='Wpisz Pytanie';
$lang['LIVE_CBOX2_TXT_1'] ='Pole Czatu';
$lang['LIVE_CBOX2_TXT_2'] ='Napisz do';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='Wszyscy';
$lang['LIVE_CBOX2_TXT_3'] ='Wiadomość';
$lang['LIVE_CBOX2_TXT_4'] ='Wyślij swoje pytania';
$lang['LIVE_CBOX2_TXT_5'] ='Wpisz swoje imię i e-mail, abyśmy mogli wysłać ci naszą odpowiedź';

$lang['LIVE_CBOX2_TXT_6'] ='dostarczyć wszystkie pola całkowicie';
$lang['LIVE_CBOX2_TXT_7'] ='Wiadomość wysłana!';
$lang['LIVE_CBOX2_TXT_8'] ='Niestety, wiadomość już wysłane';


$lang['LIVE_CBOX2_SUBMIT'] ='Wyślij Wiadomość';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='Zarejestrowani Uczestnicy';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='Uczestnicy Oglądający Powtórkę';
$lang['LIVE_ATTENDEELIST_HOST'] ='Gospodarz';
//POLL
$lang['LIVE_POLL_TXT1'] ='WYNIK';
$lang['LIVE_POLL_TXT2'] ='Dziękujemy za Twój głos';
$lang['LIVE_POLL_TXT3'] ='Sondaż ostateczne wyniki będą dostępne już wkrótce ...';

$lang['LIVE2_H1'] ='WEBINAR WŁAŚNIE SIĘ ROZPOCZYNA';
$lang['LIVE2_TXT1'] ='Witamy';
$lang['LIVE2_TXT2'] ='Ustawienia';
$lang['LIVE2_TXT3'] ='Zaloguj się';
$lang['LIVE2_TXT4'] ='Wyloguj się';
$lang['LIVE2_TXT5'] ='Prezentacja';
$lang['LIVE2_TXT6'] ='Uczestnicy';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = 'Wydarzenia Seminarium Akcji';
$lang['LIVE2_TXT9'] = 'Wpisz swoje pytania i uwagi';
$lang['LIVE3_TXT1'] ='STATUS';
$lang['LIVE3_TXT2'] ='WŁĄCZONY';
$lang['LIVE3_TXT3'] ='Identyfikator Webinaru';
$lang['LIVE3_POPUP1'] ='Opcja została wyłączona przez administratora webinaru';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='Na pewno chcesz się wylogować? Po wylogowaniu możesz nie mieć już możliwości ponownego zalogowania się do pokoju!';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = 'styczeń';
$langmonths['february'] = 'luty';
$langmonths['march'] = 'marzec';
$langmonths['april'] = 'kwiecień';
$langmonths['may'] = 'maj';
$langmonths['june'] = 'czerwiec';
$langmonths['july'] = 'lipiec';
$langmonths['august'] = 'sierpień';
$langmonths['september'] = 'wrzesień';
$langmonths['october'] = 'październik';
$langmonths['november'] = 'listopad';
$langmonths['december'] = 'grudzień';

$langdays = array();
$langdays['monday'] = 'poniedziałek';
$langdays['tuesday'] = 'wtorek';
$langdays['wednesday'] = 'środa';
$langdays['thursday'] = 'czwartek';
$langdays['friday'] = 'piątek';
$langdays['saturday'] = 'sobota';
$langdays['sunday'] = 'niedziela';



?>