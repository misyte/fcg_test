<?php
# -----------------------------------------------
# LANGUAGE: CHINESE                     UPDATE: 1
# -----------------------------------------------

$lang = array();

/**  REGISTER PAGE PRE-DEFINED VARIABLES **/

$lang['REG_LOADING'] ='報名線上講座';
//presentation-box
$lang['REG_PBOX_H1'] ='線上講座說明會';
$lang['REG_PBOX_TOPIC'] ='主題';
$lang['REG_PBOX_PRESENTER'] ='主講人';
$lang['REG_PBOX_HOST'] ='主持人';
$lang['REG_PBOX_DATETIME'] ='日期和時間';
$lang['REG_PBOX_DATETIME_VAL'] ='從選項內作選擇';
//sms
$lang['REG_FORM_SMS_SELCODE'] ='手機門號的國家代碼';
$lang['REG_FORM_SMS_INPUTPHONE'] ='在此輸入您的手機號碼';
$lang['REG_FORM_SMS_TXT1'] ='SMS簡訊提醒: 是的, 在線上講座開始前傳送簡訊給我 (非必要, 但建議)';
$lang['REG_FORM_SMS_TXT2'] ='選擇您的國家代碼然後輸入您的手機號碼(去掉0)來接收提醒訊息, 簡訊會在線上講座開始前15分鐘傳送';
//reg form
$lang['REG_FORM_ALERT_DATE'] ='麻煩請填入日期欄位';
$lang['REG_FORM_ALERT_TIME'] ='麻煩請填入時間欄位';
$lang['REG_FORM_ALERT_NAME'] ='麻煩請填入名字欄位';
$lang['REG_FORM_ALERT_EMAIL'] ='麻煩請填入email欄位';
$lang['REG_FORM_ALERT_EMAIL_2'] ='麻煩請填入一個有效的電子郵件信箱';
$lang['REG_FORM_TXTBOX_NAME'] ='在此輸入您的大名...';
$lang['REG_FORM_TXTBOX_EMAIL'] ='在此輸入您的Email...';
//dateregtime module
$lang['REG_FORM_TXT_LOCALTIME'] ='您的當地時區';
$lang['REG_FORM_TXT_SELDATE'] ='選擇想要的日期';
$lang['REG_FORM_TXT_SELTIME'] ='首先選擇您要的日期';
$lang['REG_FORM_TXT_SELTIME_2'] ='選擇想要的時間';
$lang['REG_FORM_TXT_SELECTDATE'] =' 選擇您的日期';
$lang['REG_FORM_TXT_SELECTTIME'] =' 選擇您的時間';
$lang['REG_FORM_TXT_RN_1'] = '观看昨天的网络研讨会重播吧！';
$lang['REG_FORM_TXT_RN_2'] = '现在！';
//software or squeeze
if((($get_regtpl[$page] == 0 || $get_regtpl[$page] == 1) && $is_rp == 0)|| (($rp_regtpl == 0 || $rp_regtpl == 1) && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='線上講座註冊';
$lang['REG_FORM_H2'] ='預訂保留您的位置！';
$lang['REG_FORM_H3'] ='報名參加線上講座';
$lang['REG_FORM_SMS_TXT1'] ='我想要在講座開始前收到簡訊的提醒通知';
$lang['REG_FORM_SMS_TXT2'] ='(非必要,但強烈建議) 選擇您的國家代碼並輸入您的手機號碼來接收提醒訊息, 簡訊會在線上講座開始前15分鐘發送到手機';
}
//vibrant stylish panorama facebook timeline softwarelook etherial
if((($get_regtpl[$page] == 3 || $get_regtpl[$page] == 4 || $get_regtpl[$page] == 5 || $get_regtpl[$page] == 6 || $get_regtpl[$page] == 7 || $get_regtpl[$page] == 8 || $get_regtpl[$page] =='timeline')  && $is_rp == 0)|| (($rp_regtpl == 3 || $rp_regtpl == 4 || $rp_regtpl == 5 || $rp_regtpl == 6 || $rp_regtpl == 7 || $rp_regtpl == 8)  && $is_rp == 1)) {
$lang['REG_FORM_H1'] ='預訂保留您的位置！';
$lang['REG_FORM_H2'] ='線上講座註冊';
$lang['REG_FORM_TXT_SELECTDATE'] ='您希望參與哪一天的日期？';
$lang['REG_FORM_TXT_SELECTTIME'] ='哪一個時段對您最為方便？';
$lang['REG_FORM_TXT_SENDINVITATION'] ='邀請函要寄送到哪裡';
$lang['REG_FORM_TXT_NOTIFICATION'] =' 通知';
$lang['REG_FORM_TXT_FB_SUBMIT'] ='立即註冊！';
}
//PRESENTER-BOX PRE-DEFINED VARIABLES
$lang['PBOX_H1'] ='線上講座說明會';
$lang['PBOX_TXT_TOPIC'] ='主題';
$lang['PBOX_TXT_HOST'] ='主持人';
$lang['PBOX_TXT_PRESENTER'] ='主講人';
$lang['PBOX_TXT_DATEANDTIME'] ='日期和時間';
$lang['PBOX_TXT_STATUS'] ='狀態';
$lang['PBOX_TXT_STATUS_VAL'] ='ON!';


/** THANKYOU PAGE PRE-DEFINED VARIABLES **/

//header part
$lang['TY_PRESENTEDBY'] ='主辦';
//beside the TYPAGE video info
$lang['TY_WEBINAR_DETAILS'] ='線上講座內容';
$lang['TY_WEBINAR'] ='線上講座';
$lang['TY_PRESENTER'] ='主講人';
$lang['TY_SCHEDDATE'] ='預訂日期';
$lang['TY_SCHEDTIME'] ='預訂時間';
//webinar link under the video
$lang['TY_YOUR_WEBINAR_LINK'] ='您的獨特線上講座鏈結';
//calendar details under weinar link
$lang['TY_TXT_I_CAL'] = '加入到您的iCal行事曆';
$lang['TY_TXT_GOOGLE_CAL'] = '加入到您的Google行事曆';
$lang['TY_TXT_OUTLOOK_CAL'] = '加入到您的Outlook行事曆';
$lang['TY_TXT_CLICK_VER'] = '点击版';
//Free bonus section
$lang['TY_TXT_FREEBONUS_H1'] ='告訴您的朋友們關於這個線上講座';
$lang['TY_TXT_FREEBONUS_H2'] ='就可以解鎖您的免費贈品';
$lang['TY_TXT_FREEBONUS_STEP1'] ='選擇您的推薦方式';
//step1 referring method: EMAIL
$lang['TY_TXT_FREEBONUS_STEP2'] ='傳送Email給您的朋友們';
$lang['TY_TXT_FREEBONUS_STEP2_NAME'] ='輸入您的名字';
$lang['TY_TXT_FREEBONUS_STEP2_EMAIL'] ='輸入您的Email';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSNAME'] ='您的朋友的名字';
$lang['TY_TXT_FREEBONUS_STEP2_FRIENDSEMAIL'] ='您的朋友的Email';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILFORMHEADER'] ='這個是將會寄送給您的朋友的Email';
$lang['TY_TXT_FREEBONUS_STEP2_SUBJLINE'] ='主旨';
$lang['TY_TXT_FREEBONUS_STEP2_EMAILBODY'] ='Email 內容';
$lang['TY_TXT_FREEBONUS_STEP2_SEND'] ='发送 Email!';
//step1 referring method: FACEBOK
$lang['TY_TXT_FREEBONUS_STEP2_FB'] ='Po 這個訊息到 Facebook';
$lang['TY_TXT_FREEBONUS_STEP3_FB'] ='Facebook 驗證';
$lang['TY_TXT_FREEBONUS_POST_FB']  = 'Po 這個訊息到 Facebook';
$lang['TY_TXT_FREEBONUS_VERIFY_FB']  = ' URL 驗證';
$lang['TY_TXT_FREEBONUS_STEP3_FB_DESC'] ='當您Po文到Facebook後, 在下方輸入您的Facebook頁面網址來驗證Po文. 一旦驗證通過, 您的隱藏版贈品便會立即解鎖！';
//step1 referring method: TWITTER
$lang['TY_TXT_FREEBONUS_STEP2_TWITTER'] ='"推"這個到Twitter';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER'] ='Twitter 驗證';
$lang['TY_TXT_FREEBONUS_STEP3_TWITTER_DESC'] ='當您推文到Twitter後, 在下方輸入您的Twitter頁面網址來驗證推文, 一旦驗證通過, 您的隱藏版贈品便會立即解鎖！';
$lang['TY_TXT_FREEBONUS_TWEET_TWITTER'] ='Tweet 此消息';
//unlock bonus
$lang['TY_TXT_FREEBONUS_UNLOCK_H1'] ='恭喜';
$lang['TY_TXT_FREEBONUS_UNLOCK_H2'] ='您已經解開您的贈品！';
$lang['TY_TXT_FREEBONUS_UNLOCK_FREE_DL'] ='免費下載';
$lang['TY_TXT_FREEBONUS_UNLOCK_SAVE_AS'] ='右鍵點擊下方鏈結並選擇“另存鏈結..';
$lang['TY_TXT_FREEBONUS_UNLOCK_CLICK_HERE'] ='點擊這裡';
//prompt msg
$lang['TY_TXT_FREEBONUS_ALERT_1'] ='名字是空白！';
$lang['TY_TXT_FREEBONUS_ALERT_2'] ='無效的電子郵件信箱！';
$lang['TY_TXT_FREEBONUS_ALERT_3'] ='網址尚未驗證！';

/** COUNTDOWN PRE-DEFINED VARIABLES **/
$lang['COUNTDOWN_TXT_H1'] ='這個線上講座即將開始...';
$lang['COUNTDOWN_TXT_PRESENTER'] ='主講人';
$lang['COUNTDOWN_TXT_DATE'] ='預訂日期';
$lang['COUNTDOWN_TXT_TIME'] ='預訂時間';

/** webinar live **/
$lang['LIVE_TXT_LOADING'] ='連線到線上講座中, 請稍候...';
//CBOX
$lang['LIVE_CBOX1_TXT'] ='在發送您的問題同時, 請順便輸入您的大名跟電子郵件信箱, 若有任何因素, 我們無法在線上講座中答覆您時, 我們將會透過Email回覆您的問題.';
$lang['LIVE_CBOX1_TXT_INPUT1'] ='您的名字 (必須)';
$lang['LIVE_CBOX1_TXT_INPUT2'] ='您的電子郵件信箱 (必須)';
$lang['LIVE_CBOX1_TXT_INPUT3'] ='輸入您的問題';
$lang['LIVE_CBOX2_TXT_1'] ='對話視窗';
$lang['LIVE_CBOX2_TXT_2'] ='寫給';
$lang['LIVE_CBOX2_TXT_2_INPUT'] ='全部';
$lang['LIVE_CBOX2_TXT_3'] ='訊息內容';
$lang['LIVE_CBOX2_TXT_4'] ='请将您的问题';
$lang['LIVE_CBOX2_TXT_5'] ='输入您的姓名和电子邮件地址，以便我们可以给你我们的回复';

$lang['LIVE_CBOX2_TXT_6'] ='提供各个领域的完全';
$lang['LIVE_CBOX2_TXT_7'] ='消息已发出！';
$lang['LIVE_CBOX2_TXT_8'] ='很抱歉，邮件已发送';

$lang['LIVE_CBOX2_SUBMIT'] ='傳送訊息';
//ATTENDEELIST
$lang['LIVE_ATTENDEELIST_TOP_TXT'] ='已註冊之參與者';
$lang['REPLAY_ATTENDEELIST_TOP_TXT'] ='重播 參與者';
$lang['LIVE_ATTENDEELIST_HOST'] ='主持人';
//POLL
$lang['LIVE_POLL_TXT1'] ='結果';
$lang['LIVE_POLL_TXT2'] ='感谢您的投票';
$lang['LIVE_POLL_TXT3'] ='最终的投票结果将于不久...';

$lang['LIVE2_H1'] ='線上講座APP已開始';
$lang['LIVE2_TXT1'] ='歡迎';
$lang['LIVE2_TXT2'] ='設定';
$lang['LIVE2_TXT3'] ='登入';
$lang['LIVE2_TXT4'] ='登出';
$lang['LIVE2_TXT5'] ='說明會';
$lang['LIVE2_TXT6'] ='會員';
#$lang['LIVE2_TXT7'] ='EBS PRESENTER';
$lang['LIVE2_TXT8'] = '研讨会的动作事件';
$lang['LIVE2_TXT9'] = '请输入您的问题或意见';
$lang['LIVE3_TXT1'] ='狀態';
$lang['LIVE3_TXT2'] ='ON';
$lang['LIVE3_TXT3'] ='線上講座編號';
$lang['LIVE3_POPUP1'] ='選項已被線上講座管理員關閉';
//exit webinar
$lang['LIVE_EXIT_TXT'] ='您確定要離開嗎？您可能無法再次登入回到這裡！';


// additional tezt conversions for days and months:
$langmonths = array();
$langmonths['january'] = '一月';
$langmonths['february'] = '二月';
$langmonths['march'] = '三月';
$langmonths['april'] = '四月';
$langmonths['may'] = '五月';
$langmonths['june'] = '六月';
$langmonths['july'] = '七月';
$langmonths['august'] = '八月';
$langmonths['september'] = '九月';
$langmonths['october'] = '十月';
$langmonths['november'] = '十一月';
$langmonths['december'] = '十二月';

$langdays = array();
$langdays['monday'] = '星期一';
$langdays['tuesday'] = '星期二';
$langdays['wednesday'] = '星期三';
$langdays['thursday'] = '星期四';
$langdays['friday'] = '星期五';
$langdays['saturday'] = '星期六';
$langdays['sunday'] = '星期天';


?>